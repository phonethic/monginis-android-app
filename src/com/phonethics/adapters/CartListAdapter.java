package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.adapters.ProductListAdapter.ViewHolder;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.ListClickListener;
import com.phonethics.monginis.LogFile;
import com.phonethics.monginis.R;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CartListAdapter extends ArrayAdapter<ShoppingCartModel>{

	

	private ArrayList<ShoppingCartModel> marrCartProducts;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;
	private ListClickListener mListener;
	
	
	public CartListAdapter(Activity mActivity, int resource,ArrayList<ShoppingCartModel> marrCartProducts, ListClickListener listClickListener) {
		super(mActivity, resource);
		// TODO Auto-generated constructor stub
		LogFile.LogData("CartListAdapter");	
		this.mActivity=mActivity;
		this.marrCartProducts = marrCartProducts;
		mInflator=mActivity.getLayoutInflater();
		this.mListener = listClickListener;
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return marrCartProducts.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int i = position;
		
		if(convertView==null){
			
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.shopping_cart_item, null);
			mHolder.txProdName=(TextView)convertView.findViewById(R.id.cartPrductName);
			mHolder.cartPrductQuantity_text=(TextView)convertView.findViewById(R.id.cartPrductQuantity_text);
			mHolder.txtQuantity=(TextView)convertView.findViewById(R.id.cartPrductQuantity);
			mHolder.txtTotalAmount=(TextView)convertView.findViewById(R.id.cartPrductTotalAmuont);
			mHolder.productImg=(ImageView)convertView.findViewById(R.id.cartPrductImg);
			mHolder.txtDelete = (TextView) convertView.findViewById(R.id.text_options);
			mHolder.itemType = (ImageView) convertView.findViewById(R.id.itemType);
			mHolder.txtDelete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.listItemClick(i, marrCartProducts.get(i).getProduct_id(), "");
				}
			});
			convertView.setTag(mHolder);
		}
		
		ViewHolder holder=(ViewHolder) convertView.getTag();
		
		try{
			holder.txProdName.setTypeface(tf);
			holder.txtQuantity.setTypeface(tf);
			holder.txtTotalAmount.setTypeface(tf);
			holder.cartPrductQuantity_text.setTypeface(tf);
			holder.txtDelete.setTypeface(tf,Typeface.BOLD);
			
			holder.txProdName.setText(marrCartProducts.get(position).getTitle());
			holder.txtQuantity.setText(marrCartProducts.get(position).getQuantity());
			
			int miquantity = Integer.parseInt(marrCartProducts.get(position).getQuantity());
			int miBaseAmount = Integer.parseInt(marrCartProducts.get(position).getSpecial_price());
			int miTotalPrice = miquantity * miBaseAmount;
			
			holder.txtTotalAmount.setText(""+miTotalPrice);
			
			Picasso.with(mActivity)
			.load(Config.IMAGE_BASE_URL+marrCartProducts.get(position).getImage())
			.placeholder(R.drawable.ic_launcher)
			.error(R.drawable.ic_launcher)
			// .resize(150, 150)
			.into(holder.productImg);
			
			//for showing the type of item
			
			Config.Log("TYPE_PRODUCT","TYPE_PRODUCT "+marrCartProducts.get(position).getProduct_type());
			if(marrCartProducts.get(position).getProduct_type().equalsIgnoreCase("eggless_type")){
				holder.itemType.setImageResource(R.drawable.eggless);
			}
			else if(marrCartProducts.get(position).getProduct_type().equalsIgnoreCase("withegg_type")){
				holder.itemType.setImageResource(R.drawable.egg);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return convertView;
	}

	static class ViewHolder {
		TextView txtTotalAmount,cartPrductQuantity_text;
		TextView txProdName;
		TextView txtQuantity;
		TextView txtDelete;
		ImageView productImg;
		ImageView itemType;
	}
}
