package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.phonethics.monginis.Config;
import com.phonethics.monginis.R;

public class CategoriesAdapter extends ArrayAdapter<String> {

	private ArrayList<String> mArrCategName;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;
	
	public CategoriesAdapter(Activity mActivity, int resource,
			int textViewResourceId, ArrayList<String> mArrCategName) {
		super(mActivity, resource, textViewResourceId, mArrCategName);
		this.mActivity=mActivity;
		this.mArrCategName=mArrCategName;		
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.layout_categ_row, parent, false);
			mHolder.txtCategName=(TextView)convertView.findViewById(R.id.txtCategName);
			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtCategName.setText(mArrCategName.get(position));
		holder.txtCategName.setTypeface(tf);
		return convertView;
	}



	@Override
	public int getCount() {
		return mArrCategName.size();
	}



	static class ViewHolder {
		TextView txtCategName;
	}


}
