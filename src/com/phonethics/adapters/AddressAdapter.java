package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.adapters.ApplyForAdapter.ViewHolder;
import com.phonethics.model.UserAddress;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AddressAdapter extends ArrayAdapter<UserAddress> {
	
	private ArrayList<UserAddress> uAdds;
	private Activity context;
	private LayoutInflater mInflator;

	public AddressAdapter(Activity context, int resource,
			ArrayList<UserAddress> uAdds) {
		super(context, resource, uAdds);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.uAdds = uAdds;
		mInflator=context.getLayoutInflater();
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.adreess_layout, parent, false);
			mHolder.txtMenuName=(TextView)convertView.findViewById(R.id.text_AddressTitle);
			mHolder.txtName=(TextView)convertView.findViewById(R.id.text_name);
			mHolder.txtAddress=(TextView)convertView.findViewById(R.id.text_address);
			convertView.setTag(mHolder);
		}
		
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtMenuName.setText(uAdds.get(position).getLabel());
		
		String msName = uAdds.get(position).getFirstName();
		String msLastName = uAdds.get(position).getLastName();
		
		holder.txtName.setText(msName+" "+msLastName);
		holder.txtAddress.setText(uAdds.get(position).getAddress1());
		
		
		return convertView;
	}


	static class ViewHolder {
		TextView txtMenuName;
		TextView txtName;
		TextView txtAddress;
	}

}
