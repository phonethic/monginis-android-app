package com.phonethics.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.phonethics.model.ProductModel;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.LoadImage;
import com.phonethics.monginis.R;
import com.squareup.picasso.Picasso;

public class ProductListAdapter extends ArrayAdapter<ProductModel> {

	/*private ArrayList<String> mArrProductName;
	private ArrayList<String> mArrProdImage;*/
	private ArrayList<ProductModel> mArrProducts;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;


	public ProductListAdapter(Activity mActivity, int resource,ArrayList<ProductModel> mArrProducts,
			int textViewResourceId) {
		super(mActivity, resource, textViewResourceId);
		this.mActivity=mActivity;
		/*this.mArrProductName=mArrProductName;		
		this.mArrProdImage=mArrProdImage;*/
		this.mArrProducts = mArrProducts;
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.layout_prod_list_row, parent, false);
			mHolder.txProdName=(TextView)convertView.findViewById(R.id.txProdName);
			mHolder.imgThumb=(ImageView)convertView.findViewById(R.id.imgThumb);

			mHolder.imgThumb.setScaleType(ScaleType.CENTER_INSIDE);

			mHolder.txProdPrice=(TextView)convertView.findViewById(R.id.txProdPrice);
			mHolder.txProdWeight=(TextView)convertView.findViewById(R.id.txProdWeight);

			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();

		holder.txProdName.setText(mArrProducts.get(position).getTitle());
		holder.txProdName.setTypeface(tf);

		// for Product Price
		if(mArrProducts.get(position).getSpecial_price().length()==0){
			String displayPrizeNew=mActivity.getResources().getString(R.string.Rs)+" "+mArrProducts.get(position).getOur_price();
			holder.txProdPrice.setText(displayPrizeNew);
			holder.txProdPrice.setTypeface(tf);
		}
		else{
			String displayPrizeNew=" "+mActivity.getResources().getString(R.string.Rs)+" "+mArrProducts.get(position).getSpecial_price();
			holder.txProdPrice.setText(displayPrizeNew);
			holder.txProdPrice.setTypeface(tf);
		}

		// for weight
		if(mArrProducts.get(position).getWeight_grams().length() != 0){
			float weightKG = Integer.parseInt(mArrProducts.get(position).getWeight_grams());
			weightKG = weightKG/1000;
			if((weightKG+"").contains(".0")){
				String weight = (weightKG+"").replace(".0", "");
				holder.txProdWeight.setText(weight+" "+"KG");
			}
			else{
				holder.txProdWeight.setText(weightKG+" "+"KG");	
			}
			holder.txProdWeight.setTypeface(tf, Typeface.BOLD);
		}
		else{
			holder.txProdWeight.setVisibility(View.GONE);
			holder.txProdWeight.setTypeface(tf);
		}

		String img_url = Config.IMAGE_BASE_URL+mArrProducts.get(position).getImage();
		img_url = img_url.replace(" ", "%20");
		
		Config.Log("IMG_ULR ","IMG_ULR "  + Config.IMAGE_BASE_URL+mArrProducts.get(position).getImage());
		
		Picasso.with(mActivity)
		.load(img_url)
		.placeholder(R.drawable.ic_launcher)
		.error(R.drawable.ic_launcher)
		 //.resize(150, 150)
		.into(holder.imgThumb);

		/*if(mArrProducts.get(position).getImage().equalsIgnoreCase("")){
			holder.imgThumb.setImageResource(R.drawable.ic_launcher);
		}else{
			LoadImage.load(Config.IMAGE_BASE_URL+mArrProducts.get(position).getImage(), holder.imgThumb);
		}*/
		


		return convertView;
	}

	@Override
	public int getCount() {
		return mArrProducts.size();
	}

	static class ViewHolder {
		TextView txProdName;
		ImageView imgThumb;
		TextView txProdPrice;
		TextView txProdWeight;
	}


}
