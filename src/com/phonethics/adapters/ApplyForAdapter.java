package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.adapters.MenuOptionAdapter.ViewHolder;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ApplyForAdapter extends ArrayAdapter<String>{
	
	private ArrayList<String> mFormNames;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;

	public ApplyForAdapter(Activity mActivity, int resource,
			int textViewResourceId, ArrayList<String> mFormNames) {
		super(mActivity, resource, textViewResourceId, mFormNames);
		// TODO Auto-generated constructor stub
		
		this.mActivity=mActivity;
		this.mFormNames=mFormNames;		
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.apply_form_layout, parent, false);
			mHolder.txtMenuName=(TextView)convertView.findViewById(R.id.txtMenuName);
			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtMenuName.setText(mFormNames.get(position));
		holder.txtMenuName.setTypeface(tf);
		return convertView;
	}



	@Override
	public int getCount() {
		return mFormNames.size();
	}



	static class ViewHolder {
		TextView txtMenuName;
	}

}
