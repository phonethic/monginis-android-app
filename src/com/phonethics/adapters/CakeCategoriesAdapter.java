package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.adapters.CategoriesAdapter.ViewHolder;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CakeCategoriesAdapter extends ArrayAdapter<String> {



	private ArrayList<String> mArrCategName;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;
	private int mScreen;

	public CakeCategoriesAdapter(Activity mActivity, int resource,
			int textViewResourceId, ArrayList<String> mArrCategName,int mScreen) {
		super(mActivity, resource, textViewResourceId, mArrCategName);
		this.mActivity=mActivity;
		this.mArrCategName=mArrCategName;		
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
		this.mScreen=mScreen;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.layout_cake_category, parent, false);
			mHolder.txtCategName=(TextView)convertView.findViewById(R.id.txtCategName);
			mHolder.patch=(View)convertView.findViewById(R.id.patch);
			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtCategName.setText(mArrCategName.get(position));
		holder.patch=(View)convertView.findViewById(R.id.patch);
		holder.txtCategName.setTypeface(tf,Typeface.BOLD);

		if(mScreen == 1){
			
		}
		else{
			holder.patch.setVisibility(View.VISIBLE);
		}

			return convertView;
	}



	@Override
	public int getCount() {
		return mArrCategName.size();
	}



	static class ViewHolder {
		TextView txtCategName;
		View patch;
	}



}
