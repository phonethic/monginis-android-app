package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerExpandibleAdapter extends BaseExpandableListAdapter {

	private ArrayList<ExpandibleDrawer> mItem;
	private Activity mContext;
	private LayoutInflater mInflator;
	private Typeface mTf;

	public DrawerExpandibleAdapter(Activity context, int resource, ArrayList<ExpandibleDrawer> item) {
		mContext=context;
		mItem=item;
		mInflator=context.getLayoutInflater();
//		mTf=Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC_0.TTF");
	}
	
	public Object getChild(int arg0, int arg1) {

		return null;
	}
	
	public long getChildId(int arg0, int arg1) {

		return 0;
	}
	
	public View getChildView(int groupPosition, int childPosition, boolean arg2, View convertView,
			ViewGroup arg4) {

		try
		{
			if(convertView==null)
			{
				ChildViewHolder holder=new ChildViewHolder();
				convertView=mInflator.inflate(R.layout.drawer_item_enty,null);
				holder.list_item_title=(TextView)convertView.findViewById(R.id.list_item_title);
				holder.list_item_badge=(TextView)convertView.findViewById(R.id.list_item_badge);
				holder.list_item_thumb=(ImageView)convertView.findViewById(R.id.list_item_thumb);
			
//				holder.list_item_title.setTypeface(mTf);
//				holder.list_item_badge.setTypeface(mTf);
				
				convertView.setTag(holder);
			}
			
			ChildViewHolder hold=(ChildViewHolder)convertView.getTag();
			hold.list_item_title.setText(mItem.get(groupPosition).getItem_array().get(childPosition));
			hold.list_item_thumb.setImageResource(mItem.get(groupPosition).getItem_iconID().get(childPosition));
			
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return convertView;
	}
	
	public View getGroupView(int position, boolean arg1, View convertView, ViewGroup arg3) {

		try
		{
			if(convertView==null)
			{
				ViewHolder holder=new ViewHolder();
				convertView=mInflator.inflate(R.layout.drawer_item_section,null);
				holder.list_item_section_text=(TextView)convertView.findViewById(R.id.list_item_section_text);
				holder.divider=(ImageView)convertView.findViewById(R.id.divider);
				holder.list_item_section_back=(View)convertView.findViewById(R.id.list_item_section_back);
				holder.list_item_section_back_other=(View)convertView.findViewById(R.id.list_item_section_back_other);
				convertView.setTag(holder);

			}
			ViewHolder hold=(ViewHolder) convertView.getTag();
			hold.divider.setVisibility(View.VISIBLE);
			hold.list_item_section_back.setVisibility(View.VISIBLE);
			hold.list_item_section_back_other.setVisibility(View.VISIBLE);
			if(mItem.get(position).getCustom_state()==1)
			{
				hold.list_item_section_text.setTextSize(16);
				hold.divider.setVisibility(View.GONE);
//				hold.list_item_section_back.setVisibility(View.VISIBLE);
				hold.list_item_section_back_other.setVisibility(View.GONE);
			}
			else
			{
				hold.list_item_section_text.setTextSize(20);
				hold.divider.setVisibility(View.VISIBLE);
				hold.list_item_section_back.setVisibility(View.GONE);
				hold.list_item_section_back_other.setVisibility(View.VISIBLE);
			}
			hold.list_item_section_text.setText(mItem.get(position).getHeader());
//			hold.list_item_section_text.setTypeface(mTf);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return convertView;
	}

	class ViewHolder
	{
		TextView list_item_section_text;
		ImageView divider;
		View list_item_section_back;
		View list_item_section_back_other;
	}

	class ChildViewHolder
	{
		TextView list_item_title;
		ImageView list_item_thumb;
		TextView list_item_badge;
	}
	
	public int getChildrenCount(int position) {

		return mItem.get(position).getItem_array().size();
	}
	
	public Object getGroup(int arg0) {

		return null;
	}
	
	public int getGroupCount() {

		return mItem.size();
	}
	
	public long getGroupId(int arg0) {

		return 0;
	}

	public boolean hasStableIds() {

		return false;
	}
	
	public boolean isChildSelectable(int arg0, int arg1) {

		return true;
	}

}
