package com.phonethics.adapters;

import java.util.ArrayList;

import com.phonethics.monginis.Config;

public class HeadQuarterModel {

	private String area_id;
	private String fax;
	private ArrayList<String> store_phone = new ArrayList<String>();
	private String address1;
	private String address2;
	private String contact_person;
	private String internal_shop_name;
	private String act_key;
	private String password_request;
	private String password;
	private String pincode;
	private String city_id;
	private String id;
	private String username;
	private String head_office;
	private String email;
	private String owner;
	private String active;
	private String latitude;
	private String longitude;
	private String sort_order;
	private String commission;
	private String store_name;
	private String mobile;
	public String getArea_id() {
		return area_id;
	}
	public void setArea_id(String area_id) {
		this.area_id = area_id;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public ArrayList<String> getStore_phone() {
		return store_phone;
	}
	public void setStore_phone(String store_phone) {
		this.store_phone.add(store_phone);
		Config.Log("Phones ","Phones " + store_phone);
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getContact_person() {
		return contact_person;
	}
	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}
	public String getInternal_shop_name() {
		return internal_shop_name;
	}
	public void setInternal_shop_name(String internal_shop_name) {
		this.internal_shop_name = internal_shop_name;
	}
	public String getAct_key() {
		return act_key;
	}
	public void setAct_key(String act_key) {
		this.act_key = act_key;
	}
	public String getPassword_request() {
		return password_request;
	}
	public void setPassword_request(String password_request) {
		this.password_request = password_request;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getHead_office() {
		return head_office;
	}
	public void setHead_office(String head_office) {
		this.head_office = head_office;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getSort_order() {
		return sort_order;
	}
	public void setSort_order(String sort_order) {
		this.sort_order = sort_order;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
