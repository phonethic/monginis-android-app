package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.adapters.CategoriesAdapter.ViewHolder;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MonginisConnectAdapter extends ArrayAdapter<String>{

	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;
	private ArrayList<String> connectText;
	private ArrayList<Integer> connectDrawable;
	public MonginisConnectAdapter(Activity mActivity, int resource,
			int textViewResourceId, ArrayList<String> connectText, ArrayList<Integer> connectDrawable) {
		super(mActivity, resource, textViewResourceId, connectText);
		// TODO Auto-generated constructor stub

		this.mActivity = mActivity;
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
		this.connectText = connectText;
		this.connectDrawable = connectDrawable;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return connectText.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.layout_monginis_connect, parent, false);
			mHolder.txtCategName=(TextView)convertView.findViewById(R.id.txtCategName);
			mHolder.txtCategName.setCompoundDrawablesWithIntrinsicBounds(connectDrawable.get(position),0 , 0, 0);
			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtCategName.setText(connectText.get(position));
		holder.txtCategName.setPadding(15, 0, 15, 0);
		holder.txtCategName.setTextSize(20);
		//holder.txtCategName.setGravity(Gravity.CENTER);
		holder.txtCategName.setTypeface(tf);
		return convertView;
	}

}
