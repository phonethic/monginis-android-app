package com.phonethics.adapters;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.adapters.CategoriesAdapter.ViewHolder;
import com.phonethics.monginis.Config;
import com.phonethics.monginis.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CallAdapter extends ArrayAdapter<String>{

	private ArrayList<String> mCallNumbers;
	private Activity mActivity;
	private LayoutInflater mInflator;
	private Typeface tf;
	public CallAdapter(Activity mActivity, int resource, int textViewResourceId,
			ArrayList<String> mCallNumbers) {
		super(mActivity, resource, textViewResourceId, mCallNumbers);
		this.mActivity=mActivity;
		this.mCallNumbers=mCallNumbers;		
		mInflator=mActivity.getLayoutInflater();
		tf=Typeface.createFromAsset(mActivity.getAssets(),Config.OMENS_LIGHT);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mCallNumbers.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			ViewHolder mHolder=new ViewHolder();
			convertView=mInflator.inflate(R.layout.call_custom_layout, parent, false);
			mHolder.txtStoreNumber=(TextView)convertView.findViewById(R.id.storeNumber);
			convertView.setTag(mHolder);
		}
		ViewHolder holder=(ViewHolder) convertView.getTag();
		holder.txtStoreNumber.setText(mCallNumbers.get(position));
		holder.txtStoreNumber.setTypeface(tf);
		return convertView;
	}
	static class ViewHolder {
		TextView txtStoreNumber;;
	}

}
