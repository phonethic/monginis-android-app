package com.phonethics.model;

public class UserAddress {
	
	
	private String firstName = "";
	private String lastName = "";
	private String user_id = "";
	private String country = "";
	private String state = "";
	private String city = "";
	private String email_id = "";

	private String pincone = "";
	private String label = "";
	private String id = "";
	private String mobile_number = "";
	private String home_number = "";
	private String address1 = "";
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getPincone() {
		return pincone;
	}
	public void setPincone(String pincone) {
		this.pincone = pincone;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getHome_number() {
		return home_number;
	}
	public void setHome_number(String home_number) {
		this.home_number = home_number;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	
	

	
}
