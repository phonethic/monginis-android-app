package com.phonethics.model;

import java.util.ArrayList;

public class ProductAttributeModel {

	private String attribute_id;
	private String attribute_name;
	private ArrayList<String> attribute_value_id = new ArrayList<String>();
	private ArrayList<String> attribute_value_name = new ArrayList<String>();
	
	public String getAttribute_id() {
		return attribute_id;
	}
	public void setAttribute_id(String attribute_id) {
		this.attribute_id = attribute_id;
	}
	public String getAttribute_name() {
		return attribute_name;
	}
	public void setAttribute_name(String attribute_name) {
		this.attribute_name = attribute_name;
	}
	public ArrayList<String> getAttribute_value_id() {
		return attribute_value_id;
	}
	public void setAttribute_value_id(String attribute_value_id) {
		this.attribute_value_id.add(attribute_value_id);
	}
	public ArrayList<String> getAttribute_value_name() {
		return attribute_value_name;
	}
	public void setAttribut_value_name(String attribute_value_name) {
		this.attribute_value_name.add(attribute_value_name);
	}
	
	
}
