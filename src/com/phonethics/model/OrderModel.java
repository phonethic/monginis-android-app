package com.phonethics.model;

public class OrderModel {
	
	
	private String user_id = "";
	private String order_id = "";
	private String product_amount = "";
	private String shipping_charge = "";
	private String coupon_id = "";
	private String discount_amount = "";
	private String points_redeem = "";
	private String delivery_date = "";
	private String delivery_mode = "";
	private String order_note = "";
	private String message_billing = "";
	private String totalAmount = "";
	
	
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getProduct_amount() {
		return product_amount;
	}
	public void setProduct_amount(String product_amount) {
		this.product_amount = product_amount;
	}
	public String getShipping_charge() {
		return shipping_charge;
	}
	public void setShipping_charge(String shipping_charge) {
		this.shipping_charge = shipping_charge;
	}
	public String getCoupon_id() {
		return coupon_id;
	}
	public void setCoupon_id(String coupon_id) {
		this.coupon_id = coupon_id;
	}
	public String getPoints_redeem() {
		return points_redeem;
	}
	public void setPoints_redeem(String points_redeem) {
		this.points_redeem = points_redeem;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	public String getDelivery_mode() {
		return delivery_mode;
	}
	public void setDelivery_mode(String delivery_mode) {
		this.delivery_mode = delivery_mode;
	}
	public String getOrder_note() {
		return order_note;
	}
	public void setOrder_note(String order_note) {
		this.order_note = order_note;
	}
	public String getMessage_billing() {
		return message_billing;
	}
	public void setMessage_billing(String message_billing) {
		this.message_billing = message_billing;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getDiscount_amount() {
		return discount_amount;
	}
	public void setDiscount_amount(String discount_amount) {
		this.discount_amount = discount_amount;
	}
	
	
	
	
	
	
	
	
	
	

}
