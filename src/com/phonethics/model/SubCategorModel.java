package com.phonethics.model;

public class SubCategorModel {

	private String sub_cat_id = "";
	private String main_cat_id = "";
	private String sub_cat_name = "";
	
	
	public String getSub_cat_id() {
		return sub_cat_id;
	}
	public void setSub_cat_id(String sub_cat_id) {
		this.sub_cat_id = sub_cat_id;
	}
	public String getMain_cat_id() {
		return main_cat_id;
	}
	public void setMain_cat_id(String main_cat_id) {
		this.main_cat_id = main_cat_id;
	}
	public String getSub_cat_name() {
		return sub_cat_name;
	}
	public void setSub_cat_name(String sub_cat_name) {
		this.sub_cat_name = sub_cat_name;
	}
	
	
	
	
}
