package com.phonethics.model;

public class ShoppingCartModel {

	private  String product_id="";
	private  String quantity="";
	private  String title = "";
	private  String image =  "";
	private  String special_price = "";
	private  String delivery_date = "";
	private  String delivery_mode = "";
	private  String message = "";
	private  String product_type = "";
	private  String total_amount = "";
	
	private	String withegg_type = "";
	private String eggless_type ="";
	private String sameday_delivery = "";
	private String nextday_delivery = "";
	private String free_delivery = "";
	
	
	
	public  String getProduct_id() {
		return product_id;
	}
	public  void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public  String getQuantity() {
		return quantity;
	}
	public  void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public  String getTitle() {
		return title;
	}
	public  void setTitle(String title) {
		this.title = title;
	}
	public  String getImage() {
		return image;
	}
	public  void setImage(String image) {
		this.image = image;
	}
	
	public  String getSpecial_price() {
		return special_price;
	}
	
	public  void setSpecial_price(String special_price) {
		this.special_price = special_price;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	
	
	public String getDelivery_mode() {
		return delivery_mode;
	}
	public void setDelivery_mode(String delivery_mode) {
		this.delivery_mode = delivery_mode;
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getWithegg_type() {
		return withegg_type;
	}
	public void setWithegg_type(String withegg_type) {
		this.withegg_type = withegg_type;
	}
	public String getEggless_type() {
		return eggless_type;
	}
	public void setEggless_type(String eggless_type) {
		this.eggless_type = eggless_type;
	}
	public String getSameday_delivery() {
		return sameday_delivery;
	}
	public void setSameday_delivery(String sameday_delivery) {
		this.sameday_delivery = sameday_delivery;
	}
	public String getNextday_delivery() {
		return nextday_delivery;
	}
	public void setNextday_delivery(String nextday_delivery) {
		this.nextday_delivery = nextday_delivery;
	}
	public String getFree_delivery() {
		return free_delivery;
	}
	public void setFree_delivery(String free_delivery) {
		this.free_delivery = free_delivery;
	}
	
	
	
	
}
