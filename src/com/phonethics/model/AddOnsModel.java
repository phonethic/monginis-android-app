package com.phonethics.model;

public class AddOnsModel {

	private String id;
	private String cat_id;
	private String title;
	private String description;
	private String code;
	private String weight_grams;
	private String image;
	private String list_price;
	private String our_price;
	private String special_price;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCat_id() {
		return cat_id;
	}
	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getWeight_grams() {
		return weight_grams;
	}
	public void setWeight_grams(String weight_grams) {
		this.weight_grams = weight_grams;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getList_price() {
		return list_price;
	}
	public void setList_price(String list_price) {
		this.list_price = list_price;
	}
	public String getOur_price() {
		return our_price;
	}
	public void setOur_price(String our_price) {
		this.our_price = our_price;
	}
	public String getSpecial_price() {
		return special_price;
	}
	public void setSpecial_price(String special_price) {
		this.special_price = special_price;
	}


}
