package com.phonethics.model;

public class ProductDetailsModel {

	private String mProductTitle;
	private int mProductId;
	private String mProductDescription;
	private String mProductSpecialPrice;
	private String mProductDisplayPrice;
	private String mImageUrl;
	private String mOurPrice;
	private String cat_id;
	
	
	public String getCat_id() {
		return cat_id;
	}
	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}
	public String getmOurPrice() {
		return mOurPrice;
	}
	public void setmOurPrice(String mOurPrice) {
		this.mOurPrice = mOurPrice;
	}
	public String getmImageUrl() {
		return mImageUrl;
	}
	public void setmImageUrl(String mImageUrl) {
		this.mImageUrl = mImageUrl;
	}
	public String getmProductTitle() {
		return mProductTitle;
	}
	public void setmProductTitle(String mProductTitle) {
		this.mProductTitle = mProductTitle;
	}
	public int getmProductId() {
		return mProductId;
	}
	public void setmProductId(int mProductId) {
		this.mProductId = mProductId;
	}
	public String getmProductDescription() {
		return mProductDescription;
	}
	public void setmProductDescription(String mProductDescription) {
		this.mProductDescription = mProductDescription;
	}
	public String getmProductSpecialPrice() {
		return mProductSpecialPrice;
	}
	public void setmProductSpecialPrice(String mProductSpecialPrice) {
		this.mProductSpecialPrice = mProductSpecialPrice;
	}
	public String getmProductDisplayPrice() {
		return mProductDisplayPrice;
	}
	public void setmProductDisplayPrice(String mProductDisplayPrice) {
		this.mProductDisplayPrice = mProductDisplayPrice;
	}

	
	
}
