package com.phonethics.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductModel implements Parcelable{

	private String id = "";
	private String maincat_id = "";
	private String subcat_id = "";
	private String title = "";
	private String description = "";
	private String code = "";
	private String weight_grams = "";
	private String image = "";
	private String list_price = "";
	private String our_price = "";
	private String special_price = "";
	private String eggless_type = "";
	private String withegg_type = "";
	private String sameday_delivery = "";
	private String nextday_delivery = "";
	private String normal_delivery = "";
	private String free_delivery = "";
	//added by salman for CAT_ID
	private String cat_id = "";

	public ProductModel(){

	}

	public ProductModel(Parcel source){

		id = source.readString();
		maincat_id = source.readString();
		subcat_id = source.readString();
		title = source.readString();
		description = source.readString();
		code = source.readString();
		weight_grams = source.readString();
		image = source.readString();
		list_price = source.readString();
		our_price = source.readString();
		special_price = source.readString();
		eggless_type = source.readString();
		withegg_type = source.readString();
		sameday_delivery = source.readString();
		nextday_delivery = source.readString();
		normal_delivery = source.readString();
		free_delivery = source.readString();
		
		cat_id = source.readString();
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getMaincat_id() {
		return maincat_id;
	}
	public void setMaincat_id(String maincat_id) {
		this.maincat_id = maincat_id;
	}
	public String getSubcat_id() {
		return subcat_id;
	}
	public void setSubcat_id(String subcat_id) {
		this.subcat_id = subcat_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getWeight_grams() {
		return weight_grams;
	}
	public void setWeight_grams(String weight_grams) {
		this.weight_grams = weight_grams;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getList_price() {
		return list_price;
	}
	public void setList_price(String list_price) {
		this.list_price = list_price;
	}
	public String getOur_price() {
		return our_price;
	}
	public void setOur_price(String our_price) {
		this.our_price = our_price;
	}
	public String getSpecial_price() {
		return special_price;
	}
	public void setSpecial_price(String special_price) {
		this.special_price = special_price;
	}
	public String getEggless_type() {
		return eggless_type;
	}
	public void setEggless_type(String eggless_type) {
		this.eggless_type = eggless_type;
	}
	public String getWithegg_type() {
		return withegg_type;
	}
	public void setWithegg_type(String withegg_type) {
		this.withegg_type = withegg_type;
	}
	public String getSameday_delivery() {
		return sameday_delivery;
	}
	public void setSameday_delivery(String sameday_delivery) {
		this.sameday_delivery = sameday_delivery;
	}
	public String getNextday_delivery() {
		return nextday_delivery;
	}
	public void setNextday_delivery(String nextday_delivery) {
		this.nextday_delivery = nextday_delivery;
	}
	public String getNormal_delivery() {
		return normal_delivery;
	}
	public void setNormal_delivery(String normal_delivery) {
		this.normal_delivery = normal_delivery;
	}
	public String getFree_delivery() {
		return free_delivery;
	}
	public void setFree_delivery(String free_delivery) {
		this.free_delivery = free_delivery;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String getCat_id() {
		return cat_id;
	}

	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

		dest.writeString(id);
		dest.writeString(maincat_id);
		dest.writeString(subcat_id);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(code);
		dest.writeString(weight_grams);
		dest.writeString(image);
		dest.writeString(list_price);
		dest.writeString(our_price);
		dest.writeString(special_price);
		dest.writeString(eggless_type);
		dest.writeString(withegg_type);
		dest.writeString(sameday_delivery);
		dest.writeString(nextday_delivery);
		dest.writeString(normal_delivery);
		dest.writeString(free_delivery);
		
		dest.writeString(cat_id);
	}




}
