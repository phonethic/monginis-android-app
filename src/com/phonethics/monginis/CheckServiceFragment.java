package com.phonethics.monginis;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;


/**
 * Gives the result of product availibity.
 * 
 * @author Nitin
 *
 */
public class CheckServiceFragment extends Fragment {

	private View mView;
	private ProgressDialog mProgressDialog;
	private TextView txtSelectedCity;
	private TextView txtEnteredPin;
	private TextView txtAvailability;
	private TextView mContinueBtn;
	String mCityName;
	String mPinCode;
	String PINCODE="/pincode/";
	NetworkCheck networkChk;
	RelativeLayout mMainLayout;
	SessionManager mSession;
	String product_id = "";
	String sub_cat_id = "";
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/**
		 * Initialize the class variables
		 * 
		 */
		mSession = new SessionManager(getActivity());
		networkChk = new NetworkCheck(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Checking Availability.");
		
		mCityName = getArguments().getString("CityName");
		mPinCode = getArguments().getString("PinCode");
		product_id = getArguments().getString("PRODUCT_ID");
		sub_cat_id = getArguments().getString("SUB_CAT_ID");
		Config.Log("SUBCATR ","SUBCATR " + sub_cat_id);
		PINCODE = PINCODE + mPinCode;
		if(networkChk.isNetworkAvailable())
			/**
			 * 
			 * 
			 */
			getAvailability(Config.MAIN_SELECTED_CATEGORY_ID);
		else
			showToast("No internet connection");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	
		mView = (View)inflater.inflate(R.layout.fragment_check_service, null);
		mMainLayout=(RelativeLayout)mView.findViewById(R.id.mMainLayout);
		txtSelectedCity = (TextView)mView.findViewById(R.id.txtSelectedCity);
		txtEnteredPin = (TextView)mView.findViewById(R.id.txtEnteredPin);
		txtAvailability = (TextView)mView.findViewById(R.id.txtAvailability);
		mContinueBtn = (TextView)mView.findViewById(R.id.mContinueBtn);
		
		txtSelectedCity.setTypeface(MonginisApplicationClass.getTypeFace());
		txtEnteredPin.setTypeface(MonginisApplicationClass.getTypeFace());
		txtAvailability.setTypeface(MonginisApplicationClass.getTypeFace());
		mContinueBtn.setTypeface(MonginisApplicationClass.getTypeFace());
		
		mContinueBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				final Map<String, String> param = new HashMap<String, String>();
				param.put("ProductWithCategory",  EventsName.category +" - "+EventsName.sub_category+" - "+EventsName.product);
				param.put("Availibilty_City",  EventsName.product+" - "+txtSelectedCity.getText().toString());
				EventTracker.logEvent(EventsName.CHECK_AVAILIBITY, param);
				
				
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				//  getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				AddToCartFragment chkFrag=new AddToCartFragment();
				transaction.add(R.id.content_frame, chkFrag,"AddToCart");
				transaction.addToBackStack("ProductDetails");
				// Commit the transaction
				transaction.commit();

			}
		});
		return mView;
	}
	

	/**
	 * Network request to check if product is availble in desired city and area based on selected pin code
	 * 
	 * @param catId
	 */
	private void getAvailability(String catId) {
		// TODO Auto-generated method stub
		
		try {
			
			final String TAG="CHECK_AVAILABILITY";
			showProgressDialog();
			//String URL = Config.CHECK_AVAILABILITY_BY_CATEGORY + catId + PINCODE  + "/city/" +mCityName + "/product_id/" + product_id;
			String URL = Config.CHECK_AVAILABILITY_BY_CATEGORY ;
			
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("category_id", catId);
				jsonObj.put("pincode", mPinCode);
				jsonObj.put("city", mCityName);
				jsonObj.put("product_id", product_id);
				jsonObj.put("subcategories", sub_cat_id);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			Config.Log("CHOCO ","CHOCO " + jsonObj.toString());
			
			Config.Log("CHOCO ","CHOCO " + URL);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST,URL, jsonObj, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					try
					{
						dismissDialog();
						Config.Log(TAG, response.toString());
						//String status=response.getString("success");

						if(response!=null){
							
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							String message = json.getString("message");
							if(success.equalsIgnoreCase("true")){
								mMainLayout.setVisibility(View.VISIBLE);
								//showToast(message);
								txtSelectedCity.setText(mCityName);
								txtEnteredPin.setText(mPinCode);
								mContinueBtn.setVisibility(View.VISIBLE);
								txtAvailability.setText("Available");
								txtAvailability.setCompoundDrawablesWithIntrinsicBounds(R.drawable.available, 0, 0, 0);
								mSession.saveCityPreferences(mCityName);
							}
							else{
								//showToast(message);
								txtSelectedCity.setText(mCityName);
								txtEnteredPin.setText(mPinCode);
								txtAvailability.setTextSize(14);
								txtAvailability.setText(message);
								txtAvailability.setCompoundDrawablesWithIntrinsicBounds(R.drawable.unavailable, 0, 0, 0);
								txtAvailability.setTextColor(Color.parseColor("#FFFFFF"));
								txtAvailability.setBackgroundColor(Color.parseColor("#3C362A"));
								mMainLayout.setVisibility(View.VISIBLE);
							}
						}	
					}catch(Exception ex){
						ex.printStackTrace();
						dismissDialog();
					}

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}

			};

			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	private void showToast(String text){

		Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
	}
}
