package com.phonethics.monginis;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CakeCategoriesAdapter;
import com.phonethics.model.CartProductInfo;



/**
 * Loads the categories like Cakes,chochlates,flowers and toys
 * First it loads the data from the server and cache that in database and from next occurence of this class
 * it loads the cached data.
 * 
 * @author Nitin
 *
 */
public class MainCategoriesFragment extends Fragment implements OnItemClickListener,DBListener{

	private View mView;
	private ListView mList;
	private CakeCategoriesAdapter mAdapter;
	private ArrayList<String>mArrCategName=new ArrayList<String>();
	private ArrayList<String>mArrCategId=new ArrayList<String>();
	private onMainCategoryListener mInstance;
	private ProgressDialog mProgressDialog;
	private long back_pressed;
	ImageView imgCake,imgCakeBackup,imgCakeBackupLast;
	private Map<String, String> param = new HashMap<String, String>();
	private long diffDays;
	
	SessionManager mSession;
	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try{
			mInstance=(onMainCategoryListener)activity;
		}catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
	}
	
	
	public interface onMainCategoryListener{
		public void mCategClick(String Categ,int position);
	}
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Config.Log("Fragment", "onCreate of Fragment");
//		mAdapter=new CategoriesAdapter(getActivity(), 0, 0, mArrCategName);
		
		
	
		mSession = new SessionManager(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		
		checkCachedData();
		
		
	}
	
	public void showProgressDialog(){
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}
		
	}
	
	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}
		
	}
	
	
	/**
	 * Check whether categories exits in database or not, returns the result at the implemented method of DbListener rowCount()
	 * 
	 */
	void checkCachedData(){
		
		try{
			
			new AsyncDatabaseQuery(getActivity(),
					this, 
					DatabaseHelper.TABLE_MAIN_CATEGORIES,
					DatabaseHelper.AUTO_ID, 
					Config.ROW_COUNT,  
					Config.ROW_COUNT).execute();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	/**
	 * Network call to get the categories from the server
	 * 
	 */
	void getAllCategories(){
		final String TAG="GET_ALL_CATEGORIES";
		String url=Config.GET_ALL_CATEGORIES;
		
		showProgressDialog();
		
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				try
				{
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){
						
						if(status.equalsIgnoreCase("true")){
							
							/**
							 * Parse the informartion from the sevrer
							 * 
							 */
							JSONArray jsonArr=response.getJSONArray("data");
							ContentValues[] values = new ContentValues[jsonArr.length()];

								for(int i=0;i<jsonArr.length();i++){
									ContentValues value = new ContentValues();
									JSONObject jsonObject=jsonArr.getJSONObject(i);
									value.put(DatabaseHelper.CATEGORY_NAME,jsonObject.getString("name"));
									value.put(DatabaseHelper.ID,jsonObject.getInt("id"));
									values[i]=value;
								}
								
								/**
								 *Store this information in database
								 * 
								 */
								insertIntoDb(values);
						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								dismissDialog();
							}
						}
						
					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};

		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}

	
	/**
	 * Stores the category information into database,returns the result to the implemented method of DbListner onCompleteOperation
	 * 
	 * @param contentValues
	 */
	void insertIntoDb(ContentValues[] contentValues ){
		try{
			showProgressDialog();
			new AsyncDatabaseQuery(
					getActivity(), 
					this, 
					contentValues, 
					DatabaseHelper.TABLE_MAIN_CATEGORIES,
					Config.INSERT_DATA, Config.INSERT_DATA).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * Database query to fetch the category names, returns the result to the implemented method of DbListener onCompleteRetrival()
	 * 
	 */
	void fetchData(){
		//showProgressDialog();
		new AsyncDatabaseQuery(getActivity(), this, DatabaseHelper.TABLE_MAIN_CATEGORIES, 
				DatabaseHelper.CATEGORY_NAME, Config.RETRIVE_DATA, "CATEGORIES").execute();
	}
	
	/**
	 * Database query to fetch the category ids, returns the result to the implemented method of DbListener onCompleteRetrival()
	 * 
	 */
	void fetchCategoyId(){
		//showProgressDialog();
		new AsyncDatabaseQuery(getActivity(), this, DatabaseHelper.TABLE_MAIN_CATEGORIES, 
				DatabaseHelper.ID, Config.RETRIVE_DATA, "ID").execute();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		
		/**
		 * Initialze the view and layout files
		 * 
		 */
		mView=(View)inflater.inflate(R.layout.fragment_main_categories, null);
		mList=(ListView)mView.findViewById(R.id.listMainCategory);
		imgCake=(ImageView)mView.findViewById(R.id.imgCake);
		return mView;
	}
	
	public void notifyAdapter(){
		mAdapter.notifyDataSetChanged();
	}
	
	
	/**
	 * Set the adapter for the categories
	 * 
	 * @param mArr
	 */
	public void setData(ArrayList<String> mArr){
		mArrCategName.clear();
		mArrCategName.addAll(mArr);
		Config.Log("CATEG", "CATEG"+mArr);
        
		mAdapter=new CakeCategoriesAdapter(getActivity(), 0, 0, mArrCategName,1);
		mList.setAdapter(mAdapter);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_layout_controller);
		mList.setLayoutAnimation(controller);
		mList.setOnItemClickListener(this);

	}

	void showToast(String msg){
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		
		dismissDialog();
		
		try {
			
			Config.MAIN_SELECTED_CATEGORY_ID = mArrCategId.get(position);
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
	        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
	        param.put("Categorie", mArrCategName.get(position));
	        EventsName.category = mArrCategName.get(position);
	        EventTracker.logEvent(EventsName.MAIN_CATEGORIES, param);
	        
	        SubCategoiresFragment subFrag=new SubCategoiresFragment();
	        Bundle b=new Bundle();
	        b.putString("CategoryId", mArrCategId.get(position));
	        subFrag.setArguments(b);
	        transaction.add(R.id.content_frame, subFrag);
	        transaction.addToBackStack("Main Categories");
	        // Commit the transaction
	        transaction.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
       
	}

	@Override
	public void onCompleteOperation(String tag) {
		dismissDialog();
		if(tag.equalsIgnoreCase(Config.INSERT_DATA)){
			//showToast(tag);
			
			/**
			 * Database query to fetch the category names.
			 * 
			 */
			fetchData();
			
			/**
			 * Database query to fetch the category ids.
			 * 
			 */
			fetchCategoyId();
		}
		else if(tag.equalsIgnoreCase( Config.DELETE_MAINTABLE_DATA)){
			showToast("data deleted and now fetching");
			getAllCategories();
			
		}
	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		//dismissDialog();
		if(tag.equalsIgnoreCase("CATEGORIES")){
			setData(mArr);	
		}
		if(tag.equalsIgnoreCase("ID")){
			mArrCategId.clear();
			mArrCategId.addAll(mArr);
		}
	}

	@Override
	public void rowCount(long rowCount, String tag) {
		dismissDialog();
		if(tag.equalsIgnoreCase(Config.ROW_COUNT)){
			if(rowCount==0){
				
				/**
				 * If there is no information of categories in database than load that information from the server
				 * 
				 */
				getAllCategories();
				
			}else{
				
				//check for the new data after 3 days
//				long dateDiff;
//				Calendar c = Calendar.getInstance();
//				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//				String dateStringCurrent = dateFormat.format(c.getTime());
//				String mSavedDate = mSession.getSavedDate();
//				showToast(mSavedDate);
//				
//				if(mSavedDate.equalsIgnoreCase("")){
//					
//					mSession.setSavedDate(dateStringCurrent);
//					showToast(dateStringCurrent);
//					
//					showToast("From Cached Data only");
//					/**
//					 * Database query to fetch the category names.
//					 * 
//					 */
//					fetchData();
//					/**
//					 * Database query to fetch the category ids.
//					 * 
//					 */
//					fetchCategoyId();
//				}
//				else{
//					dateDiff = checkForDate(dateStringCurrent);	
//					showToast("diff " + dateDiff  +"");
//					
//					if(diffDays>3){
//						showToast("Time to delete cache");
//						mSession.setSavedDate(dateStringCurrent);
//						showToast(mSession.getSavedDate());
//						deleteAllTables();
//					}
//					else{
//						showToast("From Cached Data only");
//						/**
//						 * Database query to fetch the category names.
//						 * 
//						 */
//						fetchData();
//						/**
//						 * Database query to fetch the category ids.
//						 * 
//						 */
//						fetchCategoyId();
//					}
//				}
				
				/**
				 * Database query to fetch the category names.
				 * 
				 */
				fetchData();
				/**
				 * Database query to fetch the category ids.
				 * 
				 */
				fetchCategoyId();
				
			}
		}
	}
	
	

	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onResume() {

	    super.onResume();

	    getView().setFocusableInTouchMode(true);
	    getView().requestFocus();
	    getView().setOnKeyListener(new View.OnKeyListener() {
	        @Override
	        public boolean onKey(View v, int keyCode, KeyEvent event) {

	            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

	                // handle back button
	            	  if (getFragmentManager().getBackStackEntryCount() == 0) {
	         	         if(back_pressed+2000 > System.currentTimeMillis()){
	         	        	 getActivity().finish();
	         	         }
	         	         else{
	         	        	 showToast("Press again to exit.");
	         	        	 back_pressed = System.currentTimeMillis();
	         	         }
	         	     } else {
	         	         getFragmentManager().popBackStack();
	         	     }
	                return true;

	            }

	            return false;
	        }
	    });
	}

	//checking date difference to refresh local DB
	private long checkForDate(String date) {
		// TODO Auto-generated method stub

		String currentDate = date;
		String savedDate = mSession.getSavedDate();
		
		Date d1 = null;
		Date d2 = null;
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		try {
			
			d1 = dateFormat.parse(savedDate);
			d2 = dateFormat.parse(currentDate);
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return diffDays;
	}


	private void deleteAllTables() {
		// TODO Auto-generated method stub
		
		//LogFile.LogData("deleteOldRecords");
		//String msWhere = DatabaseHelper.USER_ID+" ='"+UserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_MAIN_CATEGORIES,
					"", Config.DELETE_DATA,  Config.DELETE_MAINTABLE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SUB_CATEGORY,
					"", Config.DELETE_DATA,  Config.DELETE_SUBCATTABLE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_PRODUCTS,
					"", Config.DELETE_DATA,  Config.DELETE_PRODUCTTABLE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
