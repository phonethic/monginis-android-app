package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.phonethics.model.ProductModel;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

public class SortByFragment extends Fragment {

	private View mView;
	private TextView mDeliveryModeLabel;
	private CheckBox mChkNextDay;
	private TextView mTxtNextDay;

	private CheckBox mChkNormalDay;
	private TextView mTxtNormalDay;

	private CheckBox mChkFree;
	private TextView mTxtFree;

	private TextView mPriceRangeLabel;
	private CheckBox mChkFourHundred;
	private TextView mTxtFourHundred;

	private CheckBox mChkEightHundred;
	private TextView mTxtEightHundred;

	private CheckBox mChkTwelveHundred;
	private TextView mTxtTwelveHundred;

	private CheckBox mChkAboveTwelveHundred;
	private TextView mTxtAboveTwelveHundred;

	private TextView mCategoryLabel;
	private CheckBox mChkEggless;
	private TextView mTxtEggless;

	private CheckBox mChkWithEgg;
	private TextView mTxtWithEgg;

	private CheckBox mChkBoth;
	private TextView mTxtBoth;

	private TextView mWeight;
	private CheckBox mChkAbove1Kg;
	private TextView mTxtAbove1Kg;

	private CheckBox mChkOneKg;
	private TextView mTxtOneKg;

	private CheckBox mChkHalfKg;
	private TextView mTxtHalfKg;

	private CheckBox mChkSameDay;
	private TextView mTxtSameDay;

	private TextView mDoneBtn;

	private RelativeLayout EgglessRelativeLayout;
	private RelativeLayout WithEggRelativeLayout;
	private RelativeLayout BothRelativeLayout;
	private RelativeLayout Above1KgRelativeLayout;
	private RelativeLayout OneKgRelativeLayout;
	private RelativeLayout HalfKgRelativeLayout;
	private RelativeLayout SameDayRelativeLayout;

	Typeface tf;
	boolean bit = false;
	boolean bit_price = false;
	boolean bit_category = false;
	boolean bit_weight = false;
	int time ;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		LandingScreen.sortVisiblePressed();

		Calendar c = Calendar.getInstance(); 
		int hour = c.get(Calendar.HOUR_OF_DAY);
		time = hour;
		Config.Log("TIME","TIME " + time);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mView=(View)inflater.inflate(R.layout.fragment_sort_by, null);

		mDeliveryModeLabel = (TextView)mView.findViewById(R.id.mDeliveryModeLabel);
		mChkNextDay = (CheckBox)mView.findViewById(R.id.mChkNextDay);
		mTxtNextDay=(TextView)mView.findViewById(R.id.mTxtNextDay);
		mChkNormalDay=(CheckBox)mView.findViewById(R.id.mChkNormalDay);
		mTxtNormalDay=(TextView)mView.findViewById(R.id.mTxtNormalDay);
		mChkFree=(CheckBox)mView.findViewById(R.id.mChkFree);
		mTxtFree=(TextView)mView.findViewById(R.id.mTxtFree);
		mPriceRangeLabel=(TextView)mView.findViewById(R.id.mPriceRangeLabel);
		mChkFourHundred=(CheckBox)mView.findViewById(R.id.mChkFourHundred);
		mTxtFourHundred=(TextView)mView.findViewById(R.id.mTxtFourHundred);
		mChkEightHundred=(CheckBox)mView.findViewById(R.id.mChkEightHundred);
		mTxtEightHundred=(TextView)mView.findViewById(R.id.mTxtEightHundred);
		mTxtTwelveHundred=(TextView)mView.findViewById(R.id.mTxtTwelveHundred);
		mChkTwelveHundred=(CheckBox)mView.findViewById(R.id.mChkTwelveHundred);
		mTxtAboveTwelveHundred=(TextView)mView.findViewById(R.id.mTxtAboveTwelveHundred);
		mChkAboveTwelveHundred=(CheckBox)mView.findViewById(R.id.mChkAboveTwelveHundred);
		mCategoryLabel=(TextView)mView.findViewById(R.id.mCategoryLabel);
		mChkEggless=(CheckBox)mView.findViewById(R.id.mChkEggless);
		mTxtEggless=(TextView)mView.findViewById(R.id.mTxtEggless);
		mChkWithEgg=(CheckBox)mView.findViewById(R.id.mChkWithEgg);
		mTxtWithEgg=(TextView)mView.findViewById(R.id.mTxtWithEgg);
		mChkBoth=(CheckBox)mView.findViewById(R.id.mChkBoth);
		mTxtBoth=(TextView)mView.findViewById(R.id.mTxtBoth);
		mWeight=(TextView)mView.findViewById(R.id.mWeight);
		mChkAbove1Kg=(CheckBox)mView.findViewById(R.id.mChkAbove1Kg);
		mTxtAbove1Kg=(TextView)mView.findViewById(R.id.mTxtAbove1Kg);
		mChkOneKg=(CheckBox)mView.findViewById(R.id.mChkOneKg);
		mTxtOneKg=(TextView)mView.findViewById(R.id.mTxtOneKg);
		mChkHalfKg=(CheckBox)mView.findViewById(R.id.mChkHalfKg);
		mTxtHalfKg=(TextView)mView.findViewById(R.id.mTxtHalfKg);
		mDoneBtn=(TextView)mView.findViewById(R.id.mDoneBtn);
		mTxtSameDay=(TextView)mView.findViewById(R.id.mTxtSameDay);
		mChkSameDay=(CheckBox)mView.findViewById(R.id.mChkSameDay);

		EgglessRelativeLayout=(RelativeLayout)mView.findViewById(R.id.EgglessRelativeLayout);
		WithEggRelativeLayout=(RelativeLayout)mView.findViewById(R.id.WithEggRelativeLayout);
		BothRelativeLayout=(RelativeLayout)mView.findViewById(R.id.BothRelativeLayout);
		Above1KgRelativeLayout=(RelativeLayout)mView.findViewById(R.id.Above1KgRelativeLayout);
		OneKgRelativeLayout=(RelativeLayout)mView.findViewById(R.id.OneKgRelativeLayout);
		HalfKgRelativeLayout=(RelativeLayout)mView.findViewById(R.id.HalfKgRelativeLayout);
		SameDayRelativeLayout=(RelativeLayout)mView.findViewById(R.id.SameDayRelativeLayout);

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		mDeliveryModeLabel.setTypeface(tf);
		mTxtSameDay.setTypeface(tf);
		mTxtNextDay.setTypeface(tf);
		mTxtNormalDay.setTypeface(tf);
		mTxtFree.setTypeface(tf);
		mPriceRangeLabel.setTypeface(tf);
		mTxtFourHundred.setTypeface(tf);
		mTxtEightHundred.setTypeface(tf);
		mTxtTwelveHundred.setTypeface(tf);
		mTxtAboveTwelveHundred.setTypeface(tf);
		mCategoryLabel.setTypeface(tf);
		mTxtEggless.setTypeface(tf);
		mTxtWithEgg.setTypeface(tf);
		mTxtBoth.setTypeface(tf);
		mWeight.setTypeface(tf);
		mTxtAbove1Kg.setTypeface(tf);
		mTxtOneKg.setTypeface(tf);
		mTxtHalfKg.setTypeface(tf);

		//to check the timing for same day delivery
		if(time >= 15){
			//Toast.makeText(getActivity(), "Dont show", 0).show();
			Config.SAME_DAY = "false";
			mChkSameDay.setChecked(false);
			SameDayRelativeLayout.setVisibility(View.GONE);
		}
		else{
			//Toast.makeText(getActivity(), " show", 0).show();
			SameDayRelativeLayout.setVisibility(View.VISIBLE);
		}

		//to get selected check boxes
		getSelectedCheckBoxes();

		//to check for cake (layout visibility
		if(Config.MAIN_SELECTED_CATEGORY_ID.equalsIgnoreCase("1")){

			mCategoryLabel.setVisibility(View.VISIBLE);
			EgglessRelativeLayout.setVisibility(View.VISIBLE);
			WithEggRelativeLayout.setVisibility(View.VISIBLE);
			BothRelativeLayout.setVisibility(View.VISIBLE);
			mWeight.setVisibility(View.VISIBLE);
			Above1KgRelativeLayout.setVisibility(View.VISIBLE);
			OneKgRelativeLayout.setVisibility(View.VISIBLE);
			HalfKgRelativeLayout.setVisibility(View.VISIBLE);
		}

		mDoneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				LandingScreen.sortGone();
				//checkForData();
				CheckForDataNew();
			}
		});
		return mView;
	}

	void checkForData() {
		// TODO Auto-generated method stub

		//String msWhere = "WHERE "+DatabaseHelper.MAIN_CAT_ID+" = '"+msCategId+"'";
		String msWhere = "WHERE " + DatabaseHelper.PROD_SUBCAT_ID + " = '" + Config.SUB_CAT_ID + "'";

		//Delivery Section
		if(mChkNextDay.isChecked()){
			Config.NEXT_DAY = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_NEXTDAY_DELIVERY+" = "+ "'Y'";
			bit = true;
		}
		else
			Config.NEXT_DAY = "false";

		if(mChkNormalDay.isChecked()){
			//if(bit){
			Config.NORMAL = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y'";
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y'";
			//				bit = true;
			//			}
		}
		else
			Config.NORMAL = "false";

		if(mChkFree.isChecked()){
			//if(bit){
			Config.FREE = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y'";
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y'";
			//				bit = true;
			//			}
		}
		else
			Config.FREE = "false";

		//Price Range Section
		if(mChkFourHundred.isChecked()){
			//if(bit){
			Config.FOUR_HUNDRED = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" <= "+ 400;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_PRICE_RANGE_INT+" <= "+ 400;
			//				bit = true;
			//			}
		}
		else
			Config.FOUR_HUNDRED = "false";

		if(mChkEightHundred.isChecked()){
			//if(bit){
			Config.EIGHT_HUNDRED = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800;
			//				bit = true;
			//			}
		}
		else
			Config.EIGHT_HUNDRED = "false";

		if(mChkTwelveHundred.isChecked()){
			//if(bit){
			Config.TWELVE_HUNDRED = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200;
			//				bit = true;
			//			}
		}
		else
			Config.TWELVE_HUNDRED = "false";

		if(mChkAboveTwelveHundred.isChecked()){
			//if(bit){
			Config.ABOVE_TWELVE_HUNDRED = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200;
			//				bit = true;
			//			}
		}
		else
			Config.ABOVE_TWELVE_HUNDRED = "false";

		//Category Section
		if(mChkEggless.isChecked()){
			//if(bit){
			Config.EGGLESS = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'";
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'";
			//				bit = true;
			//			}
		}
		else
			Config.EGGLESS = "false";

		if(mChkWithEgg.isChecked()){
			//if(bit){
			Config.WITHEGG = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
			//				bit = true;
			//			}
		}
		else
			Config.WITHEGG = "false";

		if(mChkBoth.isChecked()){
			//if(bit){
			Config.BOTH = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
			//				bit = true;
			//			}
		}
		else
			Config.BOTH = "false";

		//Weight Section
		if(mChkAbove1Kg.isChecked()){
			//if(bit){
			Config.ABOVE_ONEKG = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" > "+ 1000 ;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" > "+ 1000 ;
			//				bit = true;
			//			}
		}
		else
			Config.ABOVE_ONEKG = "false";

		if(mChkOneKg.isChecked()){
			//if(bit){
			Config.ONEKG = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 ;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 ;
			//				bit = true;
			//			}
		}
		else
			Config.ONEKG = "false";

		if(mChkHalfKg.isChecked()){
			//if(bit){
			Config.HALFKG = "true";
			msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 ;
			//			}
			//			else{
			//				msWhere = msWhere + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 ;
			//				bit = true;
			//			}
		}
		else
			Config.HALFKG = "false";

		LogFile.LogData("fetchData");
		LogFile.LogData("" + msWhere);
		new AsyncDatabaseObjectQuery<ProductModel>(getActivity(), new OnCompleteListner<ProductModel>() {

			@Override
			public void onComplete(ArrayList<ProductModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				LogFile.LogData("onComplete");
				callProductList(response);
			}
		}, DatabaseHelper.TABLE_PRODUCTS, Config.RETRIVE_DATA_WHERE,msWhere, "SUB_CATEGORY").execute();

	}

	void callProductList(ArrayList<ProductModel> response) {
		// TODO Auto-generated method stub
		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

		ProductListFragment prodFrag=new ProductListFragment();
		Bundle b=new Bundle();
		b.putString("tag", "fromSort");
		b.putParcelableArrayList("Products", response);
		prodFrag.setArguments(b);
		transaction.add(R.id.content_frame, prodFrag);
		//transaction.addToBackStack(null);
		transaction.commit();

		//Config.SUB_CAT_ID = marrSubCat.get(position).getSub_cat_id();
	}

	//	@Override
	//	public void onDestroyView() {
	//		// TODO Auto-generated method stub
	//		super.onDestroyView();
	//		Intent intent = new Intent(getActivity(), LandingScreen.class);
	//		startActivity(intent);
	//		getActivity().overridePendingTransition(0, 0);
	//		getActivity().finish();
	//	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Config.Log("RESUME","PAUSE");
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Config.Log("RESUME","RESUME");
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Config.Log("RESUME","START");
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Config.Log("RESUME","STOP");
		//		Intent intent = new Intent(getActivity(), LandingScreen.class);
		//		startActivity(intent);
		//		getActivity().overridePendingTransition(0, 0);
		//		getActivity().finish();
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		Config.Log("RESUME","DESTROY VIEW");
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		Config.Log("RESUME","DETACH");
	}

	void getSelectedCheckBoxes(){

		//Delivery Section
		if(Config.SAME_DAY.equalsIgnoreCase("true")){
			mChkSameDay.setChecked(true);
		}
		else
			mChkSameDay.setChecked(false);

		if(Config.NEXT_DAY.equalsIgnoreCase("true")){
			mChkNextDay.setChecked(true);
		}
		else
			mChkNextDay.setChecked(false);

		if(Config.NORMAL.equalsIgnoreCase("true")){
			mChkNormalDay.setChecked(true);
		}
		else
			mChkNormalDay.setChecked(false);

		if(Config.FREE.equalsIgnoreCase("true")){
			mChkFree.setChecked(true);
		}
		else
			mChkFree.setChecked(false);

		//Price Range Section
		if(Config.FOUR_HUNDRED.equalsIgnoreCase("true")){
			mChkFourHundred.setChecked(true);
		}
		else
			mChkFourHundred.setChecked(false);

		if(Config.EIGHT_HUNDRED.equalsIgnoreCase("true")){
			mChkEightHundred.setChecked(true);
		}
		else
			mChkEightHundred.setChecked(false);

		if(Config.TWELVE_HUNDRED.equalsIgnoreCase("true")){
			mChkTwelveHundred.setChecked(true);
		}
		else
			mChkTwelveHundred.setChecked(false);

		if(Config.ABOVE_TWELVE_HUNDRED.equalsIgnoreCase("true")){
			mChkAboveTwelveHundred.setChecked(true);
		}
		else
			mChkAboveTwelveHundred.setChecked(false);

		//Category Section
		if(Config.EGGLESS.equalsIgnoreCase("true")){
			mChkEggless.setChecked(true);
		}
		else
			mChkEggless.setChecked(false);

		if(Config.WITHEGG.equalsIgnoreCase("true")){
			mChkWithEgg.setChecked(true);
		}
		else
			mChkWithEgg.setChecked(false);

		if(Config.BOTH.equalsIgnoreCase("true")){
			mChkBoth.setChecked(true);
		}
		else
			mChkBoth.setChecked(false);

		//Weight Section
		if(Config.ABOVE_ONEKG.equalsIgnoreCase("true")){
			mChkAbove1Kg.setChecked(true);
		}
		else
			mChkAbove1Kg.setChecked(false);

		if(Config.ONEKG.equalsIgnoreCase("true")){
			mChkOneKg.setChecked(true);
		}
		else
			mChkOneKg.setChecked(false);

		if(Config.HALFKG.equalsIgnoreCase("true")){
			mChkHalfKg.setChecked(true);
		}
		else
			mChkHalfKg.setChecked(false);

	}

	void CheckForDataNew(){

		String msWhere = "WHERE (" + DatabaseHelper.PROD_SUBCAT_ID + " = '" + Config.SUB_CAT_ID + "')";
		String msWhere_block = "";
		String msWhere_block_price = "";
		String msWhere_block_category = "";
		String msWhere_block_weight = "";

		//Delivery Section
		if(mChkNextDay.isChecked()){
			Config.NEXT_DAY = "true";
			//msWhere = msWhere + " AND " + DatabaseHelper.PROD_NEXTDAY_DELIVERY+" = "+ "'Y'";
			msWhere_block  = " AND (" + DatabaseHelper.PROD_NEXTDAY_DELIVERY+" = "+ "'Y' )";
			bit = true;
		}
		else
			Config.NEXT_DAY = "false";

		if(mChkSameDay.isChecked()){
			Config.SAME_DAY = "true";
			if(bit){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y'";
				msWhere_block = msWhere_block.replace(")", "") + " OR " + DatabaseHelper.PROD_SAMEDAY_DELIVERY+" = "+ "'Y' )";
			}
			else{
				//msWhere = msWhere + " AND (" + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y' )";	
				msWhere_block = msWhere_block + " AND (" + DatabaseHelper.PROD_SAMEDAY_DELIVERY+" = "+ "'Y' )";	
				bit = true;
			}

		}
		else
			Config.SAME_DAY = "false";

		if(mChkNormalDay.isChecked()){
			Config.NORMAL = "true";
			if(bit){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y'";
				msWhere_block = msWhere_block.replace(")", "") + " OR " + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y' )";
			}
			else{
				//msWhere = msWhere + " AND (" + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y' )";	
				msWhere_block = msWhere_block + " AND (" + DatabaseHelper.PROD_NORMAL_DELIVERY+" = "+ "'Y' )";	
				bit = true;
			}

		}
		else
			Config.NORMAL = "false";

		if(mChkFree.isChecked()){
			Config.FREE = "true";
			if(bit){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y'";	
				msWhere_block = msWhere_block.replace(")", "") + " OR " + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y' )";	
			}
			else{
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y'";
				msWhere_block = msWhere_block + " AND (" + DatabaseHelper.PROD_FREE_DELIVERY+" = "+ "'Y' )";
				bit = true;
			}

		}
		else
			Config.FREE = "false";

		//final delivery block query
		Config.Log("QUERY",""+msWhere_block);
		msWhere = msWhere + msWhere_block;

		//Price Range Section
		if(mChkFourHundred.isChecked()){
			Config.FOUR_HUNDRED = "true";
			//msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" <= "+ 400;
			msWhere_block_price = msWhere_block_price + " AND (" + DatabaseHelper.PROD_PRICE_RANGE_INT+" <= "+ 400 + " )";
			bit_price = true;
		}
		else
			Config.FOUR_HUNDRED = "false";


		if(mChkEightHundred.isChecked()){
			Config.EIGHT_HUNDRED = "true";
			if(bit_price){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800;
				msWhere_block_price = msWhere_block_price.replace(")", "") + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800 + " )";
			}
			else{
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800;
				msWhere_block_price = msWhere_block_price + " AND (" + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 400 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 800 + " )";
				bit_price = true;
			}
		}
		else
			Config.EIGHT_HUNDRED = "false";


		if(mChkTwelveHundred.isChecked()){
			Config.TWELVE_HUNDRED = "true";
			if(bit_price){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200;
				msWhere_block_price = msWhere_block_price.replace(")", "") + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200 + " )";
			}
			else{
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200;
				msWhere_block_price = msWhere_block_price + " AND (" + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 800 + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT + " <= " + 1200 + " )";
				bit_price = true;
			}
		}
		else
			Config.TWELVE_HUNDRED = "false";


		if(mChkAboveTwelveHundred.isChecked()){
			Config.ABOVE_TWELVE_HUNDRED = "true";
			if(bit_price){
				//msWhere = msWhere + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200;
				msWhere_block_price = msWhere_block_price.replace(")", "") + " OR " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200 + " )";
			}
			else{
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200;
				msWhere_block_price = msWhere_block_price + " AND (" + DatabaseHelper.PROD_PRICE_RANGE_INT+" > "+ 1200 + " )";
				bit_price = true;
			}
		}
		else
			Config.ABOVE_TWELVE_HUNDRED = "false";


		//final price block query
		Config.Log("QUERY",""+msWhere_block_price);
		msWhere = msWhere + msWhere_block_price;
		Config.Log("QUERY",""+msWhere);

		//Category Section
		//check first that view is visible or not
		if(EgglessRelativeLayout.isShown() && WithEggRelativeLayout.isShown() && BothRelativeLayout.isShown()){

			if(mChkEggless.isChecked()){
				Config.EGGLESS = "true";
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'";
				msWhere_block_category = msWhere_block_category + " AND (" + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y' )";
				bit_category = true;
			}
			else
				Config.EGGLESS = "false";

			if(mChkWithEgg.isChecked()){
				Config.WITHEGG = "true";
				if(bit_category){
					//msWhere = msWhere + " OR " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
					msWhere = msWhere_block_category.replace(")", "") + " OR " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y' )";
				}
				else{
					//msWhere = msWhere + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
					msWhere = msWhere + " AND (" + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y' )";
					bit_category = true;
				}
			}
			else
				Config.WITHEGG = "false";


			if(mChkBoth.isChecked()){
				Config.BOTH = "true";
				if(bit_category){
					//msWhere = msWhere + " OR " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
					msWhere_block_category = msWhere_block_category.replace(")", "") + " OR " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y' )";
				}
				else{
					//msWhere = msWhere + " AND " + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y'";
					msWhere_block_category = msWhere_block_category + " AND (" + DatabaseHelper.PROD_EGGLESS_TYPE+" = "+ "'Y'" + " AND " + DatabaseHelper.PROD_WITHEGG_TYPE+" = "+ "'Y' )";
					bit_category = true;
				}

			}
			else
				Config.BOTH = "false";

			//final price block query
			Config.Log("QUERY",""+msWhere_block_category);
			msWhere = msWhere + msWhere_block_category;

		}

		//Weight Section
		//check first whether view is visible or not
		if(Above1KgRelativeLayout.isShown() && OneKgRelativeLayout.isShown() && HalfKgRelativeLayout.isShown()){
			if(mChkAbove1Kg.isChecked()){
				Config.ABOVE_ONEKG = "true";
				//msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" > "+ 1000 ;
				msWhere_block_weight = msWhere_block_weight + " AND (" + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" > "+ 1000 + " )";
				bit_weight = true;
			}
			else
				Config.ABOVE_ONEKG = "false";


			if(mChkOneKg.isChecked()){
				Config.ONEKG = "true";
				if(bit_weight){
					//msWhere = msWhere + " OR " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 ;
					msWhere_block_weight = msWhere_block_weight.replace(")", "") + " OR " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 + " )" ;
				}
				else{
					//msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 ;
					msWhere_block_weight = msWhere_block_weight + " AND (" + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 1000 + ")";
				}
			}
			else
				Config.ONEKG = "false";


			if(mChkHalfKg.isChecked()){
				Config.HALFKG = "true";
				if(bit_weight){
					//msWhere = msWhere + " OR " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 ;
					msWhere_block_weight = msWhere_block_weight.replace(")", "") + " OR " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 + ")" ;
				}
				else{
					//msWhere = msWhere + " AND " + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 ;
					msWhere_block_weight = msWhere_block_weight + " AND (" + DatabaseHelper.PROD_WEIGHT_GRAMS_INT+" = "+ 500 + ")";
					bit_weight = true;
				}
			}
			else
				Config.HALFKG = "false";

			//final price block query
			Config.Log("QUERY",""+msWhere_block_weight);
			msWhere = msWhere + msWhere_block_weight;

		}


		LogFile.LogData("fetchData");
		LogFile.LogData("" + msWhere);
		new AsyncDatabaseObjectQuery<ProductModel>(getActivity(), new OnCompleteListner<ProductModel>() {

			@Override
			public void onComplete(ArrayList<ProductModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				LogFile.LogData("onComplete");
				callProductList(response);
			}
		}, DatabaseHelper.TABLE_PRODUCTS, Config.RETRIVE_DATA_WHERE,msWhere, "SUB_CATEGORY").execute();
	}
}
