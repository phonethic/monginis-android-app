package com.phonethics.monginis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CategoriesAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class DistributorFormFragment extends Fragment implements OnItemClickListener{

	private View mView;
	private ViewFlipper mDistributorFlipper;
	private TextView mNext;
	private TextView mDetails;
	private TextView mDateApplication;
	private EditText mLocation;
	private EditText mName;
	private EditText mAddress;
	private EditText mCity;
	private EditText mMobile;
	private EditText mState;
	private TextView mCurrentOccDetail;
	private EditText mBusinessType;
	private EditText mTurnover;
	private EditText mInvestment;
	private EditText mBrandNames;
	private TextView mProrposedDetail;
	private EditText mExpectedSales;
	private EditText mInvestmentCapacity;
	private TextView mPreviousPage2;
	private TextView mSubmit;
	private EditText mZipCode;
	private EditText mEmail;
	private TextView mManageBy;
	private TextView selectManageBy;
	RelativeLayout dividerView;

	Typeface tf;

	//for dropdown
	ArrayList<String> mArrManageBy = new ArrayList<String>();

	//dropdown list and dialogs
	private Dialog mListDialog;
	ListView mDialogList;
	CategoriesAdapter mAdapter;

	Calendar mCalDate = Calendar.getInstance();
	String mCurrentDate="";

	//form fields
	String apply_for;
	String application_date;
	String location ;
	String name;
	String address;
	String mobile;
	String email;
	String city;
	String state;
	String zipcode;
	String business_type;
	String turnover;
	String investment;
	String brands;
	String expected_sales;
	String investment_capacity;
	String business_managed_by;
	
	private ProgressDialog mProgressDialog;
	private Map<String, String> param = new HashMap<String, String>();
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");
	}
	
	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Date currentDate = Calendar.getInstance().getTime();
		mCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_distributor_form, null);

		mDistributorFlipper=(ViewFlipper)mView.findViewById(R.id.mDistributorFlipper);
		mDetails=(TextView)mView.findViewById(R.id.mDetails);
		mDateApplication=(TextView)mView.findViewById(R.id.mDateApplication);
		mLocation=(EditText)mView.findViewById(R.id.mLocation);
		mName=(EditText)mView.findViewById(R.id.mName);
		mAddress=(EditText)mView.findViewById(R.id.mAddress);
		mCity=(EditText)mView.findViewById(R.id.mCity);
		mMobile=(EditText)mView.findViewById(R.id.mMobile); 
		mState=(EditText)mView.findViewById(R.id.mState);
		mNext=(TextView)mView.findViewById(R.id.mNext);
		mCurrentOccDetail=(TextView)mView.findViewById(R.id.mCurrentOccDetail);
		mBusinessType=(EditText)mView.findViewById(R.id.mBusinessType);
		mTurnover=(EditText)mView.findViewById(R.id.mTurnover);
		mInvestment=(EditText)mView.findViewById(R.id.mInvestment);
		mBrandNames=(EditText)mView.findViewById(R.id.mBrandNames);
		mProrposedDetail=(TextView)mView.findViewById(R.id.mProrposedDetail);
		mExpectedSales=(EditText)mView.findViewById(R.id.mExpectedSales);
		mInvestmentCapacity=(EditText)mView.findViewById(R.id.mInvestmentCapacity);
		mPreviousPage2=(TextView)mView.findViewById(R.id.mPreviousPage2);
		mSubmit=(TextView)mView.findViewById(R.id.mSubmit);
		mZipCode=(EditText)mView.findViewById(R.id.mZipCode);
		mEmail=(EditText)mView.findViewById(R.id.mEmail);
		mManageBy=(TextView)mView.findViewById(R.id.mManageBy);
		selectManageBy=(TextView)mView.findViewById(R.id.selectManageBy);

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		mDetails.setTypeface(tf);
		mDateApplication.setTypeface(tf);
		mLocation.setTypeface(tf);
		mName.setTypeface(tf);
		mAddress.setTypeface(tf);
		mCity.setTypeface(tf);
		mMobile.setTypeface(tf);
		mState.setTypeface(tf);
		mNext.setTypeface(tf);
		mCurrentOccDetail.setTypeface(tf);
		mBusinessType.setTypeface(tf);
		mTurnover.setTypeface(tf);
		mInvestment.setTypeface(tf);
		mBrandNames.setTypeface(tf);
		mProrposedDetail.setTypeface(tf);
		mExpectedSales.setTypeface(tf);
		mInvestmentCapacity.setTypeface(tf);
		mPreviousPage2.setTypeface(tf);
		mSubmit.setTypeface(tf);
		mZipCode.setTypeface(tf);
		mEmail.setTypeface(tf);
		mManageBy.setTypeface(tf);
		selectManageBy.setTypeface(tf);

		mListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		dividerView.setVisibility(View.GONE);
		mListDialog.setTitle("Select Option");

		//list for the dropdowns
		mArrManageBy.add("SELF");
		mArrManageBy.add("RELATIVES");
		mArrManageBy.add("PARTNERS");
		mArrManageBy.add("PROFESSIONALS");

		mNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDistributorFlipper.showNext();
			}
		});

		mPreviousPage2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDistributorFlipper.showPrevious();
			}
		});

		selectManageBy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mAdapter = new CategoriesAdapter(getActivity(), 0, 0, mArrManageBy);
				mDialogList.setAdapter(mAdapter);
				mListDialog.show();
			}
		});

		mDateApplication.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseDate();
			}
		});

		mSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				apply_for = "distributor";
				application_date = mDateApplication.getText().toString();
				location = mLocation.getText().toString();
				name = mName.getText().toString();
				address = mAddress.getText().toString();
				mobile = mMobile.getText().toString();
				email = mEmail.getText().toString();
				city = mCity.getText().toString();
				state = mState.getText().toString();
				zipcode = mZipCode.getText().toString();
				business_type = mBusinessType.getText().toString();
				turnover = mTurnover.getText().toString();
				investment = mInvestment.getText().toString();
				brands = mBrandNames.getText().toString();
				expected_sales = mExpectedSales.getText().toString();
				investment_capacity = mInvestmentCapacity.getText().toString();
				business_managed_by = selectManageBy.getText().toString();
				
				if(mName.length() == 0){
					showToast("Please enter applicant name");
				}
				else if(mMobile.length()<10){
					showToast("Please enter valid mobile number");
				}
				else if(mEmail.getText().toString().length()==0){
					showToast("Please enter Email Id");
				}
				else if(!SignUpFragment.isEmailValid(mEmail.getText().toString())){
					showToast("Please enter a valid Email Id");
				}
				else{
					applyForDistributorForm();
				}
			}
		});
		mDialogList.setOnItemClickListener(this);
		return mView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		selectManageBy.setText(mArrManageBy.get(position));
		mListDialog.dismiss();
	}

	void chooseDate() {
		// TODO Auto-generated method stub
		new DatePickerDialog(getActivity(),date , mCalDate.get(Calendar.YEAR), mCalDate.get(Calendar.MONTH), mCalDate.get(Calendar.DATE)).show();
	}

	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			// TODO Auto-generated method stub
			mCalDate.set(Calendar.YEAR,year);
			mCalDate.set(Calendar.MONTH,monthOfYear);
			mCalDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
			DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			String setDate = dt.format(mCalDate.getTime());

			if(setDate.compareTo(mCurrentDate)>0){
				showToast("Unable to set future date");
				mDateApplication.setText("");

			}else{
				mDateApplication.setText(setDate);
			}
		}
	};

	void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}
	
	void applyForDistributorForm() {
		// TODO Auto-generated method stub
		
		try {
			
			showProgressDialog();
			String TAG="MERCHANDISING FORM";
			String url=Config.POST_APPLY_FORM;
			Config.Log("URL","URL "+url);
			
			//adding body to form
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("apply_for", apply_for);
			jsonObj.put("application_date", application_date);
			jsonObj.put("location", location);
			jsonObj.put("name", name);
			jsonObj.put("address", address);
			jsonObj.put("mobile", mobile);
			jsonObj.put("email", email);
			jsonObj.put("city", city);
			jsonObj.put("state", state);
			jsonObj.put("zipcode", zipcode);
			jsonObj.put("business_type", business_type);
			jsonObj.put("turnover", turnover);
			jsonObj.put("investment", investment);
			jsonObj.put("brands", brands);
			jsonObj.put("expected_sales", expected_sales);
			jsonObj.put("investment_capacity", investment_capacity);
			jsonObj.put("business_managed_by", business_managed_by);
			
			Config.Log("JSON ",  "JSON " + jsonObj.toString());

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							String message = json.getString("message");
							if(success.equalsIgnoreCase("true")){

								param.put("Form_Submitted", "Distributor");
								EventTracker.logEvent(EventsName.PARTNER_WITH_US,param);
								//								JSONObject data = json.getJSONObject("data");
								//								String userId = data.getString("user_id");
								//Config.USER_ID = userId;
								showAlertDialog(message);
								//showToast(message);
							}
							else{
								//message = json.getString("message");
								showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void showAlertDialog(String message) {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				MenuOptionFragment chkFrag=new MenuOptionFragment();
				transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
				//transaction.addToBackStack("MENU OPTION");
				// Commit the transaction
				transaction.commit();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
