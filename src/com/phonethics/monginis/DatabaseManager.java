package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import com.phonethics.model.CartProductInfo;
import com.phonethics.model.OrderModel;
import com.phonethics.model.ProductModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.model.SubCategorModel;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager  {

	private static DatabaseManager mDbInstance;
	private static SQLiteOpenHelper mDbSqlHelper;
	private static SQLiteDatabase mDatabase;

	private AtomicInteger mOpenCounter = new AtomicInteger();

	public DatabaseManager(SQLiteOpenHelper helper){
		mDbSqlHelper=helper;
	}

	public static synchronized void initInstance(SQLiteOpenHelper helper){
		if(mDbInstance==null){
			mDbInstance=new DatabaseManager(helper);
		}
	}
	public static synchronized DatabaseManager getInstance(){
		if(mDbInstance==null){
			throw new IllegalStateException(DatabaseManager.class.getSimpleName() +
					" is not initialized, call initializeInstance(..) method first.");
		}
		return mDbInstance;
	}
	public synchronized SQLiteDatabase openDatabase(){
		if(mOpenCounter.incrementAndGet()==1){
			mDatabase=mDbSqlHelper.getWritableDatabase();
		}
		return mDatabase;
	}

	public synchronized void closeDataBase(){
		if(mOpenCounter.decrementAndGet()==0){
			mDatabase.close();
		}
	}



	public boolean replaceOrUpdate(final String sTable,ContentValues [] contentValues,String tag){
		LogFile.LogData("replaceOrUpdate");
		if(mDatabase==null){
			openDatabase();
		}
		mDatabase.beginTransaction();
		try
		{
			int count=contentValues.length;
			for(int i=0;i<count;i++){
				ContentValues value=contentValues[i];
				long id=mDatabase.replaceOrThrow(sTable, null, value);
				Config.Log("Database Manager", "Insert ID "+id);
			}
			mDatabase.setTransactionSuccessful();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			mDatabase.endTransaction();
		}

		return true;
	}


	//my func for single value
	public boolean replaceOrUpdate(final String sTable,ContentValues contentValues,String tag){
		if(mDatabase==null){
			openDatabase();
		}
		mDatabase.beginTransaction();
		try
		{

			ContentValues value=contentValues;
			long id=mDatabase.replaceOrThrow(sTable, null, value);
			Config.Log("Database Manager", "Insert ID "+id);

			mDatabase.setTransactionSuccessful();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			mDatabase.endTransaction();
		}

		return true;
	}

	
	
	public ArrayList<String> getData(String tableName,String columnName)
	{
		ArrayList<String> data=new ArrayList<String>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT "+columnName+" FROM "+tableName;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				data.add(cursor.getString(cursor.getColumnIndex(columnName)));
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		Config.Log("DATA ","DATA " + data);
		return data;
	}
	
	
	
	//clear all records of particular table
	public int clearRecords(final String sTable,String msWhere){
		int clearedRecords = 0;
		LogFile.LogData("clearRecords");
		LogFile.LogData("Table "+sTable + " Where "+msWhere);
		if(mDatabase==null){
			openDatabase();
		}
		//mDatabase.beginTransaction();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			/*String query="DELETE FROM "+sTable;
			if(msWhere!=null && !msWhere.equalsIgnoreCase("")){
				query = query + " "+msWhere;
			}
			LogFile.LogData("query "+query);*/
			//mDatabase.rawQuery(query, null);
			if(msWhere!=null && !msWhere.equalsIgnoreCase("")){
				
				clearedRecords = mDatabase.delete(sTable, msWhere, null);
			}else{
				clearedRecords = mDatabase.delete(sTable, null, null);
			}
			
			
			
			//mDatabase.setTransactionSuccessful();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			//mDatabase.endTransaction();
		}
		LogFile.LogData("clearRecords "+clearedRecords);
		return clearedRecords;
	}

	//for all of the columns(added by salman
	public ArrayList<CartProductInfo> getData(String tableName,String columnName,String value)
	{
		ArrayList<CartProductInfo> data=new ArrayList<CartProductInfo>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+tableName + " WHERE " + columnName + " = '" + value +"'";
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				CartProductInfo productObj = new CartProductInfo();
				productObj.setProduct_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_ID)));
				productObj.setProduct_img_url(cursor.getString(cursor.getColumnIndex(DatabaseHelper.QUANTITY)));
				productObj.setProduct_msg(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MESSAGE)));
				productObj.setProduct_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_NAME)));
				productObj.setProduct_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.BASE_AMOUNT)));
				productObj.setProduct_quantity(cursor.getString(cursor.getColumnIndex(DatabaseHelper.QUANTITY)));
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		Config.Log("DATA ","DATA " + data);
		return data;
	}
	
	public ArrayList<SubCategorModel> getSubCatData(String tableName,String columnName)
	{
		LogFile.LogData("getSubCatData");
		ArrayList<SubCategorModel> data=new ArrayList<SubCategorModel>();
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+tableName+" "+columnName;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				SubCategorModel productObj = new SubCategorModel();
				
				productObj.setMain_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MAIN_CAT_ID)));
				productObj.setSub_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ID)));
				productObj.setSub_cat_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SUB_CATEGORY_NAME)));
				
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0;i<data.size();i++){
			Config.Log("DATA ","DATA " + data.get(i).getSub_cat_name());
		}
		
		return data;
	}
	
	public ArrayList<ProductModel> getProducts(String msWhereCluase){
		ArrayList<ProductModel> data=new ArrayList<ProductModel>();
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+DatabaseHelper.TABLE_PRODUCTS +" " +msWhereCluase;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ProductModel productObj = new ProductModel();
				
				/*productObj.setMain_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MAIN_CAT_ID)));
				productObj.setSub_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ID)));
				productObj.setSub_cat_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SUB_CATEGORY_NAME)));*/
				
				productObj.setMaincat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_MAINCAT_ID)));
				productObj.setSubcat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SUBCAT_ID)));
				productObj.setCode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_CODE)));
				productObj.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_DESCRIPTION)));
				productObj.setEggless_type(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_EGGLESS_TYPE)));
				productObj.setFree_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_FREE_DELIVERY)));
				productObj.setId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_ID)));
				productObj.setImage(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_IMAGE)));
				productObj.setList_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_LIST_PRCE)));
				productObj.setNextday_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_NEXTDAY_DELIVERY)));
				productObj.setNormal_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_NORMAL_DELIVERY)));
				productObj.setOur_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_OUR_PRICE)));
				productObj.setSameday_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SAMEDAY_DELIVERY)));
				productObj.setSpecial_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SPECIAL_PRICE)));
				productObj.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_TITLE)));
				productObj.setWeight_grams(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_WEIGHT_GRAMS)));
				productObj.setWithegg_type(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_WITHEGG_TYPE)));
				//added by salman for cat_id
				productObj.setCat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_CAT_ID)));
				
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0;i<data.size();i++){
			Config.Log("DATA ","DATA " + data.get(i).getTitle());
		}
		
		return data;
	}
	
	
	//added for Searched Product
	public ArrayList<ProductModel> getSearchedProducts(){
		ArrayList<ProductModel> data=new ArrayList<ProductModel>();
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+DatabaseHelper.TABLE_SEARCHED_PRODUCT_DETAILS ;
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				ProductModel productObj = new ProductModel();
				
				/*productObj.setMain_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MAIN_CAT_ID)));
				productObj.setSub_cat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ID)));
				productObj.setSub_cat_name(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SUB_CATEGORY_NAME)));*/
				
				
				productObj.setSubcat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SUBCAT_ID)));
				productObj.setCode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_CODE)));
				productObj.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_DESCRIPTION)));
				productObj.setEggless_type(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_EGGLESS_TYPE)));
				productObj.setFree_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_FREE_DELIVERY)));
				productObj.setId(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_ID)));
				productObj.setImage(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_IMAGE)));
				productObj.setList_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_LIST_PRCE)));
				productObj.setNextday_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_NEXTDAY_DELIVERY)));
				productObj.setNormal_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_NORMAL_DELIVERY)));
				productObj.setOur_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_OUR_PRICE)));
				productObj.setSameday_delivery(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SAMEDAY_DELIVERY)));
				productObj.setSpecial_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_SPECIAL_PRICE)));
				productObj.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_TITLE)));
				productObj.setWeight_grams(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_WEIGHT_GRAMS)));
				productObj.setWithegg_type(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_WITHEGG_TYPE)));
				productObj.setCat_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PROD_CAT_ID)));
				
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0;i<data.size();i++){
			Config.Log("DATA ","DATA " + data.get(i).getTitle());
		}
		
		return data;
	}
	
	public ArrayList<ShoppingCartModel> getCartProducts(String where){
		LogFile.LogData("getCartProducts");
		ArrayList<ShoppingCartModel> data=new ArrayList<ShoppingCartModel>();
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query = "";
			if(where!=null && !where.equalsIgnoreCase("")){
				//query="SELECT * FROM "+DatabaseHelper.TABLE_SHOPPING_CART;
				query="SELECT * FROM "+DatabaseHelper.TABLE_SHOPPING_CART + " "+where;
			}else{
				query="SELECT * FROM "+DatabaseHelper.TABLE_SHOPPING_CART;
			}
			LogFile.LogData(query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			
				ShoppingCartModel productObj = new ShoppingCartModel();
			
				productObj.setImage(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_IMG_URL)));
				productObj.setProduct_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_ID)));
				productObj.setQuantity(cursor.getString(cursor.getColumnIndex(DatabaseHelper.QUANTITY)));
				productObj.setSpecial_price(cursor.getString(cursor.getColumnIndex(DatabaseHelper.BASE_AMOUNT)));
				productObj.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_NAME)));
				productObj.setDelivery_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_DELIVERY_DATE)));
				productObj.setDelivery_mode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_DELIVERY_MODE)));
				productObj.setProduct_type(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_TYPE)));
				productObj.setMessage(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MESSAGE)));
				
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0;i<data.size();i++){
			Config.Log("DATA ","DATA " + data.get(i).getTitle());
		}
		
		return data;
	}

	public long getRowCount(String tableName){
		String rawQuery="SELECT COUNT("+DatabaseHelper.AUTO_ID+") AS ROW_COUNT FROM "+tableName;
		Config.Log("ROW COUNT","ROW COUNT QUERY "+ rawQuery);
		long rowcount=0;
		try{

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		finally{

		}
		Config.Log("ROW COUNT","ROW COUNT "+ rowcount);
		return rowcount;

	}
	
	public long getRowCount(String tableName, String msWhereCluase){
		String rawQuery="SELECT COUNT("+DatabaseHelper.AUTO_ID+") AS ROW_COUNT FROM "+tableName+ " "+msWhereCluase;
		Config.Log("ROW COUNT","ROW COUNT QUERY "+ rawQuery);
		long rowcount=0;
		try {

			Cursor c=mDatabase.rawQuery(rawQuery, null);
			c.moveToFirst();
			rowcount=c.getLong(c.getColumnIndex("ROW_COUNT"));
			c.close();

		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		Config.Log("ROW COUNT","ROW COUNT "+ rowcount);
		return rowcount;

	}

	public long updateOrderTable(ContentValues value,String msWhere){
		//String rawQuery="UPDATE "+DatabaseHelper.TABLE_ORDERS+" SET "+value;
		Config.Log("Update","Update Order Table ");
		long rowcount=0;
		try{
			rowcount = mDatabase.update(DatabaseHelper.TABLE_ORDERS, value, msWhere, null);
		}catch(Exception ex){ 
			ex.printStackTrace();
		}
		finally{

		}
		Config.Log("Update Count","Update COUNT "+ rowcount);
		return rowcount;
	}
	
	
	public long updateTable(ContentValues value,String msTable,String msWhere){
		//String rawQuery="UPDATE "+DatabaseHelper.TABLE_ORDERS+" SET "+value;
		Config.Log("Update","Update Table "+msTable);
		long rowcount=0;
		try{
			rowcount = mDatabase.update(msTable, value, msWhere, null);
		}catch(Exception ex){ 
			ex.printStackTrace();
		}
		finally{

		}
		Config.Log("Update Count","Update COUNT "+ rowcount);
		return rowcount;
	}
	
	
	public ArrayList<OrderModel> getOrderInfo(String where){
		LogFile.LogData("getOrderInfo db");
		ArrayList<OrderModel> data=new ArrayList<OrderModel>();
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query = "";
			if(where!=null && !where.equalsIgnoreCase("")){
				//query="SELECT * FROM "+DatabaseHelper.TABLE_SHOPPING_CART;
				//query="SELECT * FROM "+DatabaseHelper.TABLE_SHOPPING_CART + " "+where;
				query="SELECT * FROM "+DatabaseHelper.TABLE_ORDERS + " " + where;
			}else{
				query="SELECT * FROM "+DatabaseHelper.TABLE_ORDERS;
			}
			LogFile.LogData("QUESRY " + query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			
				OrderModel productObj = new OrderModel();
				productObj.setCoupon_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COUPON_ID)));
				productObj.setDelivery_date(cursor.getString(cursor.getColumnIndex(DatabaseHelper.DELIVERY_DATE)));
				productObj.setDelivery_mode(cursor.getString(cursor.getColumnIndex(DatabaseHelper.DELIVERY_MODE)));
				productObj.setMessage_billing(cursor.getString(cursor.getColumnIndex(DatabaseHelper.MESSAGE_BILLING)));
				productObj.setOrder_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ORDER_ID)));
				productObj.setOrder_note(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ORDER_NOTE)));
				productObj.setPoints_redeem(cursor.getString(cursor.getColumnIndex(DatabaseHelper.POINTS_REDEEM)));
				productObj.setProduct_amount(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PRODUCT_AMOUNT)));
				productObj.setShipping_charge(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SHIPPING_CHARGE)));
				productObj.setUser_id(cursor.getString(cursor.getColumnIndex(DatabaseHelper.USER_ID)));
				productObj.setTotalAmount(cursor.getString(cursor.getColumnIndex(DatabaseHelper.TOTAL_AMOUNT)));
				productObj.setDiscount_amount(cursor.getString(cursor.getColumnIndex(DatabaseHelper.DISCOUNT_AMOUNT)));
				
				data.add(productObj);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		for(int i=0;i<data.size();i++){
			Config.Log("DATA ","DATA " + data.get(i).getDelivery_date());
		}
		
		return data;
	}

	public String getOrderDate(String msWhere){
		String msDate = "";
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+DatabaseHelper.TABLE_ORDERS ;
		
			if(msWhere!=null || !msWhere.equalsIgnoreCase("")){
				query = query +msWhere;
				
			}
			Config.Log("QUERYY ","QUERYY " + query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				
				msDate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DELIVERY_DATE));
				Config.Log("QUERYY ","DATE " + "DATE=" + msDate);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return msDate;
	}
	
	
	public String getValueFromColumn(String msWhere,String msCoulmnValue){
		String msDate = "";
		
		try
		{
			if(mDatabase==null || !mDatabase.isOpen()){
				openDatabase();
			}
			String query="SELECT * FROM "+DatabaseHelper.TABLE_ORDERS ;
		
			if(msWhere!=null || !msWhere.equalsIgnoreCase("")){
				query = query +msWhere;
				
			}
			Config.Log("QUERYY ","QUERYY " + query);
			Cursor  cursor = mDatabase.rawQuery(query,null);
			for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
				msDate = cursor.getString(cursor.getColumnIndex(msCoulmnValue));
				Config.Log("QUERYY ","DATE " + "DATE=" + msDate);
			}
			cursor.close();
		}catch(SQLException sqx){
			sqx.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return msDate;
	}


}
