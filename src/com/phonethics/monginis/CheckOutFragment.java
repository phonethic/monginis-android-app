package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CartListAdapter;
import com.phonethics.model.OrderModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CheckOutFragment extends Fragment {


	private View mView;
	private SessionManager mSessionManager;
	private ProgDialog pDialog;
	protected JSONObject orderInfoJson;
	private String msUserId;
	private HashMap<String,String> userDetails = new HashMap<String, String>();
	private String msOrderId;	
	private boolean mbDebug = true;
	private JSONArray jArr;
	private ProgressDialog mProgressDialog;
	private TextView mtvTryAddons;
	private final String msSameDayLocal = "sameday_delivery";
	private final String msNextDayLocal = "nextday_delivery";
	private final String msNormalDeliveryLocal = "normal_delivery";
	private final String msVirtualDeliveryLocal = "free_delivery";
	private final String msWithEgg = "withegg_type";
	private final String msEggLess = "eggless_type"; 
	
	String del_date = "";
	String del_mode = "";
	private final String msNormalDelivery = "NORMAL DELIVERY (48 HRS.)";
	private final String msNextDayDelivery = "Next Day";
	private final String msSameDay = "Same Day";
	private final String msVirtualDelivery = "Virtual Delivery";

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		
		
		/**
		 * initialize the class variables
		 * 
		 * 
		 */
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");

		pDialog = new ProgDialog(getActivity());
		mSessionManager = new SessionManager(getActivity());
		if(mSessionManager.isLoggedInCustomer()){
			userDetails = mSessionManager.getUserDetails();
			msUserId = userDetails.get(SessionManager.USER_ID);
		}else{
			msUserId = "0";
		}

		
		fetchData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (View)inflater.inflate(R.layout.fragment_checkout, null);

		TextView mtvCheckOut = (TextView) mView.findViewById(R.id.txtCheckOut);
		TextView mtvContinueShopping = (TextView) mView.findViewById(R.id.text_continueshopping);
		mtvTryAddons = (TextView) mView.findViewById(R.id.text_tryaddons);
		
		
		mtvCheckOut.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvContinueShopping.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvTryAddons.setTypeface(MonginisApplicationClass.getTypeFace());
		final Map<String, String> params = new HashMap<String, String>();

		mtvCheckOut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//getOrderDetails();
				params.put("Action", "CheckOut");
				EventTracker.logEvent(EventsName.CHECKOUT_SCREEN,params);
				
				/**
				 * 
				 */
				getShoppingCartData();
				//callNextFragment();
				
			}
		});

		mtvContinueShopping.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				params.put("Action", "ConitnueShopping");
				EventTracker.logEvent(EventsName.CHECKOUT_SCREEN,params);
				Intent intent = new Intent(getActivity(), LandingScreen.class);
				startActivity(intent);
				getActivity().overridePendingTransition(0, 0);
				getActivity().finish();
			}

		});

		mtvTryAddons.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				params.put("Action", "TryAddons");
				EventTracker.logEvent(EventsName.TRY_ADDONS);
				EventTracker.logEvent(EventsName.CHECKOUT_SCREEN,params);
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);


				TryAddOnsFragment chkFrag =new TryAddOnsFragment();
				transaction.add(R.id.content_frame, chkFrag,"ContinueAs");
				transaction.addToBackStack("ContinueAs");
				transaction.commit();

			}

		});
		return mView;
	}

	
	
	/**
	 * Call next fragment
	 * 
	 */
	private void callNextFragment(){

		/*FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		//To remove all Stack behind
		//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		pDialog.dismissDialog();
		ShippingAddressFragment shippingAdd =new ShippingAddressFragment();
		transaction.add(R.id.content_frame, shippingAdd,"CheckOut");
		transaction.addToBackStack(null);
		transaction.commit();*/
		
		
		
		
		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		//To remove all Stack behind
		//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		pDialog.dismissDialog();
		BillingAddressFragment billngAdd =new BillingAddressFragment();
		transaction.add(R.id.content_frame, billngAdd,"CheckOut");
		transaction.addToBackStack(null);
		transaction.commit();
	}

	
	
	/**
	 * Db query to get order details 
	 * 
	 */
	private void getOrderDetails(){
		//ProgDialog.showProgressDialog(getActivity());

		String msWhere = " WHERE "+DatabaseHelper.USER_ID+" ='"+msUserId+"'";
		//pDialog.showProgressDialog();
		LogFile.LogData("getOrderDetails");
		
		new AsyncDatabaseObjectQuery<OrderModel>(getActivity(),new OnCompleteListner<OrderModel>() {

			@Override
			public void onComplete(ArrayList<OrderModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				//pDialog.dismissDialog();
				for(int i =0;i<response.size();i++){
					LogFile.LogData("=======================================================");
					LogFile.LogData("Coupon id "+response.get(i).getCoupon_id());
					LogFile.LogData("Delivery Date TABLE_ORDERS "+response.get(i).getDelivery_date());
					LogFile.LogData("Delievry Mode TABLE_ORDERS "+response.get(i).getDelivery_mode());
					LogFile.LogData("Message Billing "+response.get(i).getMessage_billing());
					LogFile.LogData("Order Note "+response.get(i).getOrder_note());
					LogFile.LogData("Order id "+response.get(i).getOrder_id());
					LogFile.LogData("Coupon id "+response.get(i).getCoupon_id());
					LogFile.LogData("Discount Amount "+response.get(i).getDiscount_amount());
					LogFile.LogData("Points Redeem "+response.get(i).getPoints_redeem());
					LogFile.LogData("Product Amount "+response.get(i).getProduct_amount());
					LogFile.LogData("Shipping charge "+response.get(i).getShipping_charge());
					LogFile.LogData("Total Amount "+response.get(i).getTotalAmount());
					LogFile.LogData("User id "+response.get(i).getUser_id());

					//JSONObject orderInfoJson = new JSONObject();
					try{

						msOrderId = response.get(i).getOrder_id();
						orderInfoJson = new JSONObject();
						orderInfoJson.put("coupon_id", response.get(i).getCoupon_id());
//						orderInfoJson.put("delivery_date", response.get(i).getDelivery_date());
//						orderInfoJson.put("delivery_mode", response.get(i).getDelivery_mode());
						
						orderInfoJson.put("delivery_date", del_date);
						if(del_mode.equalsIgnoreCase(msNormalDeliveryLocal))
							orderInfoJson.put("delivery_mode", msNormalDelivery);
						else if(del_mode.equalsIgnoreCase(msNextDayLocal))
							orderInfoJson.put("delivery_mode",msNextDayDelivery);
						else if(del_mode.equalsIgnoreCase(msSameDayLocal))
							orderInfoJson.put("delivery_mode",msSameDay);
						else if(del_mode.equalsIgnoreCase(msVirtualDeliveryLocal))
							orderInfoJson.put("delivery_mode",msVirtualDelivery);
						
						orderInfoJson.put("message_billing", response.get(i).getMessage_billing());
						orderInfoJson.put("order_note", response.get(i).getOrder_note());
						if(response.get(i).getOrder_id().equalsIgnoreCase("")){
							orderInfoJson.put("order_id", "0");
						} else {
							orderInfoJson.put("order_id", response.get(i).getOrder_id());
						}
						orderInfoJson.put("points_redeemed", response.get(i).getPoints_redeem());
						orderInfoJson.put("product_amount", response.get(i).getProduct_amount());
						orderInfoJson.put("shipping_charge", response.get(i).getShipping_charge());
						orderInfoJson.put("total_amount", response.get(i).getTotalAmount());
						orderInfoJson.put("discount_amount", response.get(i).getDiscount_amount());
						orderInfoJson.put("user_id", response.get(i).getUser_id());
						orderInfoJson.put("payment_mode", "ccavenue");
						orderInfoJson.put("products", jArr);
						
						//if(mbDebug){
						//	mbDebug = false;
						
						//}
						//showToast("DB_ID " + response.get(i).getOrder_id());

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
				
				checkOut();	


			}
		},DatabaseHelper.TABLE_ORDERS,Config.RETRIVE_ORDER_INFO,msWhere,Config.RETRIVE_ORDER_INFO).execute();

	}


	
	/**
	 * Network request to send order details on server
	 * 
	 */
	private void checkOut(){
		try{
			LogFile.LogData("checkOut()");
			final String TAG="CHECK_OUT";
			String url=Config.CHECK_OUT;
			LogFile.LogData("URL "+url);
			LogFile.LogData("orderInfoJson--"+orderInfoJson.toString());
			//pDialog.showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, orderInfoJson, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						pDialog.dismissDialog();
						Config.Log(TAG, response.toString());
						String status=response.getString("success");

						if(response!=null){
							boolean mbIsOrdeId = false;
							if(status.equalsIgnoreCase("true")){
								JSONObject dataObj=response.getJSONObject("data");
								String orderNumber = "";
								int orderId = 0;
								try{
									orderId = dataObj.getInt("order_id");
									orderNumber = dataObj.getString("order_number");
									//Config.ORDER_ID = orderId+"";
									Config.ORDER_NUMBER = orderNumber;
									mbIsOrdeId = true;
									LogFile.LogData(mbIsOrdeId+"");
									//showToast("Server_ID " + orderId +  " " + orderNumber);
								}catch(JSONException jex){
									jex.printStackTrace();
									orderId = 0;
									mbIsOrdeId = false;
									LogFile.LogData(mbIsOrdeId+"");
								}

								if(mbIsOrdeId){
									String msOrderId = orderId+"";
									updateOrdeInfo(msOrderId);
								}else{
									
									updateOrdeInfo("-1");
								}	


							} else {
								showToast(getResources().getString(R.string.Parse));
								
							}
						}	
					}catch(Exception ex){
						ex.printStackTrace();
						pDialog.dismissDialog();
					}
					
					

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					pDialog.dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	void showToast(String tag) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+tag, Toast.LENGTH_SHORT).show();
	}
	
	
	
	/**
	 * Db query to fecth shopping cart details of the loggedIn or guest user
	 */
	private void getShoppingCartData(){
		
		jArr = new JSONArray();
		try{
			pDialog.showProgressDialog();
			LogFile.LogData("fetchData");	
			String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='"+msUserId+"' ";
			new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {

				@Override
				public void onComplete(ArrayList<ShoppingCartModel> response,
						String msTag) {
					// TODO Auto-generated method stub
					//pDialog.dismissDialog();
					if(response!=null){
						for(int i=0;i<response.size();i++){
							JSONObject jObj = new JSONObject();
							
							try {
								
								jObj.put("product_id", response.get(i).getProduct_id().toString());
								jObj.put("quantity", response.get(i).getQuantity().toString());
								jObj.put("amount", response.get(i).getSpecial_price().toString());
								jObj.put("message", response.get(i).getMessage().toString());
								
								String msDeliveryMode =  response.get(i).getDelivery_mode();
								
								if(msDeliveryMode.equalsIgnoreCase(msNormalDeliveryLocal)){
									jObj.put("sameday_delivery", "N");
									jObj.put("nextday_delivery", "N");
									jObj.put("normal_delivery", "Y");
									jObj.put("free_delivery", "N");
								} else if(msDeliveryMode.equalsIgnoreCase(msNextDayLocal)){
									jObj.put("sameday_delivery", "N");
									jObj.put("nextday_delivery", "Y");
									jObj.put("normal_delivery", "N");
									jObj.put("free_delivery", "N");
								} else if(msDeliveryMode.equalsIgnoreCase(msSameDayLocal)){
									jObj.put("sameday_delivery", "Y");
									jObj.put("nextday_delivery", "N");
									jObj.put("normal_delivery", "N");
									jObj.put("free_delivery", "N");
								} else if(msDeliveryMode.equalsIgnoreCase(msVirtualDeliveryLocal)){
									jObj.put("sameday_delivery", "N");
									jObj.put("nextday_delivery", "N");
									jObj.put("normal_delivery", "N");
									jObj.put("free_delivery", "Y");
								} else {
									jObj.put("sameday_delivery", "N");
									jObj.put("nextday_delivery", "N");
									jObj.put("normal_delivery", "N");
									jObj.put("free_delivery", "N");
								}
								
								String msProductType =  response.get(i).getProduct_type();
								if(msProductType.equalsIgnoreCase(msWithEgg)){
									jObj.put("withegg_type", "Y");
									jObj.put("eggless_type", "N");
								}else{
									jObj.put("withegg_type", "N");
									jObj.put("eggless_type", "Y");
								}
								
								jArr.put(jObj);
								LogFile.LogData("=========================");
								LogFile.LogData(jObj.toString());
								LogFile.LogData("Delivery Date TABLE_SHOPPING_CART "+response.get(i).getDelivery_date());
								LogFile.LogData("Delievry Mode TABLE_SHOPPING_CART "+response.get(i).getDelivery_mode());
								
								del_date = response.get(i).getDelivery_date();
								del_mode = response.get(i).getDelivery_mode();
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						
						
						getOrderDetails();
						
					} else {
						showToast(getResources().getString(R.string.Parse));
					}
					
				}
			},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	} 

	
	/**
	 * Db query to update order details with order id
	 * 
	 * @param msOrderId
	 */
	private void updateOrdeInfo(String msOrderId){
		
		LogFile.LogData("updateOrdeInfo");
		String msWhere = DatabaseHelper.USER_ID+"='"+msUserId+"'" ;
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.ORDER_ID, msOrderId);
		try{
			pDialog.showProgressDialog();

			if(msOrderId.equalsIgnoreCase("-1")){
				pDialog.dismissDialog();
				callNextFragment();
				//showToast("No update");
			}else{
				new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {
					@Override
					public void onComplete(String response, String msTag) {
						// TODO Auto-generated method stub
						LogFile.LogData("onComplete");
						//showToast("update " + response);
						pDialog.dismissDialog();
						if(response!=null){
							callNextFragment();
						}

					}
				}, DatabaseHelper.TABLE_ORDERS, Config.UPDATE_ORDER_TABLE, msWhere, "updateorder",value).execute();
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	//added by Salman to fetch data from the cart
	
	
	/**
	 	Db query to fetch shoping cart details of the logged in or guest user. 
	 */
	void fetchData(){

		showProgressDialog();
		LogFile.LogData("fetchData");	
		String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='"+msUserId+"' ";
		new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {

			@Override
			public void onComplete(ArrayList<ShoppingCartModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				
				for(int i=0;i<response.size();i++){
					Config.Log("Got the data", "Got the data " + response.get(i).getDelivery_mode());
					if(response.get(i).getDelivery_mode().equalsIgnoreCase("free_delivery")){
						mtvTryAddons.setVisibility(View.GONE);
					}
				}
				dismissDialog();
				
			}
		},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();


	}
	
	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	
}
