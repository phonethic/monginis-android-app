package com.phonethics.monginis;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;





/**
 * First screen on the mobile app.
 * Shows the app logo with background image
 * App logo remains for some time and gets fadeout after certain time limit, and start the next activity as the animation gets end
 * 
 */
public class SplashSplahScreen extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;
	Context mContext;
	ImageView imgMonginisLogo;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_splah_screen);
		mContext = this;
		
		
		/**
		 * Initialize  the layout and class variables
		 * 
		 * 
		 */
		imgMonginisLogo = (ImageView)findViewById(R.id.imgMonginisLogo);
		Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
		imgMonginisLogo.startAnimation(anim);
		
		
		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.phonethics.monginis",PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("MY KEY HASH:", "MY KEY HASH:" +Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
		
		anim.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				
				
				
				Intent intent = new Intent(mContext, LandingScreen.class);
				intent.putExtra("tag","true");
				startActivity(intent);
				finish();
			}
		});
//		new Handler().postDelayed(new Runnable() {
//
//			/*
//			 * Showing splash screen with a timer. This will be useful when you
//			 * want to show case your app logo / company
//			 */
//
//			@Override
//			public void run() {
//				Intent intent = new Intent(mContext, LandingScreen.class);
//				intent.putExtra("tag","true");
//				startActivity(intent);
//				finish();
//			}
//		}, SPLASH_TIME_OUT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_splah_screen, menu);
		return true;
	}

}
