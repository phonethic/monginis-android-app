package com.phonethics.monginis;

import java.util.ArrayList;

import android.graphics.Typeface;
import android.view.ViewGroup;

public class Config {


	public static final String HEADER_KEY="X-API-KEY";
	public static final String HEADER_VALUE="mngphntcs";
	public static final String DEVICE_ID_KEY="mngphntcs";
	public static final String DEVICE_ID="b4b39be60089467ab239d5e3e85b03a2e34972f4";

	public static final String INSERT_DATA="INSERT";
	public static final String UPDATE_DATA="UPDATE";
	public static final String UPDATE_ORDER_TABLE="UPDATE_ORDER_TABLE";
	public static final String UPDATE_SHOPPING_TABLE="UPDATE_SHOPPING_TABLE";
	public static final String DELETE_DATA="DELETE";
	public static final String RETRIVE_DATA="RETRIVE";
	public static final String RETRIVE_DATA_WHERE="RETRIVE_WHERE";
	public static final String RETRIVE_SUB_CAT="RETRIVE_SUB_CAT";
	public static final String ROW_COUNT="ROW_COUNT";
	public static final String ROW_COUNT_WHERE="ROW_COUNT_WHERE";
	public static final String RETRIVE_CART_DATA="RETRIVE_CART_DATA";
	public static final String RETRIVE_SEARCHED_PRODUCT_DATA="RETRIVE_SEARCHED_PRODUCT_DATA";
	public static final String RETRIVE_ORDER_INFO="RETRIVE_ORDER_INFO";
	public static final String GET_ORDER_DATE="get_order_date";
	public static final String GET_VALUE="get_value";

	public static final String DELETE_MAINTABLE_DATA = "delete_maintable_data";
	public static final String DELETE_SUBCATTABLE_DATA = "delete_subcattable_data";
	public static final String  DELETE_PRODUCTTABLE_DATA = "delete_producttable_data";

	//added by me
	public static final String INSERT_SINGLE_DATA="INSERT_SINGLE";
	public static final String RETRIVE_PRODUCT="RETRIVE_PRODUCT";

	public static String MAIN_SELECTED_CATEGORY_ID="";
	public static String SUB_CATEGORY_SELECTED_PRODUCT_ID="";
	public static String SUB_CAT_ID="";


	public static final String OMENS_REGULAR="fonts/Omnes.ttf";

	public static final String OMENS_LIGHT="fonts/Omnes Light.ttf";

	public static final String OMENS_MEDIUM="fonts/Omnes Medium.ttf";

	public static final String CURSIVE="fonts/Cursive.ttf";

	public static final String IMAGE_BASE_URL="http://www.monginis.net/images/products/";

	public static final String LIVE_URL = "http://monginis.net/api-new/api-1/";

	public static final String STAGE_URL = "http://stage.phonethics.in/monginis/api-1/";

	public static final String SERVER_URL = LIVE_URL;

	//	http://monginis.net/api-new/api-1/

	//	http://stage.phonethics.in/monginis/api-1/


	public static final String RESPONSE_URL="http://monginis.net/api-new/paymentresponse";

	//public static final String RESPONSE_URL="http://stage.phonethics.in/monginis/paymentresponse";

	public static final String GET_ALL_CATEGORIES= SERVER_URL +"category_api/allcategories";

	public static final String GET_ALL_SUB_CATEGORIES=SERVER_URL +"category_api/subcategories/category_id/";

	public static final String GET_ALL_PRODUCTS=SERVER_URL +"product_api/productofsubcategory/category_id/";

	public static final String GET_PARTICULAR_PRODUCT=SERVER_URL +"product_api/productdetails/product_id/";

	public static final String GET_CITIES_BY_CATEGORY=SERVER_URL +"service_api/citylist/category_id/";

	public static final String GET_PRODUCT_ATTRIBUTES=SERVER_URL +"product_api/productattribute/product_id/";

	//public static final String CHECK_AVAILABILITY_BY_CATEGORY=SERVER_URL +"service_api/checkservice/category_id/";

	public static final String CHECK_AVAILABILITY_BY_CATEGORY=SERVER_URL +"service_api/checkservice";

	public static final String GET_CITY_LIST_FOR_STORELOCATOR=SERVER_URL +"service_api/citylocator";

	public static final String GET_AREA_LIST_FOR_SELECTEDCITY_STORELOCATOR=SERVER_URL +"service_api/arealocator/city_id/";

	public static final String GET_CITY_HEADQUARTER_STORELOCATOR=SERVER_URL +"service_api/storelocator/city_id/";

	public static final String USER_REGISTRATION=SERVER_URL +"/user_api/registeruser";

	public static final String USER_LOGIN=SERVER_URL +"/user_api/loginuser";

	public static final String USER_PROFILE_UPDATE=SERVER_URL +"/user_api/updateprofile";

	public static final String GET_USER_PROFILE= SERVER_URL +"/user_api/updateprofile/user_id/";

	public static final String TRY_ADD_ONS= SERVER_URL +"product_api/addons";

	public static final String ADD_TO_CART= SERVER_URL +"cart_api/usercart";

	public static final String GET_CART_PRODUCTS=SERVER_URL +"cart_api/usercart?user_id=";


	public static final String DELETE_SELECTED_CART_PRODUCTS=SERVER_URL +"cart_api/usercart?user_id=";


	public static final String ADD_PRODUCT_RATING=SERVER_URL +"product_api/ratings";

	public static final String GET_PRODUCT_RATING=SERVER_URL +"product_api/ratings/product_id/";

	public static final String SEARCH_PRODUCTS_WITHOUT_CITY=SERVER_URL +"product_api/searchproduct/offset/";

	public static final String GET_MAINCAT_FROM_SUBCAT=SERVER_URL +"category_api/categoryfromsubcategory/subcategory_id/";

	public static final String POST_APPLY_FORM=SERVER_URL +"service_api/applyfor";

	public static final String GET_COUNTIRES=SERVER_URL +"service_api/countrylocator/";

	public static final String GET_STATES=SERVER_URL +"service_api/statelocator/country_id/";

	public static final String GET_CITY=SERVER_URL +"service_api/citylocator/state_id/";

	public static final String CHECK_OUT=SERVER_URL +"cart_api/checkout";

	public static final String SHIPPING=SERVER_URL +"user_api/shippingaddress";

	public static final String BILLING=SERVER_URL +"user_api/billingaddress";


	public static final String REDEEM_POINTS= SERVER_URL +"user_api/redeempoints/user_id";

	public static final String REDEEM_COUPAN= SERVER_URL +"service_api/redeemcoupon/coupon_code/";

	public static final String CONFIRMRESPONSE_URL="http://www.monginis.net/api-new/confirmresponse?";

	public static final String STATE_BY_CITY_NAME=SERVER_URL +"service_api/statename/city/";

	public static final String WORKING_KEY="wvhynco3t3abwhpakaw9v9g0rju32t80";
	public static final String MERCHANT_ID="M_mongcc_5128";
	public static String TOTAL_AMOUNT="";
	public static String ORDER_ID="";
	public static String APP_ID="";

	//added to take Order_Number
	public static String ORDER_NUMBER = "";

	// WebView Links for Monginis Connect
	public static final String PHONETHICS_URL = "http://phonethics.in/";
	public static final String FACEBOOK_URL="https://www.facebook.com/monginis";
	public static final String TWITTER_URL="https://twitter.com/MonginisBrand";
	public static final String GPLUS_URL="https://plus.google.com/u/0/+monginis/posts";
	public static final String PINTEREST_URL="http://www.pinterest.com/monginis/pins/";
	public static final String LINKEDIN="http://www.linkedin.com/company/monginis-foods-pvt-ltd-?trk=top_nav_home";

	public static final String MEDIA_ROOM="http://www.monginis.net/media-app.php/";

	public static final String UPDATE_ORDER_STATUS = SERVER_URL + "cart_api/orderstatus";

	//User Specific Data
	public static String USER_ID = "";

	//Predefined Quantity List
	public static ArrayList<String> list = new ArrayList<String>() {{
		add("1");
		add("2");
		add("3");
		add("4");
		add("5");
		add("6");
		add("7");
		add("8");
		add("9");
		add("10");
	}};


	public static void Log(String TAG,String msg){
		//android.util.Log.d(TAG,TAG+" "+msg);
	}


	public static String GARND_TOTAL = "";
	public static String DELIVERY_DATE_SERVER = "";
	public static String DELIVERY_MODE = "";


	//for remember sorting items
	public static  String SAME_DAY = "false"; 
	public static  String NEXT_DAY = "fasle";
	public static  String NORMAL = "false";
	public static  String FREE = "false";

	public static  String FOUR_HUNDRED = "false";
	public static  String EIGHT_HUNDRED = "false";
	public static  String TWELVE_HUNDRED = "false";
	public static  String ABOVE_TWELVE_HUNDRED = "false";

	public static  String EGGLESS = "false";
	public static  String WITHEGG = "false";
	public static  String BOTH = "false";

	public static  String ABOVE_ONEKG = "false";
	public static  String ONEKG = "false";
	public static  String HALFKG = "false";

	//public static String SAVED_DATE = "-1";
	public static int SHIPPING_PRICE = 0;
}
