package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.phonethics.adapters.MenuOptionAdapter;
import com.phonethics.model.CartProductInfo;

public class MenuOptionFragment extends Fragment implements OnItemClickListener {

	private View mView;
	private ListView mList;
	ArrayList<String> mOptionList = new ArrayList<String>();
	private MenuOptionAdapter mAdapter;
	SessionManager mSessionManager;
	TextView mLogoutBtn;
	Typeface tf;
	TextView phonethicsLink;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mSessionManager = new SessionManager(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
		mView=(View)inflater.inflate(R.layout.fragment_menu_options, null);
		mList=(ListView)mView.findViewById(R.id.listOptions);
		mLogoutBtn=(TextView)mView.findViewById(R.id.mLogoutBtn);
		phonethicsLink=(TextView)mView.findViewById(R.id.phonethicsLink);
		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		mLogoutBtn.setTypeface(tf);
		phonethicsLink.setTypeface(tf);
		
		mOptionList.add("STORE LOCATOR");
		mOptionList.add("REGISTER/LOGIN");
		//		mOptionList.add("LOGOUT");
		mOptionList.add("PARTNER WITH US");
		mOptionList.add("MEDIA ROOM");
		mOptionList.add("SOCIAL CONNECT");
		mOptionList.add("ABOUT US");
		mOptionList.add("CONTACT US");
		
		
		if(mSessionManager.isLoggedInCustomer()){
			//mOptionList.add("LOGOUT");
			mOptionList.set(1, "MY PROFILE");
		}

		mAdapter=new MenuOptionAdapter(getActivity(), 0, 0, mOptionList);
		mList.setAdapter(mAdapter);
		mList.setOnItemClickListener(this);

		if(mSessionManager.isLoggedInCustomer()){
			mLogoutBtn.setVisibility(View.VISIBLE);
		}
		else{
			mLogoutBtn.setVisibility(View.GONE);
		}

		mLogoutBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Map<String, String> param = new HashMap<String, String>();
				param.put("Option_Selected", "Logout");
				EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
				logoutAlert();

			}
		});
		
		phonethicsLink.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(getActivity(),WebLinkView.class);
				intent.putExtra("position", 6);
				startActivity(intent);
			}
		});
		return mView;
	}

	void logoutAlert() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(getResources().getString(R.string.logout_confirmation_message))
		.setCancelable(true)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				deleteOrderInfoRecords();
				mSessionManager.logoutCustomer();
				mLogoutBtn.setVisibility(View.GONE);
				mOptionList.set(1,"REGISTER/LOGIN");
				mAdapter=new MenuOptionAdapter(getActivity(), 0, 0, mOptionList);
				mList.setAdapter(mAdapter);
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	void deleteOrderInfoRecords(){
		LogFile.LogData("deleteOrderInfoRecords");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"++"'";
		try{
			new AsyncDatabaseQuery(getActivity(),new DBListener() {

				@Override
				public void rowCount(long rowCount, String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
						String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteOperation(String tag) {
					// TODO Auto-generated method stub

				}
			}, DatabaseHelper.TABLE_ORDERS,
			"", Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

		if(position == 0){

			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "Store Locator");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			StoreLocatorFragment chkFrag=new StoreLocatorFragment();
			transaction.add(R.id.content_frame, chkFrag,"STORE LOCATOR");
			transaction.addToBackStack("STORE LOCATOR");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 1){

			if(mSessionManager.isLoggedInCustomer()){

				Map<String, String> param = new HashMap<String, String>();
				param.put("Option_Selected", "My Profile");
				EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
				//showToast("Call User Profile");
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				CustomerProfileFragment chkFrag=new CustomerProfileFragment();
				transaction.add(R.id.content_frame, chkFrag,"MY PROFILE");
				transaction.addToBackStack("MY PROFILE");
				// Commit the transaction
				transaction.commit();
			}
			else{

				Map<String, String> param = new HashMap<String, String>();
				param.put("Option_Selected", "Login/Register");
				EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
				
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				LoginSignUpFragment chkFrag=new LoginSignUpFragment();
				transaction.add(R.id.content_frame, chkFrag,"LOGIN SIGNUP");
				transaction.addToBackStack("LOGIN SIGNUP");
				// Commit the transaction 
				transaction.commit();
			}

		}
		else if(position == 2){

			EventTracker.logEvent(EventsName.PARTNER_WITH_US);
			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "Partner With Us");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			//showToast("Apply for");
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			ApplyForFormsFragment chkFrag=new ApplyForFormsFragment();
			transaction.add(R.id.content_frame, chkFrag,"MONGINIS CONNECT");
			transaction.addToBackStack("APPLY FOR");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 4){

			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "Social Connect");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			
			EventTracker.logEvent(EventsName.SOICIAL_CONNECT);
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			MonginisConnectFragment chkFrag=new MonginisConnectFragment();
			transaction.add(R.id.content_frame, chkFrag,"MONGINIS CONNECT");
			transaction.addToBackStack("MONGINIS CONNECT");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 3){
			
			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "Media Room");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			
			Intent intent = new Intent(getActivity(), WebLinkView.class);
			intent.putExtra("position", 5);
			startActivity(intent);
		}
		else if(position == 5){
			
			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "About Us");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			AboutUsFragment chkFrag=new AboutUsFragment();
			transaction.add(R.id.content_frame, chkFrag,"AboutUsFragment");
			transaction.addToBackStack("AboutUsFragment");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 6){
			
			Map<String, String> param = new HashMap<String, String>();
			param.put("Option_Selected", "Contact Us");
			EventTracker.logEvent(EventsName.MENU_OPTIONS, param);
			
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			ContactUsFragment chkFrag=new ContactUsFragment();
			transaction.add(R.id.content_frame, chkFrag,"ContactUsFragment");
			transaction.addToBackStack("ContactUsFragment");
			// Commit the transaction
			transaction.commit();
		}

	}

	private void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

}
