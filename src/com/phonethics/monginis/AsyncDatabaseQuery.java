package com.phonethics.monginis;

import java.util.ArrayList;

import com.phonethics.model.CartProductInfo;
import com.phonethics.model.SubCategorModel;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 
 * @author manoj
 * AsyncDatabaseQuery is used for Query Database in Background thread
 */
public class AsyncDatabaseQuery extends AsyncTask<Void, Void, Integer> {

	private String msTag="";
	private String msQuery="";
	private String msColumn="";
	private String msQueryType="";
	private Context mContext;
	private String msTable="";
	private String value="";

	private ContentValues [] contentValues;
	private DBListener mDbListener;
	private ArrayList<String>mArrData=new ArrayList<String>();
	private ArrayList<CartProductInfo>mProductList=new ArrayList<CartProductInfo>();
	
	private long mlRowCount;
	private ContentValues contentValue;
	
	
	
	/**
	 * 
	 * @param context : Context of Activity
	 * @param mDbListener : Database Listener
	 * @param contentValues : Values to be inserted in Database
	 * @param msTable : Table name to insert in Database.
	 * @param msQueryType : Type of Query Eg. INSERT,UPDATE,DELTE
	 * @param msTag : Tag of calling Api
	 */
	public AsyncDatabaseQuery(Context context,DBListener mDbListener,ContentValues [] contentValues,
			String msTable,String msQueryType,String msTag){
		mContext=context;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.contentValues=contentValues;
		this.mDbListener=mDbListener;
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}
	
	/**
	 * 
	 * @param context :  Context of Activity
	 * @param mDbListener : Database Listener
	 * @param msTable : Table name to insert in Database.
	 * @param msColumn : Column name to fetch data from
	 * @param msQueryType  : Type of Query Eg. INSERT,UPDATE,DELTE
	 * @param msTag : Tag of calling Api
	 */
	public AsyncDatabaseQuery(Context context,DBListener mDbListener,String msTable,String msColumn,String msQueryType,String msTag){
		mContext=context;
		this.msColumn=msColumn;
		this.value = msColumn;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.mDbListener=mDbListener;
		
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}

	
	//added for retrieval according to a particular value
	public AsyncDatabaseQuery(Context context,DBListener mDbListener,String msTable,String msColumn,String msQueryType,String msTag, String value){
		mContext=context;
		this.msColumn=msColumn;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.mDbListener=mDbListener;
		this.value=value;
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}
	
	//added for single content value
	public AsyncDatabaseQuery(Context context,DBListener mDbListener,ContentValues contentValues,
			String msTable,String msQueryType,String msTag){
		mContext=context;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.contentValue=contentValues;
		this.mDbListener=mDbListener;
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}

	@Override
	protected Integer doInBackground(Void... params) {
		
		LogFile.LogData("doInBackground");
		if(msQueryType==Config.INSERT_DATA){
			LogFile.LogData(Config.INSERT_DATA);
			DatabaseManager.getInstance().openDatabase();
			DatabaseManager.getInstance().replaceOrUpdate(msTable, contentValues, msTag);
			DatabaseManager.getInstance().closeDataBase();
		}
		if(msQueryType==Config.UPDATE_DATA){

		}
		if(msQueryType==Config.DELETE_DATA){
			LogFile.LogData("Table "+msTable + "Where "+value);
			DatabaseManager.getInstance().openDatabase();
			DatabaseManager.getInstance().clearRecords(msTable,value);
			DatabaseManager.getInstance().closeDataBase();
		}
		
		if(msQueryType==Config.ROW_COUNT_WHERE){
			LogFile.LogData(Config.ROW_COUNT_WHERE);
			DatabaseManager.getInstance().openDatabase();
			mlRowCount=DatabaseManager.getInstance().getRowCount(msTable,value);
			DatabaseManager.getInstance().closeDataBase();
		}
		if(msQueryType==Config.RETRIVE_DATA){
			LogFile.LogData(Config.RETRIVE_DATA);
			DatabaseManager.getInstance().openDatabase();
			mArrData=DatabaseManager.getInstance().getData(msTable, msColumn);
			DatabaseManager.getInstance().closeDataBase();
		}
		if(msQueryType==Config.ROW_COUNT){
			LogFile.LogData(Config.ROW_COUNT);
			DatabaseManager.getInstance().openDatabase();
			mlRowCount=DatabaseManager.getInstance().getRowCount(msTable);
			DatabaseManager.getInstance().closeDataBase();
		}
		
		//added by me for single entry
		if(msQueryType==Config.INSERT_SINGLE_DATA){
			LogFile.LogData(Config.INSERT_SINGLE_DATA);
			DatabaseManager.getInstance().openDatabase();
			DatabaseManager.getInstance().replaceOrUpdate(msTable, contentValue, msTag);
			DatabaseManager.getInstance().closeDataBase();
		}
		
		//for product retrieval
		if(msQueryType==Config.RETRIVE_PRODUCT){
			LogFile.LogData(Config.RETRIVE_PRODUCT);
			DatabaseManager.getInstance().openDatabase();
			mProductList=DatabaseManager.getInstance().getData(msTable, msColumn, value);
			DatabaseManager.getInstance().closeDataBase();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		LogFile.LogData("onPostExecute");
		if(msQueryType==Config.INSERT_DATA){
			mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.UPDATE_DATA){
			mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.DELETE_DATA){
			mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.RETRIVE_DATA){
			mDbListener.onCompleteRetrival(mArrData,msTag);
		}
		if(msQueryType==Config.ROW_COUNT){
			mDbListener.rowCount(mlRowCount,msTag);
		}
		if(msQueryType==Config.INSERT_SINGLE_DATA){
			mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.RETRIVE_PRODUCT){
			mDbListener.onCompleteRetrivalObject(mProductList, msTag);
		}
		if(msQueryType==Config.ROW_COUNT_WHERE){
			mDbListener.rowCount(mlRowCount,msTag);
		}
	}
	
	

}
