package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
//import com.phonethics.adapters.CartListAdapter;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
//import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class RedeemFragment extends Fragment {


	private View mView;
	private SessionManager mSessionManager;
	private String msGrnadTotal;
	private LinearLayout mllRedeemedLayout;
	private LinearLayout mllTotalLayout;
	private TextView mtvRedeemAmuont;
	private LinearLayout mllGrandTotal;
	private TextView mtvGrandTotal;
	private ProgDialog pDialog;
	private String UserId="0";
	private HashMap<String,String> userDetails = new HashMap<String, String>();
	private TextView mtvRedeemPoint;
	private String msTotalRedeemPoints = "";
	private String msRedeemPoints = "";
	private String msSavingAmount = "";
	private boolean mbFromContinue;
	private TextView mtvRedeemCoupns;
	private String msRedeemPointsAmnt = "0";
	private String msRedeemCoupnsAmnt = "0";
	private LinearLayout mllRedeemedCouponLayout;
	private TextView mtvRedeemCouponsAmount;
	private Dialog redeemDialog;
	private String msCouponId = "0";
	private boolean mbIsReedeemPoints = false;
	private boolean mbIsReedeemCoupons = false;

	int delivery_charges = 0;
	String amt_type = "";
	int totalAmount = 0;
	boolean fromCoupanCode = false;
	boolean fromCoupanPoint = false;
	int tmpCoupan = 0;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);


		//msGrnadTotal=getArguments().getString("GrandTotal");

		/**
		 * Initialize the class varibles
		 * 
		 */

		msGrnadTotal = Config.GARND_TOTAL;
		//msGrnadTotal = "1";
		mSessionManager = new SessionManager(getActivity());
		pDialog = new ProgDialog(getActivity());

		Bundle mBundle = getArguments();
		if(mBundle!=null){
			mbFromContinue  = mBundle.getBoolean("FromContinue",false);
		}



		/**
		 * If the user is logged in than get the redeem points of the user from server
		 * 
		 */
		if(mSessionManager.isLoggedInCustomer()){
			userDetails = mSessionManager.getUserDetails();
			UserId = userDetails.get(SessionManager.USER_ID);
			if(mSessionManager.isLoggedInCustomer()){
				//deleteOldRecords();

				/**
				 * get redeem points of this user from server
				 */
				getRedeemPoints();

				if(mbFromContinue){
					deleteProduct();
				}
			}else{
				//checkCachedData();	
			}

		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (View)inflater.inflate(R.layout.grand_total, null);

		TextView mtvContinue = (TextView) mView.findViewById(R.id.txtContinue);
		TextView mtvTotalmount = (TextView) mView.findViewById(R.id.txtTotalAmuont);
		mtvRedeemPoint = (TextView) mView.findViewById(R.id.txtRedeemPoint);
		mtvRedeemCouponsAmount= (TextView) mView.findViewById(R.id.txtRedeemAmuontCoupon);
		mtvRedeemCoupns = (TextView) mView.findViewById(R.id.txtRedeemCoupns);
		mtvRedeemAmuont = (TextView) mView.findViewById(R.id.txtRedeemAmuont);
		mtvGrandTotal = (TextView) mView.findViewById(R.id.text_grandTotal);
		mtvTotalmount.setText(msGrnadTotal);
		mtvGrandTotal.setText(msGrnadTotal);
		mllRedeemedLayout = (LinearLayout) mView.findViewById(R.id.redeemedLayout);
		mllRedeemedCouponLayout = (LinearLayout) mView.findViewById(R.id.redeemedLayoutCoupons);

		mllTotalLayout = (LinearLayout) mView.findViewById(R.id.totalLayout);
		mllGrandTotal = (LinearLayout) mView.findViewById(R.id.grandTotal);
		mllRedeemedLayout.setVisibility(View.GONE);
		mllGrandTotal.setVisibility(View.GONE);
		mllTotalLayout.setVisibility(View.VISIBLE);
		mtvRedeemCoupns.setVisibility(View.GONE);
		mllRedeemedCouponLayout.setVisibility(View.GONE);

		mtvContinue.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvTotalmount.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvRedeemPoint.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvRedeemCoupns.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvRedeemAmuont.setTypeface(MonginisApplicationClass.getTypeFace());	
		mtvGrandTotal.setTypeface(MonginisApplicationClass.getTypeFace());


		if(mSessionManager.isLoggedInCustomer()){
			mtvRedeemPoint.setVisibility(View.VISIBLE);
			//mtvRedeemCoupns.setVisibility(View.VISIBLE);
		}else{
			mtvRedeemPoint.setVisibility(View.GONE);
			mtvRedeemCoupns.setVisibility(View.GONE);
			mllTotalLayout.setVisibility(View.GONE);
			mllGrandTotal.setVisibility(View.VISIBLE);
			mtvGrandTotal.setText(msGrnadTotal);
		}

		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Map<String, String> param = new HashMap<String, String>();


				if(mbIsReedeemPoints && mbIsReedeemCoupons ){
					param.put("Redeem_Parameter", "Both");
				}else if(mbIsReedeemPoints){
					param.put("Redeem_Parameter", "Points");
				}else if(mbIsReedeemCoupons){
					param.put("Redeem_Parameter", "Coupons");
				}else{
					param.put("Redeem_Parameter", "None");
				}
				EventTracker.logEvent(EventsName.REDEEM, param);

				updateOrdeInfo();

			}
		});


		mtvRedeemPoint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showRedeemDialog();
			}

		});

		mtvRedeemCoupns.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showRedeemCouponsDialog();
			}

		});



		return mView;

	}


	/**
	 * Dialog to get avail the redeem points for logged in user
	 * 
	 */
	void showRedeemDialog(){

		
		final int tmpRedeemPoint = Integer.parseInt(msRedeemPointsAmnt);
		//showToast(tmpRedeemPoint+"");
		msRedeemPointsAmnt = "0";
		final Dialog redeemDialog = new Dialog(getActivity());
		redeemDialog.setTitle("Redeem Points");
		redeemDialog.setContentView(R.layout.redeem_dialog);

		TextView txtPoint = (TextView) redeemDialog.findViewById(R.id.text_points);
		TextView text_textPoints = (TextView) redeemDialog.findViewById(R.id.text_textPoints);
		TextView text_textSave = (TextView) redeemDialog.findViewById(R.id.text_textSave);
		TextView text_textRedeem = (TextView) redeemDialog.findViewById(R.id.text_textRedeem);
		TextView text_pts = (TextView) redeemDialog.findViewById(R.id.text_pts);
		TextView text_textSavinf = (TextView) redeemDialog.findViewById(R.id.text_textSavinf);
		TextView text_pts1 = (TextView) redeemDialog.findViewById(R.id.text_pts1);

		TextView txtCanSaveAmnt = (TextView) redeemDialog.findViewById(R.id.text_canSaveAmount);
		final EditText mEtPoint = (EditText) redeemDialog.findViewById(R.id.edit_pts);
		TextView mtvRedeem = (TextView) redeemDialog.findViewById(R.id.text_Redeem);
		TextView mtvNoPoints = (TextView) redeemDialog.findViewById(R.id.text_no_points);
		final TextView mtvSavingAmount = (TextView) redeemDialog.findViewById(R.id.text_savingAmount);


		mtvNoPoints.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvRedeem.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvSavingAmount.setTypeface(MonginisApplicationClass.getTypeFace());
		txtCanSaveAmnt.setTypeface(MonginisApplicationClass.getTypeFace());
		txtPoint.setTypeface(MonginisApplicationClass.getTypeFace());
		mEtPoint.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textPoints.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textSave.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textRedeem.setTypeface(MonginisApplicationClass.getTypeFace());
		text_pts.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textSavinf.setTypeface(MonginisApplicationClass.getTypeFace());
		text_pts1.setTypeface(MonginisApplicationClass.getTypeFace());

		mtvNoPoints.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvRedeem.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvSavingAmount.setTypeface(MonginisApplicationClass.getTypeFace());
		txtCanSaveAmnt.setTypeface(MonginisApplicationClass.getTypeFace());
		txtPoint.setTypeface(MonginisApplicationClass.getTypeFace());
		mEtPoint.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textPoints.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textSave.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textRedeem.setTypeface(MonginisApplicationClass.getTypeFace());
		text_pts.setTypeface(MonginisApplicationClass.getTypeFace());
		text_textSavinf.setTypeface(MonginisApplicationClass.getTypeFace());
		text_pts1.setTypeface(MonginisApplicationClass.getTypeFace());


		LinearLayout totalPointsLayout = (LinearLayout) redeemDialog.findViewById(R.id.redeemTotalPointsLayout);
		LinearLayout redeemPointsLayout = (LinearLayout) redeemDialog.findViewById(R.id.redeem_points);
		LinearLayout savingPoints = (LinearLayout) redeemDialog.findViewById(R.id.saving_points);

		if(msTotalRedeemPoints.equalsIgnoreCase("0")){

			totalPointsLayout.setVisibility(View.GONE);
			redeemPointsLayout.setVisibility(View.GONE);
			savingPoints.setVisibility(View.GONE);

			mtvNoPoints.setVisibility(View.VISIBLE);


			mtvNoPoints.setText("Sorry! You dont have any points to redeem now.");
			mtvRedeem.setText("Ok");

			mtvRedeem.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					redeemDialog.dismiss();
				}
			});

		}else{


			totalPointsLayout.setVisibility(View.VISIBLE);
			redeemPointsLayout.setVisibility(View.VISIBLE);
			savingPoints.setVisibility(View.VISIBLE);

			mtvNoPoints.setVisibility(View.GONE);


			txtPoint.setText(msTotalRedeemPoints);
			txtCanSaveAmnt.setText(getResources().getString(R.string.Rs)+"   "+msTotalRedeemPoints);
			mEtPoint.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					// TODO Auto-generated method stub
					if(actionId==EditorInfo.IME_ACTION_DONE){
						/*	int  msSavingAmount = Integer.parseInt(msTotalRedeemPoints) 
								- Integer.parseInt(mEtPoint.getText().toString());*/
						msRedeemPoints = mEtPoint.getText().toString();
						msSavingAmount = mEtPoint.getText().toString();
						mtvSavingAmount.setText(mEtPoint.getText().toString());
						hideKeyBoard(mEtPoint);

						return true;
					}
					return false;
				}
			});

			mEtPoint.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					msRedeemPoints = mEtPoint.getText().toString();
					msSavingAmount = mEtPoint.getText().toString();


					mtvSavingAmount.setText(mEtPoint.getText().toString());
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			mtvRedeem.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						hideKeyBoard(mEtPoint);
						int redeemPoints;
						int totalPOints;


						if(msRedeemPoints!=null && !msRedeemPoints.equalsIgnoreCase("")){

							redeemPoints = Integer.parseInt(msRedeemPoints);
							
							//redeemPoints = 99;
							totalPOints = Integer.parseInt(msTotalRedeemPoints);
							//showToast(""+totalPOints);

							int savedAmount = Integer.parseInt(msSavingAmount);
							int procutTotal = Integer.parseInt(Config.GARND_TOTAL);

							//to get the cart price only
							//procutTotal = procutTotal - delivery_charges;
							procutTotal = procutTotal - Config.SHIPPING_PRICE;

							//showToast("SA " + savedAmount + " " + "PT " + procutTotal);

							if(redeemPoints>totalPOints){
								Toast.makeText(getActivity(), "Redeem points can not be greater than total points!", 1).show();
								mbIsReedeemPoints = false;
							} else if(savedAmount>procutTotal){

								Toast.makeText(getActivity(), 
										"Redeemable amount can not be greater than "+getResources().getString(R.string.Rs)+" "+procutTotal, 1).show();
								mbIsReedeemPoints = false;

							} else if(redeemPoints<0){

								Toast.makeText(getActivity(), "Enter valid redeem points", 1).show();
								mbIsReedeemPoints = false;

							}
							//added by salman for minimum 100 points to check for redeem
							else if(totalPOints<100){

								Toast.makeText(getActivity(), "You don't have enough points in your account to Redeem", 1).show();
								mbIsReedeemPoints = false;
							}
							else{

//								mtvRedeemAmuont.setText(msSavingAmount);
//								mllRedeemedLayout.setVisibility(View.VISIBLE);
//								mllGrandTotal.setVisibility(View.VISIBLE);


								int redeemAmount = Integer.parseInt(mtvRedeemAmuont.getText().toString());

								//int redeemCouponAMnt = Integer.parseInt(mtvRedeemAmuont.getText().toString());
								int grandTotal = procutTotal - redeemAmount;

								//for checking whether grand total is O or not
								//								if(grandTotal<=0){
								//									
								//									//msRedeemPointsAmnt = mtvRedeemAmuont.getText().toString();
								//									mbIsReedeemPoints = false;
								//									//mtvGrandTotal.setText(getAmount());
								//									showToast("Grand Total can't be zero");
								//									mllRedeemedLayout.setVisibility(View.GONE);
								//									msRedeemPointsAmnt = "0";
								//									mtvGrandTotal.setText(getAmount());
								//								}
								//								else{
								
//								int redeemPoint = Integer.parseInt(msRedeemPointsAmnt);
								int redeemCoupan = Integer.parseInt(msRedeemCoupnsAmnt);
								int totalRedeemAmnt = redeemPoints ;
								//showToast(totalRedeemAmnt+ " " + mtvGrandTotal.getText().toString());
								int mainDeductedTotal  = 0;
								
								
								if(fromCoupanCode){
									mainDeductedTotal = Integer.parseInt( mtvGrandTotal.getText().toString()) - Config.SHIPPING_PRICE;	
								}
								else{
									mainDeductedTotal = procutTotal;
								}
								

								if(totalRedeemAmnt>mainDeductedTotal+tmpRedeemPoint){
									showToast("Total Reddem Amount cannot be greater than Product Amount");
									totalRedeemAmnt = 0;
									msRedeemPointsAmnt = "0";
									//redeemPoint = 0;
									fromCoupanPoint = false;
								}
								else{
									
									fromCoupanPoint = true;
									mtvRedeemAmuont.setText(msSavingAmount);
									mllRedeemedLayout.setVisibility(View.VISIBLE);
									mllGrandTotal.setVisibility(View.VISIBLE);

									msRedeemPointsAmnt = mtvRedeemAmuont.getText().toString();
									mbIsReedeemPoints = true;
									mtvGrandTotal.setText(getAmount());
									redeemDialog.dismiss();

								}
								//								}



							}
						}else{
							Toast.makeText(getActivity(), "Please enter points to redeem", 0).show();
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}



				}
			});
		}

		redeemDialog.show();

	}

	void showRedeemCouponsDialog(){

		final int tmpRedeemCoupan = Integer.parseInt(msRedeemCoupnsAmnt);
		msRedeemCoupnsAmnt = "0";
		boolean mbHide = true;
		redeemDialog = new Dialog(getActivity());
		redeemDialog.setTitle("Redeem Coupon");
		redeemDialog.setContentView(R.layout.redeem_coupon);

		final TextView text_savingAmount = (TextView) redeemDialog.findViewById(R.id.text_savingAmount);
		final EditText metCouponsNumber = (EditText) redeemDialog.findViewById(R.id.edit_couponsNumber);
		final TextView mtvRedeem = (TextView) redeemDialog.findViewById(R.id.text_Redeem);
		final TextView text_notNow = (TextView) redeemDialog.findViewById(R.id.text_notNow);


		text_savingAmount.setVisibility(View.GONE);
		text_notNow.setVisibility(View.GONE);
		metCouponsNumber.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if(actionId==EditorInfo.IME_ACTION_DONE){
					//mtvSavingAmount.setText("200");

					hideKeyBoard(metCouponsNumber);
					return true;
				}
				return false;
			}
		});
		mtvRedeem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(metCouponsNumber.length()!=0){
					callRedeemCoupanApi(metCouponsNumber.getText().toString(),tmpRedeemCoupan);

					hideKeyBoard(metCouponsNumber);


				}
				else{
					showToast("Please enter your code first");
				}

			}
		});

		text_notNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				redeemDialog.dismiss();
			}

		});

		redeemDialog.show();

	}

	void hideKeyBoard(EditText mEtPoint){
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
				getActivity().INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEtPoint.getWindowToken(), 0);
	}



	/**
	 * Network request to delete all the shopping cart products of the logged in user from server
	 * 
	 */
	void deleteProduct(){
		LogFile.LogData("deleteProduct");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.DELETE_SELECTED_CART_PRODUCTS+UserId;
		LogFile.LogData("URL "+url);
		pDialog.showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.DELETE,url,null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					LogFile.LogData("Delete Response "+response.toString());
					if(response!=null){
						String status=response.getString("success");
						if(status.equalsIgnoreCase("true")) {

							fethGuestCartData();
						}

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Config.Log(TAG, error.getMessage());
				pDialog.dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}

		}){


			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}


	/**
	 * Db query to get the shopping cart products of guest users
	 * 
	 */
	void fethGuestCartData(){
		LogFile.LogData("fethGuestCartData");
		pDialog.showProgressDialog();
		LogFile.LogData("fetchData");	
		String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='0' ";
		new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {

			@Override
			public void onComplete(ArrayList<ShoppingCartModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				pDialog.dismissDialog();

				if(response.size()>0){
					int miCartPrice = 0;
					for(int i=0;i<response.size();i++){
						int miquantity = Integer.parseInt(response.get(i).getQuantity());
						int miBaseAmount = Integer.parseInt(response.get(i).getSpecial_price());
						int miTotalPrice = miquantity * miBaseAmount;
						miCartPrice = miCartPrice + miTotalPrice;
					}
					String productShippingPrice = response.get(0).getDelivery_mode();
					//showToast(productShippingPrice+"");

					if(productShippingPrice.equalsIgnoreCase("sameday_delivery")){
						miCartPrice = miCartPrice + 200;
						delivery_charges = 200;
					}
					else if(productShippingPrice.equalsIgnoreCase("nextday_delivery")){
						miCartPrice = miCartPrice+150;
						delivery_charges = 150;
					}
					else if(productShippingPrice.equalsIgnoreCase("normal_delivery")){
						miCartPrice = miCartPrice+100;
						delivery_charges = 100;
					}
					else if(productShippingPrice.equalsIgnoreCase("free_delivery")){
						miCartPrice = miCartPrice+0;
						delivery_charges = 0;
					}

					String grandTotal = miCartPrice+"";

					Config.GARND_TOTAL = grandTotal;
					Config.TOTAL_AMOUNT = Config.GARND_TOTAL;  

					//Config.TOTAL_AMOUNT = "1";
					mSessionManager.setRefreshNeeded(true);


					addToCartServer(response);

				} else {

				}

			}
		},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();



	}


	/**
	 * Network request to upload the shopping cart information of guest user to the server, 
	 * and assign these products to currently logged in user
	 * 
	 * @param response
	 */
	void addToCartServer(ArrayList<ShoppingCartModel> response){
		LogFile.LogData("addToCartServer");
		final String TAG="ADD_TO_CART";
		String url=Config.ADD_TO_CART;
		JSONObject mainObj = new JSONObject();

		JSONArray jArrProducts = new JSONArray(); 

		try{	
			mainObj.put("user_id", UserId);

			for(int i=0;i<response.size();i++){
				JSONObject json = new JSONObject();
				json.put("product_id", response.get(i).getProduct_id());
				json.put("quantity", response.get(i).getQuantity());
				json.put("delivery_date", response.get(i).getDelivery_date());

				String msType = response.get(i).getProduct_type();

				if(msType!=null && msType.equalsIgnoreCase("eggless_type")){
					json.put("eggless_type", "Y" );
					json.put("withegg_type", "N");
				} else if(msType!=null && msType.equalsIgnoreCase("withegg_type")){
					json.put("eggless_type", "N" );
					json.put("withegg_type", "Y");
				} else {
					json.put("eggless_type", "N" );
					json.put("withegg_type", "N");
				}

				String msDeliveryMode = response.get(i).getDelivery_mode();

				if(msDeliveryMode==null){
					json.put("sameday_delivery", "N");
					json.put("nextday_delivery", "N");
					json.put("normal_delivery", "N");
					json.put("free_delivery", "N");
				}else{

					if(msDeliveryMode!=null && msDeliveryMode.equalsIgnoreCase("normal_delivery")){
						json.put("sameday_delivery", "N");
						json.put("nextday_delivery", "N");
						json.put("normal_delivery", "Y");
						json.put("free_delivery", "N");
					} else if(msDeliveryMode!=null && msDeliveryMode.equalsIgnoreCase("nextday_delivery")){
						json.put("sameday_delivery", "N");
						json.put("nextday_delivery", "Y");
						json.put("normal_delivery", "N");
						json.put("free_delivery", "N");
					} else if(msDeliveryMode!=null && msDeliveryMode.equalsIgnoreCase("sameday_delivery")){
						json.put("sameday_delivery", "Y");
						json.put("nextday_delivery", "N");
						json.put("normal_delivery", "N");
						json.put("free_delivery", "N");
					} else if(msDeliveryMode!=null && msDeliveryMode.equalsIgnoreCase("free_delivery")){
						json.put("sameday_delivery", "N");
						json.put("nextday_delivery", "N");
						json.put("normal_delivery", "N");
						json.put("free_delivery", "Y");
					} else {
						json.put("sameday_delivery", "N");
						json.put("nextday_delivery", "N");
						json.put("normal_delivery", "N");
						json.put("free_delivery", "N");
					}
				}

				json.put("message", response.get(i).getMessage());
				jArrProducts.put(json);
			}
			mainObj.put("products", jArrProducts);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		pDialog.showProgressDialog();
		LogFile.LogData("Add to cart Url - "+url);
		LogFile.LogData("Add json - "+mainObj.toString());
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, mainObj, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {

				try
				{
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){
						pDialog.dismissDialog();
						LogFile.LogData("Add to cart response - "+response.toString());
						if(status.equalsIgnoreCase("true")){

							mSessionManager.setRefreshNeeded(true);
							String msMessage = response.getString("message");
							showToast(msMessage);

							/**
							 * Change the guest user shopping products to logged in users products
							 */
							updateItems();

							/**
							 *  update Order Table according to the user login
							 */

							updateOrderTable();

						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								pDialog.dismissDialog();
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					pDialog.dismissDialog();
				}

			}

		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				pDialog.dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};

		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);


	}



	/**
	 *  Db query to change the guest user shopping products to logged in users products
	 * 
	 */
	private void updateItems(){

		LogFile.LogData("updateItems");
		String msWhere = DatabaseHelper.USER_ID+"='0'" ;
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.USER_ID, UserId);

		try{
			pDialog.showProgressDialog();

			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete");
					pDialog.dismissDialog();
					//showToast(response);
					if(response!=null){
						//callRedeemFragment();
						LogFile.LogData("db response "+response);

					}

				}
			}, DatabaseHelper.TABLE_SHOPPING_CART, Config.UPDATE_SHOPPING_TABLE, msWhere, "updateorder",value).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}


	} 



	/**
	 * Network request to get the redeem points from the server
	 */
	void getRedeemPoints(){
		LogFile.LogData("getRedeemPoints");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.REDEEM_POINTS+"/"+UserId;
		LogFile.LogData("URL "+url);
		pDialog.showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				try
				{
					pDialog.dismissDialog();
					Config.Log(TAG, response.toString());
					LogFile.LogData(response.toString());
					String status=response.getString("success");

					if(response!=null){
						if(status.equalsIgnoreCase("true")){
							//LogFile.LogData("")
							JSONArray jArr = response.getJSONArray("data");
							for(int i=0;i<jArr.length();i++){
								JSONObject jObj = jArr.getJSONObject(i);
								String totalPoints = jObj.getString("total_points");
								if(totalPoints.equalsIgnoreCase("0")){
									//mtvRedeemPoint.setVisibility(View.GONE);
								}else{
									mtvRedeemPoint.setVisibility(View.VISIBLE);

								}
								msTotalRedeemPoints = totalPoints;
							}
						}else{

							if(status.equalsIgnoreCase("false")){

							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					pDialog.dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				pDialog.dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}
		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}

	void showToast(String tag) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+tag, Toast.LENGTH_SHORT).show();
	}


	/**
	 * Db query to update the order table 
	 * 
	 */
	private void updateOrdeInfo(){

		LogFile.LogData("updateOrdeInfo");
		String msWhere = DatabaseHelper.USER_ID+"='"+UserId+"'" ;
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.POINTS_REDEEM, msRedeemPoints);
		value.put(DatabaseHelper.TOTAL_AMOUNT, mtvGrandTotal.getText().toString());
		value.put(DatabaseHelper.COUPON_ID, msCouponId);
		value.put(DatabaseHelper.DISCOUNT_AMOUNT, msRedeemCoupnsAmnt);
		Config.TOTAL_AMOUNT = mtvGrandTotal.getText().toString();
		LogFile.LogData("Config.TOTAL_AMOUNT "+Config.TOTAL_AMOUNT);

		try{
			pDialog.showProgressDialog();

			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete");
					pDialog.dismissDialog();
					//showToast(response);
					if(response!=null){
						//callRedeemFragment();
						LogFile.LogData("db response "+response);
						FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
						transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
						//To remove all Stack behind
						//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);



						CheckOutFragment continueAs = new CheckOutFragment();
						transaction.add(R.id.content_frame, continueAs,"Redeem");
						transaction.addToBackStack("Redeem");
						transaction.commit();
					}

				}
			}, DatabaseHelper.TABLE_ORDERS, Config.UPDATE_ORDER_TABLE, msWhere, "updateorder",value).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	protected void callRedeemCoupanApi(String coupan_number,final int tmpRedeemCoupan) {
		// TODO Auto-generated method stub

		//to get oly cart amount

		int total = Integer.parseInt(Config.GARND_TOTAL);
		int shipping_price = Config.SHIPPING_PRICE;

		totalAmount = total - shipping_price;

		LogFile.LogData("getRedeemPoints");
		final String TAG="REEDEM_COUPAN";
		String url= Config.REDEEM_COUPAN + coupan_number + "/total_amount/"+totalAmount+"" ;
		LogFile.LogData("URL "+url);
		pDialog.showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {




			@Override
			public void onResponse(JSONObject response) {
				try
				{
					pDialog.dismissDialog();
					Config.Log(TAG, response.toString());
					LogFile.LogData(response.toString());
					String status=response.getString("success");

					if(response!=null){
						if(status.equalsIgnoreCase("true")){
							mbIsReedeemCoupons = true;
							//LogFile.LogData("")
							JSONArray jArr = response.getJSONArray("data");
							for(int i=0;i<jArr.length();i++){
								JSONObject jObj = jArr.getJSONObject(i);
								amt_type = jObj.getString("type");
								msCouponId = jObj.getString("id");

								if(amt_type.equalsIgnoreCase("1")){
									msRedeemCoupnsAmnt = jObj.getString("discount_amount");
								}
								else if(amt_type.equalsIgnoreCase("2")){
									String percent = jObj.getString("discount_amount");
									float discountPercent = Integer.parseInt(percent);
									int deduction = (int)((float)(discountPercent/100)*totalAmount);
									//showToast(""+totalAmount + " " + deduction + " " + discountPercent);
									msRedeemCoupnsAmnt = deduction+"";
								}

								int redeemPoint = Integer.parseInt(msRedeemPointsAmnt);
								int redeemCoupan = Integer.parseInt(msRedeemCoupnsAmnt);
								int totalRedeemAmnt = redeemCoupan;
								//showToast(totalRedeemAmnt+ " " + mtvGrandTotal.getText().toString());
								
								int mainDeductedTotal  = 0;
								if(fromCoupanPoint){
									mainDeductedTotal =  Integer.parseInt( mtvGrandTotal.getText().toString()) - Config.SHIPPING_PRICE;
								}
								else{
									mainDeductedTotal = totalAmount;
								}
								//showToast("TRA " + totalRedeemAmnt + " " + "MDT " + mainDeductedTotal + "TRC "  + tmpRedeemCoupan);
								
								if(totalRedeemAmnt>mainDeductedTotal+tmpRedeemCoupan){
									showToast("Total Reddem Amount cannot be greater than Product Amount");
									totalRedeemAmnt = 0;
									msRedeemCoupnsAmnt = "0";
									fromCoupanCode = false;
									redeemCoupan = 0;
								}
								else{
									fromCoupanCode = true;
									mllGrandTotal.setVisibility(View.VISIBLE);
									mtvGrandTotal.setText(getAmount());
									mtvRedeemCouponsAmount.setText(msRedeemCoupnsAmnt);
									mllRedeemedCouponLayout.setVisibility(View.VISIBLE);
									redeemDialog.dismiss();

								}

							}

						}else{
							mbIsReedeemCoupons = false;
							String message=response.getString("message");
							Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					pDialog.dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				pDialog.dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}
		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}

	private String getAmount(){


		int procutTotal = Integer.parseInt(Config.GARND_TOTAL);
		//		if(fromCoupanCode){
		//
		//			procutTotal = procutTotal - Config.SHIPPING_PRICE;
		//		}
		int reedemPointsAmnt = Integer.parseInt(msRedeemPointsAmnt);
		int redeemCoupnsAmnt = Integer.parseInt(msRedeemCoupnsAmnt);

		int amount = procutTotal - reedemPointsAmnt - redeemCoupnsAmnt;
		if(amount<0){
			amount = 0;
		}
		return amount+"";
	}


	protected void updateOrderTable() {
		// TODO Auto-generated method stub

		String msWhere = DatabaseHelper.USER_ID+"='0'";
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.USER_ID, UserId);
		value.put(DatabaseHelper.ORDER_ID, "");

		try {

			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete");
					pDialog.dismissDialog();
					//showToast("Rows UPdated " + response);
					if(response!=null){
						//callRedeemFragment();
						LogFile.LogData("db response "+response);

					}

				}
			}, DatabaseHelper.TABLE_ORDERS, Config.UPDATE_ORDER_TABLE, msWhere, "updateorder",value).execute();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
