package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;


public class ProductDetails extends ActionBarActivity {

	private ViewPager mProdPager;
	private ActionBar mActionBar;
	
	private ArrayList<String> mArrProductIds=new ArrayList<String>();
	
	private ProductFragmentAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_AppCompat);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_details);
		
		Bundle b=getIntent().getExtras();
		if(b!=null){
			mArrProductIds=b.getStringArrayList("ProductIds");
		}
		
		mProdPager =(ViewPager) findViewById(R.id.prodPager);
		mActionBar=getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setIcon(R.drawable.ic_navigation_drawer);
		mActionBar.hide();
		
		mAdapter=new ProductFragmentAdapter(getSupportFragmentManager(),getFragments());
		mProdPager.setAdapter(mAdapter);
	}
	
	private List<Fragment> getFragments()
	{
		List<Fragment> fList = new ArrayList<Fragment>();
		for(int i=0;i<mArrProductIds.size();i++){
			//fList.add(ProductDetailsFragment.newInstance(mArrProductIds.get(i)));	
		}
		
		return fList;
	}
	

	
	private class ProductFragmentAdapter extends FragmentStatePagerAdapter
	{
		List<Fragment>fragments;
		public ProductFragmentAdapter(FragmentManager fm,List<Fragment>fragments) {
			super(fm);
			this.fragments=fragments;
		}

		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
		
	}

}
