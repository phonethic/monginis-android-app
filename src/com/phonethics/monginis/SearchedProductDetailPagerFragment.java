package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.List;

import com.phonethics.model.ProductModel;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class SearchedProductDetailPagerFragment extends Fragment implements OnPageChangeListener{

	private View mView;
	private ViewPager mProdPager;
	private ProductFragmentAdapter mAdapter;
	private ArrayList<ProductModel> mArrProducts=new ArrayList<ProductModel>();
	private int position;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mArrProducts=getArguments().getParcelableArrayList("Searched Products");
		position=getArguments().getInt("position");
		//Toast.makeText(getActivity(), "pos " + position, 0).show();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mView = (View)inflater.inflate(R.layout.fragment_details_pager, container, false);
		mProdPager=(ViewPager)mView.findViewById(R.id.prodPager);
		
		mAdapter=new ProductFragmentAdapter(getActivity().getSupportFragmentManager(),getFragments());
		mProdPager.setAdapter(mAdapter);
		mProdPager.setCurrentItem(position);
		mProdPager.setOffscreenPageLimit(5);
		
		mProdPager.setOnPageChangeListener(this);
		return mView;
	}

	private List<Fragment> getFragments()
	{
		List<Fragment> fList = new ArrayList<Fragment>();
		for(int i=0;i<mArrProducts.size();i++){
			fList.add(SearchedProductDetailsFragment.newInstance(mArrProducts.get(i)));	
		}
		
		return fList;
	}
	
	private class ProductFragmentAdapter extends FragmentStatePagerAdapter
	{
		List<Fragment>fragments;
		public ProductFragmentAdapter(FragmentManager fm,List<Fragment>fragments) {
			super(fm);
			this.fragments=fragments;
		}

		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}
		
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub
		
	}

}
