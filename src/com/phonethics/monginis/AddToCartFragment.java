package com.phonethics.monginis;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.ProductAttributeModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;


/**
 * Shows the information of product, allows the user to add the particular product into shopping cart.
 * If user is logged in, this will save the information on server else it will save the information in database
 * 
 * @author Nitin
 *
 */
public class AddToCartFragment extends Fragment implements DBListener{

	private View mView;
	private ProgressDialog mProgressDialog;
	private EditText mMessageBox;
	private TextView txtAddToCart;
	private LinearLayout mChildLayout;
	private ArrayList<String> mAttribute_ValueId = new ArrayList<String>();
	private ArrayList<String> mAttribute_ValueName = new ArrayList<String>();
	private TextView[] txtView;
	private TextView txtQuantity;
	private TextView txtDate;
	private Calendar cal;
	private Calendar requiredCal;

	private ArrayList<ProductAttributeModel> mProductAttributModel = new ArrayList<ProductAttributeModel>();
	private Dialog mListDialog;
	private Dialog mQuantityListDialog;
	ListView mDialogList;
	ListView mQuantityList;
	CategoriesAdapter mAdapter;
	int clicked;
	RelativeLayout dividerView;
	RelativeLayout dividerViewNew;
	HashMap<String,String> userDetails = new HashMap<String, String>();
	SessionManager mSessionManager;
	String mUserId="";
	String mPTitle="";
	String mPAmount="";
	String mPImageUrl="";	
	String mPSpecialPrice="";
	String mPListPrice="";
	String mPOurPrice="";
	String mPQuantity="";
	boolean isError;
	String errorMessage="";
	boolean mbIsDelieveryOption = false;

	private ArrayList<ShoppingCartModel> marrCartData;

	boolean isErrorArr[];
	private String deliveryDat;
	int miCartItems = 0;
	ContentValues valueUpdate = new ContentValues();

	private String msType = "";
	private String msDeliveryMode = "";

	private final String msRegular = "REGULAR";
	private final String msEggType = "EGGLESS";
	private final String msNormalDelivery = "NORMAL DELIVERY (48 HRS.)";
	private final String msNextDayDelivery = "Next Day";
	private final String msSameDay = "Same Day";
	private final String msVirtualDelivery = "Virtual Delivery";

	private final String msSameDayLocal = "sameday_delivery";
	private final String msNextDayLocal = "nextday_delivery";
	private final String msNormalDeliveryLocal = "normal_delivery";
	private final String msVirtualDeliveryLocal = "free_delivery";

	protected String msCartDeliveryMode;
	protected int layoutSize = 0;
	private boolean isProductExits = false; 
	View patch;
	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mSessionManager = new SessionManager(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		
		/**
		 * Get the product information from server
		 * 
		 */
		getProductDetails(Config.SUB_CATEGORY_SELECTED_PRODUCT_ID);
		
		
		/**
		 * get the product attributes information like delivery mode and product type from server
		 * 
		 */
		getAttributeType(Config.SUB_CATEGORY_SELECTED_PRODUCT_ID);



		/**
		 *Check whether user is logged in or not. 
		 */
		userDetails = mSessionManager.getUserDetails();
		if(mSessionManager.isLoggedInCustomer()){
			mUserId = userDetails.get(SessionManager.USER_ID);
		} else {
			mUserId = "0";
		}
		final Map<String, String> param = new HashMap<String, String>();
		param.put("AddToCart_screen",  EventsName.category +" - "+EventsName.sub_category+" - "+EventsName.product);
		EventTracker.logEvent("AddToCart", param);
		
		
		
		/** Db query to get the information of previously added products in shopping cart, 
		 * this is require to get the information of delivery date and delivery mode of previously added products */

		getProductsFromCart();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		/**
		 * 
		 * Initialize the layout
		 * 
		 */
		mView=(View)inflater.inflate(R.layout.fragment_addtocart, null);
		txtDate  =(TextView)mView.findViewById(R.id.txtDate);
		mMessageBox=(EditText)mView.findViewById(R.id.mMessageBox);
		txtAddToCart=(TextView)mView.findViewById(R.id.txtAddToCart);
		mChildLayout=(LinearLayout)mView.findViewById(R.id.mChildLayout);
		mListDialog = new Dialog(getActivity());
		mQuantityListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mQuantityListDialog.setContentView(R.layout.attribute_type_selection);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		mQuantityList = (ListView)mQuantityListDialog.findViewById(R.id.mAttributeType);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		dividerViewNew = (RelativeLayout)mQuantityListDialog.findViewById(R.id.dividerView);
		txtQuantity=(TextView)mView.findViewById(R.id.txtQuantity);
		dividerView.setVisibility(View.GONE);
		dividerViewNew.setVisibility(View.GONE);
		patch = (View)mView.findViewById(R.id.patch);


		txtDate.setTypeface(MonginisApplicationClass.getTypeFace());
		txtAddToCart.setTypeface(MonginisApplicationClass.getTypeFace());
		txtQuantity.setTypeface(MonginisApplicationClass.getTypeFace());
		mMessageBox.setTypeface(MonginisApplicationClass.getTypeFace());



		txtAddToCart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				boolean errorCondition = false;


				/**
				 * check whether all data is being filled properly or not
				 */

				if(isErrorArr!=null){
					for(int m=0;m<isErrorArr.length;m++){
						if(isErrorArr[m]){
							showToast("Please fill all the details first");
							break;
						} else {
							errorCondition = true;
						}
					}	
				} else{
					errorCondition = true;
				}


				/**
				 * check which data is missing and propmt for that
				 */

				if(errorCondition){
					
					/**
					 * If quantity is not being selected
					 * 
					 */
					if(txtQuantity.getText().toString().equalsIgnoreCase("QUANTITY")){
						showToast("Please select quantity first");
						
						
						
						/**
						 * Check if date is being entered or not
						 */	
					}else if(txtDate.getText().toString().equalsIgnoreCase("SELECT DATE") && txtDate.isShown()){
						showToast("Please select date");
						
						
						
						/**
						 *
						 * check if shopping cart contains some products
						 * 
						 */
					}else if(!mbIsDelieveryOption && marrCartData.size()!=0){
						
						if(layoutSize>0){
							
							/**
							 * if there is no difference in delievry mode and delivery date than add this product to shopping cart
							 * 
							 */
							if(checkDeliveryMode()){
								
								addData();
							}	
							
							
						/**
						 * Check if present delivery option and previously added delivery option are same or different 
						 * if both are same than add this product
						 * 	
						 */
						} else if(msCartDeliveryMode.equalsIgnoreCase(msNormalDeliveryLocal)){
							addData();
						} else {
							
							/**
							 * Prompt the user if delivery options are different
							 * 
							 */
							showDelievryModeDialog();
						}	
					} else {
						/**
						 * 
						 * 
						 */
						addData();
					}
				} 
			}
		});

		txtQuantity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mQuantityListDialog.setTitle("Select Quantity");
				CategoriesAdapter mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, Config.list);
				mQuantityList.setAdapter(mAdapter);
				mQuantityListDialog.show();
			}
		});

		mQuantityList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				mPQuantity = Config.list.get(position);
				txtQuantity.setText(Config.list.get(position));
				mQuantityListDialog.dismiss();
			}
		});
		return mView;
	}

	
	
	/**
	 *Add this product information on server or on local database based on the condition whether customer is logged in or not
	 */
	private void addData(){
		
		final Map<String, String> param = new HashMap<String, String>();
		param.put("AddToCart_click",  EventsName.category +" - "+EventsName.sub_category+" - "+EventsName.product);
	
		
		
		if(mSessionManager.isLoggedInCustomer()){

			param.put("StoredOn",  "Server");
			
			addToCartServer();
			
			/**
			 * 
			 * add info on server if user is logged in or on local database if not looged in.
			 * 
			 */	
		} else {

			param.put("StoredOn",  "ClientDatabase");
			checkCachedOrderData();

		}
		EventTracker.logEvent("AddToCart", param);
	}

	
	/**
	 * Store the product information on content values to store the information on database
	 * 
	 */
	protected void callCartFragment() {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.PRODUCT_ID, Config.SUB_CATEGORY_SELECTED_PRODUCT_ID);
		value.put(DatabaseHelper.PRODUCT_NAME, mPTitle);
		value.put(DatabaseHelper.PRODUCT_IMG_URL, mPImageUrl);
		value.put(DatabaseHelper.MESSAGE, mMessageBox.getText().toString());
		value.put(DatabaseHelper.QUANTITY, mPQuantity);
		value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE,deliveryDat);

		if(mPSpecialPrice.length()==0){

			value.put(DatabaseHelper.BASE_AMOUNT, mPOurPrice);	
		}
		else{
			value.put(DatabaseHelper.BASE_AMOUNT, mPSpecialPrice);
		}

		if(mSessionManager.isLoggedInCustomer())
			value.put(DatabaseHelper.USER_ID, mUserId);
		else
			value.put(DatabaseHelper.USER_ID, "0");


		if(msType.equalsIgnoreCase(msRegular)){
			value.put(DatabaseHelper.PRODUCT_TYPE,"withegg_type");
		} else if(msType.equalsIgnoreCase(msEggType)){
			value.put(DatabaseHelper.PRODUCT_TYPE,"eggless_type");
		} else {
			value.put(DatabaseHelper.PRODUCT_TYPE,"N");
		}

		if(msDeliveryMode.equalsIgnoreCase(msSameDay)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msSameDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNextDayDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNextDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNormalDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNormalDeliveryLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msVirtualDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msVirtualDeliveryLocal);
		}

		if(isProductExits){
			
			/**
			 * If product already present in shopping cart than update the information of that product
			 * 
			 */
			updateProduct(value);
			
		}else{
			
			
			/**
			 * Save the information database if user is not logged in
			 */
			insertIntoDbSingle(value);
		}
		
	}

	
	/**
	 * 
	 * Network request to add the product information on the server
	 * 
	 */
	void addToCartServer(){

		final String TAG="ADD_TO_CART";
		String url=Config.ADD_TO_CART;
		JSONObject mainObj = new JSONObject();
		JSONObject json = new JSONObject();
		JSONArray jArrProducts = new JSONArray(); 

		try{	
			mainObj.put("user_id", mUserId);
			json.put("product_id", Config.SUB_CATEGORY_SELECTED_PRODUCT_ID);
			json.put("quantity", mPQuantity);
			json.put("delivery_date", deliveryDat);

			if(msType.equalsIgnoreCase(msEggType)){
				json.put("eggless_type", "Y" );
				json.put("withegg_type", "N");
			} else if(msType.equalsIgnoreCase(msRegular)){
				json.put("eggless_type", "N" );
				json.put("withegg_type", "Y");
			} else {
				json.put("eggless_type", "N" );
				json.put("withegg_type", "N");
			}

			if(msDeliveryMode.equalsIgnoreCase(msNormalDelivery)){
				json.put("sameday_delivery", "N");
				json.put("nextday_delivery", "N");
				json.put("normal_delivery", "Y");
				json.put("free_delivery", "N");
			} else if(msDeliveryMode.equalsIgnoreCase(msNextDayDelivery)){
				json.put("sameday_delivery", "N");
				json.put("nextday_delivery", "Y");
				json.put("normal_delivery", "N");
				json.put("free_delivery", "N");
			} else if(msDeliveryMode.equalsIgnoreCase(msSameDay)){
				json.put("sameday_delivery", "Y");
				json.put("nextday_delivery", "N");
				json.put("normal_delivery", "N");
				json.put("free_delivery", "N");
			} else if(msDeliveryMode.equalsIgnoreCase(msVirtualDelivery)){
				json.put("sameday_delivery", "N");
				json.put("nextday_delivery", "N");
				json.put("normal_delivery", "N");
				json.put("free_delivery", "Y");
			} else {
				json.put("sameday_delivery", "N");
				json.put("nextday_delivery", "N");
				json.put("normal_delivery", "N");
				json.put("free_delivery", "N");
			}

			json.put("message", mMessageBox.getText().toString());
			jArrProducts.put(json);
			mainObj.put("products", jArrProducts);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		showProgressDialog();
		LogFile.LogData("Add to cart Url - "+url);
		LogFile.LogData("Add json - "+mainObj.toString());
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, mainObj, new Response.Listener<JSONObject>() {



			@Override
			public void onResponse(JSONObject response) {
				try
				{
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){
						dismissDialog();
						LogFile.LogData("Add to cart response - "+response.toString());
						if(status.equalsIgnoreCase("true")){

							mSessionManager.setRefreshNeeded(true);
							Config.DELIVERY_DATE_SERVER = deliveryDat;
							Config.DELIVERY_MODE = msDeliveryMode;

							String msMessage = response.getString("message");
							showToast(msMessage);
						
							
							/**
							 * Check the count order table
							 * 
							 */
							checkCachedOrderData();
							//updateOrdeInfo();

						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								dismissDialog();
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}

		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};

		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);


	}

	private void callNextFragment(){

		try{

			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			ShoppingCartFragment chkFrag=new ShoppingCartFragment();
			transaction.add(R.id.content_frame, chkFrag,"SHOPPING CART");
			transaction.commit();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Network request to get the product attributes information like delivery mode and product type
	 * 
	 * @param id
	 */
	void getAttributeType(String id) {
		// TODO Auto-generated method stub

		//final ArrayList<String> type_attValues = new ArrayList<String>();
		try {
			showProgressDialog();
			String TAG="PRODUCT_ATTRIBUTES";
			String url=Config.GET_PRODUCT_ATTRIBUTES+id;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {


				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try {
						if(response!=null) {

							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								layoutSize = data.length();
								for(int i=0;i<data.length();i++){

									ProductAttributeModel obj = new ProductAttributeModel();
									JSONObject value = data.getJSONObject(i);
									String att_type = value.getString("attribute_name");
									String att_id = value.getString("attribute_id");
								
	
									obj.setAttribute_id(att_id);
									obj.setAttribute_name(att_type);
									//for(int j=0;j<value.length();j++){

									JSONArray attribute_values = value.getJSONArray("attribute_valus");
									for(int k=0;k<attribute_values.length();k++){

										JSONObject inner_values = attribute_values.getJSONObject(k);
										String att_value_id = inner_values.getString("attribute_value_id");
										String att_value_name = inner_values.getString("attribute_value");
										Config.Log("ATT_VALUE","ATT_VALUE "+att_value_name);
										//type_attValues.add(att_value);
										if(att_type.equalsIgnoreCase("Delivery Mode")){
										
											if(att_value_name.equalsIgnoreCase("Same Day")){
												
												Calendar c = Calendar.getInstance(); 
												int hour = c.get(Calendar.HOUR_OF_DAY);
												//to check the timing for same day delivery
												if(hour >= 15){
													
												}
												else{
													obj.setAttribute_value_id(att_value_id);
													obj.setAttribut_value_name(att_value_name);
												}
											}
											else{
												obj.setAttribute_value_id(att_value_id);
												obj.setAttribut_value_name(att_value_name);
											}
										}
										else{
											obj.setAttribute_value_id(att_value_id);
											obj.setAttribut_value_name(att_value_name);
										}
//										obj.setAttribute_value_id(att_value_id);
//										obj.setAttribut_value_name(att_value_name);
									}
									//}

									mProductAttributModel.add(obj);
								}

								createLayout(layoutSize);
							}
							else{

								msDeliveryMode = msNormalDelivery;
								getDateAfter(2);

							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);



		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	protected void createLayout(int layoutSize) {
		// TODO Auto-generated method stub

		isErrorArr = new boolean[layoutSize];
		txtView = new TextView[layoutSize];
		TableRow.LayoutParams paramsExample = null ;

		//for displayng height according to resolution
		switch (getResources().getDisplayMetrics().densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			// ...
			//showToast("Low");
			paramsExample = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,40,0);
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			// ...
			//showToast("Med");
			paramsExample = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,50,0);
			break;
		case DisplayMetrics.DENSITY_HIGH:
			// ...
			//showToast("High");
			paramsExample = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,70,0);
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			// ...
			//showToast("XHigh");
			paramsExample = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,90,0);
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			// ...
			//showToast("XXHigh");
			paramsExample = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,130,0);
			break;
		}

		//for Populating list of dialog

		for(int i=0;i<txtView.length;i++){

			//boolean array
			isErrorArr[i] = true;

			txtView[i] = new TextView(getActivity());
			txtView[i].setText(mProductAttributModel.get(i).getAttribute_name());
			txtView[i].setBackgroundColor(Color.parseColor("#FFFFFF"));
			txtView[i].setTextColor(Color.parseColor("#000000"));
			txtView[i].setGravity(Gravity.CENTER);
			txtView[i].setTextSize(20);
			txtView[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
			txtView[i].setPadding(0, 0, 20, 0);
			txtView[i].setTypeface(MonginisApplicationClass.getTypeFace());
			//paramsExample.setMargins(0, 5, 0, 0);
			txtView[i].setLayoutParams(paramsExample);
			mChildLayout.addView(txtView[i]);
			
			//to add view for grey lines
			View view = new View(getActivity());
			LayoutParams viewParam = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT,4,0);
			view.setBackgroundResource(R.color.fields);
			view.setLayoutParams(viewParam);
			mChildLayout.addView(view);
			
			final int count = i;
			txtView[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					//isError = false;
					clicked = count;
					mAttribute_ValueId =  mProductAttributModel.get(count).getAttribute_value_id();
					mAttribute_ValueName = mProductAttributModel.get(count).getAttribute_value_name();
					mListDialog.setTitle(mProductAttributModel.get(count).getAttribute_name());
					mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mAttribute_ValueName);
					mDialogList.setAdapter(mAdapter);
					mListDialog.show();
				}
			});
			
		
			mDialogList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) {
					// TODO Auto-generated method stub
					txtView[clicked].setText(mAttribute_ValueName.get(position));
					isErrorArr[clicked] = false;

					if(mAttribute_ValueName.get(position).toString().contains("Normal Del")){
						msDeliveryMode = msNormalDelivery;
						txtDate.setVisibility(View.VISIBLE);
						patch.setVisibility(View.VISIBLE);
						txtDate.setText("SELECT DATE");
						txtDate.setClickable(true);
					} else if(mAttribute_ValueName.get(position).toString().contains("Next Day")){
						msDeliveryMode = msNextDayDelivery;
						txtDate.setVisibility(View.VISIBLE);
						patch.setVisibility(View.VISIBLE);
						txtDate.setText(getDateAfter(1));
						txtDate.setClickable(false);

					} else if(mAttribute_ValueName.get(position).toString().contains("Same Day")){
						msDeliveryMode = msSameDay;
						//txtDate.setVisibility(View.GONE);
						patch.setVisibility(View.VISIBLE);
						txtDate.setVisibility(View.VISIBLE);
						txtDate.setText(getDateAfter(0));
						txtDate.setClickable(false);
					} else if(mAttribute_ValueName.get(position).toString().contains("Virtual Delivery")){
						msDeliveryMode = msVirtualDelivery;
						txtDate.setVisibility(View.VISIBLE);
						txtDate.setText("SELECT DATE");
						txtDate.setClickable(true);
						patch.setVisibility(View.VISIBLE);
					} else if(mAttribute_ValueName.get(position).toString().contains("Regular")){
						msType = msRegular;
					} else if(mAttribute_ValueName.get(position).toString().contains("Eggless")){
						msType = msEggType;
					} else {
						//msType = msEggType;
						msType = "";
						msDeliveryMode = "";

					}

					if(mProductAttributModel.get(count).getAttribute_name().equalsIgnoreCase("Delivery Mode")){
						valueUpdate.put(DatabaseHelper.DELIVERY_MODE, mAttribute_ValueName.get(position).toString());
					}


					mListDialog.dismiss();
				}
			});

			txtDate.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDateDialog();
				}
			});

		}

	}

	void insertIntoDb(ContentValues[] contentValues ){
		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_PRODUCT_ATTRIBUTE,
					Config.INSERT_DATA, "PRODUCT_ATTRIBUTE").execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	
	/**
	 * Db query to store the information of the product in shopping cart table
	 * 
	 * @param contentValues
	 */
	void insertIntoDbSingle(ContentValues contentValues ){
		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_SHOPPING_CART,
					Config.INSERT_SINGLE_DATA, Config.INSERT_SINGLE_DATA).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	
	
	
	/**
	 * Db query to update the product information if product already present in database.
	 * 
	 * @param values
	 */
	private void updateProduct(ContentValues values){
		LogFile.LogData("updateProduct");
		try{
			String msWhere = DatabaseHelper.USER_ID+"='"+mUserId+"' AND "+DatabaseHelper.PRODUCT_ID+" ='"+Config.SUB_CATEGORY_SELECTED_PRODUCT_ID+"'" ;
			
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete-updateProduct"+response);
					
					
					callNextFragment();
					
					
					//updateOrdeInfo();
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_SHOPPING_CART, Config.UPDATE_SHOPPING_TABLE, msWhere, "updateShopping",values).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void showToast(String msg){
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCompleteOperation(String tag) {
		// TODO Auto-generated method stub
		if(tag.equalsIgnoreCase(Config.INSERT_SINGLE_DATA)){
			//showToast(tag);
			//fetchData();
			callNextFragment();
		} else if(tag.equalsIgnoreCase("OrderData")){
			LogFile.LogData("OrderDataOnCompleteOperation");
			dismissDialog();
			
			
			/**
			 * 
			 * add info on server if user is logged in or on local database if not looged in.
			 * 
			 */
			if(mSessionManager.isLoggedInCustomer()){
				//showToast("Adding on server");
				addToCartServer();	
			} else {
				//showToast("Adding on local database");
				callCartFragment();
			}
		} else if(tag.equalsIgnoreCase("updateorder")){
			callNextFragment();
		}
	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		// TODO Auto-generated method stub
	
		if(tag.equalsIgnoreCase("CART DATA")){
		
			for(int i=0;i<mArr.size();i++){
				Config.Log("PRODUCT ","PRODUCT " + mArr.get(i));
				
			}
		}
	}

	@Override
	public void rowCount(long rowCount, String tag) {
		// TODO Auto-generated method stub
		if(tag.equalsIgnoreCase(Config.ROW_COUNT)){
			LogFile.LogData(Config.ROW_COUNT);
			if(rowCount==0){
				
				/**
				 * If there order information does not exists than insert this recode
				 * 
				 */
				insertOrderInfo();
				
			} else {
			
				/**
				 * If there order information exists than update it
				 * 
				 */
				updateOrdeInfo();		
			}
		}
	}

	void fetchData(){
		//showProgressDialog();
		//showToast("Called fetch data");
		new AsyncDatabaseQuery(getActivity(), this, DatabaseHelper.TABLE_SHOPPING_CART, 
				DatabaseHelper.QUANTITY, Config.RETRIVE_DATA, "CART DATA").execute();
	}

	
	/**
	 * Network request to get the details of product based on product id. 
	 * 
	 * @param id
	 */
	void getProductDetails(String id) {
		
		try {

			String TAG="PRODUCT_DETAILS";
			String url=Config.GET_PARTICULAR_PRODUCT+id;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try {
						if(response!=null){
							JSONObject json=response.getJSONObject("data");
							json.getInt("id");
							mPTitle = json.getString("title");
							json.getString("description");
							mPSpecialPrice=  json.getString("special_price");
							mPListPrice = json.getString("list_price");
							mPImageUrl = json.getString("image");
							mPOurPrice = json.getString("our_price");
							Config.Log("Product ", "Product "+json.toString());
						}

					} catch(Exception ex) {
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {


					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}

	void showDateDialog(){

		cal = Calendar.getInstance();
		requiredCal=Calendar.getInstance();	
		requiredCal.set(Calendar.YEAR,cal.get(Calendar.YEAR));
		requiredCal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		requiredCal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+2);

		cal.add(Calendar.DAY_OF_MONTH, 2);


		new DatePickerDialog(getActivity(),datePicker, cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)).show();



	}

	DatePickerDialog.OnDateSetListener datePicker=new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			cal.set(Calendar.YEAR,year);
			cal.set(Calendar.MONTH, monthOfYear);
			cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

			checkDate();

		}


	};

	protected String msCartDate;

	void checkDate(){

		try{


			LogFile.LogData("Current Date "+requiredCal.get(Calendar.YEAR)+" "+requiredCal.get(Calendar.MONTH)+" "+requiredCal.get(Calendar.DAY_OF_MONTH));
			LogFile.LogData("Current Date  changed "+cal.get(Calendar.YEAR)+" "+cal.get(Calendar.MONTH)+" "+cal.get(Calendar.DAY_OF_MONTH));

			Date d1=requiredCal.getTime();
			Date d2=cal.getTime();

			if(dateDiff(d2, d1)<0){		
				showToast("Order date cannot be less than 48 hours");
			}else{			
				
				if(dateDiff(d2, d1)>365){
					showToast("You can't select date greater than 1 year");
				}
				else{
					DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
					DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
					txtDate.setText(dtReview.format(cal.getTime()));
					deliveryDat = dt.format(cal.getTime());	
				}

			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public long dateDiff(Date d1,Date d2){
		long datediff=0;
		datediff=(d1.getTime()-d2.getTime())/(24 * 60 * 60 * 1000);
		LogFile.LogData("daysBetween "+datediff);
		return datediff;
	}

	
	
	/**
	 * Db query to get the information of previously added products in shopping cart, this is require to get the information
	 * of delivery date and delivery mode of previously added products
	 * 
	 */
	void getProductsFromCart(){
		//showProgressDialog();
		String UserId = "";
		if(mSessionManager.isLoggedInCustomer()){
			UserId = userDetails.get(SessionManager.USER_ID);
		}else{
			UserId = "0";
		}
		LogFile.LogData("fetchData");	
		String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='"+UserId+"' ";
		
		new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {



			@Override
			public void onComplete(ArrayList<ShoppingCartModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				try{
					//dismissDialog();
					marrCartData = response;
					if(response.size()!=0){

						marrCartData = response;
						miCartItems = response.size();
						boolean isProdSame = false;
						for(int i=0;i<response.size();i++){

							msCartDate = response.get(i).getDelivery_date();
							String msDeliveryMode = response.get(i).getDelivery_mode();
							msCartDeliveryMode = msDeliveryMode;

							String msProdId = response.get(i).getProduct_id();
							if(Config.SUB_CATEGORY_SELECTED_PRODUCT_ID.equalsIgnoreCase(msProdId) && !isProdSame){
								isProdSame = true;
								isProductExits = true;
							}

							LogFile.LogData("CartDate "+msCartDate + " Cart Delivery Mode "+msCartDeliveryMode);

						}
					}else{
						miCartItems = 0;
					}

				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();
	}

	private void showDateWarningDialog(){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.alert_dialog);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_continue);
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		TextView mtvCancel = (TextView) dateWarning.findViewById(R.id.text_cancel);
		mtvMessage.setText("Delievery date is already set in your cart if you wish to change it, please click on continue" +
				"\n\n" +
				"Please note: The delivery date of your cart will change upon hitting continue.");
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mbIsDelieveryOption = true;
				txtAddToCart.performClick();
				dateWarning.dismiss();

			}
		});
		mtvCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dateWarning.dismiss();

			}
		});
		dateWarning.show();	

	}

	
	/**
	 * Db query to insert the required fields for the order table
	 * 
	 */
	private void insertOrderInfo(){
		LogFile.LogData("insertOrderInfo()");
		valueUpdate.put(DatabaseHelper.USER_ID, mUserId);
		valueUpdate.put(DatabaseHelper.ORDER_ID, "");
		valueUpdate.put(DatabaseHelper.PRODUCT_AMOUNT, "");
		valueUpdate.put(DatabaseHelper.SHIPPING_CHARGE,"");
		valueUpdate.put(DatabaseHelper.COUPON_ID, "");
		valueUpdate.put(DatabaseHelper.POINTS_REDEEM,"");
		valueUpdate.put(DatabaseHelper.DELIVERY_DATE,deliveryDat);
		valueUpdate.put(DatabaseHelper.ORDER_NOTE,"");
		valueUpdate.put(DatabaseHelper.TOTAL_AMOUNT,"");
		valueUpdate.put(DatabaseHelper.MESSAGE_BILLING,"");

		try{
			showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, valueUpdate, DatabaseHelper.TABLE_ORDERS,
					Config.INSERT_SINGLE_DATA, "OrderData").execute();
		}catch(Exception ex){
			ex.printStackTrace();
			callNextFragment();
		}
	}

	
	/**
	 * Db query to update the required fields for the order table
	 * 
	 */
	private void updateOrdeInfo(){

		LogFile.LogData("updateOrdeInfo()");
		String msWhere = DatabaseHelper.USER_ID+"='"+mUserId+"'" ;
		valueUpdate.put(DatabaseHelper.DELIVERY_DATE, deliveryDat);
		LogFile.LogData("Date "+deliveryDat);
		try{
			showProgressDialog();
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {
				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete-updateOrdeInfo");
			
					updateDeliveryOptions();
					
					if(mSessionManager.isLoggedInCustomer()){
					
						/**
						 * If user is logged in than call next fragment
						 * 
						 */
						callNextFragment();
						
					} else {
						
						callCartFragment();
					}
					
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_ORDERS, Config.UPDATE_ORDER_TABLE, msWhere, "updateorder",valueUpdate).execute();

		}catch(Exception ex){
			ex.printStackTrace();
			callCartFragment();
		}
	}

	
	/**
	 * Db query to check the count of order table return the result to implemented method DbListner rowCount()
	 * 
	 */
	private void checkCachedOrderData(){
		LogFile.LogData("checkCachedOrderData");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"+mUserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_ORDERS,
					DatabaseHelper.AUTO_ID, Config.ROW_COUNT,  Config.ROW_COUNT,"").execute();

		}catch(Exception ex){
			ex.printStackTrace();
			showToast(getResources().getString(R.string.Parse));
			callCartFragment();
		}
	}

	
	/**
	 * Check if previously added products delivery mode and selected product delivery mode is different or same
	 * 
	 * @return
	 */
	private boolean checkDeliveryMode(){

		try{

			if(msCartDeliveryMode.equalsIgnoreCase(msNormalDeliveryLocal)){ // database
				if(msDeliveryMode.equalsIgnoreCase(msNormalDelivery)){ // present selected delivery mode		
					
					/**
					 * check date diff;
					 * 
					 */
					return checkDeliveryDate();

				}else{
					showDelievryModeDialog();
				}
			} else if (msCartDeliveryMode.equalsIgnoreCase(msSameDayLocal)){// database
				if(msDeliveryMode.equalsIgnoreCase(msSameDay)){// present selected delivery mode
				
					/*
					 * check date diff;
					 * 
					 */
					return checkDeliveryDate();
				} else {
					showDelievryModeDialog();
					return false;
				}

			} else if (msCartDeliveryMode.equalsIgnoreCase(msNextDayLocal)) {// database
				if(msDeliveryMode.equalsIgnoreCase(msNextDayDelivery)){// present selected delivery mode
					
					/*
					 * check date diff;
					 * 
					 */
					return checkDeliveryDate();
				} else {
					showDelievryModeDialog();
					return false;
				}
			} else if (msCartDeliveryMode.equalsIgnoreCase(msVirtualDeliveryLocal)) {// database
				if(msDeliveryMode.equalsIgnoreCase(msVirtualDelivery)){// present selected delivery mode
					
					/*
					 * check date diff;
					 * 
					 */
					return checkDeliveryDate();
				} else {
					showDelievryModeDialog();
					return false;
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			showToast("Somthing went wrong! Please try again");
		}
		return false;
	}

	
	/**
	 * Check if previously added products delivery date and selected product delivery date is different or same
	 * 
	 * @return
	 */
	private boolean checkDeliveryDate(){
		try{
			String selectedDate = deliveryDat;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = format.parse(msCartDate);
			Date date2 = format.parse(selectedDate);
			long mlLong = dateDiff(date2, date1);
			LogFile.LogData("Selected date d2 "+selectedDate+" Cart date "+msCartDate);
			LogFile.LogData("Difference "+mlLong);
			
			if(mlLong>0){
				mbIsDelieveryOption = false;
				showDateWarningDialog();
				return false;
			} else if(mlLong<0){
				mbIsDelieveryOption = false;
				showDateWarningDialog();
				return false;
			} else {
				//showToast("All fine");
				mbIsDelieveryOption = true;
				return true;

			}

		}catch(Exception ex){
			ex.printStackTrace();
			LogFile.LogData("Error "+ex.toString());
			showToast("Somthing went wrong! Please try again after some time.");
		}
		LogFile.LogData("deliveryDat "+deliveryDat);
		return false;
	}

	private void showDelievryModeDialog(){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.alert_dialog);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_continue);
		mtvContinue.setText("Delete Now");
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		TextView mtvCancel = (TextView) dateWarning.findViewById(R.id.text_cancel);
		mtvMessage.setText("Previously added products in your shopping cart has different delivery option.\n\n" +
				"Please delete them to add this product in your cart");
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mbIsDelieveryOption  = false;
				//txtAddToCart.performClick();
				dateWarning.dismiss();
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				ShoppingCartFragment chkFrag=new ShoppingCartFragment();
				transaction.add(R.id.content_frame, chkFrag,"SHOPPING CART");
				transaction.commit();
			}
		});
		mtvCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dateWarning.dismiss();

			}
		});
		/*	if(dateWarning.isShowing()){

		}else{
			dateWarning.show();	
		}
		 */
		dateWarning.show();	

	}

	private String getDateAfter(int days){

		String msTommDate = "";

		try{


			cal = Calendar.getInstance();
			cal.set(Calendar.YEAR,cal.get(Calendar.YEAR));
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+days);

			Date d2= cal.getTime();

			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			deliveryDat = dt.format(cal.getTime());
			msTommDate = dtReview.format(cal.getTime());



		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msTommDate;
	}

	private void updateDeliveryOptions(){
		
		LogFile.LogData("updateDeliveryOptions()");
		String msWhere = DatabaseHelper.USER_ID+"='"+mUserId+"'" ;
		ContentValues value = new ContentValues();

		value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE, deliveryDat);
		if(msDeliveryMode.equalsIgnoreCase(msSameDay)){
			Config.DELIVERY_MODE = msSameDayLocal;
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msSameDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNextDayDelivery)){
			Config.DELIVERY_MODE = msNextDayLocal;
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNextDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNormalDelivery)){
			Config.DELIVERY_MODE = msNormalDeliveryLocal;
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNormalDeliveryLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msVirtualDelivery)){
			Config.DELIVERY_MODE = msVirtualDeliveryLocal;
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msVirtualDeliveryLocal);
		} else {
			
		}
		
		LogFile.LogData("Delievery Mode "+msCartDeliveryMode);

		try{
			showProgressDialog();
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete "+response);
					//updateOrdeInfo();
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_SHOPPING_CART, Config.UPDATE_SHOPPING_TABLE, msWhere, "updateShopping",value).execute();

		}catch(Exception ex){
			ex.printStackTrace();
			callCartFragment();
		}
	}





	/*private void checkCartDateInfo(ArrayList<ShoppingCartModel> response){
		try{
			LogFile.LogData("Response size "+response.size());
			if(response.size()!=0){
				marrCartData = response;
				miCartItems = response.size();
				for(int i=0;i<response.size();i++){

					String msDate1 = response.get(i).getDelivery_date();
					String selectedDate = deliveryDat;
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date date1 = format.parse(msDate1);
					Date date2 = format.parse(selectedDate);
					if(dateDiff(date1, date2)!=0){
						dateDifference = true;
						break;
					}
				}
			}else{
				//showToast("No data in cart");
				miCartItems = 0;
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}


	}*/
	
	
	
	
}
