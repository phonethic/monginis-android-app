package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.phonethics.adapters.MonginisConnectAdapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class MonginisConnectFragment extends Fragment implements OnItemClickListener{

	private View mView;
	private ListView connectList;
	private ArrayList<String> mArrConnectList = new ArrayList<String>();
	private ArrayList<Integer> mArrDrawables = new ArrayList<Integer>();
	MonginisConnectAdapter mAdapter;
	TextView txtConnect;
	Typeface tf;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_monginis_connect, null);
		connectList=(ListView)mView.findViewById(R.id.connectList);
		txtConnect=(TextView)mView.findViewById(R.id.txtConnect);

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		txtConnect.setTypeface(tf);
		
		//text array
		mArrConnectList.add("FACEBOOK");
		mArrConnectList.add("TWITTER");
		mArrConnectList.add("GOOGLE PLUS");
		mArrConnectList.add("PINTEREST");
		mArrConnectList.add("Linkedin");
		mArrConnectList.add("1800-729-1800");
		

		//drawable array
		mArrDrawables.add(R.drawable.monginis_connect_fb);
		mArrDrawables.add(R.drawable.monginis_connect_twitter);
		mArrDrawables.add(R.drawable.monginis_connect_googleplus);
		mArrDrawables.add(R.drawable.monginis_connect_pinterest);
		mArrDrawables.add(R.drawable.monginis_connect_linkedin);
		mArrDrawables.add(R.drawable.monginis_connect_call);

		mAdapter = new MonginisConnectAdapter(getActivity(), R.drawable.ic_launcher, R.layout.layout_categ_row, mArrConnectList, mArrDrawables);
		connectList.setAdapter(mAdapter);

		connectList.setOnItemClickListener(this);
		return mView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

		if(position!=mArrConnectList.size()-1){
			Intent intent = new Intent(getActivity(), WebLinkView.class);
			intent.putExtra("position", position);
			startActivity(intent);
		}
		else{

			String callNumber = mArrConnectList.get(position);
			callNumber = callNumber.replace("-", "");
			callNumber = callNumber.trim();
			
			Map<String, String> param = new HashMap<String, String>();
			param.put("Platform", "Call");
			EventTracker.logEvent(EventsName.SOICIAL_CONNECT,param);
			
			Intent caller = new Intent(android.content.Intent.ACTION_DIAL);
			caller.setData(Uri.parse("tel:" +callNumber));
			startActivity(caller);
		}

	}

}
