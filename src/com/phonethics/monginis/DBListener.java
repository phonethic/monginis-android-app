package com.phonethics.monginis;

import java.util.ArrayList;

import com.phonethics.model.CartProductInfo;

public interface DBListener {

    public void onCompleteOperation(String tag);
    public void onCompleteRetrival(ArrayList<String> mArr,String tag);
    //public void onCompleteData(ArrayList<Object> mArr,String tag);
    //public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr, String tag);
    public void rowCount(long rowCount,String tag);
  
    public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr, String tag);

    

}
