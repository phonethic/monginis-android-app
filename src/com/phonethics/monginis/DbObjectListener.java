package com.phonethics.monginis;

import java.util.ArrayList;

public class DbObjectListener<T> {

	public interface OnCompleteListner<T>{
		 public void onComplete(ArrayList<T> response,String msTag);
	}
	
	public interface OnComplete<T>{
		 public void onComplete(String response,String msTag);
	}
}
