package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.model.CartProductInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginSignUpFragment extends Fragment {

	private View mView;
	private TextView signUpBtn;
	private TextView loginBtn;
	private EditText txtPassword;
	private EditText txtEmail;
	private ProgressDialog mProgressDialog;
	String success;
	String message;
	SessionManager mSessionManager;
	private boolean mbFromContinue = false;
	String TAG = "";
	TextView mSkip;
	Map<String, String> param = new HashMap<String, String>();
	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");
		mSessionManager=new SessionManager(getActivity());
		
		

		Bundle mBundle = getArguments();
		if(mBundle!=null){
			mbFromContinue  = mBundle.getBoolean("FromContinue",false);
			TAG = mBundle.getString("tag");
			//showToast(TAG);
			
			if(mbFromContinue){
				param.put("from", "continueAs");
			} else if(TAG != null && TAG.equalsIgnoreCase("fromSplash")){
				param.put("from", "splash");
			} else {
				param.put("from", "menuOptions");
			}
			
		}else{
			param.put("from", "menuOptions");
		}
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		

	}
	


	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_login_signup, null);
		txtEmail =(EditText)mView.findViewById(R.id.txtEmail);
		txtPassword=(EditText)mView.findViewById(R.id.txtPassword);
		loginBtn=(TextView)mView.findViewById(R.id.loginBtn);
		signUpBtn=(TextView)mView.findViewById(R.id.signUpBtn);
		mSkip=(TextView)mView.findViewById(R.id.mSkip);

		/*void hideKeyBoard(){
			InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
					getActivity().INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow((IBinder) getActivity().getCurrentFocus(), 0);
		}*/
		
		txtEmail.setTypeface(MonginisApplicationClass.getTypeFace());
		txtPassword.setTypeface(MonginisApplicationClass.getTypeFace());
		loginBtn.setTypeface(MonginisApplicationClass.getTypeFace());
		signUpBtn.setTypeface(MonginisApplicationClass.getTypeFace());
		mSkip.setTypeface(MonginisApplicationClass.getTypeFace());
		

		
		if(TAG != null && TAG.equalsIgnoreCase("fromSplash")){

			LandingScreen.HideNavigation();
			mSkip.setVisibility(View.VISIBLE);

			mSkip.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
					LandingScreen.showNavigation();
					FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//To remove all Stack behind
					getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

					MainCategoriesFragment chkFrag=new MainCategoriesFragment();
					transaction.add(R.id.content_frame, chkFrag,"SKIP");
					//transaction.addToBackStack("SIGNUP");
					// Commit the transaction
					transaction.commit();
					EventTracker.logEvent(EventsName.SKIP_FROM_SPLASH);
				}
			});
		}

		signUpBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				EventTracker.logEvent(EventsName.SIGNUP,param);
				SignUpFragment chkFrag=new SignUpFragment();
				
				transaction.add(R.id.content_frame, chkFrag,"SIGNUP");
				transaction.addToBackStack("SIGNUP");
				//if(TAG!=null && TAG.length()!=0){
					Bundle bi=new Bundle();
					bi.putString("tag","");
					bi.putBoolean("fromContinue", mbFromContinue);
					chkFrag.setArguments(bi);	
				//}
				
				// Commit the transaction
				transaction.commit();
			}
		});

		loginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

				String emailId = txtEmail.getText().toString();
				String password = txtPassword.getText().toString();
				if(emailId.length()==0){
					showToast("Please enter Email Id");
				}
				else if(!SignUpFragment.isEmailValid(emailId)){
					showToast("Please enter a valid Email Id");
				}
				else if(password.length()==0){
					showToast("Please enter password");
				}
				else{
					//showToast("Call Login Api");
					callLoginApi();
				}
			}
		});
		return mView;
	}

	void callLoginApi() {
		// TODO Auto-generated method stub

		try {
			showProgressDialog();
			String TAG="USER LOGIN";
			String url=Config.USER_LOGIN;
			Config.Log("URL","URL "+url);

			//adding body to Login
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("email",txtEmail.getText().toString());
			jsonObj.put("password",txtPassword.getText().toString());

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							String USER_ID="";
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							success = json.getString("success");
							message = json.getString("message");
							if(success.equalsIgnoreCase("true")){

								
								//added for cart
								mSessionManager.setRefreshNeeded(true);
								//showToast(message);
								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject obj = data.getJSONObject(i);
									USER_ID = obj.getString("user_id");
								}

								if(mbFromContinue){

								} else {
									//deleteOrderInfoRecords();	
								}

								mSessionManager.createLoginSessionCustomer(txtEmail.getText().toString(), txtPassword.getText().toString(), USER_ID, "");
								showAlertDialog();
							}
							else{

								showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			EventTracker.logEvent(EventsName.LOGIN, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

	private void showAlertDialog() {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {

				if(!mbFromContinue){

					if(TAG.equalsIgnoreCase("fromSplash")){
						LandingScreen.showNavigation();
						FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
						transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
						getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
						MainCategoriesFragment chkFrag=new MainCategoriesFragment();
						transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
						transaction.commit();
					}
					else{
						FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
						transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
						getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
						MenuOptionFragment chkFrag=new MenuOptionFragment();
						transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
						transaction.commit();	
					}

				}
				else {
					//getActivity().getSupportFragmentManager().popBackStack();

					FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//To remove all Stack behind
					getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

					RedeemFragment chkFrag =new RedeemFragment();
					Bundle mbundle = new Bundle();
					mbundle.putBoolean("FromContinue", true);
					chkFrag.setArguments(mbundle);
					transaction.add(R.id.content_frame, chkFrag,"ContinueAs");
					transaction.addToBackStack("ContinueAs");
					transaction.commit();


				}

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}


	/*void deleteOrderInfoRecords(){
		LogFile.LogData("deleteOrderInfoRecords");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"++"'";
		try{
			new AsyncDatabaseQuery(getActivity(),new DBListener() {

				@Override
				public void rowCount(long rowCount, String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
						String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onCompleteOperation(String tag) {
					// TODO Auto-generated method stub

				}
			}, DatabaseHelper.TABLE_ORDERS,
			"", Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}*/

	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventTracker.startFlurrySession(getActivity());
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(getActivity());
	}

	

}
