package com.phonethics.monginis;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContinueAs extends Fragment {


	private View mView;
	private SessionManager mSessionManager;
	String mShippingCharges = "";


	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Bundle mBundle = getArguments();
		if(mBundle!=null){
			mShippingCharges = mBundle.getString("ShippingCharges");
		}
		

	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (View)inflater.inflate(R.layout.continue_as, null);

		TextView mtvContinue = (TextView) mView.findViewById(R.id.txtguest);
		TextView mtvLogin = (TextView) mView.findViewById(R.id.txtLoginSign);
		
		mtvContinue.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvLogin.setTypeface(MonginisApplicationClass.getTypeFace());

		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				final Map<String, String> param = new HashMap<String, String>();
				param.put("Continue_as", "Guest");
				EventTracker.logEvent(EventsName.CONITNUE_AS, param);
				RedeemFragment chkFrag =new RedeemFragment();
				transaction.add(R.id.content_frame, chkFrag,"ContinueAs");
				transaction.addToBackStack("ContinueAs");
				transaction.commit();
				
				
				
			}
		});
		
		mtvLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				final Map<String, String> param = new HashMap<String, String>();
				param.put("Continue_as", "Login/SignUp");  
				EventTracker.logEvent(EventsName.CONITNUE_AS, param);
				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				LoginSignUpFragment chkFrag=new LoginSignUpFragment();
				transaction.add(R.id.content_frame, chkFrag,"LOGIN SIGNUP");
				Bundle mbundle = new Bundle();
				mbundle.putBoolean("FromContinue", true);
				chkFrag.setArguments(mbundle);
				transaction.addToBackStack("LOGIN SIGNUP");
				// Commit the transaction
				transaction.commit();
			}
			
		});



		return mView;

	}

}
