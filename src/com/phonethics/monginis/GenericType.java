package com.phonethics.monginis;

public class GenericType<T> {

	private T t;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;	
	}
	
}
