package com.phonethics.monginis;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashFragment extends Fragment {

	private String msUrl;
	private String msTitle;

	private String msId;

	private View mView;
	private ImageView mImage;


	public SplashFragment()
	{

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public static SplashFragment newInstance(String url,String id,String title)
	{
		SplashFragment sp=new SplashFragment();
		Bundle bdl=new Bundle();
		bdl.putString("url", url);
		bdl.putString("id",id);
		bdl.putString("title", title);
		sp.setArguments(bdl);
		return sp;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		msUrl=getArguments().getString("url");
		msId=getArguments().getString("id");
		msTitle=getArguments().getString("title");

		mView=(View)inflater.inflate(R.layout.layout_splashpager, container, false);
		mImage=(ImageView)mView.findViewById(R.id.imgSplash);
		mImage.setImageResource(Integer.parseInt(msId.replaceAll("drawable://", "")));
		final Animation anim=AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
		mImage.startAnimation(anim);
//		TranslateAnimation animation = new TranslateAnimation(0.0f, 0.0f,
//				400.0f, 0.0f);
//		animation.setDuration(5000);
//		animation.setRepeatCount(5);
//		animation.setRepeatMode(2);
//		animation.setFillAfter(false);
//		mImage.startAnimation(animation);
//				imageLoader.displayImage(msId, mImage, options, new ImageLoadingListener() {
//
//					@Override
//					public void onLoadingCancelled(String arg0, View arg1) {
//						
//					}
//
//					@Override
//					public void onLoadingComplete(String arg0, View arg1,
//							Bitmap arg2) {
//						mImage.startAnimation(anim);
//					}
//
//					@Override
//					public void onLoadingFailed(String arg0, View arg1,
//							FailReason arg2) {
//						
//					}
//
//					@Override
//					public void onLoadingStarted(String arg0, View arg1) {
//						
//					}
//					
//				});
		return mView;
	}



}
