package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.ProductListAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.ProductModel;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

public class SearchFragment extends Fragment implements OnItemClickListener,DBListener, OnScrollListener{

	private View mView;
	private EditText mGlobalSearchBox;
	private ListView mSearchProductList;
	private ProgressDialog mProgressDialog;
	ArrayList<ProductModel> mProdArr = new ArrayList<ProductModel>();
	private ProductListAdapter mAdapter;
	private ArrayList<String> mArrProductsIds = new ArrayList<String>();
	String sub_catId="";
	private ArrayList<ProductModel> mFetchedData = new ArrayList<ProductModel>();

	int OFFSET = 0;
	String URL_SECOND = "/count/20/src_string/";
	boolean noProduct = false;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mView=(View)inflater.inflate(R.layout.fragment_search, null);
		mGlobalSearchBox=(EditText)mView.findViewById(R.id.mGlobalSearchBox);
		mSearchProductList=(ListView)mView.findViewById(R.id.mSearchProductList);

		mGlobalSearchBox.setTypeface(MonginisApplicationClass.getTypeFace());



		Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);
		mGlobalSearchBox.startAnimation(anim);

		mSearchProductList.setOnScrollListener(this);
		mGlobalSearchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					//showToast("api called");
					//noProduct = false;
					if(mGlobalSearchBox.getText().length()!=0){

						
						OFFSET = 0;
						mProdArr.clear();
						mArrProductsIds.clear();
						deleteOldRecords();
						//boolean mChk = mGlobalSearchBox.getText().toString().matches(".*[a-zA-Z]+.*");
						Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
						Matcher m = p.matcher( mGlobalSearchBox.getText().toString());
						Config.Log("CHK","CHK " + m.find());
						if(!m.find())
							callSearchApi(mGlobalSearchBox.getText().toString());
						else
						{
							showToast("Please enter alphanumeric values only");
							InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
							inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

						}

					}
					else{
						showToast("Please enter your search keyword");
					}

					return true;
				}
				return false;
			}

		});

		mAdapter=new ProductListAdapter(getActivity(), 0, mProdArr, 0);
		mSearchProductList.setAdapter(mAdapter);
		
		mSearchProductList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				try {
					
					FragmentManager manager=getActivity().getSupportFragmentManager();
					FragmentTransaction transaction =manager.beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

					SearchedProductDetailPagerFragment subFrag=new SearchedProductDetailPagerFragment();
					Bundle b=new Bundle();
					b.putParcelableArrayList("Searched Products", mFetchedData);
					b.putInt("position", position);
					subFrag.setArguments(b);
					transaction.add(R.id.content_frame, subFrag, "Detils");
					transaction.addToBackStack(null);
					Config.SUB_CATEGORY_SELECTED_PRODUCT_ID = mArrProductsIds.get(position);
					// Commit the transaction
					transaction.commit();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		return mView;
	}

	void callSearchApi(String searchString) {
		// TODO Auto-generated method stub

		try {
			showProgressDialog();
			String TAG="PRODUCT_ATTRIBUTES";
			String url=Config.SEARCH_PRODUCTS_WITHOUT_CITY+ "" + OFFSET + URL_SECOND + searchString;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
							inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){
								noProduct = false;
								JSONArray data = json.getJSONArray("data");
								ContentValues[] conValues = new ContentValues[data.length()];
								for(int i=0;i<data.length();i++){
									ContentValues value = new ContentValues();
									JSONObject values = data.getJSONObject(i);
									ProductModel pObjModel = new ProductModel();
									pObjModel.setId(values.getString("id"));
									pObjModel.setSubcat_id(values.getString("cat_id"));
									pObjModel.setTitle(values.getString("title"));
									pObjModel.setDescription(values.getString("description"));
									pObjModel.setCode(values.getString("code"));
									pObjModel.setWeight_grams(values.getString("weight_grams"));
									pObjModel.setImage(values.getString("image"));
									pObjModel.setList_price(values.getString("list_price"));
									pObjModel.setOur_price(values.getString("our_price"));
									pObjModel.setSpecial_price(values.getString("special_price"));
									pObjModel.setEggless_type(values.getString("eggless_type"));
									pObjModel.setWithegg_type(values.getString("withegg_type"));
									pObjModel.setSameday_delivery(values.getString("sameday_delivery"));
									pObjModel.setNextday_delivery(values.getString("nextday_delivery"));
									pObjModel.setNormal_delivery(values.getString("normal_delivery"));
									pObjModel.setFree_delivery(values.getString("free_delivery"));
									//added by salman for cat_id
									pObjModel.setCat_id(values.getString("cat_id"));
									mProdArr.add(pObjModel);

									//get subCatId
									String temp_cat_id = pObjModel.getSubcat_id();
									boolean isMultiple = temp_cat_id.contains(",");
									if(isMultiple){
										int index = temp_cat_id.indexOf(",");
										temp_cat_id = temp_cat_id.substring(0, index);
										sub_catId = temp_cat_id;
									}
									else{
										sub_catId = temp_cat_id;
									}

									value.put(DatabaseHelper.PRODUCT_ID,pObjModel.getId());
									value.put(DatabaseHelper.PROD_SUBCAT_ID,sub_catId);
									value.put(DatabaseHelper.PROD_CODE,pObjModel.getCode());
									value.put(DatabaseHelper.PROD_DESCRIPTION,pObjModel.getDescription());
									value.put(DatabaseHelper.PROD_EGGLESS_TYPE,pObjModel.getEggless_type());
									value.put(DatabaseHelper.PROD_FREE_DELIVERY,pObjModel.getFree_delivery());
									value.put(DatabaseHelper.PROD_IMAGE,pObjModel.getImage());
									value.put(DatabaseHelper.PROD_LIST_PRCE,pObjModel.getList_price());
									value.put(DatabaseHelper.PROD_NEXTDAY_DELIVERY,pObjModel.getNextday_delivery());
									value.put(DatabaseHelper.PROD_NORMAL_DELIVERY,pObjModel.getNormal_delivery());
									value.put(DatabaseHelper.PROD_OUR_PRICE,pObjModel.getOur_price());
									value.put(DatabaseHelper.PROD_SAMEDAY_DELIVERY,pObjModel.getSameday_delivery());
									value.put(DatabaseHelper.PROD_SPECIAL_PRICE,pObjModel.getSpecial_price());
									value.put(DatabaseHelper.PROD_TITLE,pObjModel.getTitle());
									value.put(DatabaseHelper.PROD_WEIGHT_GRAMS,pObjModel.getWeight_grams());
									value.put(DatabaseHelper.PROD_WITHEGG_TYPE,pObjModel.getWithegg_type());
									value.put(DatabaseHelper.PROD_CAT_ID, pObjModel.getCat_id());
									conValues[i]=value;
								}
								setData();
								insertIntoDb(conValues);
							}
							else{

								String errorMsg = json.getString("message");

								if(OFFSET>0){
									showToast("No more product to show");
									noProduct = true;
								}
								else{

									showToast(errorMsg);
									mProdArr.clear();
									mArrProductsIds.clear();
									deleteOldRecords();
									mAdapter = null;
									//mSearchProductList.setAdapter(mAdapter);
									mSearchProductList.setVisibility(View.GONE);
								}

							}
						}
						else{


						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void setData(){
		dismissDialog();

		mSearchProductList.setVisibility(View.VISIBLE);
		//		mAdapter = null;
		//		mAdapter=new ProductListAdapter(getActivity(), 0, mArrProducts, 0);
		//		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_layout_controller);
		//		mSearchProductList.setAdapter(mAdapter);
		//		mSearchProductList.setLayoutAnimation(controller);
		if(mAdapter!=null){
			mAdapter.notifyDataSetChanged();	
		}else{
			mAdapter=new ProductListAdapter(getActivity(), 0, mProdArr, 0);
			mSearchProductList.setAdapter(mAdapter);
		}
		
		//mSearchProductList.setOnItemClickListener(this);
		mSearchProductList.setVisibility(View.VISIBLE);

		if(OFFSET>0){
			mArrProductsIds.clear();
			for(int i=0;i<mProdArr.size();i++)
				mArrProductsIds.add(mProdArr.get(i).getId());

		}
		else{
			for(int i=0;i<mProdArr.size();i++)
				mArrProductsIds.add(mProdArr.get(i).getId());

		}


	}

	void showToast(String msg){
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		//		FragmentManager manager=getActivity().getSupportFragmentManager();
		//		FragmentTransaction transaction =manager.beginTransaction();
		//		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		//
		//		ProductDetailPagerFragment subFrag=new ProductDetailPagerFragment();
		//		Bundle b=new Bundle();
		//		b.putStringArrayList("CategoryId", mArrProductsIds);
		//		b.putInt("position", position);
		//		subFrag.setArguments(b);
		//		transaction.add(R.id.content_frame, subFrag, "Detils");
		//		transaction.addToBackStack(null);
		//		Config.SUB_CATEGORY_SELECTED_PRODUCT_ID = mArrProductsIds.get(position);
		//		// Commit the transaction
		//		transaction.commit();

		FragmentManager manager=getActivity().getSupportFragmentManager();
		FragmentTransaction transaction =manager.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

		SearchedProductDetailPagerFragment subFrag=new SearchedProductDetailPagerFragment();
		Bundle b=new Bundle();
		b.putParcelableArrayList("Searched Products", mFetchedData);
		b.putInt("position", position);
		subFrag.setArguments(b);
		transaction.add(R.id.content_frame, subFrag, "Detils");
		transaction.addToBackStack(null);
		Config.SUB_CATEGORY_SELECTED_PRODUCT_ID = mArrProductsIds.get(position);
		// Commit the transaction
		transaction.commit();


	}

	void deleteOldRecords(){
		LogFile.LogData("deleteOldRecords");
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SEARCHED_PRODUCT_DETAILS,
					null, Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	void insertIntoDb(ContentValues[] contentValues ){
		LogFile.LogData("Prodcuts insertIntoDb");
		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_SEARCHED_PRODUCT_DETAILS,
					Config.INSERT_DATA, "Prodcuts").execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onCompleteOperation(String tag) {
		// TODO Auto-generated method stub

		if(tag.equalsIgnoreCase("Prodcuts")){
			LogFile.LogData("onCompleteOperation");
			fetchData();
		}
		if(tag.equalsIgnoreCase(Config.DELETE_DATA)){
			LogFile.LogData("DELETED ALL RECORDS");
		}
	}

	private void fetchData() {
		// TODO Auto-generated method stub

		try {
			new AsyncDatabaseObjectQuery<ProductModel>(getActivity(), new OnCompleteListner<ProductModel>() {

				@Override
				public void onComplete(ArrayList<ProductModel> response,
						String msTag) {
					// TODO Auto-generated method stub
					mFetchedData.clear();
					mFetchedData = response;
					Config.Log("RES","RES " + response.size());
					for(int i=0;i<response.size();i++){
						//mArrProductsIds.add(mArrProducts.get(i).getId());
						Config.Log("CAT_IDS ","CAT_IDS " + response.get(i).getCat_id());
					}
				}
			}, DatabaseHelper.TABLE_SEARCHED_PRODUCT_DETAILS, Config.RETRIVE_SEARCHED_PRODUCT_DATA, "", Config.RETRIVE_SEARCHED_PRODUCT_DATA)
			.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rowCount(long rowCount, String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		if (scrollState == SCROLL_STATE_IDLE) {
			if(mAdapter!=null){

				if (mSearchProductList.getLastVisiblePosition() == mAdapter.getCount() - 1) {
					//Update new data here
					// and then, notify your adapter 
					//adapter.notifyDataSetChanged();
					OFFSET = OFFSET + 1;
					if(!noProduct)
						callSearchApi(mGlobalSearchBox.getText().toString());
				}
			}

		}
	}
}
