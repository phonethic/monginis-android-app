
package com.phonethics.monginis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CartListAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.ProductModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;
import com.phonethics.monginis.R.layout;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Shows the list of products which has been added by the user in shopping cart. 
 * For non logged in users products are fetched from local database, 
 * where as for logged in user products are fetched from the server and than stores in local database.
 * 
 * @author Nitin
 *
 */
public class ShoppingCartFragment extends Fragment implements DBListener,ListClickListener{

	private View mView;
	private TextView txtCheckOut;
	private TextView txtContinueShop;
	private TextView txtAddOn;
	SessionManager mSessionManager;
	ListView mShoppingItems;
	private ProgressDialog mProgressDialog;
	private ArrayList<ShoppingCartModel> marrCartData = new ArrayList<ShoppingCartModel>();

	CartListAdapter cAdapter;
	int mTotalAmount;
	TextView txtTotalAmuont;
	String UserId="0";
	HashMap<String,String> userDetails = new HashMap<String, String>();
	private Dialog optionsDialog;
	private String grandTotal="";
	private LinearLayout mScroll;
	private String msDelievryDate = "";
	private String msDelievryMode ="";
	private String msNormalDelivery = "NORMAL DELIVERY (48 HRS.)";
	private String msNextDayDelivery = "Next Day";
	private String msSameDay = "Same Day";
	private String msVirtualDelivery = "Virtual Delivery";

	private TextView txtShippingAmount;
	private TextView txtShipping;
	private TextView txtTotal;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		/**
		 * Initialize the class variables
		 * 
		 */
		mSessionManager = new SessionManager(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");

		EventTracker.logEvent(EventsName.SHOPPING_CART_SCREEN);

		
		/**
		 * Chechk if user is logged in or not
		 * 
		 */
		if(mSessionManager.isLoggedInCustomer()) {
			
			/**
			 * fetch the users details from session.
			 * 
			 */
			userDetails = mSessionManager.getUserDetails();
			UserId = userDetails.get(SessionManager.USER_ID);
			
			/**
			 * Check if server request it needed or not.
			 * 
			 */
			if(mSessionManager.isRefreshNeeded()) {
				
				/**
				 * If refresh is true than delete the old records and get new oned from server.
				 * 
				 */
				deleteOldRecords();
			} else {
				
				/**
				 * Else check whether records are there or not in shopping cart for logged in user.
				 * 
				 */
				checkCachedData();	
			}

		} else {
			/**
			 * Else check whether records are there or not in shopping cart for guest user.
			 * 
			 */
			checkCachedData();	
		}



	}


	public void showProgressDialog() {
		if(mProgressDialog!=null) {
			if(!mProgressDialog.isShowing()) {
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog() {
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		

		/**
		 * Initialize the layout
		 * 
		 */

		mView=(View)inflater.inflate(R.layout.fragment_shopping_cart, null);
		mScroll = (LinearLayout) mView.findViewById(R.id.mScroll);
		txtCheckOut=(TextView)mView.findViewById(R.id.txtCheckOut);
		txtAddOn=(TextView)mView.findViewById(R.id.txtAddOn);
		txtShipping=(TextView)mView.findViewById(R.id.txtShipping);
		txtTotal=(TextView)mView.findViewById(R.id.txtTotal);
		txtContinueShop=(TextView)mView.findViewById(R.id.txtContinueShop);
		mShoppingItems=(ListView)mView.findViewById(R.id.mShoppingItems);
		txtTotalAmuont=(TextView)mView.findViewById(R.id.txtTotalAmuont);
		txtShippingAmount=(TextView)mView.findViewById(R.id.txtShippingAmount);

		txtContinueShop.setVisibility(View.GONE);
		txtAddOn.setVisibility(View.GONE);	

		mScroll.setVisibility(View.GONE);
		
		txtCheckOut.setTypeface(MonginisApplicationClass.getTypeFace());
		txtAddOn.setTypeface(MonginisApplicationClass.getTypeFace());
		txtContinueShop.setTypeface(MonginisApplicationClass.getTypeFace());
		txtTotalAmuont.setTypeface(MonginisApplicationClass.getTypeFace());
		txtShippingAmount.setTypeface(MonginisApplicationClass.getTypeFace());
		txtTotal.setTypeface(MonginisApplicationClass.getTypeFace());
		txtShipping.setTypeface(MonginisApplicationClass.getTypeFace());
		

		txtAddOn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

				TryAddOnsFragment chkFrag=new TryAddOnsFragment();
				transaction.add(R.id.content_frame, chkFrag,"TRY ADD ONS");
				transaction.addToBackStack("TRY ADD ONS");
				// Commit the transaction
				transaction.commit();
			}
		});

		txtCheckOut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EventTracker.logEvent(EventsName.SHOPPING_CART_CONTINUE);
				updateOrdeInfo();

			}

		});

		return mView;
	}


	/**
	 * Starts the next fragment, for logged in user it will initialze the Redeem fragment and for guest user it will initialize the ContinueAs fragment
	 * 
	 */
	void callNextragment(){
		
		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
	
		if(mSessionManager.isLoggedInCustomer()){

			RedeemFragment chkFrag =new RedeemFragment();
			transaction.add(R.id.content_frame, chkFrag,"ShoppingCart");
			transaction.addToBackStack("ShoppingCart");
			transaction.commit();
			
		} else {

			ContinueAs continueAs =new ContinueAs();
			transaction.add(R.id.content_frame, continueAs,"ShoppingCart");
			transaction.addToBackStack("ShoppingCart");
			transaction.commit();
		}

	}

	@Override
	public void onCompleteOperation(String tag) {
		// TODO Auto-generated method stub
		LogFile.LogData("onCompleteOperation");
		if(tag.equalsIgnoreCase("ShoppingCart")){
			if(mSessionManager.isLoggedInCustomer()){
				//updateDeliveryOptions();
			}
			fetchData();
		}else if(tag.equalsIgnoreCase(Config.DELETE_DATA)){
			checkCachedData();
		} else if (tag.equalsIgnoreCase("DeleteThisItem")){
			checkCachedData();
		}
		else if(tag.equalsIgnoreCase("OrderData")){
			//showToast("inserted");
		}
		else if(tag.equalsIgnoreCase("Delete OrderTable")){
			//showToast("inserted");
		}
	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rowCount(long rowCount, String tag) {
		// TODO Auto-generated method stub
		LogFile.LogData("rowCount");
		if(tag.equalsIgnoreCase(Config.ROW_COUNT_WHERE)){
			if(rowCount==0){
				
				/**
				 * If there are no products for user in shopping cart than get them from server
				 * 
				 */
				getCartDetails();
			}else{
				
				/**
				 * Else fetch the records from database the set the list
				 */
				fetchData();
			}
			
			checkCachedOrderData();
			
		} else if(tag.equalsIgnoreCase(Config.ROW_COUNT)){
			if(rowCount==0){
				insertOrderInfo();
			}else{

			}
		}
	}



	/**
	 * Db query to delete all the products from shopping cart table
	 */
	void deleteOldRecords(){
		LogFile.LogData("deleteOldRecords");
		String msWhere = DatabaseHelper.USER_ID+" ='"+UserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SHOPPING_CART,
					msWhere, Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * Db query to check the count of shopping cart table for logged in or geust user
	 * 
	 */
	void checkCachedData(){
		LogFile.LogData("checkCachedData");
		String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"+UserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SHOPPING_CART,
					DatabaseHelper.AUTO_ID, Config.ROW_COUNT_WHERE,  Config.ROW_COUNT_WHERE,msWhere).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * Network request to get shopping cart products from server
	 * 
	 */
	void getCartDetails(){
		LogFile.LogData("getCartDetails");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.GET_CART_PRODUCTS+UserId;
		LogFile.LogData("URL "+url);
		showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {



			@Override
			public void onResponse(JSONObject response) {
				try
				{
					dismissDialog();
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){

						if(status.equalsIgnoreCase("true")){
							JSONArray jsonArr=response.getJSONArray("data");
							ContentValues[] values = new ContentValues[jsonArr.length()];
							for(int i=0;i<jsonArr.length();i++){
								
								
								/**
								 * Parse the information
								 */
								ContentValues value = new ContentValues();
								JSONObject jsonObject=jsonArr.getJSONObject(i);

								ShoppingCartModel mShopCartModel = new ShoppingCartModel();

								mShopCartModel.setImage(jsonObject.getString("image"));
								mShopCartModel.setProduct_id(jsonObject.getString("product_id"));
								mShopCartModel.setQuantity(jsonObject.getString("quantity"));
								mShopCartModel.setSpecial_price(jsonObject.getString("special_price"));
								mShopCartModel.setTitle(jsonObject.getString("title"));
								mShopCartModel.setMessage(jsonObject.getString("message"));
								mShopCartModel.setDelivery_date(jsonObject.getString("delivery_date"));

								msDelievryDate = jsonObject.getString("delivery_date");

								String msProductTypeEgless = jsonObject.getString("eggless_type");
								String msProductTypeWithEgg = jsonObject.getString("withegg_type");

								if(msProductTypeEgless.equalsIgnoreCase("Y")){
									mShopCartModel.setProduct_type("eggless_type");
								} else if(msProductTypeWithEgg.equalsIgnoreCase("Y")){
									mShopCartModel.setProduct_type("withegg_type");
								}

								String msSamedayDelievey = jsonObject.getString("sameday_delivery");
								String msNextdayDelievey = jsonObject.getString("nextday_delivery");
								String msNormalDelievey = jsonObject.getString("normal_delivery");
								String msFreeDelievey = jsonObject.getString("free_delivery");

								if(msSamedayDelievey.equalsIgnoreCase("Y")){
									mShopCartModel.setDelivery_mode("sameday_delivery");
									msDelievryMode = msSameDay;
								} else if(msNextdayDelievey.equalsIgnoreCase("Y")){
									mShopCartModel.setDelivery_mode("nextday_delivery");
									msDelievryMode = msNextDayDelivery;
								} else if(msNormalDelievey.equalsIgnoreCase("Y")){
									mShopCartModel.setDelivery_mode("normal_delivery");
									msDelievryMode = msNormalDelivery;
								} else if(msFreeDelievey.equalsIgnoreCase("Y")){
									mShopCartModel.setDelivery_mode("free_delivery");
									msDelievryMode = msVirtualDelivery;
								}

								String msDeliverDate = mShopCartModel.getDelivery_date();
								DateFormat  dtSource=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								DateFormat  dtDestination=new SimpleDateFormat("yyyy-MM-dd");

								Date date = dtSource.parse(msDeliverDate);

								msDeliverDate = dtDestination.format(date);

								msDelievryDate = dtDestination.format(date);
								LogFile.LogData("Delivery Date "+msDelievryDate);

								value.put(DatabaseHelper.PRODUCT_ID,mShopCartModel.getProduct_id());
								value.put(DatabaseHelper.QUANTITY,mShopCartModel.getQuantity());
								value.put(DatabaseHelper.BASE_AMOUNT,mShopCartModel.getSpecial_price());
								value.put(DatabaseHelper.PRODUCT_IMG_URL,mShopCartModel.getImage());
								value.put(DatabaseHelper.PRODUCT_NAME,mShopCartModel.getTitle());
								value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE,msDeliverDate);	
								value.put(DatabaseHelper.MESSAGE,mShopCartModel.getMessage());
								value.put(DatabaseHelper.USER_ID,UserId);
								value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,mShopCartModel.getDelivery_mode());
								value.put(DatabaseHelper.PRODUCT_TYPE,mShopCartModel.getProduct_type());
								//value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE,mShopCartModel.getDelivery_date());

								values[i]=value;

								Config.Log("SUB CAT ", "SUB CAT "+jsonObject.getString("title"));
							}
						
							
							/**
							 * insert all the values into Db 
							 */
							insertIntoDb(values);


							//setData();
						}else{

							
							/**
							 * If user don't have any products added in shopping cart than prompt to start shopping.
							 * 
							 */
							if(status.equalsIgnoreCase("false")){
								showNoCartDilaog();
								mScroll.setVisibility(View.GONE);
								//showToast(response.getString("message"));
								dismissDialog();
								marrCartData.clear();
								cAdapter = new CartListAdapter(getActivity(), R.drawable.ic_launcher,marrCartData, ShoppingCartFragment.this);
								mShoppingItems.setAdapter(cAdapter);
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}
		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}


	
	/**
	 * Network request to delete the product from shoppping cart
	 * 
	 * @param productId
	 */
	void deleteProduct(String productId){
		LogFile.LogData("deleteProduct");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.DELETE_SELECTED_CART_PRODUCTS+UserId+"&products="+productId;
		LogFile.LogData("URL "+url);
		showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.DELETE,url,null,new Response.Listener<JSONObject>() {




			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					LogFile.LogData("Delete Response "+response.toString());
					if(response!=null){
						String status=response.getString("success");
						if(status.equalsIgnoreCase("true")){
							deleteOldRecords();
						}

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}

		}){


			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}


	/**
	 * Insert all the values of shopping cart into database.
	 * 
	 * @param contentValues
	 */
	void insertIntoDb(ContentValues[] contentValues ){
		LogFile.LogData("Cart insertIntoDb");
		try{
			showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_SHOPPING_CART,
					Config.INSERT_DATA, "ShoppingCart").execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * Db query to get the product from shopping cart and show them in list
	 * 
	 */
	void fetchData(){

		showProgressDialog();
		LogFile.LogData("fetchData");	
		String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='"+UserId+"' ";
		new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {

			@Override
			public void onComplete(ArrayList<ShoppingCartModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				
				try {
					boolean NoShipping = false;
					boolean sameDayShipping = false;
					boolean nextDayShipping = false;
					boolean normalDelivery = false;
					dismissDialog();
					mScroll.setVisibility(View.VISIBLE);
					marrCartData = response;
					cAdapter = new CartListAdapter(getActivity(), R.drawable.ic_launcher,response, ShoppingCartFragment.this);
					mShoppingItems.setAdapter(cAdapter);
					mShoppingItems.setVisibility(View.VISIBLE);

					int miCartPrice = 0;
					for(int i=0;i<response.size();i++){
						int miquantity = Integer.parseInt(response.get(i).getQuantity());
						int miBaseAmount = Integer.parseInt(response.get(i).getSpecial_price());
						int miTotalPrice = miquantity * miBaseAmount;
						miCartPrice = miCartPrice + miTotalPrice;
						
					}
					
					//added by salman for free shipping check
					for(int i=0;i<response.size();i++){
						Config.Log("DELIVERY:","DELIVERY: " +response.get(i).getDelivery_mode());
						if(response.get(i).getDelivery_mode().equalsIgnoreCase("free_delivery")){
							Config.Log("SHIPPING_CHARGE","FREE");
							NoShipping = true;
							break;
						} else if(response.get(i).getDelivery_mode().equalsIgnoreCase("sameday_delivery")){
							sameDayShipping = true;
							break;
						}else if(response.get(i).getDelivery_mode().equalsIgnoreCase("nextday_delivery")){
							Config.Log("SHIPPING_CHARGE","NEXT DAY");
							nextDayShipping = true;
							break;
						}
						else if(response.get(i).getDelivery_mode().equalsIgnoreCase("normal_delivery")){
							Config.Log("SHIPPING_CHARGE","");
							//NoShipping = false;
							normalDelivery = true;
							break;
						}
					}
					
					 if(sameDayShipping){
						
						grandTotal = miCartPrice+200+"";	
						txtShippingAmount.setText("200");
						Config.SHIPPING_PRICE = 200;
						
					} else if(normalDelivery){
						
						grandTotal = miCartPrice+100+"";	
						txtShippingAmount.setText("100");
						Config.SHIPPING_PRICE = 100;
						
					} else if(nextDayShipping){
						
						grandTotal = miCartPrice+150+"";	
						txtShippingAmount.setText("150");
						Config.SHIPPING_PRICE = 150;
					}
					else {
						
						txtShippingAmount.setText("0");
						grandTotal = miCartPrice+"";
						Config.SHIPPING_PRICE = 0;
					}
					
					txtTotalAmuont.setText(miCartPrice+"");
					Config.GARND_TOTAL = grandTotal;
					Config.TOTAL_AMOUNT = Config.GARND_TOTAL;  

					//showToast("GT " + Config.GARND_TOTAL);
					//Config.TOTAL_AMOUNT = "1";
					mSessionManager.setRefreshNeeded(false);
					//updateOrdeInfo();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				

			}
		},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();


	}

	
	
	private void insertOrderInfo(){
		
		//showToast("inserted");
		LogFile.LogData("insertOrderInfo()");
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.USER_ID, UserId);
		value.put(DatabaseHelper.ORDER_ID, "");
		value.put(DatabaseHelper.PRODUCT_AMOUNT, "");
		value.put(DatabaseHelper.SHIPPING_CHARGE,"");
		value.put(DatabaseHelper.COUPON_ID, "");
		value.put(DatabaseHelper.POINTS_REDEEM,"");
		value.put(DatabaseHelper.DELIVERY_DATE,msDelievryDate);
		value.put(DatabaseHelper.ORDER_NOTE,"");
		value.put(DatabaseHelper.TOTAL_AMOUNT,"");
		value.put(DatabaseHelper.MESSAGE_BILLING,"");
		value.put(DatabaseHelper.DELIVERY_MODE,msDelievryMode);


		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, value, DatabaseHelper.TABLE_ORDERS,
					Config.INSERT_SINGLE_DATA, "OrderData").execute();
		}catch(Exception ex){
			ex.printStackTrace();
			//callNextFragment();
		}
	}

	private void updateOrdeInfo(){

		//showToast("updated");
		LogFile.LogData("updateOrdeInfo");
		String msWhere = DatabaseHelper.USER_ID+"='"+UserId+"'" ;
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.PRODUCT_AMOUNT, txtTotalAmuont.getText().toString());
		value.put(DatabaseHelper.SHIPPING_CHARGE, txtShippingAmount.getText().toString());
		value.put(DatabaseHelper.TOTAL_AMOUNT, grandTotal);

		try{
			showProgressDialog();

			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete");
					dismissDialog();
					//showToast(response);
					if(response!=null && !response.equalsIgnoreCase("0")){
						callNextragment();
					}else{
						insertOrderInfo();
					}

				}
			}, DatabaseHelper.TABLE_ORDERS, Config.UPDATE_ORDER_TABLE, msWhere, "updateorder",value).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void checkCachedOrderData(){
		LogFile.LogData("checkCachedOrderData");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"+UserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_ORDERS,
					DatabaseHelper.AUTO_ID, Config.ROW_COUNT,  Config.ROW_COUNT,"").execute();

		}catch(Exception ex){
			ex.printStackTrace();
			showToast(getResources().getString(R.string.Parse));
			//callCartFragment();
		}
	}


	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		
	}

	void showToast(String tag) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+tag, Toast.LENGTH_SHORT).show();
	}

	
	/**
	 * Prompt the user to start shopping when they dont have any product in there shopping cart
	 * 
	 */
	private void showNoCartDilaog(){
		final Dialog nocartDialog = new Dialog(getActivity());
		nocartDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		nocartDialog.setContentView(R.layout.no_products_in_cart);
		
		TextView text_emptyCart = (TextView) nocartDialog.findViewById(R.id.text_emptyCart);
		TextView mtvContinueShopping = (TextView) nocartDialog.findViewById(R.id.text_continue);
		mtvContinueShopping.setTypeface(MonginisApplicationClass.getTypeFace());
		text_emptyCart.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvContinueShopping.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nocartDialog.dismiss();
				Intent intent = new Intent(getActivity(), LandingScreen.class);
				startActivity(intent);
				getActivity().overridePendingTransition(0, 0);
				getActivity().finish();
			}
		});
		nocartDialog.show();
		deleteOrderInfoRecords();
	}

	
	
	private void showDeleteWarningDialog(final int listPosition){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.alert_dialog);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_continue);
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		TextView mtvCancel = (TextView) dateWarning.findViewById(R.id.text_cancel);

		mtvContinue.setText("Yes");
		mtvCancel.setText("No");
		mtvMessage.setText("Are you sure you want to delete this item from your cart ?");
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(UserId.equalsIgnoreCase("0")){
					deleteFromLocalDtabase(marrCartData.get(listPosition).getProduct_id());
				}else{
					deleteProduct(marrCartData.get(listPosition).getProduct_id());
				}

				dateWarning.dismiss();

			}
		});
		mtvCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dateWarning.dismiss();

			}
		});

		dateWarning.show();	

	}

	@Override
	public void listItemClick(int position, String msPostId, String tag) {
		// TODO Auto-generated method stub
		showDeleteWarningDialog(position);
	}


	/**
	 * Db query to delete the product from local database for guest user
	 * 
	 * @param msProductId
	 */
	private void deleteFromLocalDtabase(String msProductId){
		LogFile.LogData("deleteThisRecord");
		String msWhere = DatabaseHelper.USER_ID+" ='"+UserId+"' AND "+DatabaseHelper.PRODUCT_ID+" ='"+msProductId+"' " ;
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SHOPPING_CART,
					"", Config.DELETE_DATA,  "DeleteThisItem",msWhere).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	private void updateDeliveryOptions(){
		LogFile.LogData("updateDeliveryOptions()");
		String msWhere = DatabaseHelper.USER_ID+"='"+UserId+"'" ;
		ContentValues value = new ContentValues();

		value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE, Config.DELIVERY_DATE_SERVER);

		/*if(Config.DELIVERY_MODE.equalsIgnoreCase(msSameDay)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msSameDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNextDayDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNextDayLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msNormalDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msNormalDeliveryLocal);
		} else if(msDeliveryMode.equalsIgnoreCase(msVirtualDelivery)){
			value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msVirtualDeliveryLocal);
		}*/

		value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,Config.DELIVERY_MODE);


		try{
			showProgressDialog();
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {

				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete "+response);
					//updateOrdeInfo();
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_SHOPPING_CART, Config.UPDATE_SHOPPING_TABLE, msWhere, "updateShopping",value).execute();

		}catch(Exception ex){
			ex.printStackTrace();
			//callCartFragment();
		}
	}
	
	void deleteOrderInfoRecords(){
		LogFile.LogData("deleteOrderInfoRecords");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"++"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_ORDERS,
					"", Config.DELETE_DATA,  "Delete OrderTable").execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}
