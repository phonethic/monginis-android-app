package com.phonethics.monginis;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class SessionManager {

	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "AndroidHivePref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN_CUSTOMER = "IsLoggedInCustomer";
	private static final String IS_REFRESH_NEEDED = "IsRefrehsNeeded";

	//Customer Mobile NUM name (make variable public to access from outside)
	public static final String EMAIL_ID = "emailId";
	//Customer Password address (make variable public to access from outside)
	public static final String PASSWORD = "password";
	//Customer Customer_Id
	public static final String USER_ID = "user_id";
	//Customer Auth_Id
	public static final String AUTH_ID = "auth_id";


	private static final String COUNTRIES = "countries";
	private static final String STATES = "states";
	private static final String SHIPPING_ADDRESS = "shipping_address";
	private static final String BILLING_ADDRESS = "billing_address";

	private static final String CITY_SELECTED="city_selected";
	public static final String SAVED_DATE = "saved_date";
	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session customer
	 * */
	public void createLoginSessionCustomer(String email_id, String password, String user_id, String auth_id){
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN_CUSTOMER, true);

		// Storing mobile_num in pref
		editor.putString(EMAIL_ID, email_id);

		// Storing password in pref
		editor.putString(PASSWORD, password);

		//storing customer_id
		editor.putString(USER_ID, user_id);

		//storing auth_id
		editor.putString(AUTH_ID, auth_id);

		// commit changes
		editor.commit();
	}   


	public void setRefreshNeeded(boolean isRefresh){
		editor.putBoolean(IS_REFRESH_NEEDED, isRefresh);
		editor.commit();
	}

	public boolean isRefreshNeeded(){
		return pref.getBoolean(IS_REFRESH_NEEDED, false);
	}

	public void saveCountriesData(String msCountire){
		editor.putString(COUNTRIES, msCountire);
		editor.commit();
	}

	public String getCountiresData(){
		return pref.getString(COUNTRIES, "");
	}

	public void saveStateData(String msState){
		editor.putString(STATES, msState);
		editor.commit();
	}

	public String getStateData(){
		return pref.getString(STATES, "");
	}


	/**
	 * Get stored session data customer
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// Customer mobile
		user.put(EMAIL_ID, pref.getString(EMAIL_ID, null));

		// Customer password
		user.put(PASSWORD, pref.getString(PASSWORD, null));

		// customer_id
		user.put(USER_ID, pref.getString(USER_ID, null));

		// Auth_id
		user.put(AUTH_ID, pref.getString(AUTH_ID, null));

		// return user
		return user;
	}

	// Get Login State Customer
	public boolean isLoggedInCustomer(){
		return pref.getBoolean(IS_LOGIN_CUSTOMER, false);
	}




	/**
	 * Clear session details customer
	 * */
	public void logoutCustomer(){
		// Clearing all data from Shared Preferences
		//editor.clear();

		editor.remove(EMAIL_ID);
		editor.remove(PASSWORD);
		editor.remove(USER_ID);
		editor.remove(AUTH_ID);
		editor.remove(IS_LOGIN_CUSTOMER);
		editor.commit();

	}


	public void saveShippingAddress(String msShippingAddress){
		editor.putString(SHIPPING_ADDRESS, msShippingAddress);
		editor.commit();
	}

	public String getShippingDetails(){
		return pref.getString(SHIPPING_ADDRESS, "");
	}


	public void saveBillingAddress(String msBillingAddress){
		editor.putString(BILLING_ADDRESS, msBillingAddress);
		editor.commit();
	}

	public String getBillingAddress(){
		return pref.getString(BILLING_ADDRESS, "");
	}

	//city selection by the user
	public void saveCityPreferences(String value) {
		editor.putString(CITY_SELECTED, value);
		editor.commit();
		Config.Log("CITY","CITY " + value);
	}
	
	public String getCitySelected(){

		return pref.getString(CITY_SELECTED, "");
	}

	//save date for refresh
	public void setSavedDate(String date){
		editor.putString(SAVED_DATE, date);
		editor.commit();
	}
	public String getSavedDate(){
		return pref.getString(SAVED_DATE, "");
	}
	public void clearDate(){
		editor.remove(SAVED_DATE);
		editor.commit();
	}
}
