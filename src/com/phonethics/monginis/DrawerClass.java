package com.phonethics.monginis;

import java.util.ArrayList;

import com.phonethics.model.ExpandibleDrawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;

public class DrawerClass {

	Activity context;


	ArrayList<ExpandibleDrawer> drawerMenu=new ArrayList<ExpandibleDrawer>();


	boolean showMore=false;

	String PREF_NAME="DRAWER_MODE";
	String PREF_KEY="DRAWER_STAE";

	public DrawerClass(Activity context)
	{
		this.context=context;
	}


	public boolean isShowMore() {
		getDrawerPref();
		return showMore;
	}

	public void setShowMore(boolean showMore) {
		this.showMore = showMore;
		setDrawerPref();
	}

	public ArrayList<ExpandibleDrawer> getDrawerAdapter()
	{

		drawerMenu.clear();

		getDrawerPref();
		//Shoplocal
		ExpandibleDrawer drawer=new ExpandibleDrawer();

		drawer.setOpened_state(Integer.parseInt(context.getResources().getString(R.string.open_status)));
		drawer.setCustom_state(1);

		drawer.setHeader(context.getResources().getString(R.string.section1));
		drawer.setItem(context.getResources().getString(R.string.itemCake));	
		drawer.setIcon(R.drawable.ic_launcher);

		drawer.setItem(context.getResources().getString(R.string.itemChocolate));
		drawer.setIcon(R.drawable.ic_launcher);


		drawer.setItem(context.getResources().getString(R.string.itemFlowers));	
		drawer.setIcon(R.drawable.ic_launcher);
		//		drawer.setIcon(R.drawable.myshoplocal_flat);

		drawer.setItem(context.getResources().getString(R.string.itemGiftHampers));	
		drawer.setIcon(R.drawable.ic_launcher);


		drawer.setItem(context.getResources().getString(R.string.itemStoreLocator));
		drawer.setIcon(R.drawable.ic_launcher);

		drawer.setItem(context.getResources().getString(R.string.itemRegisterLogin));
		drawer.setIcon(R.drawable.ic_launcher);

		drawerMenu.add(drawer);

		ExpandibleDrawer drawer2=new ExpandibleDrawer();

		drawer2.setHeader(context.getResources().getString(R.string.section2));
		drawer2.setItem(context.getResources().getString(R.string.itemRetailFranchisee));	
		drawer2.setIcon(R.drawable.ic_launcher);

		drawer2.setItem(context.getResources().getString(R.string.itemManufacturingFranchisee));
		drawer2.setIcon(R.drawable.ic_launcher);


		drawer2.setItem(context.getResources().getString(R.string.itemDistributor));	
		drawer2.setIcon(R.drawable.ic_launcher);
		//		drawer.setIcon(R.drawable.myshoplocal_flat);

		drawer2.setItem(context.getResources().getString(R.string.itemSuperstockiest));	
		drawer2.setIcon(R.drawable.ic_launcher);


		drawer2.setItem(context.getResources().getString(R.string.itemAffiliateProgram));
		drawer2.setIcon(R.drawable.ic_launcher);

		drawerMenu.add(drawer2);

		ExpandibleDrawer drawer3=new ExpandibleDrawer();

		drawer3.setHeader(context.getResources().getString(R.string.section3));
		drawer3.setItem(context.getResources().getString(R.string.itemCorporateGifts));	
		drawer3.setIcon(R.drawable.ic_launcher);

		drawer3.setItem(context.getResources().getString(R.string.itemMediaRoom));
		drawer3.setIcon(R.drawable.ic_launcher);


		drawer3.setItem(context.getResources().getString(R.string.itemSocialLinks));	
		drawer3.setIcon(R.drawable.ic_launcher);
		//		drawer.setIcon(R.drawable.myshoplocal_flat);

		drawer3.setItem(context.getResources().getString(R.string.itemContactUs));	
		drawer3.setIcon(R.drawable.ic_launcher);

		drawerMenu.add(drawer3);

		return drawerMenu;
	}

	void setDrawerPref()
	{
		try
		{
			Log.i("showMore", "showMore "+showMore);
			SharedPreferences prfs=context.getSharedPreferences(PREF_NAME,1);
			Editor editor=prfs.edit();
			editor.putBoolean(PREF_KEY, showMore);
			editor.commit();
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	void getDrawerPref()
	{
		try
		{
			SharedPreferences prfs=context.getSharedPreferences(PREF_NAME,1);
			showMore=prfs.getBoolean(PREF_KEY, false);

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}




}
