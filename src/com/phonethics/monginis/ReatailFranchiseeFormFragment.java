package com.phonethics.monginis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CategoriesAdapter;

public class ReatailFranchiseeFormFragment extends Fragment implements OnItemClickListener{

	private View mView;
	private ViewFlipper retailFlipper;
	private TextView mNext;
	private TextView mPreviousPage2;
	private TextView mPreviousPage3;
	private TextView mSubmitPage3;
	private TextView mNextPage2;
	RelativeLayout dividerView;
	private TextView mDetails;
	private TextView mDateApplication;
	private EditText mLocation;
	private EditText mName;
	private EditText mAddress;
	private EditText mTelephone;
	private EditText mMobile;
	private EditText mEmail;
	private TextView mDetailsProposedShop;
	private TextView mTitleProposedShop;
	private TextView mProposedArea;
	private EditText mLength;
	private EditText mWidth;
	private EditText mHeight;
	private TextView mTotal;
	private TextView mLegalStatus; 
	private EditText mEducation;
	private EditText mOccupation;

	//for selection of particular fields
	ArrayList<String> titleProposedShop = new ArrayList<String>();
	ArrayList<String> legalStatus = new ArrayList<String>();

	//dropdown fields
	TextView selectLegalStatus;
	TextView selectTitle;
	boolean isLegal;
	boolean isTitle;

	//dropdown list and dialogs
	private Dialog mListDialog;
	ListView mDialogList;
	CategoriesAdapter mAdapter;

	Typeface tf;
	Calendar mCalDate = Calendar.getInstance();
	String mCurrentDate="";
	private ProgressDialog mProgressDialog;
	
	//form fields
	String apply_for;
	String application_date;
	String location ;
	String name;
	String address;
	String phone;
	String mobile;
	String email;
	String education;
	String occupation;

	String shop_title;
	String shop_ownership ;

	String shop_length;
	String shop_width;
	String shop_height;
	String shop_total_area;
	String shop_legal_status ;
	private Map<String, String> param = new HashMap<String, String>();
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");
	}
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Date currentDate = Calendar.getInstance().getTime();
		mCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
	}
	
	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_retail_franchisee, null);

		retailFlipper=(ViewFlipper)mView.findViewById(R.id.retailFlipper);
		mNext=(TextView)mView.findViewById(R.id.mNext);
		mPreviousPage2=(TextView)mView.findViewById(R.id.mPreviousPage2);
		mPreviousPage3=(TextView)mView.findViewById(R.id.mPreviousPage3);
		mNextPage2=(TextView)mView.findViewById(R.id.mNextPage2);
		mSubmitPage3=(TextView)mView.findViewById(R.id.mSubmitPage3);
		selectTitle=(TextView)mView.findViewById(R.id.selectTitle);
		selectLegalStatus=(TextView)mView.findViewById(R.id.selectLegalStatus);
		mDetails=(TextView)mView.findViewById(R.id.mDetails);
		mDateApplication=(TextView)mView.findViewById(R.id.mDateApplication);
		mLocation=(EditText)mView.findViewById(R.id.mLocation);
		mName=(EditText)mView.findViewById(R.id.mName);
		mAddress=(EditText)mView.findViewById(R.id.mAddress);
		mTelephone=(EditText)mView.findViewById(R.id.mTelephone);
		mMobile=(EditText)mView.findViewById(R.id.mMobile);
		mEmail=(EditText)mView.findViewById(R.id.mEmail);
		mDetailsProposedShop=(TextView)mView.findViewById(R.id.mDetailsProposedShop);
		mTitleProposedShop=(TextView)mView.findViewById(R.id.mTitleProposedShop);
		mProposedArea=(TextView)mView.findViewById(R.id.mProposedArea);
		mLength=(EditText)mView.findViewById(R.id.mLength);
		mWidth=(EditText)mView.findViewById(R.id.mWidth);
		mHeight=(EditText)mView.findViewById(R.id.mHeight);
		mTotal=(EditText)mView.findViewById(R.id.mTotal);
		mLegalStatus=(TextView)mView.findViewById(R.id.mLegalStatus);
		mEducation=(EditText)mView.findViewById(R.id.mEducation);
		mOccupation=(EditText)mView.findViewById(R.id.mOccupation);

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		mDetails.setTypeface(tf);
		mDateApplication.setTypeface(tf);
		mLocation.setTypeface(tf);
		mName.setTypeface(tf);
		mAddress.setTypeface(tf);
		mTelephone.setTypeface(tf);
		mMobile.setTypeface(tf);
		mEmail.setTypeface(tf);
		mNext.setTypeface(tf);
		mDetailsProposedShop.setTypeface(tf);
		mTitleProposedShop.setTypeface(tf);
		selectTitle.setTypeface(tf);
		mPreviousPage2.setTypeface(tf);
		mNextPage2.setTypeface(tf);
		mProposedArea.setTypeface(tf);
		mLength.setTypeface(tf);
		mWidth.setTypeface(tf);
		mHeight.setTypeface(tf);
		mTotal.setTypeface(tf);
		mLegalStatus.setTypeface(tf);
		selectLegalStatus.setTypeface(tf);
		mPreviousPage3.setTypeface(tf);
		mSubmitPage3.setTypeface(tf);
		mEducation.setTypeface(tf);
		mOccupation.setTypeface(tf);

		mListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		dividerView.setVisibility(View.GONE);
		mListDialog.setTitle("Select Option");

		//add predefined items to selection dropdown
		legalStatus.add("PROPRIETORSHIP");
		legalStatus.add("PARTNERSHIP");

		titleProposedShop.add("OWNED");
		titleProposedShop.add("RENTED");
		titleProposedShop.add("LEASED");

		mNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				retailFlipper.showNext();
			}
		});

		mPreviousPage2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				retailFlipper.showPrevious();
			}
		});

		mNextPage2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				retailFlipper.showNext();
			}
		});

		mPreviousPage3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				retailFlipper.showPrevious();
			}
		});

		//for dropdown things
		selectLegalStatus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mAdapter = new CategoriesAdapter(getActivity(), 0, 0, legalStatus);
				mDialogList.setAdapter(mAdapter);
				mListDialog.show();
				isLegal = true;
				isTitle = false;
			}
		});

		selectTitle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mAdapter = new CategoriesAdapter(getActivity(), 0, 0, titleProposedShop);
				mDialogList.setAdapter(mAdapter);
				mListDialog.show();
				isTitle = true;
				isLegal = false;
			}
		});

		mDateApplication.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				chooseDate();
			}
		});

		mSubmitPage3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				 apply_for = "retail";
				 application_date = mDateApplication.getText().toString();
				 location = mLocation.getText().toString();
				 name = mName.getText().toString();
				 address = mAddress.getText().toString();
				 phone = mTelephone.getText().toString();
				 mobile = mMobile.getText().toString();
				 email = mEmail.getText().toString();
				 education = mEducation.getText().toString();
				 occupation = mOccupation.getText().toString();

				 shop_title = mTitleProposedShop.getText().toString();
				 shop_ownership = selectTitle.getText().toString();

				 shop_length = mLength.getText().toString();
				 shop_width = mWidth.getText().toString();
				 shop_height = mHeight.getText().toString();
				 shop_total_area = mTotal.getText().toString();
				 shop_legal_status = mLegalStatus.getText().toString();

				if(mName.length() == 0){
					showToast("Please enter applicant name");
				}
				else if(mMobile.length()<10){
					showToast("Please enter valid mobile number");
				}
				else if(mEmail.getText().toString().length()==0){
					showToast("Please enter Email Id");
				}
				else if(!SignUpFragment.isEmailValid(mEmail.getText().toString())){
					showToast("Please enter a valid Email Id");
				}
				else{
					applyForReatilForm();
				}

			}
		});


		mDialogList.setOnItemClickListener(this);
		return mView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub
		//showToast(position+"");
		if(isLegal){
			selectLegalStatus.setText(legalStatus.get(position));
			mListDialog.dismiss();
		}
		else if(isTitle){
			selectTitle.setText(titleProposedShop.get(position));
			mListDialog.dismiss();
		}
	}

	void chooseDate() {
		// TODO Auto-generated method stub
		new DatePickerDialog(getActivity(),date , mCalDate.get(Calendar.YEAR), mCalDate.get(Calendar.MONTH), mCalDate.get(Calendar.DATE)).show();
	}

	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			// TODO Auto-generated method stub
			mCalDate.set(Calendar.YEAR,year);
			mCalDate.set(Calendar.MONTH,monthOfYear);
			mCalDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
			DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			String setDate = dt.format(mCalDate.getTime());

			if(setDate.compareTo(mCurrentDate)>0){
				showToast("Unable to set future date");
				mDateApplication.setText("");

			}else{
				mDateApplication.setText(setDate);
			}
		}
	};

	void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg,Toast.LENGTH_SHORT).show();
	}

	void applyForReatilForm() {
		// TODO Auto-generated method stub

		try {
			
			showProgressDialog();
			String TAG="RETAIL FORM";
			String url=Config.POST_APPLY_FORM;
			Config.Log("URL","URL "+url);

			//adding body to form
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("apply_for", apply_for);
			jsonObj.put("application_date", application_date);
			jsonObj.put("location", location);
			jsonObj.put("name", name);
			jsonObj.put("address", address);
			jsonObj.put("phone", phone);
			jsonObj.put("mobile", mobile);
			jsonObj.put("email", email);
			jsonObj.put("education", education);
			jsonObj.put("occupation", occupation);
			jsonObj.put("shop_title", shop_title);
			jsonObj.put("shop_ownership", shop_ownership);
			jsonObj.put("shop_length", shop_length);
			jsonObj.put("shop_width", shop_width);
			jsonObj.put("shop_height", shop_height);
			jsonObj.put("shop_total_area", shop_total_area);
			jsonObj.put("shop_legal_status", shop_legal_status);
			
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							String message = json.getString("message");
							if(success.equalsIgnoreCase("true")){

								param.put("Form_Submitted", "Retail Franchisee");
								EventTracker.logEvent(EventsName.PARTNER_WITH_US,param);
								//								JSONObject data = json.getJSONObject("data");
								//								String userId = data.getString("user_id");
								//Config.USER_ID = userId;
								showAlertDialog(message);
								//showToast(message);
							}
							else{
								//message = json.getString("message");
								showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void showAlertDialog(String message) {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				MenuOptionFragment chkFrag=new MenuOptionFragment();
				transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
				//transaction.addToBackStack("MENU OPTION");
				// Commit the transaction
				transaction.commit();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}
