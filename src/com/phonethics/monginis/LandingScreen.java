package com.phonethics.monginis;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.phonethics.model.ExpandibleDrawer;
import com.phonethics.monginis.MainCategoriesFragment.onMainCategoryListener;

public class LandingScreen extends ActionBarActivity implements onMainCategoryListener{

	private DrawerLayout mDrawerLayout;
	private ExpandableListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;


	private ActionBar mActionBar;
	private FrameLayout mFrame;
	private MainCategoriesFragment mFragment;
	private ArrayList<ExpandibleDrawer> mDrawerMenu=new ArrayList<ExpandibleDrawer>();
	private ArrayList<String>mArrCategId=new ArrayList<String>();
	private ProgressDialog mProgressDialog;
	//Menu icons
	static ImageView mHomeBtn;
	static ImageView mOptionBtn;
	ImageView mShoppingCartBtn;
	ImageView mSearchBtn;
	String fromSplash = "false";
	static RelativeLayout mMenuLayout;
	SessionManager mSessionManager;
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setTheme(R.style.Theme_AppCompat);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing_screen);


		mActionBar=getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setIcon(R.drawable.ic_navigation_drawer);
		mActionBar.hide();

		mContext = this;
		mSessionManager = new SessionManager(this);
		mProgressDialog=new ProgressDialog(this);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		mFrame=(FrameLayout)findViewById(R.id.content_frame);
		mHomeBtn=(ImageView)findViewById(R.id.HomeBtn);
		mOptionBtn=(ImageView)findViewById(R.id.OptionBtn);
		mShoppingCartBtn=(ImageView)findViewById(R.id.ShoppingCartBtn);
		mSearchBtn=(ImageView)findViewById(R.id.SearchBtn);
		mHomeBtn.setImageResource(R.drawable.home_btn_pressed);
		mMenuLayout = (RelativeLayout)findViewById(R.id.mMenuLayout);

		Bundle b = getIntent().getExtras();
		if(b!=null){
			fromSplash = b.getString("tag");
		}


	
		if(fromSplash.equalsIgnoreCase("true")){
			if(mSessionManager.isLoggedInCustomer()){

				if (findViewById(R.id.content_frame) != null) {
					if (savedInstanceState != null) {
						return;
					}

					mFragment=new MainCategoriesFragment();
					getSupportFragmentManager().beginTransaction()
					.add(R.id.content_frame, mFragment, "Main Fragment").commit();

				}
			}
			else{

				if (findViewById(R.id.content_frame) != null) {
					
					if (savedInstanceState != null) {
						return;
					}
					
					LoginSignUpFragment mFragment = new LoginSignUpFragment();
					Bundle bi=new Bundle();
					bi.putString("tag","fromSplash");
					mFragment.setArguments(bi);
					getSupportFragmentManager().beginTransaction()
					.add(R.id.content_frame, mFragment, "LOGIN").commit();
				}
			}


		}
		else{
		
			if (findViewById(R.id.content_frame) != null) {
				if (savedInstanceState != null) {
					return;
				}

				mFragment=new MainCategoriesFragment();
				getSupportFragmentManager().beginTransaction()
				.add(R.id.content_frame, mFragment, "Main Fragment").commit();

			}

		}
		
		mHomeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				InputMethodManager inputMethodManager = (InputMethodManager)  mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

				mHomeBtn.setImageResource(R.drawable.home_btn_pressed);
				mOptionBtn.setImageResource(R.drawable.option_btn);
				mShoppingCartBtn.setImageResource(R.drawable.shopping_cart_btn);
				mSearchBtn.setImageResource(R.drawable.search_btn);
			

				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				MainCategoriesFragment chkFrag=new MainCategoriesFragment();
				transaction.add(R.id.content_frame, chkFrag,"MainCategory");
				//transaction.addToBackStack("ProductDetails");
				// Commit the transaction
				transaction.commit();

			}
		});

		mOptionBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				InputMethodManager inputMethodManager = (InputMethodManager)  mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

				mHomeBtn.setImageResource(R.drawable.home_btn);
				mOptionBtn.setImageResource(R.drawable.option_btn_pressed);
				mShoppingCartBtn.setImageResource(R.drawable.shopping_cart_btn);
				mSearchBtn.setImageResource(R.drawable.search_btn);
				//		mCustomerProfileBtn.setImageResource(R.drawable.sort_btn);

				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				MenuOptionFragment chkFrag=new MenuOptionFragment();
				transaction.add(R.id.content_frame, chkFrag,"MenuOption");
				//transaction.addToBackStack("ProductDetails");
				// Commit the transaction
				transaction.commit();
			}
		});

		mShoppingCartBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//	mCustomerProfileBtn.setVisibility(View.GONE);
				InputMethodManager inputMethodManager = (InputMethodManager)  mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

				mHomeBtn.setImageResource(R.drawable.home_btn);
				mOptionBtn.setImageResource(R.drawable.option_btn);
				mShoppingCartBtn.setImageResource(R.drawable.shopping_cart_btn_pressed);
				mSearchBtn.setImageResource(R.drawable.search_btn);
				//	mCustomerProfileBtn.setImageResource(R.drawable.sort_btn);

				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				ShoppingCartFragment chkFrag=new ShoppingCartFragment();
				transaction.add(R.id.content_frame, chkFrag,"SHOPPING CART");
				//transaction.addToBackStack("STORE LOCATOR");
				// Commit the transaction
				transaction.commit();
			}
		});

		mSearchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//		mCustomerProfileBtn.setVisibility(View.GONE);
				InputMethodManager inputMethodManager = (InputMethodManager)  mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

				mHomeBtn.setImageResource(R.drawable.home_btn);
				mOptionBtn.setImageResource(R.drawable.option_btn);
				mShoppingCartBtn.setImageResource(R.drawable.shopping_cart_btn);
				mSearchBtn.setImageResource(R.drawable.search_btn_pressed);
				//		mCustomerProfileBtn.setImageResource(R.drawable.sort_btn);

				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				SearchFragment chkFrag=new SearchFragment();
				transaction.add(R.id.content_frame, chkFrag,"SEARCH");
				//transaction.addToBackStack("STORE LOCATOR");
				// Commit the transaction
				transaction.commit();
			}
		});

		
		// Add code to print out the key hash
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.phonethics.monginis", 
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Config.Log("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	private class ExpandedDrawerClickListener implements ExpandableListView.OnChildClickListener {
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {
			return false;
		}
	}

	void showToast(String msg){
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case android.R.id.home:
			
		}
		return true;
	}

	//Click Listener
	@Override
	public void mCategClick(String Categ,int position) {
	}




	void showProductDetailView(){

		mFrame.setVisibility(View.GONE);
	}

	void showFragments(){
		mFrame.setVisibility(View.VISIBLE);
	}

	public static void sortVisible(){
		
	}

	public static void sortGone(){
		//mCustomerProfileBtn.setVisibility(View.GONE);
	}

	public static void sortVisiblePressed(){
		
	}

	public static void HideNavigation(){
		mMenuLayout.setVisibility(View.GONE);
	}
	public static void showNavigation(){
		mMenuLayout.setVisibility(View.VISIBLE);
		
	}


	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		String apid = Config.APP_ID;
		LogFile.LogData("My Application onCreate - App APID: " + apid);
		EventTracker.startFlurrySession(getApplicationContext());
		FlurryAgent.setUserId(apid);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EventTracker.endFlurrySession(getApplicationContext());
	}

}
