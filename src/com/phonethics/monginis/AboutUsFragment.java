package com.phonethics.monginis;

import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class AboutUsFragment extends Fragment {

	private View mView;
	private TextView txtAboutUs;
	private TextView phonethicsLink;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_about_us, null);
		txtAboutUs=(TextView)mView.findViewById(R.id.txtAboutUs);
		phonethicsLink=(TextView)mView.findViewById(R.id.phonethicsLink);
		txtAboutUs.setTextColor(Color.BLACK);
		txtAboutUs.setTextSize(19);
		Typeface tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		txtAboutUs.setTypeface(tf);
		phonethicsLink.setTypeface(tf);
		txtAboutUs.setText(getResources().getString(R.string.about_us));
		//txtAboutUs.setText(Html.fromHtml(getResources().getString(R.string.about_us)));

		phonethicsLink.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(getActivity(),WebLinkView.class);
				intent.putExtra("position", 6);
				startActivity(intent);
			}
		});
		return mView;
	}





	/* Check whether offer description has any URL attached with this
	 *
	 *
	 * @param msUrl
	 * @return
	 */
	private String checkUrlInString(String msUrl){
		String s = msUrl;
		String newString = "";
		// separate input by spaces ( URLs don't have spaces )
		String [] parts = s.split("[\\s^\n]+");
		//newString = msUrl;

		// Attempt to convert each item into an URL.
		for( String item : parts ) {
			LogFile.LogData("Item ====" +item);
			try {
				//InorbitLog.d("Info item "+item);

				// If possible then replace with anchor...
				if(item.startsWith("www.")){
					item = item.replace("www.", "http://");
				}

				URL url = new URL(item);
				newString = newString + " <a href=\"" + url + "\">"+ url + "</a> " ;

			} catch (MalformedURLException e) {
				// If there was an URL that was not it!.,..
				System.out.print( item + " " );
				//InorbitLog.d("Info "+item);
				newString = newString + " "+item;

			}

		}

		LogFile.LogData("New string ====" +newString);

		//InorbitLog.d("Info " + newString);
		return newString;
	}

}
