package com.phonethics.monginis;

import java.io.File;
import java.util.Calendar;

import android.app.Application;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;
import com.urbanairship.push.CustomPushNotificationBuilder;
import com.urbanairship.push.PushManager;

public class MonginisApplicationClass extends Application{

	File cacheDir;

	public static final String TAG = MonginisApplicationClass.class.getSimpleName();
	private RequestQueue mRequestQueue;

	private ImageLoader mImageLoader;
	private static MonginisApplicationClass mInstance;
	private static Typeface tf;
	
	/** Urban airship keys */
	String urbanAirshiptAppKey;
	String urbanAirshipAppSecret;
	String urbanAirshipGCMProjectId;
	
	//public static int mDateToCompare = 0;
	//Calendar mCalender;


	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		tf				= Typeface.createFromAsset(getAssets(), "fonts/Omnes.ttf");
		//ImageLoader 
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"/.monginisCache");
		}else{
			cacheDir=getApplicationContext().getCacheDir();
		}

		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
		
		
		
		try{
			
			AirshipConfigOptions options = AirshipConfigOptions.loadDefaultOptions(this);
			urbanAirshipGCMProjectId = getResources().getString(R.string.urbanAirshipGCMProjectId);
			options.inProduction = false;
			
			if(options.inProduction==false)
			{
				options.developmentAppKey 		= getResources().getString(R.string.urbanAirshipdevelopmentAppKey);
				options.developmentAppSecret 	= getResources().getString(R.string.urbanAirshipdevelopmentAppSecret);
			}
			else
			{
				//options.productionAppKey		= getResources().getString(R.string.urbanAirshipProductionAppKey);
				//options.productionAppSecret 	= getResources().getString(R.string.urbanAirshipProductionAppSecret);

			}
			
			
			
			options.gcmSender = urbanAirshipGCMProjectId;
			options.transport = "gcm";


			UAirship.takeOff(this, options);

			//        Logger.logLevel = Log.VERBOSE;

			//use CustomPushNotificationBuilder to specify a custom layout
			CustomPushNotificationBuilder nb = new CustomPushNotificationBuilder();

			nb.statusBarIconDrawableId = R.drawable.ic_launcher_monginis;//custom status bar icon

			nb.layout = R.layout.notification;
			nb.layoutIconDrawableId = R.drawable.ic_launcher_monginis;//custom layout icon
			nb.layoutIconId = R.id.icon;
			nb.layoutSubjectId = R.id.subject;
			nb.layoutMessageId = R.id.message;

			PushManager.enablePush();
			PushManager.shared().setNotificationBuilder(nb);
			PushManager.shared().setIntentReceiver(IntentReceiver.class);
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
//		mCalender = Calendar.getInstance();
//		if(mDateToCompare==0){
//			mDateToCompare = mCalender.get(Calendar.DATE);
//			Toast.makeText(getApplicationContext(), ""+mDateToCompare, 0).show();
//		}	
//		else{
//			//compare with the previous dates
//			int current_date = mCalender.get(Calendar.DATE);
//			int date_difference = current_date - mDateToCompare;
//			if(date_difference >= 3){
//				mDateToCompare = current_date;
//				Toast.makeText(getApplicationContext(), "Drop all the tables " + date_difference, 0).show();
//			}
//			else{
//				Toast.makeText(getApplicationContext(), "Diff " + date_difference, 0).show();
//			}
//		}
		
	}

	public static synchronized MonginisApplicationClass getInstance(){
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}
	
	public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
 
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
 
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    
    
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

	public static Typeface getTypeFace(){
		return tf;
	}
    
}
