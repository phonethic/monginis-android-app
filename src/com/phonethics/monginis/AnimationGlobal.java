package com.phonethics.monginis;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

public class AnimationGlobal {

	ImageView mImgCake;
	ImageView imgCakeBackup;
	Activity mContext;

	public AnimationGlobal(ImageView imgCake, ImageView imgCakeBackup, Activity mContext){

		this.mContext = mContext;
		this.mImgCake = imgCake;
		this.imgCakeBackup = imgCakeBackup;
	}

	public void startBackgroudAnimation(){
		

		Animation logoMoveAnimation = AnimationUtils.loadAnimation(mContext, R.anim.zoom); 
		mImgCake.startAnimation(logoMoveAnimation);

		//logoMoveAnimation.setRepeatCount(Animation.INFINITE);
		logoMoveAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				//Functionality here
				mImgCake.setVisibility(View.GONE);
				Animation logoMoveAnimation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_reverse_alpha); 
				imgCakeBackup.startAnimation(logoMoveAnimation);
				
				logoMoveAnimation.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
//						Animation logoMoveAnimation = AnimationUtils.loadAnimation(mContext, R.anim.middle_reverse); 
//						mImgCake.startAnimation(logoMoveAnimation);
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						startBackgroudAnimation();
					}
				});
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				Animation logoMoveAnimation = AnimationUtils.loadAnimation(mContext, R.anim.middle_anim); 
				imgCakeBackup.startAnimation(logoMoveAnimation);
			}
		});
	}
}
