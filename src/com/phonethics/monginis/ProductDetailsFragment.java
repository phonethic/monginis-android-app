package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.model.ProductDetailsModel;
import com.phonethics.model.ProductModel;
import com.squareup.picasso.Picasso;

public class ProductDetailsFragment extends Fragment implements OnTouchListener{

	private View mView;
	//private String msProductId="";

	private TextView mTxtProdName;
	private TextView mTxtProdDesc;
	private TextView mTxtProdDispPrize;
	private TextView mTxtProdSplPrize;
	private TextView mTxtProdShare;
	private TextView mTxtProdCheck;
	private Typeface tf;
	private Typeface tf2;
	private Typeface mCursive;
	private ImageView mImgCake;
	private ImageView mDetailBtn;
	private TextView mFrontText;
	private RelativeLayout mDetailView;
	private ImageView mCollapseView;
	private RelativeLayout typeImage;
	private ImageView cakeTypeImg1;
	private ImageView cakeTypeImg2;
	private ProgressDialog mProgressDialog;
	int downx = 0,downy = 0,upx = 0,upy = 0;
	NetworkCheck netChk;
	boolean parsingError;
	String product_img_url="";
	String mBasePrice="";
	RatingBar mProductRating;

	//TextView mAvgText;
	RelativeLayout mAvgLayout;
	ImageView m1Star,m2Star,m3Star,m4Star,m5Star;
	String sub_catId="";
	String avg_rating="";
	ImageView imgCakeBackup;
	private ProductModel msProduct;
	RelativeLayout fontRateLayout;


	private TextView mRateText;
	String cat_id = "";

	public ProductDetailsFragment()
	{

	}
	String mProductId = "";
	TextView mCakeWeight;

	public static ProductDetailsFragment newInstance(ProductModel pModel){
		ProductDetailsFragment frag=new ProductDetailsFragment();
		Bundle b=new Bundle();
		b.putParcelable("Product", pModel);
		frag.setArguments(b);
		return frag;
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		netChk = new NetworkCheck(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		//msProductId=getArguments().getString("productId");
		//added by salman
		msProduct=getArguments().getParcelable("Product");

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		tf2=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_MEDIUM);
		mCursive=Typeface.createFromAsset(getActivity().getAssets(), Config.CURSIVE);
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//	msProductId=getArguments().getString("productId");
		mView=(View)inflater.inflate(R.layout.fragment_product_details, container, false);
		mTxtProdName=(TextView)mView.findViewById(R.id.txtProdName);
		mTxtProdDesc=(TextView)mView.findViewById(R.id.txtProdDesc);
		mRateText=(TextView)mView.findViewById(R.id.mRateText);
		mTxtProdDispPrize=(TextView)mView.findViewById(R.id.txtProdDispPrize);
		mTxtProdSplPrize=(TextView)mView.findViewById(R.id.txtProdSplPrize);
		mTxtProdShare=(TextView)mView.findViewById(R.id.txtProdShare);
		mTxtProdCheck=(TextView)mView.findViewById(R.id.txtProdCheck);
		mDetailBtn=(ImageView)mView.findViewById(R.id.mDetailBtn);
		mFrontText=(TextView)mView.findViewById(R.id.mFrontText);
		mDetailView=(RelativeLayout)mView.findViewById(R.id.mDetailView);
		mCollapseView=(ImageView)mView.findViewById(R.id.imgCollapseDetails);
		typeImage=(RelativeLayout)mView.findViewById(R.id.typeImage);
		cakeTypeImg1=(ImageView)mView.findViewById(R.id.cakeTypeImg1);
		cakeTypeImg2=(ImageView)mView.findViewById(R.id.cakeTypeImg2);
		mProductRating=(RatingBar)mView.findViewById(R.id.mProductRating);
		//mAvgText=(TextView)mView.findViewById(R.id.mAvgText);
		mAvgLayout=(RelativeLayout)mView.findViewById(R.id.mAvgLayout);
		m1Star=(ImageView)mView.findViewById(R.id.Star1);
		m2Star=(ImageView)mView.findViewById(R.id.Star2);
		m3Star=(ImageView)mView.findViewById(R.id.Star3);
		m4Star=(ImageView)mView.findViewById(R.id.Star4);	
		m5Star=(ImageView)mView.findViewById(R.id.Star5);
		imgCakeBackup=(ImageView)mView.findViewById(R.id.imgCakeBackup);
		fontRateLayout=(RelativeLayout)mView.findViewById(R.id.fontRateLayout);
		mCakeWeight=(TextView)mView.findViewById(R.id.mCakeWeight);

		mTxtProdName.setTypeface(tf);
		mTxtProdDesc.setTypeface(tf);
		mTxtProdDispPrize.setTypeface(tf2);
		mRateText.setTypeface(tf);
		mTxtProdSplPrize.setTypeface(tf);
		mTxtProdShare.setTypeface(tf);
		mTxtProdCheck.setTypeface(tf,Typeface.BOLD);
		mFrontText.setTypeface(tf,Typeface.BOLD);
		mCakeWeight.setTypeface(tf,Typeface.BOLD);

		//added for setting data from DB
		setCachedData(msProduct);

		mTxtProdCheck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(netChk.isNetworkAvailable())
					checkAvailability();
				else
					showToast("No internet connection");
			}
		});
		mImgCake=(ImageView)mView.findViewById(R.id.imgCake);

		Config.Log("Img URL ","Img URL " + Config.IMAGE_BASE_URL+msProduct.getImage());
		try {

			Picasso.with(getActivity())
			.load(Config.IMAGE_BASE_URL+msProduct.getImage())
			///.resize(400, 400)
			.into(mImgCake);

			Picasso.with(getActivity())
			.load(Config.IMAGE_BASE_URL+msProduct.getImage())
			///.resize(400, 400)
			.into(imgCakeBackup);

			callImageTranslate();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		mDetailBtn.setOnTouchListener(this);
		mDetailBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!parsingError){
					if(netChk.isNetworkAvailable()){
						callAnimationDetail();
						//	mFrontText.setVisibility(View.GONE);
						mDetailBtn.setVisibility(View.GONE);
						mDetailView.setVisibility(View.VISIBLE);
						//	mAvgLayout.setVisibility(View.GONE);
						fontRateLayout.setVisibility(View.GONE);
					}
					else
						showToast("No internet connection");
				}
				else
					showToast(getResources().getString(R.string.TimeOut));
			}
		});
		mCollapseView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callAnimationCollapse();
				//				mFrontText.setVisibility(View.VISIBLE);
				//				mDetailBtn.setVisibility(View.VISIBLE);
				mDetailView.setVisibility(View.GONE);

			}
		});

		mTxtProdShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(netChk.isNetworkAvailable()){

					ShareThisApp share = new ShareThisApp(getActivity());
					share.showDialog(mFrontText.getText().toString());
				}
				else
					showToast("No internet connection");

			}
		});


		return mView;
	}

	private void callImageTranslate() {

		Config.Log("Coutn","Coutn ");
		imgCakeBackup.setVisibility(View.INVISIBLE);
		Animation logoMoveAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom); 
		mImgCake.startAnimation(logoMoveAnimation);

		//logoMoveAnimation.setRepeatCount(Animation.INFINITE);
		logoMoveAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				//Functionality here

				mImgCake.setVisibility(View.GONE);
				//imgCakeBackup.setVisibility(View.VISIBLE);
				Animation logoMoveAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_reverse); 
				imgCakeBackup.startAnimation(logoMoveAnimation);

				logoMoveAnimation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						callImageTranslate();
					}
				});
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				Animation logoMoveAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.middle_anim); 
				imgCakeBackup.startAnimation(logoMoveAnimation);
			}
		});

	}

	protected void callAnimationCollapse() {
		// TODO Auto-generated method stub.
		float yValue = getYCoordinateValue();
		AnimationSet animation = new AnimationSet(true);
		ScaleAnimation scaleAnimation = new ScaleAnimation(1, 0, 1, 0,0, yValue);
		scaleAnimation.setDuration(1000);
		animation.addAnimation(scaleAnimation);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				mDetailView.clearAnimation();
				//mFrontText.setVisibility(View.VISIBLE);
				mDetailBtn.setVisibility(View.VISIBLE);
				fontRateLayout.setVisibility(View.VISIBLE);
				if(avg_rating.equalsIgnoreCase("0"))
					mAvgLayout.setVisibility(View.GONE);
				else
					mAvgLayout.setVisibility(View.VISIBLE);
				// Change view to state B by modifying its layout params and scroll
			}

			@Override public void onAnimationRepeat(Animation arg0) {}
			@Override public void onAnimationStart(Animation arg0) {}
		});
		mDetailView.startAnimation(animation);
	}
	protected void callAnimationDetail() {
		// TODO Auto-generated method stub
		float yValue = getYCoordinateValue();
		AnimationSet animation = new AnimationSet(true);

		ScaleAnimation scaleAnimation = new ScaleAnimation(0, 1, 0, 1,0, yValue);
		scaleAnimation.setDuration(1000);
		animation.addAnimation(scaleAnimation);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				mDetailView.clearAnimation();
				// Change view to state B by modifying its layout params and scroll
			}

			@Override public void onAnimationRepeat(Animation arg0) {}
			@Override public void onAnimationStart(Animation arg0) {}
		});
		mDetailView.startAnimation(animation);
	}

	void checkAvailability(){

		Config.Log("PRODUCT_ID","PRODUCT_ID " + mProductId);


		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		//To remove all Stack behind
		//  getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

		CheckAvailabilityFragment chkFrag=new CheckAvailabilityFragment();
		Bundle b=new Bundle();
		b.putString("PRODUCT_ID",mProductId);
		b.putString("SUB_CAT_ID",cat_id);
		Config.Log("SUBCATR ","SUBCATR " + cat_id);
		chkFrag.setArguments(b);
		transaction.add(R.id.content_frame, chkFrag,"ProductDetails");
		transaction.addToBackStack("ProductDetails");
		// Commit the transaction
		transaction.commit();

	}

	void getProductDetails(String id){


		try {

			String TAG="PRODUCT_DETAILS";
			String url=Config.GET_PARTICULAR_PRODUCT+id;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						if(response!=null){

							JSONObject json=response.getJSONObject("data");

							ProductDetailsModel pMod=new ProductDetailsModel();
							pMod.setmProductId(json.getInt("id"));
							pMod.setmProductTitle(json.getString("title"));
							pMod.setmProductDescription(json.getString("description"));
							pMod.setmProductSpecialPrice(json.getString("special_price"));
							pMod.setmProductDisplayPrice(json.getString("list_price"));
							pMod.setmImageUrl(json.getString("image"));
							pMod.setmOurPrice(json.getString("our_price"));
							pMod.setCat_id(json.getString("cat_id"));
							//setData(pMod);
							Config.Log("Product ", "Product "+json.toString());
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					parsingError = true;					
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void getProductAttribute(String id) {
		// TODO Auto-generated method stub

		final ArrayList<String> type_attValues = new ArrayList<String>();
		try {
			//showProgressDialog();
			String TAG="PRODUCT_ATTRIBUTES";
			String url=Config.GET_PRODUCT_ATTRIBUTES+id;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Attributes", "Attributes "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject value = data.getJSONObject(i);
									String att_type = value.getString("attribute_name");
									if(att_type.equalsIgnoreCase("Type")){

										for(int j=0;j<value.length();j++){

											//											JSONObject innerValue = value.getJSONObject(j);
											JSONArray attribute_values = value.getJSONArray("attribute_valus");
											for(int k=0;k<attribute_values.length();k++){

												JSONObject inner_values = attribute_values.getJSONObject(k);
												String att_value = inner_values.getString("attribute_value");
												Config.Log("ATT_VALUE","ATT_VALUE "+att_value);
												type_attValues.add(att_value);
											}
										}
									}

								}
								if(type_attValues.size()!=0){

									for(int i=0;i<type_attValues.size();i++){
										if(type_attValues.get(i).equalsIgnoreCase("Regular")){
											cakeTypeImg1.setImageResource(R.drawable.egg);
										}
										else if(type_attValues.get(i).equalsIgnoreCase("Eggless")){
											cakeTypeImg2.setImageResource(R.drawable.eggless);
										}
									}
								}

							}
							else{

								typeImage.setVisibility(View.GONE);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					//dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	void setData(ProductDetailsModel pMod){
		try{

			final int productId = pMod.getmProductId();
			getAverageRating(productId);

			String displayPrize="Price : ";
			mFrontText.setText(pMod.getmProductTitle());
			mTxtProdName.setText(pMod.getmProductTitle());
			//mTxtProdDesc.setText(Html.fromHtml(pMod.getmProductDescription()));

			Spanned sp = Html.fromHtml(pMod.getmProductDescription());

			mTxtProdDesc.setText(Html.fromHtml(pMod.getmProductDescription()));


			String displayPrizeNew=" "+getResources().getString(R.string.Rs)+pMod.getmProductDisplayPrice()+" ";
			mTxtProdDispPrize.setText(displayPrizeNew);
			mTxtProdDispPrize.setPaintFlags(mTxtProdDispPrize.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			mTxtProdSplPrize.setText(" "+getResources().getString(R.string.Rs)+pMod.getmProductSpecialPrice());

			//chk for special price
			if(pMod.getmProductSpecialPrice().length()==0){
				mBasePrice = pMod.getmOurPrice();
			}
			else{
				mBasePrice = pMod.getmProductSpecialPrice();
				Config.Log("PRICE " ,"SPECIAL PRICE " + pMod.getmProductSpecialPrice());
				Config.Log("PRICE " ,"OUR PRICE " + pMod.getmOurPrice());
				Config.Log("PRICE " ,"DISPLAY PRICE " + pMod.getmProductDisplayPrice());
			}


			mProductRating.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

				@Override
				public void onRatingChanged(RatingBar ratingBar, float rating,
						boolean fromUser) {
					// TODO Auto-generated method stub

					postRatingApi(productId, rating);
				}
			});

			//get subCatId
			String temp_cat_id = pMod.getCat_id();
			boolean isMultiple = temp_cat_id.contains(",");
			if(isMultiple){
				int index = temp_cat_id.indexOf(",");
				temp_cat_id = temp_cat_id.substring(0, index);
				sub_catId = temp_cat_id;
			}
			else{
				sub_catId = temp_cat_id;
			}

			Config.Log("SUB_CAT_ID","SUB_CAT_ID " + sub_catId);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	//added by salman
	void setCachedData(ProductModel pMod){
		try{

			mProductId = pMod.getId();
			cat_id = pMod.getCat_id();
			Config.Log("CAT_ID ","CAT_ID " + cat_id);

			final int productId = Integer.parseInt(pMod.getId());
			getAverageRating(productId);

			String displayPrize="Price : ";
			String title = pMod.getTitle();
			//title = title.replace(" ", "\n");
			mFrontText.setText(title);
			mTxtProdName.setText(pMod.getTitle());

			mTxtProdDesc.setText(Html.fromHtml(pMod.getDescription()));
			mTxtProdDesc.setMovementMethod(LinkMovementMethod.getInstance());

			String displayPrizeNew=" "+getResources().getString(R.string.Rs)+pMod.getList_price()+" ";
			mTxtProdDispPrize.setText(displayPrizeNew);
			mTxtProdDispPrize.setPaintFlags(mTxtProdDispPrize.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			mTxtProdSplPrize.setText(" "+getResources().getString(R.string.Rs)+pMod.getSpecial_price());

			//chk for special price
			if(pMod.getSpecial_price().length()==0){
				mBasePrice = pMod.getOur_price();
			}
			else{
				mBasePrice = pMod.getSpecial_price();
				Config.Log("PRICE " ,"SPECIAL PRICE " + pMod.getSpecial_price());
				Config.Log("PRICE " ,"OUR PRICE " + pMod.getOur_price());
				Config.Log("PRICE " ,"DISPLAY PRICE " + pMod.getList_price());
			}


			mProductRating.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

				@Override
				public void onRatingChanged(RatingBar ratingBar, float rating,
						boolean fromUser) {
					// TODO Auto-generated method stub

					postRatingApi(productId, rating);
				}
			});

			//get subCatId
			String temp_cat_id = pMod.getSubcat_id();
			boolean isMultiple = temp_cat_id.contains(",");
			if(isMultiple){
				int index = temp_cat_id.indexOf(",");
				temp_cat_id = temp_cat_id.substring(0, index);
				sub_catId = temp_cat_id;
			}
			else{
				sub_catId = temp_cat_id;
			}

			Config.Log("SUB_CAT_ID","SUB_CAT_ID " + sub_catId);

			//cached egg.eggless images
			Config.Log("TYPE_CAKE","TYPE_CAKE " + pMod.getWithegg_type() + " " + pMod.getEggless_type());
			if(pMod.getWithegg_type().equalsIgnoreCase("Y")){
				cakeTypeImg1.setImageResource(R.drawable.egg);
			}
			if(pMod.getEggless_type().equalsIgnoreCase("Y")){
				cakeTypeImg2.setImageResource(R.drawable.eggless);
			}

			if(pMod.getWeight_grams().length() != 0){
				float weightKG = Integer.parseInt(pMod.getWeight_grams());
				weightKG = weightKG/1000;
				if((weightKG+"").contains(".0")){
					String weight = (weightKG+"").replace(".0", "");
					mCakeWeight.setText(weight+" "+"KG");
				}
				else{
					mCakeWeight.setText(weightKG+" "+"KG");	
				}
			}
			else{
				mCakeWeight.setVisibility(View.GONE);
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void getAverageRating(int productID) {
		// TODO Auto-generated method stub

		try {

			String TAG="RATING_GET";
			String url=Config.GET_PRODUCT_RATING + productID;
			Config.Log("URL","URL "+url);


			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						if(response!=null){

							Config.Log("Attributes", "RATING "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject value = data.getJSONObject(i);
									//showToast(""+value.getString("avg_rating"));
									//mAvgText.setText(value.getString("avg_rating"));
									avg_rating = value.getString("avg_rating");
									if(avg_rating.equalsIgnoreCase("0")){
										mAvgLayout.setVisibility(View.GONE);
									}
									else if(avg_rating.equalsIgnoreCase("1")){
										mAvgLayout.setVisibility(View.VISIBLE);
										m1Star.setVisibility(View.VISIBLE);
										m2Star.setVisibility(View.GONE);
										m3Star.setVisibility(View.GONE);
										m4Star.setVisibility(View.GONE);
										m5Star.setVisibility(View.GONE);
									}
									else if(avg_rating.equalsIgnoreCase("2")){
										mAvgLayout.setVisibility(View.VISIBLE);
										m1Star.setVisibility(View.VISIBLE);
										m2Star.setVisibility(View.VISIBLE);
										m3Star.setVisibility(View.GONE);
										m4Star.setVisibility(View.GONE);
										m5Star.setVisibility(View.GONE);
									}
									else if(avg_rating.equalsIgnoreCase("3")){
										mAvgLayout.setVisibility(View.VISIBLE);
										m1Star.setVisibility(View.VISIBLE);
										m2Star.setVisibility(View.VISIBLE);
										m3Star.setVisibility(View.VISIBLE);
										m4Star.setVisibility(View.GONE);
										m5Star.setVisibility(View.GONE);
									}
									else if(avg_rating.equalsIgnoreCase("4")){
										mAvgLayout.setVisibility(View.VISIBLE);
										m1Star.setVisibility(View.VISIBLE);
										m2Star.setVisibility(View.VISIBLE);
										m3Star.setVisibility(View.VISIBLE);
										m4Star.setVisibility(View.VISIBLE);
										m5Star.setVisibility(View.GONE);
									}
									else if(avg_rating.equalsIgnoreCase("5")){
										mAvgLayout.setVisibility(View.VISIBLE);
									}
								}
							}
							else{

								showToast("Error");
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					//dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);			


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void postRatingApi(int productId, float rating) {
		// TODO Auto-generated method stub

		try {

			String TAG="RATING_POST";
			String url=Config.ADD_PRODUCT_RATING;
			Config.Log("URL","URL "+url);

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("product_id", productId+"");
			jsonObj.put("rating", rating+"");
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						if(response!=null){

							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								//showToast("Rating Done");
							}
							else{

								//showToast("Error");
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					//dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);			

			final Map<String, String> param = new HashMap<String, String>();
			param.put("ProductWithCategory",  EventsName.category +" - "+EventsName.sub_category +" - "+EventsName.PRODUCTS);
			EventTracker.logEvent(EventsName.RATING, param);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			downx = (int) event.getRawX();
			downy = (int) event.getRawY();
			Log.d("Coordinates:","DOWN X-"+downx);
			Log.d("Coordinates:","DOWN Y-"+downy);
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			upx = (int) event.getRawX();
			upy = (int) event.getRawY();

			Log.d("Coordinates:","UP X-"+downx);
			Log.d("Coordinates:","UP Y-"+downy);
			break;
		case MotionEvent.ACTION_CANCEL:
			break;
		default:
			break;
		}
		return false;
	}

	float getYCoordinateValue(){

		Display display = getActivity().getWindowManager().getDefaultDisplay(); 
		int height = display.getHeight();  // deprecated
		int midPoint = height/2;
		float yValue = (17*midPoint)/100;
		yValue = midPoint - yValue;
		Log.d("Coordinates:","YVALUE "+ yValue);
		return yValue;
	}

	void showToast(String msg){
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}


	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	private void unbindDrawables(View view)
	{
		if (view.getBackground() != null)
		{
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup && !(view instanceof AdapterView))
		{
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++)
			{
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			((ViewGroup) view).removeAllViews();
		}
	}
}

