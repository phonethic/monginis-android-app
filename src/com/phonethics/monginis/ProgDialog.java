package com.phonethics.monginis;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

public class ProgDialog {

	private   ProgressDialog mProgressDialog;


	public ProgDialog(Context context){

		mProgressDialog=new ProgressDialog(context);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait..");

	}

	

	public  void showProgressDialog(){
		
	/*	mProgressDialog=new ProgressDialog(context);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait..");*/
		
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}


	public  void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}


}
