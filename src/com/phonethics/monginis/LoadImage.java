package com.phonethics.monginis;

import java.io.UnsupportedEncodingException;

import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;

public class LoadImage {

	private static ImageLoader imageLoader = MonginisApplicationClass.getInstance().getImageLoader();

	public static void load(String image_url, final ImageView imgView){


		imageLoader.get(image_url, ImageLoader.getImageListener(
				imgView, R.drawable.ic_launcher, R.drawable.ic_launcher));

		/*Cache cache = MonginisApplicationClass.getInstance().getRequestQueue().getCache();
		Entry entry = cache.get(image_url);
		if(entry != null){
			try {
				String data = new String(entry.data, "UTF-8");
				// handle data, like converting it to xml, json, bitmap etc.,
				LogFile.LogData("ImageData " + data);
//				imgView.setImageBitmap(StringToBitMap(data));

			} catch (UnsupportedEncodingException e) {      
				e.printStackTrace();
			}
		}*/



		imageLoader.get(image_url, new ImageListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				//imgView.setImageResource(R.drawable.ic_launcher);
			}

			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				// TODO Auto-generated method stub
				imgView.setImageBitmap(response.getBitmap());
			}
		});

	}


	public Bitmap StringToBitMap(String encodedString){
		try{
			byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
			Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
			return bitmap;
		}catch(Exception e){
			e.getMessage();
			return null;
		}
	}


}
