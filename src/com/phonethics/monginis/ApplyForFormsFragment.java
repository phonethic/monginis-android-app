package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.phonethics.adapters.ApplyForAdapter;
import com.phonethics.adapters.MenuOptionAdapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ApplyForFormsFragment extends Fragment implements OnItemClickListener{

	private View mView;
	private ListView mFormsList;
	ArrayList<String> mFormsOptionList = new ArrayList<String>();
	private ApplyForAdapter mAdapter;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mView=(View)inflater.inflate(R.layout.fragment_apply_for_forms, null);
		mFormsList=(ListView)mView.findViewById(R.id.mFormsList);
		mFormsOptionList.add("RETAIL FRANCHISEE");
		mFormsOptionList.add("MANUFACTURING FRANCHISEE");
		mFormsOptionList.add("DISTRIBUTOR");
		mFormsOptionList.add("SUPERSTOCKIEST");

		mAdapter=new ApplyForAdapter(getActivity(), 0, 0, mFormsOptionList);
		mFormsList.setAdapter(mAdapter);
		mFormsList.setOnItemClickListener(this);
		return mView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub

		Map<String, String> param = new HashMap<String, String>();

		if(position == 0){
			//Retail fragment

			param.put("Form_Selected", "Retail Franchisee");
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			ReatailFranchiseeFormFragment chkFrag=new ReatailFranchiseeFormFragment();
			transaction.add(R.id.content_frame, chkFrag,"RETAIL FORM");
			transaction.addToBackStack("RETAIL FORM");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 1){
			// Manufacturing fragment

			param.put("Form_Selected", "Manufacturing Franchisee");
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			ManufacturingFormFragment chkFrag=new ManufacturingFormFragment();
			transaction.add(R.id.content_frame, chkFrag,"Manufac FORM");
			transaction.addToBackStack("Manufac FORM");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 2){
			// Distributor

			param.put("Form_Selected", "Distributor");
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			DistributorFormFragment chkFrag=new DistributorFormFragment();
			transaction.add(R.id.content_frame, chkFrag,"DISTRIBUTOR FORM");
			transaction.addToBackStack("DISTRIBUTOR FORM");
			// Commit the transaction
			transaction.commit();
		}
		else if(position == 3){
			// Superstockiest

			param.put("Form_Selected", "Superstockiest");
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			//To remove all Stack behind
			//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			StockiestFormFragment chkFrag=new StockiestFormFragment();
			transaction.add(R.id.content_frame, chkFrag,"STOCKIEST FORM");
			transaction.addToBackStack("STOCKIEST FORM");
			// Commit the transaction
			transaction.commit();
		}

		EventTracker.logEvent(EventsName.PARTNER_WITH_US, param);
	}

}
