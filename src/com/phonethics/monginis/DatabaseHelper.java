package com.phonethics.monginis;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME="MONGINIS.db";

	private static final int DATABASE_VERSION=3;

	//Table Main Category
	public static final String TABLE_MAIN_CATEGORIES="MAIN_CATEGORIES";
	public static final String AUTO_ID="_id";
	public static final String ID="id";
	public static final String CATEGORY_NAME="name";
	private String CREATE_CATEGORIES_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_MAIN_CATEGORIES
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ ID + " INTEGER UNIQUE ,"
			+ CATEGORY_NAME + " TEXT );";

	
	
	
	
	
	
	//Table City for Check Availability
	public static final String TABLE_CITY="CITY_BY_CATEGORY";
	public static final String CITY_NAME="city_name";
	private String CREATE_CITY_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_CITY
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ ID + " INTEGER UNIQUE ,"
			+ CITY_NAME + " TEXT );";


	
	
	
	
	
	//Table Sub Category of Main Category
	public static final String TABLE_SUB_CATEGORY="SUB_CATEGORY";
	public static final String SUB_CATEGORY_NAME="sub_category";
	public static final String MAIN_CAT_ID="main_cat_id";
	private String CREATE_SUBCATEGORY_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_SUB_CATEGORY
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ ID + " TEXT ,"
			+ MAIN_CAT_ID + " Text ,"
			+ SUB_CATEGORY_NAME + " TEXT );";
	
	
	
	
	
	
	
	
	//Table Shopping Cart
	public static final String TABLE_SHOPPING_CART="shopping_cart";
	public static final String PRODUCT_ID="product_id";
	public static final String QUANTITY="quantity";
	public static final String BASE_AMOUNT="base_amount";
	public static final String MESSAGE="message";
	public static final String PRODUCT_IMG_URL="product_img_url";
	public static final String USER_ID="user_id";
	public static final String PRODUCT_NAME="product_name";
	public static final String PRODUCT_DELIVERY_DATE="product_delivery_date";
	public static final String PRODUCT_TYPE="product_type";
	public static final String PRODUCT_DELIVERY_MODE="product_delivery_mode";

	
	
	private String CREATE_TABLE_SHOPPING_CART="CREATE TABLE IF NOT EXISTS "+TABLE_SHOPPING_CART
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ PRODUCT_ID + " TEXT ,"
			+ QUANTITY + " TEXT ,"
			+ BASE_AMOUNT + " TEXT ,"
			+ MESSAGE + " TEXT ,"
			
			+ PRODUCT_IMG_URL + " TEXT ,"
			+ PRODUCT_NAME + " TEXT ,"
			+ PRODUCT_DELIVERY_DATE + " TEXT ,"
			+ PRODUCT_TYPE + " TEXT ,"
			+ PRODUCT_DELIVERY_MODE + " TEXT ,"
		
			+ USER_ID + " TEXT );";

	
	
	
	
	
	
	//Table Attribute For Shopping Cart
	public static final String TABLE_PRODUCT_ATTRIBUTE="product_attribute";
	public static final String PRODUCT_ID_ATT_TABLE="product_id";
	public static final String ATTRIBUTE_ID="attribute_id";
	public static final String ATTRIBUTE_VALUE_ID="attribute_value_id";
	
	private String CREATE_PRODUCT_ATTRIBUTE="CREATE TABLE IF NOT EXISTS "+TABLE_PRODUCT_ATTRIBUTE
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ PRODUCT_ID_ATT_TABLE + " TEXT ,"
			+ ATTRIBUTE_ID + " TEXT ,"
			+ ATTRIBUTE_VALUE_ID + " TEXT );";
	
	
	
	//Table Product Details
	public static final String TABLE_PRODUCTS 		= "product_table";
	public static final String PROD_MAINCAT_ID 		= "prod_maincat_id";
	public static final String PROD_SUBCAT_ID 		= "prod_subcat_id";
	public static final String PROD_TITLE 			= "prod_title";
	public static final String PROD_DESCRIPTION	= "prod_description";
	public static final String PROD_CODE 			= "prod_code";
	public static final String PROD_WEIGHT_GRAMS 	= "prod_weight_grams";
	public static final String PROD_IMAGE 			= "prod_image";
	public static final String PROD_LIST_PRCE 		= "prod_list_price";
	public static final String PROD_OUR_PRICE 		= "prod_our_price";
	public static final String PROD_SPECIAL_PRICE 	= "prod_special_price";
	public static final String PROD_EGGLESS_TYPE 	= "prod_eggless_type";
	public static final String PROD_WITHEGG_TYPE 	= "prod_withegg_type";
	public static final String PROD_SAMEDAY_DELIVERY 	= "prod_sameday_delivery";
	public static final String PROD_NEXTDAY_DELIVERY 	= "prod_nextday_delivery";
	public static final String PROD_NORMAL_DELIVERY 	= "prod_normal_delivery";
	public static final String PROD_FREE_DELIVERY 	= "prod_free_delivery";
	//added by salman for sorting
	public static final String PROD_WEIGHT_GRAMS_INT = "prod_weight_grams_int";
	public static final String PROD_PRICE_RANGE_INT = "prod_price_range_int"; 
	//added by salman for cat_id
	public static final String PROD_CAT_ID = "prod_cat_id";
	
	private String CREATE_PRODUCT_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_PRODUCTS
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ PRODUCT_ID + " TEXT ,"
			+ PROD_MAINCAT_ID + " TEXT ,"
			+ PROD_SUBCAT_ID + " TEXT ,"
			+ PROD_CAT_ID + " TEXT ,"
			+ PROD_TITLE + " TEXT ,"
			+ PROD_DESCRIPTION + " TEXT ,"
			+ PROD_CODE + " TEXT ,"
			+ PROD_WEIGHT_GRAMS + " TEXT ,"
			+ PROD_IMAGE + " TEXT ,"
			+ PROD_LIST_PRCE + " TEXT ,"
			+ PROD_OUR_PRICE + " TEXT ,"
			+ PROD_SPECIAL_PRICE + " TEXT ,"
			+ PROD_EGGLESS_TYPE + " TEXT ,"
			+ PROD_WITHEGG_TYPE + " TEXT ,"
			+ PROD_SAMEDAY_DELIVERY + " TEXT ,"
			+ PROD_NEXTDAY_DELIVERY + " TEXT ,"
			+ PROD_WEIGHT_GRAMS_INT + " INTEGER ,"
			+ PROD_PRICE_RANGE_INT + " INTEGER ,"
			+ PROD_NORMAL_DELIVERY + " TEXT ,"
			+ PROD_FREE_DELIVERY + " TEXT );";
	
	
	//Table Search Product Details
	public static final String TABLE_SEARCHED_PRODUCT_DETAILS = "searched_product_details";
	
	private String CREATE_SEARCHED_PRODUCT_DETAILS="CREATE TABLE IF NOT EXISTS "+TABLE_SEARCHED_PRODUCT_DETAILS
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ PRODUCT_ID + " TEXT ,"
			+ PROD_SUBCAT_ID + " TEXT ,"
			+ PROD_CAT_ID + " TEXT ,"
			+ PROD_TITLE + " TEXT ,"
			+ PROD_DESCRIPTION + " TEXT ,"
			+ PROD_CODE + " TEXT ,"
			+ PROD_WEIGHT_GRAMS + " TEXT ,"
			+ PROD_IMAGE + " TEXT ,"
			+ PROD_LIST_PRCE + " TEXT ,"
			+ PROD_OUR_PRICE + " TEXT ,"
			+ PROD_SPECIAL_PRICE + " TEXT ,"
			+ PROD_EGGLESS_TYPE + " TEXT ,"
			+ PROD_WITHEGG_TYPE + " TEXT ,"
			+ PROD_SAMEDAY_DELIVERY + " TEXT ,"
			+ PROD_NEXTDAY_DELIVERY + " TEXT ,"
			+ PROD_NORMAL_DELIVERY + " TEXT ,"
			+ PROD_FREE_DELIVERY + " TEXT );";
	
	public static final String TABLE_ORDERS 		= "order_table";
	public static final String ORDER_ID				= "order_id";
	public static final String PRODUCT_AMOUNT = "product_amount";
	public static final String SHIPPING_CHARGE = "shipping_charge";
	public static final String COUPON_ID = "coupon_id";
	public static final String DISCOUNT_AMOUNT = "discount_amount";
	public static final String POINTS_REDEEM = "points_id";
	public static final String DELIVERY_DATE = "delivery_date";
	public static final String TOTAL_AMOUNT = "total_amount";
	public static final String DELIVERY_MODE = "delivery_mode";
	
	public static final String ORDER_NOTE = "order_note";
	public static final String MESSAGE_BILLING = "message_billing";
	private String CREATE_ORDER_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_ORDERS
			+" ( "+AUTO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "
			+ ORDER_ID + " TEXT ,"
			+ USER_ID + " TEXT, "
			+ PRODUCT_AMOUNT + " TEXT ,"
			+ SHIPPING_CHARGE + " TEXT ,"
			+ COUPON_ID + " TEXT ,"
			+ POINTS_REDEEM + " TEXT ,"
			+ DELIVERY_DATE + " TEXT ,"
			+ DELIVERY_MODE + " TEXT ,"
			+ DISCOUNT_AMOUNT + " TEXT ,"
			+ ORDER_NOTE + " TEXT ,"
			+ TOTAL_AMOUNT + " TEXT ,"
			+ MESSAGE_BILLING + " TEXT );";

	
	public  DatabaseHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_CATEGORIES_TABLE);
		db.execSQL(CREATE_CITY_TABLE);
		db.execSQL(CREATE_SUBCATEGORY_TABLE);
		db.execSQL(CREATE_TABLE_SHOPPING_CART);
		db.execSQL(CREATE_PRODUCT_ATTRIBUTE);
		db.execSQL(CREATE_PRODUCT_TABLE);
		db.execSQL(CREATE_SEARCHED_PRODUCT_DETAILS);
		db.execSQL(CREATE_ORDER_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MAIN_CATEGORIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPPING_CART);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_ATTRIBUTE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCHED_PRODUCT_DETAILS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
		onCreate(db);
	}

}
