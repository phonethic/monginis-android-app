package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CakeCategoriesAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.SubCategorModel;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;



/**
 * Load the Sub categories from main category. 
 * Gets the data from the server on the first occurrence and stores that in database
 * and from next occurrence it fetches the data from database.
 *
 */
public class SubCategoiresFragment extends Fragment implements DBListener,OnItemClickListener{

	private View mView;
	private ListView mList;

	private ArrayList<String>mArrSubCateg=new ArrayList<String>();
	private ArrayList<String>mArrSubCategId=new ArrayList<String>();


	private CakeCategoriesAdapter mAdapter;
	private String msCategId="";
	private ProgressDialog mProgressDialog;

	private ArrayList<SubCategorModel> marrSubCat;
	protected ArrayList<String> msarrCatName;

	
	  
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/**
		 * Initialize the class variables 
		 * 
		 */
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		msCategId=getArguments().getString("CategoryId");
		Config.Log("MAIN_CAT:","MAIN_CAT: "+msCategId);
		
		/**
		 * 
		 * Check the local data
		 */
		checkCachedData();
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView=(View)inflater.inflate(R.layout.fragment_sub_categories, null);
		mList=(ListView)mView.findViewById(R.id.listSubCategory);
		mList.setOnItemClickListener(this);


		return mView;
	}

	
	/**
	 * Network request to load the sub-categories based on the selected main categori id
	 * 
	 * @param categ_id
	 */
	void getAllSubCategories(String categ_id){

		msCategId=categ_id;
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.GET_ALL_SUB_CATEGORIES+categ_id;
		showProgressDialog();
		Config.Log(TAG, TAG+" Called ");
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				try
				{
					Config.Log(TAG, response.toString());
					if(response!=null){
						String status=response.getString("success");
						if(status.equalsIgnoreCase("true")){
							/**
							 * parse the information from the server
							 * 
							 */
							JSONArray jsonArr=response.getJSONArray("data");
							ContentValues[] values = new ContentValues[jsonArr.length()];

							for(int i=0;i<jsonArr.length();i++){
								ContentValues value = new ContentValues();
								JSONObject jsonObject=jsonArr.getJSONObject(i);
								value.put(DatabaseHelper.SUB_CATEGORY_NAME,jsonObject.getString("name"));
								value.put(DatabaseHelper.ID,jsonObject.getInt("id"));
								value.put(DatabaseHelper.MAIN_CAT_ID, Integer.parseInt(msCategId));
								values[i]=value;
								mArrSubCateg.add(jsonObject.getString("name"));
								mArrSubCategId.add(jsonObject.getString("id"));
								Config.Log("SUB CAT ", "SUB CAT "+jsonObject.getString("name"));
							}
							
							/**
							 * Insert into database
							 * 
							 */
							insertIntoDb(values);
							//setData(mArrSubCateg);
						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								dismissDialog();
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}
		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}

	
	/**
	 * Set the list of Sub-Categories
	 * 
	 */
	public void setData(){
		dismissDialog();
		LogFile.LogData("setData");
		mAdapter=new CakeCategoriesAdapter(getActivity(), 0, 0, msarrCatName,2);
		mList.setAdapter(mAdapter);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_layout_controller);
		mList.setLayoutAnimation(controller);
	}

	void showToast(String text){
		Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCompleteOperation(String tag) {
		LogFile.LogData("onCompleteOperation");
		dismissDialog();
		if(tag.equalsIgnoreCase("SUB_CATEGORY")){
			
			/**
			 * Fetch the sub-category information from database
			 * 
			 */
			fetchData();
			//fetchCategoyId();
		}

	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {

	}

	@Override
	public void rowCount(long rowCount, String tag) {
		dismissDialog();
		LogFile.LogData("rowCount");
		if(tag.equalsIgnoreCase(Config.ROW_COUNT)){
			if(rowCount==0){
				LogFile.LogData("rowCount 0");
				
				/**
				 * If sub-categories data not present in database than make a network request
				 * 
				 */
				getAllSubCategories(msCategId);
			}else{
				
				/**
				 * Else make a database queriy to load the data from database
				 * 
				 */
				fetchData();
			}
		}else if(tag.equalsIgnoreCase(Config.ROW_COUNT_WHERE)){
			if(rowCount==0){
				LogFile.LogData("rowCount 0");
				/**
				 * If sub-categories data not present in database than make a network request
				 * 
				 */
				getAllSubCategories(msCategId);
			}else{	
				/**
				 * Else make a database queriy to load the data from database
				 * 
				 */
				fetchData();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		
		final Map<String, String> param = new HashMap<String, String>();
		param.put("SubCat_Title",  EventsName.category +" - "+msarrCatName.get(position));
		EventsName.sub_category = msarrCatName.get(position);
		EventTracker.logEvent(EventsName.SUB_CATEGORIES, param);
		
		ProductListFragment prodFrag=new ProductListFragment();
		Bundle b=new Bundle();
		b.putString("categId", msCategId);
		b.putString("prodId",  marrSubCat.get(position).getSub_cat_id());
		b.putString("tag", "fromSubCat");
		prodFrag.setArguments(b);
		
		transaction.add(R.id.content_frame, prodFrag);
		transaction.addToBackStack(null);
		transaction.commit();

		Config.SUB_CAT_ID = marrSubCat.get(position).getSub_cat_id();
	}

	/**
	 * Database query to stores the sub-category information into database,
	 * returns the result to the implemented method of DbListner onCompleteOperation
	 * 
	 * @param contentValues
	 */
	void insertIntoDb(ContentValues[] contentValues ){
		LogFile.LogData("sub categories insertIntoDb");
		try{
			showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_SUB_CATEGORY,
					Config.INSERT_DATA, "SUB_CATEGORY").execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * Db query to fetch the sub-category information
	 * 
	 */
	void fetchData() {
		String msWhere = "WHERE "+DatabaseHelper.MAIN_CAT_ID+" = '"+msCategId+"'";
		LogFile.LogData("fetchData");
		
		new AsyncDatabaseObjectQuery<SubCategorModel>(getActivity(), new OnCompleteListner<SubCategorModel>() {

			@Override
			public void onComplete(ArrayList<SubCategorModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				
				LogFile.LogData("onComplete");

				marrSubCat = response;
				LogFile.LogData("Size "+marrSubCat.size());
				
				msarrCatName = new ArrayList<String>();
				for(int i=0;i<response.size();i++){
					msarrCatName.add(response.get(i).getSub_cat_name());
				}
				
				/**
				 * Set the list of sub-categories
				 * 
				 */
				setData();
			}
		}, 
		DatabaseHelper.TABLE_SUB_CATEGORY,
		Config.RETRIVE_SUB_CAT,
		msWhere, 
		"SUB_CATEGORY").execute();


	}
	void fetchCategoyId(){
		showProgressDialog();
	}

	
	/**
	 * Database query to check the data of subcategories, return the result to the implemented method of DbListener rowCount()
	 * 
	 */
	void checkCachedData() {
		String msWhere = "WHERE "+DatabaseHelper.MAIN_CAT_ID+" = '"+msCategId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),
					this, 
					DatabaseHelper.TABLE_SUB_CATEGORY,
					DatabaseHelper.AUTO_ID, 
					Config.ROW_COUNT_WHERE, 
					Config.ROW_COUNT_WHERE,
					msWhere).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Config.Log("RESUME","PAUSE");
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Config.Log("RESUME","RESUME");
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Config.Log("RESUME","START");
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Config.Log("RESUME","STOP");
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		Config.Log("RESUME","DESTROY VIEW");
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		Config.Log("RESUME","DETACH");
	}


}
