package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.ProductListAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.ProductModel;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;


/**
 * Load the Product list based on Sub categories and main category. 
 * Gets the data from the server on the first occurrence and stores that in database
 * and from next occurrence it fetches the data from database.
 *
 */
public class ProductListFragment extends Fragment implements DBListener, OnItemClickListener{

	private ArrayList<ProductModel> mArrProducts=new ArrayList<ProductModel>();
	private ArrayList<String> mArrProductsIds=new ArrayList<String>();
	private ProductListAdapter mAdapter;
	private View mView;
	private ListView mList;
	private String msCategId="";
	private String msProductId="";
	private ProgressDialog mProgressDialog;
	private onProductListClicked mInstance;
	ArrayList<ProductModel> pModel = new ArrayList<ProductModel>();
	String tag = "";

	ImageView CustomerProfileBtn;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	public interface onProductListClicked{

		public void onProductClicked(ArrayList<String> mArrProductId);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		/**
		 * Initiliaze the class varaibles
		 * 
		 */
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		tag = getArguments().getString("tag");

		if(tag.equalsIgnoreCase("fromSubCat")){

			msCategId=getArguments().getString("categId");
			msProductId=getArguments().getString("prodId");
			LogFile.LogData("Cat_ID "+msCategId+" Prod_ID "+msProductId);


			/**
			 * Db operation to check the product count
			 * 
			 */
			checkCachedDataWhere();

		} else {

			pModel = getArguments().getParcelableArrayList("Products");
		}
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		/**
		 * Initialize the layout
		 * 
		 */
		mView=(View)inflater.inflate(R.layout.fragment_product_list, null);
		mList=(ListView)mView.findViewById(R.id.listProduct);
		CustomerProfileBtn = (ImageView)mView.findViewById(R.id.CustomerProfileBtn);

		CustomerProfileBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				FragmentManager manager=getActivity().getSupportFragmentManager();
				FragmentTransaction transaction =manager.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

				SortByFragment subFrag=new SortByFragment();
				transaction.add(R.id.content_frame, subFrag, "Detils");
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});

		LandingScreen.sortVisible();
		if(tag.equalsIgnoreCase("fromSort")){

			if(pModel!=null){

				if(pModel.size()==0){
					showToast("Sorry, No product to show.");
				}
				else{
					mArrProductsIds.clear();
					for(int i=0;i<pModel.size();i++){

						mArrProductsIds.add(pModel.get(i).getId());
					}
					mArrProducts = pModel;
					setData(pModel);
				}
			}
		}


		return mView;
	}

	/**
	 * Set the product list
	 * 
	 * @param mArrProducts
	 */
	void setData(ArrayList<ProductModel> mArrProducts){
		dismissDialog();
		mAdapter=new ProductListAdapter(getActivity(), 0, mArrProducts, 0);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_layout_controller);
		mList.setAdapter(mAdapter);
		mList.setLayoutAnimation(controller);
		mList.setOnItemClickListener(this);

	}


	/**
	 * Network request to get the product list based on selected Category id and Sub-Category
	 * 
	 * @param categ_id
	 * @param productId
	 */
	void getAllProducts(String categ_id,String productId){
		LogFile.LogData("getAllProducts");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.GET_ALL_PRODUCTS+categ_id+"/subcategory_id/"+productId;
		//Config.Log("URL ", "URL"+url);
		LogFile.LogData("URL "+url);
		showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				try
				{
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){

						if(status.equalsIgnoreCase("true")){

							/**
							 * Parse the data from server
							 * 
							 */
							JSONArray jsonArr=response.getJSONArray("data");
							ContentValues[] values = new ContentValues[jsonArr.length()];
							for(int i=0;i<jsonArr.length();i++){
								ContentValues value = new ContentValues();
								JSONObject jsonObject=jsonArr.getJSONObject(i);
								ProductModel mProd = new ProductModel();
								mProd.setMaincat_id(msCategId);
								mProd.setSubcat_id(msProductId);
								mProd.setCode(jsonObject.getString("code"));
								mProd.setDescription(jsonObject.getString("description"));
								mProd.setEggless_type(jsonObject.getString("eggless_type"));
								mProd.setFree_delivery(jsonObject.getString("free_delivery"));
								mProd.setId(jsonObject.getString("id"));
								mProd.setImage(jsonObject.getString("image"));
								mProd.setList_price(jsonObject.getString("list_price"));
								mProd.setNextday_delivery(jsonObject.getString("nextday_delivery"));
								mProd.setNormal_delivery(jsonObject.getString("normal_delivery"));
								mProd.setOur_price(jsonObject.getString("our_price"));
								mProd.setSameday_delivery(jsonObject.getString("sameday_delivery"));
								mProd.setSpecial_price(jsonObject.getString("special_price"));
								mProd.setTitle(jsonObject.getString("title"));
								mProd.setWeight_grams(jsonObject.getString("weight_grams"));
								mProd.setWithegg_type(jsonObject.getString("withegg_type"));	
								//added by salman for cat_id
								mProd.setCat_id(jsonObject.getString("cat_id"));
								mArrProducts.add(mProd);

								value.put(DatabaseHelper.PRODUCT_ID,mProd.getId());
								value.put(DatabaseHelper.PROD_SUBCAT_ID,mProd.getSubcat_id());
								value.put(DatabaseHelper.PROD_MAINCAT_ID,mProd.getMaincat_id());
								value.put(DatabaseHelper.PROD_CODE,mProd.getCode());
								value.put(DatabaseHelper.PROD_DESCRIPTION,mProd.getDescription());
								value.put(DatabaseHelper.PROD_EGGLESS_TYPE,mProd.getEggless_type());
								value.put(DatabaseHelper.PROD_FREE_DELIVERY,mProd.getFree_delivery());
								value.put(DatabaseHelper.PROD_IMAGE,mProd.getImage());
								value.put(DatabaseHelper.PROD_LIST_PRCE,mProd.getList_price());
								value.put(DatabaseHelper.PROD_NEXTDAY_DELIVERY,mProd.getNextday_delivery());
								value.put(DatabaseHelper.PROD_NORMAL_DELIVERY,mProd.getNormal_delivery());
								value.put(DatabaseHelper.PROD_OUR_PRICE,mProd.getOur_price());
								value.put(DatabaseHelper.PROD_SAMEDAY_DELIVERY,mProd.getSameday_delivery());
								value.put(DatabaseHelper.PROD_SPECIAL_PRICE,mProd.getSpecial_price());
								value.put(DatabaseHelper.PROD_TITLE,mProd.getTitle());
								value.put(DatabaseHelper.PROD_WEIGHT_GRAMS,mProd.getWeight_grams());
								value.put(DatabaseHelper.PROD_WITHEGG_TYPE,mProd.getWithegg_type());
								value.put(DatabaseHelper.PROD_CAT_ID, mProd.getCat_id());
								if(!mProd.getWeight_grams().equalsIgnoreCase(""))
									value.put(DatabaseHelper.PROD_WEIGHT_GRAMS_INT, Integer.parseInt(mProd.getWeight_grams()));
								value.put(DatabaseHelper.PROD_PRICE_RANGE_INT, Integer.parseInt(mProd.getSpecial_price()));
								values[i]=value;

								Config.Log("SUB CAT ", "SUB CAT "+jsonObject.getString("title"));
								Config.Log("CAT CAT ID", "CAT CAT ID "+ mProd.getCat_id());
							}

							/**
							 * Insert into db
							 * 
							 */
							insertIntoDb(values);
							//setData();
						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								dismissDialog();
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}
		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}


	/**
	 * Db query to save the product information into database, returns the result to the implemenetd method of DbListener onCompleteOperation() 
	 * 
	 * @param contentValues
	 */
	void insertIntoDb(ContentValues[] contentValues ){
		LogFile.LogData("Prodcuts insertIntoDb");
		try{
			showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_PRODUCTS,
					Config.INSERT_DATA, "Prodcuts").execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void checkCachedData(){
		LogFile.LogData("checkCachedData");
		try{

			new AsyncDatabaseQuery(getActivity(),
					this, 
					DatabaseHelper.TABLE_PRODUCTS,
					DatabaseHelper.AUTO_ID, 
					Config.ROW_COUNT,  
					Config.ROW_COUNT)
			.execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	/**
	 * Db query to check the products count based on selected sub-categorie and selected main category,
	 * returns the result to the implemented method of DbListener
	 * 
	 */
	void checkCachedDataWhere(){
		LogFile.LogData("checkCachedDataWhere");
		try{
			String msWhere =  "WHERE "+DatabaseHelper.PROD_MAINCAT_ID+" = '"+msCategId+"' AND "+DatabaseHelper.PROD_SUBCAT_ID+" ='"+msProductId+"' ";
			new AsyncDatabaseQuery(getActivity(),
					this, 
					DatabaseHelper.TABLE_PRODUCTS,
					DatabaseHelper.AUTO_ID, 
					Config.ROW_COUNT_WHERE,  
					Config.ROW_COUNT_WHERE,
					msWhere)
			.execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	/**
	 * 
	 * Database query to fetch the products information from database based on the selected Category id and Sub Category id 
	 */
	void fetchData(){
		//showProgressDialog();
		LogFile.LogData("fetchData");
		String msWhere =  "WHERE "+DatabaseHelper.PROD_MAINCAT_ID+" = '"+msCategId+"' AND "+DatabaseHelper.PROD_SUBCAT_ID+" ='"+msProductId+"' ";
		new AsyncDatabaseObjectQuery<ProductModel>(getActivity(), new OnCompleteListner<ProductModel>() {

			@Override
			public void onComplete(ArrayList<ProductModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				//dismissDialog();
				LogFile.LogData("onComplete");
				mArrProducts.clear();
				mArrProducts = response;
				for(int i=0;i<response.size();i++){
					mArrProductsIds.add(mArrProducts.get(i).getId());
					Config.Log("CAT_IDS ","CAT_IDS " + mArrProducts.get(i).getCat_id());
				}

				/**
				 * 
				 * set the product list
				 */
				setData(mArrProducts);
			}
		}, DatabaseHelper.TABLE_PRODUCTS, Config.RETRIVE_DATA_WHERE,msWhere, "SUB_CATEGORY").execute();


	}


	void showToast(String msg){
		Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
	}



	@Override
	public void onCompleteOperation(String tag) {
		LogFile.LogData("onCompleteOperation");
		dismissDialog();
		if(tag.equalsIgnoreCase("Prodcuts")){

			/**
			 * Fetch the products from database
			 */
			fetchData();
		}

	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {


	}

	@Override
	public void rowCount(long rowCount, String tag) {
		LogFile.LogData("rowCount");
		if(tag.equalsIgnoreCase(Config.ROW_COUNT)){
			if(rowCount==0){
				LogFile.LogData("rowCount 0");
				getAllProducts(msCategId, msProductId);
				//getAllSubCategories(msCategId);
			}else{
				LogFile.LogData("else");
				fetchData();
			}
		}else if(tag.equalsIgnoreCase(Config.ROW_COUNT_WHERE)){
			if(rowCount==0){
				LogFile.LogData("rowCountWhere 0");

				/**
				 * If no-information of the products in database than make a network request
				 * 
				 */
				getAllProducts(msCategId, msProductId);
			}else{
				LogFile.LogData("else");
				fetchData();
			}
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		FragmentManager manager=getActivity().getSupportFragmentManager();
		FragmentTransaction transaction =manager.beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);


		final Map<String, String> param = new HashMap<String, String>();
		param.put("ProductWithCategory",  EventsName.category +" - "+EventsName.sub_category +" - "+mArrProducts.get(position).getTitle());
		EventsName.product = mArrProducts.get(position).getTitle();
		EventTracker.logEvent(EventsName.PRODUCTS, param);

		ProductDetailPagerFragment subFrag=new ProductDetailPagerFragment();
		Bundle b=new Bundle();
		b.putStringArrayList("CategoryId", mArrProductsIds);
		b.putInt("position", position);

		//added by salman to get from DB
		b.putParcelableArrayList("Product", mArrProducts);

		subFrag.setArguments(b);
		transaction.add(R.id.content_frame, subFrag, "Detils");
		transaction.addToBackStack(null);
		Config.SUB_CATEGORY_SELECTED_PRODUCT_ID = mArrProductsIds.get(position);
		// Commit the transaction
		transaction.commit();

		//to remove sort by
		LandingScreen.sortGone();

	}

	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		LandingScreen.sortGone();
	}


}
