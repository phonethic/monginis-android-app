package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.phonethics.adapters.CallAdapter;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.adapters.HeadQuarterModel;

public class StoreLocatorFragment extends Fragment implements OnItemClickListener,LocationListener{

	private View mView;
	private TextView txtSelectedCity;
	private TextView txtSelectedArea;
	private ProgressDialog mProgressDialog;
	private Dialog mListDialog;
	ListView mDialogList;
	ListView mCallList;
	ArrayList<String> mCityId = new ArrayList<String>();
	ArrayList<String> mCityNames = new ArrayList<String>();
	ArrayList<String> mAreaId = new ArrayList<String>();
	ArrayList<String> mAreaNames = new ArrayList<String>();
	CategoriesAdapter mAdapter;
	boolean cityClicked = false;
	boolean areaClciked = false;
	String citysuccess = "";
	String areasuccess = "";
	ArrayList<HeadQuarterModel> storeModelList = new ArrayList<HeadQuarterModel>();
	private GoogleMap map;
	Location mLocation;
	LocationManager locationManager ;
	String provider;
	SupportMapFragment fm;
	String selectedCityId;
	Marker plottedMarkers[];
	HashMap<String,HeadQuarterModel> markerDetail = new HashMap<String, HeadQuarterModel>();
	ArrayList<String> callNumbers = new ArrayList<String>();
	Dialog mCallDialog;
	CallAdapter mCallAdapter;
	TextView txtCancel;
	Location location;
	NetworkCheck netCheck;
	String mCurrentCity="";
	String mCurrentCityId="";
	boolean autoCity = false;
	RelativeLayout dividerView;
	EditText citySearch;
	int textlength=0;
	boolean cityParsingError;
	String selectedStoreId = "";
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		netCheck = new NetworkCheck(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(netCheck.isNetworkAvailable()){
			checkCurrentLocation();
		}
		getCityList();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_store_locator, null);
		txtSelectedArea=(TextView)mView.findViewById(R.id.txtStoreArea);
		txtSelectedCity=(TextView)mView.findViewById(R.id.txtStoreCity);
		mListDialog = new Dialog(getActivity());
		mCallDialog = new Dialog(getActivity());
		mCallDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mCallDialog.setContentView(R.layout.call_list);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		mCallList=(ListView)mCallDialog.findViewById(R.id.mCallListView);
		mDialogList.setOnItemClickListener(this);
		txtCancel=(TextView)mCallDialog.findViewById(R.id.txtCancel);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		citySearch = (EditText)mListDialog.findViewById(R.id.citySearch);


		txtSelectedArea.setTypeface(MonginisApplicationClass.getTypeFace());
		txtSelectedCity.setTypeface(MonginisApplicationClass.getTypeFace());
		txtCancel.setTypeface(MonginisApplicationClass.getTypeFace());

		txtSelectedCity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				txtSelectedArea.setText("SELECT AREA");
				cityClicked = true;
				areaClciked = false;
				mListDialog.setTitle("Select City");
				if(netCheck.isNetworkAvailable()){

					if(!cityParsingError){

						if(citysuccess.equalsIgnoreCase("true"))
						{
							mAdapter = null;
							mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mCityNames);
							mDialogList.setAdapter(mAdapter);
						}
						mListDialog.show();
					}
					else
						showToast("Unable to fetch city list due to slow network connection");

				}
				else{
					showToast("No internet connection.");
				}

			}
		});
		txtSelectedArea.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(txtSelectedCity.getText().toString().equalsIgnoreCase("SELECT CITY")){
					showToast("Please select a city first.");
				}
				else{

					cityClicked = false;
					areaClciked = true;
					mListDialog.setTitle("Select Area");
					if(areasuccess.equalsIgnoreCase("true")){
						mAdapter = null;
						mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mAreaNames);
						mDialogList.setAdapter(mAdapter);
					}
					mListDialog.show();
				}

			}
		});

		mCallList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				//				if(position == callNumbers.size()-1){
				//					mCallDialog.dismiss();
				//					//callNumbers.clear();
				//				}
				//				else{
				Map<String, String> param = new HashMap<String, String>();
				param.put("Store_Id", selectedStoreId);
				param.put("Store_Number", callNumbers.get(position));
				EventTracker.logEvent(EventsName.CALL_STORE,param);

				Intent caller = new Intent(android.content.Intent.ACTION_DIAL);
				caller.setData(Uri.parse("tel:" +callNumbers.get(position)));
				startActivity(caller);
				//				}
				//showToast(callNumbers.get(position));
			}
		});

		txtCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mCallDialog.isShowing()){
					mCallDialog.dismiss();
				}
			}
		});

		// rendering map 
		// Getting Google Play availability status
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());


		if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

			int requestCode = 10;
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
			dialog.show();

		}
		else{

			fm = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapView);
			map = fm.getMap();
			map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble("21.7679"),Double.parseDouble("78.8718"))));
			// Zoom in the Google Map
			map.animateCamera(CameraUpdateFactory.zoomTo(4));
		}

		//to search for city and area from list
		citySearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				final ArrayList<String> CITY_NAME_TEMP = new ArrayList<String>();
				final ArrayList<String> CITY_ID_TEMP = new ArrayList<String>();
				final ArrayList<String> AREA_ID_TEMP = new ArrayList<String>();
				final ArrayList<String> AREA_NAME_TEMP = new ArrayList<String>();

				if(cityClicked){

					textlength = citySearch.getText().length();
					String text = citySearch.getText().toString();


					for (int i = 0; i < mCityNames.size(); i++){

						if(textlength <= mCityNames.get(i).length()){

							if (text.equalsIgnoreCase((String) mCityNames.get(i).subSequence(0, textlength))){

								CITY_NAME_TEMP.add(mCityNames.get(i)); 
								CITY_ID_TEMP.add(mCityId.get(i));
								//Log.d("TEXT ","TEXT TITLE " + citySearch.getText().toString());

							}

						}
					}
					mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, CITY_NAME_TEMP);
					mDialogList.setAdapter(mAdapter);
				}
				else if(areaClciked){

					textlength = citySearch.getText().length();
					String text = citySearch.getText().toString();


					for (int i = 0; i < mAreaNames.size(); i++){

						if(textlength <= mAreaNames.get(i).length()){

							if (text.equalsIgnoreCase((String) mAreaNames.get(i).subSequence(0, textlength))){

								AREA_NAME_TEMP.add(mAreaNames.get(i)); 
								AREA_ID_TEMP.add(mAreaId.get(i));
								//Log.d("TEXT ","TEXT TITLE " + citySearch.getText().toString());

							}

						}
					}
					mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, AREA_NAME_TEMP);
					mDialogList.setAdapter(mAdapter);
				}


				mDialogList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						//showToast(CITY_ID_TEMP.get(position));

						try {

//							InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
//							inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

							InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
							mgr.hideSoftInputFromWindow(citySearch.getWindowToken(), 0);

							citySearch.setText("");
							if(cityClicked){

								txtSelectedCity.setText(CITY_NAME_TEMP.get(position));
								mListDialog.dismiss();
								txtSelectedArea.setVisibility(View.VISIBLE);
								//getting areas for selected city
								getAreaListOfSelectedCity(CITY_ID_TEMP.get(position));
								//getting headquarter for selected city
								getHeadQuarterOfSelectedCity(CITY_ID_TEMP.get(position));
								selectedCityId = CITY_ID_TEMP.get(position);
							}
							else if(areaClciked){
								getAllStoresOfArea(selectedCityId,AREA_ID_TEMP.get(position));
								txtSelectedArea.setText(AREA_NAME_TEMP.get(position));
								
								
								Map<String, String> param = new HashMap<String, String>();
								param.put("City", txtSelectedCity.getText().toString());
								param.put("Area", txtSelectedCity.getText().toString() + "-" + txtSelectedArea.getText().toString());
								EventTracker.logEvent(EventsName.STORE_LOCATOR, param);
								
								mListDialog.dismiss();
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}
				});
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});


		return mView;
	}

	private void drawMap() {
		// TODO Auto-generated method stub

		//for map 
		// Getting Google Play availability status
		//int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());


		//		if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
		//
		//			int requestCode = 10;
		//			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
		//			dialog.show();
		//
		//		}
		//		else{
		//
		//			fm = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapView);
		//			map = fm.getMap();
		if(map!=null){

			//chk for Location Service on or not
			LocationManager lm = null;
			boolean gps_enabled = false,network_enabled = false;
			if(lm==null)
				lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
			try{
				gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
			}catch(Exception ex){}
			try{
				network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			}catch(Exception ex){}

			if(!gps_enabled && !network_enabled){
				Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage("Location service not enabled. Do you want to enable it.");
				dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface paramDialogInterface, int paramInt) {
						// TODO Auto-generated method stub
						Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
						getActivity().startActivity(myIntent);
						//get gps
					}
				});
				dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface paramDialogInterface, int paramInt) {
						// TODO Auto-generated method stub

					}
				});
				dialog.show();

			}
			else{
				//showToast("Already enabled");
				map.clear();
				plottedMarkers = new Marker[storeModelList.size()];
				for(int i=0;i<storeModelList.size();i++){

					String address = storeModelList.get(i).getAddress1() + " " + storeModelList.get(i).getAddress2();
					//						map.addMarker(new MarkerOptions()
					//						.position(new LatLng(Double.parseDouble(storeModelList.get(i).getLatitude()),Double.parseDouble(storeModelList.get(i).getLongitude())))
					//								);
					plottedMarkers[i] = map.addMarker(new MarkerOptions()
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
					.position(new LatLng(Double.parseDouble(storeModelList.get(i).getLatitude()),Double.parseDouble(storeModelList.get(i).getLongitude())))
					.title(address));

					markerDetail.put(plottedMarkers[i].getId(), storeModelList.get(i));
					drawWInfoWindow(address);
				}
				// Showing the current location in Google Map
				//if(!(storeModelList.get(0).getLatitude()).equalsIgnoreCase("0") || !(storeModelList.get(0).getLongitude().equalsIgnoreCase("0"))){

				if(areaClciked){

					if(!storeModelList.get(0).getLatitude().equalsIgnoreCase("0")){

						map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(storeModelList.get(0).getLatitude()),Double.parseDouble(storeModelList.get(0).getLongitude()))));
						// Zoom in the Google Map
						map.animateCamera(CameraUpdateFactory.zoomTo(12));
						//}
					}
				}
				else{
					map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble("21.7679"),Double.parseDouble("78.8718"))));
					// Zoom in the Google Map
					map.animateCamera(CameraUpdateFactory.zoomTo(4));
					//}
				}

			}


			//}
		}
	}

	void drawWInfoWindow(String address) {
		// TODO Auto-generated method stub

		final String add = address;
		map.setInfoWindowAdapter(new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {
				// TODO Auto-generated method stub
				//Config.Log("MARKER","MARKER " +arg0);
				View v = getActivity().getLayoutInflater().inflate(R.layout.info_window_layout, null);
				// Getting reference to the TextView to set latitude
				TextView txtAddress = (TextView) v.findViewById(R.id.txtStoreAddress);
				// Setting the longitude
				txtAddress.setText(""+ arg0.getTitle());
				//arg0.setTitle(add);
				return v;
			}

			@Override
			public View getInfoContents(Marker arg0) {

				return null;
			}
		});

		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				// TODO Auto-generated method stub
				//				Config.Log("SIZE NEW","SIZE NEW " + address);Config.Log("SIZE NEW","SIZE NEW " + address);
				//				showToast(arg0.getTitle());

				HeadQuarterModel values = markerDetail.get(arg0.getId());
				callNumbers = values.getStore_phone();
				selectedStoreId = values.getId();
				//				showToast(""+callNumbers.size());
				//				for(int j=0;j<callNumbers.size();j++){
				//
				//				}
				//callNumbers.add("CANCEL");
				mCallAdapter = new CallAdapter(getActivity(), R.drawable.ic_launcher,R.layout.call_list, callNumbers);
				mCallList.setAdapter(mCallAdapter);
				mCallDialog.show();
				//				showToast(values.getStore_phone());
				//				String phone = values.getStore_phone();
				//				if(phone.contains("/")){
				//					//showToast("true");
				//					String fullNumber = phone.replace("-", "");
				//					fullNumber.trim();
				//					//					showToast(fullNumber);
				//
				//					//find the string for /
				//					int index = fullNumber.indexOf("/");
				//					String call = (fullNumber.substring(0,index));
				//					Intent caller = new Intent(android.content.Intent.ACTION_DIAL);
				//					caller.setData(Uri.parse("tel:" +call));
				//					startActivity(caller);
				//				}
				//				else{
				//					String call = phone.trim();
				//					//showToast("false");
				//					Intent caller = new Intent(android.content.Intent.ACTION_DIAL);
				//					caller.setData(Uri.parse("tel:" +call));
				//					startActivity(caller);
				//				}

			}
		});
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	void getCityList() {
		// TODO Auto-generated method stub

		try {

			showProgressDialog();
			String TAG="STORE LOCATOR CITY LIST";
			String url=Config.GET_CITY_LIST_FOR_STORELOCATOR;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							citysuccess = json.getString("success");
							//							mCityNames.clear();
							//							mCityId.clear();
							if(citysuccess.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject obj = data.getJSONObject(i);
									String city_id=obj.getString("id");
									String city_name=obj.getString("city_name");
									mCityId.add(city_id);
									mCityNames.add(city_name);
								}
								mAdapter = null;
								mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mCityNames);
								mDialogList.setAdapter(mAdapter);

								if(mCurrentCity.length()!=0){

									autoCity = true;
									for(int i=0;i<mCityNames.size();i++){

										if(mCurrentCity.equalsIgnoreCase(mCityNames.get(i))){
											txtSelectedCity.setText(mCurrentCity);
											mCurrentCityId = mCityId.get(i);
											selectedCityId = mCurrentCityId;
											//showToast(selectedCityId);
										}
									}
								}

								if(autoCity == true){
									getAreaListOfSelectedCity(mCurrentCityId);
									getHeadQuarterOfSelectedCity(mCurrentCityId);
								}
							}
							else{
								map.clear();
								showToast("City List not available.");
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					cityParsingError = true;
					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void showToast(String msg) {
		// TODO Auto-generated method stub

		Toast.makeText(getActivity(), ""+msg,Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		// TODO Auto-generated method stub

		try {
			//			InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
			//			inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

			InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.hideSoftInputFromWindow(txtSelectedCity.getWindowToken(), 0);

			if(cityClicked){

				txtSelectedCity.setText(mCityNames.get(position));
				mListDialog.dismiss();
				txtSelectedArea.setVisibility(View.VISIBLE);
				//getting areas for selected city
				getAreaListOfSelectedCity(mCityId.get(position));
				//getting headquarter for selected city
				getHeadQuarterOfSelectedCity(mCityId.get(position));
				selectedCityId = mCityId.get(position);
			}
			else if(areaClciked){
				getAllStoresOfArea(selectedCityId,mAreaId.get(position));
				txtSelectedArea.setText(mAreaNames.get(position));
				
				Map<String, String> param = new HashMap<String, String>();
				param.put("City", txtSelectedCity.getText().toString());
				param.put("Area", txtSelectedCity.getText().toString() + "-" + txtSelectedArea.getText().toString());
				EventTracker.logEvent(EventsName.STORE_LOCATOR, param);
				
				mListDialog.dismiss();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

	void getAllStoresOfArea(String cityId, String areaId) {
		// TODO Auto-generated method stub
		try {

			showProgressDialog();
			String TAG="ALL STORES OF AREA";
			String url=Config.GET_CITY_HEADQUARTER_STORELOCATOR+cityId+"/area_id/"+areaId;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								storeModelList.clear();
								JSONArray data = json.getJSONArray("data");
								for(int j=0;j<data.length();j++){

									HeadQuarterModel obj = new HeadQuarterModel();
									JSONObject jsonObj = data.getJSONObject(j);
									obj.setId(jsonObj.getString("id"));
									obj.setCity_id(jsonObj.getString("city_id"));
									obj.setArea_id(jsonObj.getString("area_id"));
									obj.setInternal_shop_name(jsonObj.getString("internal_shop_name"));
									obj.setStore_name(jsonObj.getString("store_name"));
									obj.setUsername(jsonObj.getString("username"));
									obj.setPassword(jsonObj.getString("password"));
									obj.setOwner(jsonObj.getString("owner"));
									obj.setContact_person(jsonObj.getString("contact_person"));
									obj.setAddress1(jsonObj.getString("address1"));
									obj.setAddress2(jsonObj.getString("address2"));
									//obj.setStore_phone(jsonObj.getString("store_phone"));

									JSONArray phone = jsonObj.getJSONArray("store_phone");

									if(phone.length()!=0){
										for(int m=0;m<phone.length();m++){
											obj.setStore_phone(phone.getString(m));
										}
									}

									obj.setMobile(jsonObj.getString("mobile"));
									obj.setFax(jsonObj.getString("fax"));
									obj.setPincode(jsonObj.getString("pincode"));
									obj.setEmail(jsonObj.getString("email"));
									obj.setActive(jsonObj.getString("active"));
									obj.setHead_office(jsonObj.getString("head_office"));
									obj.setSort_order(jsonObj.getString("sort_order"));
									obj.setAct_key(jsonObj.getString("act_key"));
									obj.setPassword_request(jsonObj.getString("password_request"));
									obj.setCommission(jsonObj.getString("commission"));
									obj.setLatitude(jsonObj.getString("latitude"));
									obj.setLongitude(jsonObj.getString("longitude"));

									storeModelList.add(obj);
								}
								drawMap();
							}
							else{

								//map.clear();
								String message = json.getString("message"); 
								showToast(""+message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void getHeadQuarterOfSelectedCity(String cityId) {
		// TODO Auto-generated method stub

		try {

			showProgressDialog();
			String TAG="HEADQUARTER LIST";
			String url=Config.GET_CITY_HEADQUARTER_STORELOCATOR+cityId;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								storeModelList.clear();
								JSONArray data = json.getJSONArray("data");
								for(int j=0;j<data.length();j++){

									HeadQuarterModel obj = new HeadQuarterModel();
									JSONObject jsonObj = data.getJSONObject(j);
									obj.setId(jsonObj.getString("id"));
									obj.setCity_id(jsonObj.getString("city_id"));
									obj.setArea_id(jsonObj.getString("area_id"));
									obj.setInternal_shop_name(jsonObj.getString("internal_shop_name"));
									obj.setStore_name(jsonObj.getString("store_name"));
									obj.setUsername(jsonObj.getString("username"));
									obj.setPassword(jsonObj.getString("password"));
									obj.setOwner(jsonObj.getString("owner"));
									obj.setContact_person(jsonObj.getString("contact_person"));
									obj.setAddress1(jsonObj.getString("address1"));
									obj.setAddress2(jsonObj.getString("address2"));
									//obj.setStore_phone(jsonObj.getString("store_phone"));

									JSONArray phone = jsonObj.getJSONArray("store_phone");
									if(phone.length()!=0){
										for(int m=0;m<phone.length();m++){
											obj.setStore_phone(phone.getString(m));
										}
									}
									obj.setMobile(jsonObj.getString("mobile"));
									obj.setFax(jsonObj.getString("fax"));
									obj.setPincode(jsonObj.getString("pincode"));
									obj.setEmail(jsonObj.getString("email"));
									obj.setActive(jsonObj.getString("active"));
									obj.setHead_office(jsonObj.getString("head_office"));
									obj.setSort_order(jsonObj.getString("sort_order"));
									obj.setAct_key(jsonObj.getString("act_key"));
									obj.setPassword_request(jsonObj.getString("password_request"));
									obj.setCommission(jsonObj.getString("commission"));
									obj.setLatitude(jsonObj.getString("latitude"));
									obj.setLongitude(jsonObj.getString("longitude"));

									storeModelList.add(obj);
								}
								drawMap();
							}
							else{

								map.clear();
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void getAreaListOfSelectedCity(String cityId) {
		// TODO Auto-generated method stub

		try {

			showProgressDialog();
			String TAG="STORE LOCATOR AREA LIST";
			String url=Config.GET_AREA_LIST_FOR_SELECTEDCITY_STORELOCATOR+cityId;
			Config.Log("URL","URL "+url);
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							areasuccess = json.getString("success");
							mAreaId.clear();
							mAreaNames.clear();
							if(areasuccess.equalsIgnoreCase("true")){

								txtSelectedArea.setClickable(true);
								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject obj = data.getJSONObject(i);
									String area_id=obj.getString("id");
									String area_name=obj.getString("area_name");
									mAreaId.add(area_id);
									mAreaNames.add(area_name);
								}

								mAdapter = null;
								mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mAreaNames);
								mDialogList.setAdapter(mAdapter);
							}
							else{

								showToast("Area list not available.");
								txtSelectedArea.setClickable(false);
								map.clear();
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try{
			SupportMapFragment fragment = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapView));
			FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
			ft.remove(fragment);
			ft.commit();
		}catch(Exception e){
			e.printStackTrace();
		}

	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		//		if (map == null) {
		//			map = fm.getMap();
		//
		//		}
	}

	void checkCurrentLocation(){


		LocationManager lm = null;
		boolean gps_enabled = false,network_enabled = false;
		if(lm==null){
			lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

		}

		try{
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
			location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){}
		try{
			network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}catch(Exception ex){}

		if(!gps_enabled && !network_enabled){

		}
		else{
			//showToast("Got Location");
			if(location!=null)
				onLocationChanged(location);
			//			else
			//				showToast("null location");
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		//showToast("Called");
		double curr_lat = location.getLatitude();
		double curr_lon = location.getLongitude();

		Geocoder geoCoder = new Geocoder(getActivity(),Locale.getDefault());
		try {
			List<Address> add = geoCoder.getFromLocation(curr_lat, curr_lon, 1);
			//String CityName = add.get(0).getAddressLine(0);
			//String  StateName = add.get(0).getAddressLine(1);
			mCurrentCity = add.get(0).getLocality();
			//showToast(mCurrentCity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
