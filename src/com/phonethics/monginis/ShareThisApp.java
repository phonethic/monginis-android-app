package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.phonethics.adapters.SharedListViewAdapter;


public class ShareThisApp {
	
	Dialog dialogShare;
	ListView listViewShare;
	Context mContext;
	ArrayList<String> packageNames = new ArrayList<String>();
	ArrayList<String> appName = new ArrayList<String>();
	ArrayList<Drawable> appIcon = new ArrayList<Drawable>();
	boolean isFacebookPresent = false;
	LayoutInflater inflate;
	private UiLifecycleHelper uiHelper;
	
	
	public ShareThisApp(Activity mContext){
		this.mContext = mContext;
		dialogShare = new Dialog(mContext);
		inflate = ((Activity) mContext).getLayoutInflater();
		uiHelper = new UiLifecycleHelper(mContext, null);
		getShareIntents();
		

		
	}
	
	
	public void getShareIntents(){
		
		Intent intentShareActivity = new Intent(Intent.ACTION_SEND);
		intentShareActivity.setType("text/plain");
		intentShareActivity.putExtra(Intent.EXTRA_TEXT, "");
		final PackageManager pm = mContext.getPackageManager();
		List<ResolveInfo> packages = pm.queryIntentActivities(intentShareActivity, 0);
		ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) pm.queryIntentActivities(intentShareActivity, PackageManager.PERMISSION_GRANTED);
	
		if(packageNames.size() == 0){
			appName.add("Facebook");
			packageNames.add("com.facebook");
			appIcon.add((Drawable)(mContext.getResources().getDrawable(R.drawable.facebookiconforcustomshare)));
			for (ResolveInfo rInfo : list) {
				Log.i("Package Name","App Name: "+rInfo.activityInfo.applicationInfo.loadLabel(pm)+ " Package Name: "+rInfo.activityInfo.applicationInfo.packageName);
				String app = rInfo.activityInfo.applicationInfo.loadLabel(pm).toString();
				if(app.equalsIgnoreCase("facebook") == true && isFacebookPresent == false){
					isFacebookPresent = true;
					//inviteFriendsLayout.setVisibility(View.VISIBLE);
				}else{
					//inviteFriendsLayout.setVisibility(View.GONE);
				}
			

				if(app.equalsIgnoreCase("facebook") == false){
					packageNames.add(rInfo.activityInfo.applicationInfo.packageName);
					appName.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());
					appIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));
				}

			}

			if(!isFacebookPresent){
				appName.remove(0);
				packageNames.remove(0);
				appIcon.remove(0);
			}
		}
		
		dialogShare.setContentView(R.layout.share_dialog);
		dialogShare.setTitle("Select an action");
		listViewShare = (ListView) dialogShare.findViewById(R.id.listViewForShare);
		listViewShare.setAdapter(new SharedListViewAdapter(mContext, 0, inflate, appName, packageNames, appIcon));
	}
	
	public void showDialog(final String shareText){
		dialogShare.show();
		listViewShare.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				String app = appName.get(position);
				if(app.equalsIgnoreCase("facebook") && isFacebookPresent == true){

					FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity) mContext)
					.setLink("https://www.facebook.com/monginis")
					.setDescription(shareText)
					.setName("Monginis")
					.build();
					uiHelper.trackPendingDialogCall(shareDialog.present());
					dismissDialog();
				}
				else if(app.equalsIgnoreCase("facebook") && isFacebookPresent == false){
					Toast.makeText(mContext, "Looks like you dont have facebook installed in your device! You may want to install it to share a post using facebook", Toast.LENGTH_LONG).show();
				}

				else if(!app.equalsIgnoreCase("facebook")){
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setPackage(packageNames.get(position));
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, shareText);
					mContext.startActivity(i);
				}

				final Map<String, String> param = new HashMap<String, String>();
				param.put("ProductTitle",  EventsName.category +" - "+EventsName.sub_category+" - "+EventsName.product);
				param.put("SharingPlatform",  appName.get(position));
				param.put("Product-SharingPlatform",  EventsName.product +" - "+appName.get(position));
				//EventsName.sub_category = msarrCatName.get(position);
				EventTracker.logEvent(EventsName.SHARE_PRODUCT, param);
				
				dismissDialog();

			}
		});
	}
	
	void dismissDialog(){
		dialogShare.dismiss();
	}

}
