package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.CityModel;
import com.phonethics.model.CountriesModel;
import com.phonethics.model.StateModel;



/**
 * Checks the product availability as per users desired location 
 * 
 * @author Nitin
 *
 */
public class CheckAvailabilityFragment extends Fragment implements DBListener,OnItemClickListener,LocationListener {

	private View mView;
	private ProgressDialog mProgressDialog;
	private ListView mCityList;
	private ArrayList<String> mArrCityName=new ArrayList<String>();
	private CategoriesAdapter mAdapter;
	private TextView mTxtSelectCity;
	private EditText mPinCode;
	private TextView mCheckNow;
	int count = 1;
	Location location;
	String mCurrentCity="";
	NetworkCheck networkChk;
	Typeface tf;
	SessionManager mSession;
	private Dialog cityDialog;

	String mUserId = "0";
	HashMap<String,String> userDetails = new HashMap<String, String>();
	long rowCount;
	String productId = "";
	String sub_cat_id = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		/**
		 * Initialize the class variables
		 * 
		 */
		mSession=new SessionManager(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		networkChk = new NetworkCheck(getActivity());
		productId = getArguments().getString("PRODUCT_ID");
		sub_cat_id = getArguments().getString("SUB_CAT_ID");
		Config.Log("SUBCATR ","SUBCATR " + sub_cat_id);
		if(networkChk.isNetworkAvailable()){
			checkCurrentLocation();
		}
		//checkCachedData();
		Config.Log("MAIN_ID","MAIN_ID " + Config.MAIN_SELECTED_CATEGORY_ID);

		/**
		 * Get the city list based on the selected Category id
		 * 
		 */
		getAllCities(Config.MAIN_SELECTED_CATEGORY_ID);

		if(mSession.isLoggedInCustomer()) {
			userDetails = mSession.getUserDetails();
			mUserId = userDetails.get(SessionManager.USER_ID);
		}
		checkCachedDataWhere();


	}
	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mView=(View)inflater.inflate(R.layout.fragment_check_availability, null);
		mCityList=(ListView)mView.findViewById(R.id.mCityList);
		mTxtSelectCity=(TextView)mView.findViewById(R.id.txtSelectCity);
		mPinCode=(EditText)mView.findViewById(R.id.mPinCode);
		mCheckNow=(TextView)mView.findViewById(R.id.mCheckNow);
		mCityList.setOnItemClickListener(this);
		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);

		mTxtSelectCity.setTypeface(tf,Typeface.BOLD);
		//		mPinCode.setTypeface(tf,Typeface.BOLD);
		mCheckNow.setTypeface(tf,Typeface.BOLD);
		mPinCode.setTypeface(tf);
		mTxtSelectCity.setTypeface(tf);

		//to invisible CityList
		mPinCode.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(MotionEvent.ACTION_UP == event.getAction()) {
					mCityList.setVisibility(View.GONE);
				}
				return false; // return is important...
			}
		});

		mTxtSelectCity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				if(mArrCityName.size()!=0){
					//showToast("Click");

					showCityDialog(mArrCityName);	
				}

			}
		});

		mCheckNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);


				if(mTxtSelectCity.getText().toString().equalsIgnoreCase("SELECT CITY")){
					showToast("Please select a city first");
				}
				else if(mPinCode.length()==0||mPinCode.length()<6){
					showToast("Please enter correct pin code");
				}
				else{

					//mSession.saveCityPreferences(mTxtSelectCity.getText().toString());
					//checkForPinCode();

					//to check whether city is already selected or not

					String selectedCity = mSession.getCitySelected();
					if(!selectedCity.equalsIgnoreCase("")){
						if(selectedCity.equalsIgnoreCase(mTxtSelectCity.getText().toString())){

							FragmentManager manager=getActivity().getSupportFragmentManager();
							FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
							transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

							CheckServiceFragment chkServiceFragment = new CheckServiceFragment();
							Bundle b=new Bundle();
							b.putString("CityName", mTxtSelectCity.getText().toString());
							b.putString("PinCode", mPinCode.getText().toString());
							b.putString("PRODUCT_ID",productId);
							b.putString("SUB_CAT_ID",sub_cat_id);
							chkServiceFragment.setArguments(b);
							transaction.add(R.id.content_frame, chkServiceFragment, "CheckService");
							transaction.addToBackStack(null);
							// Commit the transaction
							transaction.commit();
						}
						else{
							if(rowCount!=0){
								showWarningDialog();	
							}
							else{
								//showToast("Now:");
								FragmentManager manager=getActivity().getSupportFragmentManager();
								FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
								transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

								CheckServiceFragment chkServiceFragment = new CheckServiceFragment();
								Bundle b=new Bundle();
								b.putString("CityName", mTxtSelectCity.getText().toString());
								b.putString("PinCode", mPinCode.getText().toString());
								b.putString("PRODUCT_ID",productId);
								b.putString("SUB_CAT_ID",sub_cat_id);
								chkServiceFragment.setArguments(b);
								transaction.add(R.id.content_frame, chkServiceFragment, "CheckService");
								transaction.addToBackStack(null);
								// Commit the transaction
								transaction.commit();
							}
						}
					}
					else{
						FragmentManager manager=getActivity().getSupportFragmentManager();
						FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
						transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

						CheckServiceFragment chkServiceFragment = new CheckServiceFragment();
						Bundle b=new Bundle();
						b.putString("CityName", mTxtSelectCity.getText().toString());
						b.putString("PinCode", mPinCode.getText().toString());
						b.putString("PRODUCT_ID",productId);
						b.putString("SUB_CAT_ID",sub_cat_id);
						chkServiceFragment.setArguments(b);
						transaction.add(R.id.content_frame, chkServiceFragment, "CheckService");
						transaction.addToBackStack(null);
						// Commit the transaction
						transaction.commit();
					}


					//					FragmentManager manager=getActivity().getSupportFragmentManager();
					//					FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
					//					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//
					//					CheckServiceFragment chkServiceFragment = new CheckServiceFragment();
					//					Bundle b=new Bundle();
					//					b.putString("CityName", mTxtSelectCity.getText().toString());
					//					b.putString("PinCode", mPinCode.getText().toString());
					//					chkServiceFragment.setArguments(b);
					//					transaction.add(R.id.content_frame, chkServiceFragment, "CheckService");
					//					transaction.addToBackStack(null);
					//					// Commit the transaction
					//					transaction.commit();

				}
			}
		});



		return mView;
	}


	/**
	 * Network call to get the city list based on the selected Category id
	 * 
	 * @param catId
	 */
	void getAllCities(String catId){

		try {

			final String TAG="GET_ALL_CITIES";
			final String PRODUCT_ID = "/product_id/";
			Config.Log("URL ", "URL " + Config.GET_CITIES_BY_CATEGORY + catId + PRODUCT_ID + productId);

			showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, Config.GET_CITIES_BY_CATEGORY + catId + PRODUCT_ID + productId, null, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					try
					{
						Config.Log(TAG, response.toString());
						String status=response.getString("success");

						if(response!=null){
							dismissDialog();
							if(status.equalsIgnoreCase("true")){
								JSONArray jsonArr=response.getJSONArray("data");
								ContentValues[] values = new ContentValues[jsonArr.length()];

								for(int i=0;i<jsonArr.length();i++){
									ContentValues value = new ContentValues();
									JSONObject jsonObject=jsonArr.getJSONObject(i);
									value.put(DatabaseHelper.CITY_NAME,jsonObject.getString("city_name"));
									value.put(DatabaseHelper.ID,jsonObject.getInt("id"));
									values[i]=value;

									mArrCityName.add(jsonObject.getString("city_name"));
								}

								/**
								 * Set the list of cities
								 * 
								 */
								setData(mArrCityName);

							}else{
								if(status.equalsIgnoreCase("false")){
									showToast(response.getString("message"));

									mCityList.setVisibility(View.GONE);

								}
							}

						}	
					}catch(Exception ex){
						ex.printStackTrace();
						dismissDialog();
					}

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					dismissDialog();
					mCityList.setVisibility(View.GONE);
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}

			};

			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
		}



	}

	/*void checkCachedData() {
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_CITY,
					DatabaseHelper.AUTO_ID, Config.ROW_COUNT,  Config.ROW_COUNT).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	} 

	void insertIntoDb(ContentValues[] contentValues ){
		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_CITY,
					Config.INSERT_DATA, Config.INSERT_DATA).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}*/

	private void showToast(String text){

		Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
	}
	@Override
	public void onCompleteOperation(String tag) {
		//dismissDialog();
		if(tag.equalsIgnoreCase(Config.INSERT_DATA)){
			fetchCityName();
		}
		else if(tag.equalsIgnoreCase(Config.DELETE_DATA)){
			dismissDialog();
			rowCount = 0;
			FragmentManager manager=getActivity().getSupportFragmentManager();
			FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

			CheckServiceFragment chkServiceFragment = new CheckServiceFragment();
			Bundle b=new Bundle();
			b.putString("CityName", mTxtSelectCity.getText().toString());
			b.putString("PinCode", mPinCode.getText().toString());
			b.putString("PRODUCT_ID",productId);
			b.putString("SUB_CAT_ID",sub_cat_id);
			chkServiceFragment.setArguments(b);
			transaction.add(R.id.content_frame, chkServiceFragment, "CheckService");
			transaction.addToBackStack(null);
			// Commit the transaction
			transaction.commit();
		}
	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		//dismissDialog();
		Config.Log("CHK", "CHK " +mArr.toString());

		if(tag.equalsIgnoreCase("CITY_NAME")){
			//showToast("City names parsed");
			setData(mArr);
		}

	}

	@Override
	public void rowCount(long rowCount, String tag) {

		//dismissDialog();
		if(tag.equalsIgnoreCase(Config.ROW_COUNT)){

			if(rowCount==0){

				if(networkChk.isNetworkAvailable()){

					getAllCities(Config.MAIN_SELECTED_CATEGORY_ID);
					mTxtSelectCity.setClickable(true);
				}

				else{
					mTxtSelectCity.setClickable(false);
					showToast("No internet connection.");
				}
			}
			else{

				mTxtSelectCity.setClickable(true);
				fetchCityName();
				fetchCityID();
			}
		}
		else if(tag.equalsIgnoreCase(Config.ROW_COUNT_WHERE)){
			//LogFile.LogData("Row Count " + rowCount);
			//showToast("Count " + rowCount + " User Id " + mUserId );
			this.rowCount = rowCount;
		}
	}

	void fetchCityName(){
		//showProgressDialog();
		new AsyncDatabaseQuery(getActivity(), this, DatabaseHelper.TABLE_CITY, 
				DatabaseHelper.CITY_NAME, Config.RETRIVE_DATA, "CITY_NAME").execute();
	}

	void fetchCityID(){
		//showProgressDialog();
		new AsyncDatabaseQuery(getActivity(), this, DatabaseHelper.TABLE_CITY, 
				DatabaseHelper.ID, Config.RETRIVE_DATA, "CITY_ID").execute();
	}


	/**
	 * Set the list of cities
	 * 
	 * @param mArr
	 */
	public void setData(ArrayList<String> mArr){
		Config.Log("CATEG", "CATEG"+mArr);

		mAdapter=new CategoriesAdapter(getActivity(), 0, 0, mArrCityName);
		mCityList.setAdapter(mAdapter);

		if(networkChk.isNetworkAvailable()){

			if(mCurrentCity.length()!=0){

				for(int i=0;i<mArrCityName.size();i++){

					if(mCurrentCity.equalsIgnoreCase(mArrCityName.get(i))){
						mTxtSelectCity.setText(mCurrentCity);
					}
				}
			}
		}


	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		// TODO Auto-generated method stub
		mCityList.setVisibility(View.GONE);	
		mTxtSelectCity.setText(mArrCityName.get(pos));

	}


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

		double curr_lat = location.getLatitude();
		double curr_lon = location.getLongitude();

		Geocoder geoCoder = new Geocoder(getActivity(),Locale.getDefault());
		try {
			List<Address> add = geoCoder.getFromLocation(curr_lat, curr_lon, 1);

			mCurrentCity = add.get(0).getLocality();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


	/**
	 * Network request to check users present location
	 * 
	 */
	void checkCurrentLocation(){

		LocationManager lm = null;
		boolean gps_enabled = false,network_enabled = false;
		if(lm==null){
			lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

		}

		try{
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
			location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){}
		try{
			network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}catch(Exception ex){}

		if(!gps_enabled && !network_enabled){

		}
		else{
			//showToast("Got Location");
			if(location!=null)
				onLocationChanged(location);
		}
	}


	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}



	void showCityDialog(final ArrayList<String> mArr){
		cityDialog = new Dialog(getActivity());
		cityDialog.setContentView(R.layout.attribute_type_selection);
		final ListView cityList = (ListView)cityDialog.findViewById(R.id.mAttributeType);
		final EditText metSearch = (EditText)cityDialog.findViewById(R.id.citySearch);
		CategoriesAdapter mAdapter = new CategoriesAdapter(getActivity(), 0, 0, mArrCityName);
		cityList.setAdapter(mAdapter);
		cityDialog.setTitle("Select City");
		cityList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				mTxtSelectCity.setText(mArr.get(arg2));
				cityDialog.dismiss();
			}

		});

		metSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				final ArrayList<String> temp_place = new ArrayList<String>();
				for(int i=0;i<mArr.size();i++){

					if(metSearch.getText().toString().length()<=mArr.get(i).toString().length()){

						if(metSearch.getText().toString().equalsIgnoreCase((String) mArr.get(i).subSequence(0, metSearch.getText().toString().length()))){

							temp_place.add(mArr.get(i));
						}


					}
				}

				CategoriesAdapter mAdapter = new CategoriesAdapter(getActivity(), 0, 0, temp_place);
				cityList.setAdapter(mAdapter);

				cityList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
							long arg3) {
						// TODO Auto-generated method stub
						mTxtSelectCity.setText(temp_place.get(arg2));
						cityDialog.dismiss();
					}
				});

			}

		});

		cityDialog.show();
	}

	void showWarningDialog() {
		// TODO Auto-generated method stub
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.alert_dialog);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_continue);
		//mtvContinue.setText("Delete Now");
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		TextView mtvCancel = (TextView) dateWarning.findViewById(R.id.text_cancel);
		String message = "Delivery Location is already set to " + mSession.getCitySelected()+".";
		mtvMessage.setText(message+"\nPlease note: The cart will be emptied upon selecting another city.");
		mtvMessage.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvCancel.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvContinue.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//showToast("Delete All records");

				//chack for user logged in or not
				if(mSession.isLoggedInCustomer()) {
					//					userDetails = mSession.getUserDetails();
					//					mUserId = userDetails.get(SessionManager.USER_ID);
					deleteProduct();
					deleteShoppingCartRecords();
				}
				else{
					deleteShoppingCartRecords();
				}

				dateWarning.dismiss();
			}
		});
		mtvCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dateWarning.dismiss();

			}
		});

		dateWarning.show();	
	}

	void deleteShoppingCartRecords(){
		showProgressDialog();
		LogFile.LogData("deleteShoppingCartRecords");
		String msWhere = DatabaseHelper.USER_ID+" ='"+mUserId+"'";
		try{
			new AsyncDatabaseQuery(getActivity(),this, DatabaseHelper.TABLE_SHOPPING_CART,
					msWhere, Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Network request to delete all the shopping cart products of the logged in user from server
	 * 
	 */
	void deleteProduct(){
		LogFile.LogData("deleteProduct");
		final String TAG="GET_ALL_SUB_CATEGORIES";
		String url=Config.DELETE_SELECTED_CART_PRODUCTS+mUserId;
		LogFile.LogData("URL "+url);
		//pDialog.showProgressDialog();
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.DELETE,url,null,new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				// TODO Auto-generated method stub
				try{
					LogFile.LogData("Delete Response "+response.toString());
					if(response!=null){
						String status=response.getString("success");
						if(status.equalsIgnoreCase("true")) {

							//fethGuestCartData();
							//showToast("All user specific data deleted");
						}

					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}

		},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				Config.Log(TAG, error.getMessage());
				//pDialog.dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}

		}){


			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};
		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
	}	

	//to check the row count of cart table
	void checkCachedDataWhere(){
		LogFile.LogData("checkCachedDataWhere");
		try{
			String msWhere =  "WHERE " + DatabaseHelper.USER_ID+" = '"+ mUserId+ "'";
			new AsyncDatabaseQuery(getActivity(),
					this, 
					DatabaseHelper.TABLE_SHOPPING_CART,
					DatabaseHelper.AUTO_ID, 
					Config.ROW_COUNT_WHERE,  
					Config.ROW_COUNT_WHERE,
					msWhere)
			.execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


}
