package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.phonethics.adapters.CallAdapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ContactUsFragment extends Fragment {

	private View mView;
	private TextView textHeadOffice,textAddress,textEmailHeader,textEmail,textNumberHeaders,
	textOfflineNumberHeader,textOnlineNumber,textWorkingHeaders,textWorkingHrs;
	private ArrayList<String> onLineNumbers;


	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_contact_us, null);


		Typeface tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);

		
		

		textHeadOffice = (TextView) mView.findViewById(R.id.txtAddressHeading);
		textAddress = (TextView) mView.findViewById(R.id.txtAddress);
		textEmailHeader = (TextView) mView.findViewById(R.id.txtEmailHeading);
		textEmail = (TextView) mView.findViewById(R.id.txtEmail);
		textNumberHeaders = (TextView) mView.findViewById(R.id.txtTelephoneHeading);
		textOfflineNumberHeader = (TextView) mView.findViewById(R.id.txtOfflineEnq);
		textOnlineNumber = (TextView) mView.findViewById(R.id.txtOnlineEnq);
		textWorkingHeaders = (TextView) mView.findViewById(R.id.txtWorkingHrsHeading);
		textWorkingHrs = (TextView) mView.findViewById(R.id.txtWrknHrs);

		textHeadOffice.setTypeface(tf);
		textAddress.setTypeface(tf);
		textEmailHeader.setTypeface(tf);
		textEmail.setTypeface(tf);
		textNumberHeaders.setTypeface(tf);
		textOfflineNumberHeader.setTypeface(tf);
		textOnlineNumber.setTypeface(tf);
		textWorkingHeaders.setTypeface(tf);
		textWorkingHrs.setTypeface(tf);
		
		
		textEmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL, new String[]{textEmail.getText().toString()});
				email.setType("message/rfc822");
				startActivity(Intent.createChooser(email, "Choose an option :"));
			}
		});
		
		textOfflineNumberHeader.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onLineNumbers = new ArrayList<String>();
				onLineNumbers.add("02240786702");
				showDateWarningDialog(onLineNumbers);
			}
		});
		
		textOnlineNumber.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onLineNumbers = new ArrayList<String>();
				onLineNumbers.add("02240786759");
				onLineNumbers.add("02240786789");
				showDateWarningDialog(onLineNumbers);
			}
		});


		return mView;
	}


	private void showDateWarningDialog(final ArrayList<String> numbers){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.call_list);
		ListView list = (ListView) dateWarning.findViewById(R.id.mCallListView);
		TextView txtCancel =(TextView)dateWarning.findViewById(R.id.txtCancel);
		list.setAdapter(new CallAdapter(getActivity(), R.drawable.ic_launcher,R.layout.call_list, numbers));

		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Map<String, String> param = new HashMap<String, String>();
				param.put("Number", numbers.get(position));
				EventTracker.logEvent(EventsName.CONTACT_US_CALL,param);
				
				String callNumber = numbers.get(position);
				Intent caller = new Intent(android.content.Intent.ACTION_DIAL);
				caller.setData(Uri.parse("tel:" +callNumber));
				startActivity(caller);
				
				dateWarning.dismiss();
			}
		});
		
		txtCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dateWarning.dismiss();
			}
		});
		/*	if(dateWarning.isShowing()){

		}else{
			dateWarning.show();	
		}
		 */
		dateWarning.show();	

	}

}
