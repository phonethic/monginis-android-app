package com.phonethics.monginis;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class WebLinkView extends Activity {

	Context context;
	private WebView linkwebview;
	private ImageView bckBtn;
	private ImageView forthBtn;
	private ImageView refreshBtn;
	ProgressBar prog;
	int screen;
	private Map<String, String> param = new HashMap<String, String>();
	RelativeLayout bck;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_link_view);

		context = this;
		linkwebview = (WebView) findViewById(R.id.linkwebview);

		bckBtn = (ImageView) findViewById(R.id.webback);
		forthBtn = (ImageView) findViewById(R.id.forth);
		refreshBtn = (ImageView) findViewById(R.id.refresh);
		prog=(ProgressBar)findViewById(R.id.showProgress);
		bck=(RelativeLayout)findViewById(R.id.bck);

		linkwebview.getSettings().setJavaScriptEnabled(true);
		linkwebview.getSettings().setPluginState(PluginState.ON);
		linkwebview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		linkwebview.getSettings().setBuiltInZoomControls(true);
		linkwebview.setWebViewClient(new MyWebViewClient());

		Bundle b = getIntent().getExtras();
		if(b!=null){

			screen = b.getInt("position");
		}

		if(screen == 0){
			param.put("Platform", "Facebook");
			linkwebview.loadUrl(Config.FACEBOOK_URL);
			EventTracker.logEvent("SocialConnect",param);
		}
			
		else if(screen == 1){
			param.put("Platform", "Twitter");
			linkwebview.loadUrl(Config.TWITTER_URL);
			EventTracker.logEvent("SocialConnect",param);
		}
			
		else if(screen == 2){
			param.put("Platform", "Google Plus");
			linkwebview.loadUrl(Config.GPLUS_URL);
			EventTracker.logEvent("SocialConnect",param);
		}
			
		else if(screen == 3){
			param.put("Platform", "Pinterest");
			linkwebview.loadUrl(Config.PINTEREST_URL);
			EventTracker.logEvent("SocialConnect",param);
		}
			
		else if(screen == 6){
			EventTracker.logEvent("PoweredByPhonethics");
			linkwebview.loadUrl(Config.PHONETHICS_URL);
		}
		
		else if(screen == 5){
			EventTracker.logEvent("MediaRoom");
			linkwebview.loadUrl(Config.MEDIA_ROOM);
			bck.setVisibility(View.GONE);
		}
		else if(screen == 4){
			EventTracker.logEvent("Linkedin");
			linkwebview.loadUrl(Config.LINKEDIN);
		}
		
	}

	private void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(context, ""+msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.web_link_view, menu);
		return false;
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			view.loadUrl(url);



			bckBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(linkwebview.canGoBack())
					{
						linkwebview.goBack();
					}
				}
			});

			forthBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(linkwebview.canGoForward())
					{
						linkwebview.goForward();
					}


				}
			});


			refreshBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					linkwebview.reload();

				}
			});



			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			prog.setVisibility(View.GONE);
		}


		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			prog.setVisibility(View.VISIBLE);


		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);

		}

	}

}
