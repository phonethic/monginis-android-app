package com.phonethics.monginis;

public class EventsName {

	
	public static final String LOGIN = "Login";
	
	public static final String SIGNUP = "SignUp";
	
	public static final String SKIP_FROM_SPLASH = "SkipFromSplash";
	
	public static final String MAIN_CATEGORIES = "MainCategories";
	
	public static final String SUB_CATEGORIES = "SubCategories";
	
	public static final String PRODUCTS = "Products";
	
	public static final String RATING = "Rating";
	
	public static final String SHARE_PRODUCT = "ShareProduct";
	
	public static final String CHECK_AVAILIBITY = "CheckAvailibilty";
	
	public static final String ADD_TO_CART = "AddToCart";
	
	public static final String SHOPPING_CART_SCREEN = "ShoppingCart_Screen";
	
	public static final String SHOPPING_CART_CONTINUE = "ShoppingCart_Continue";
	
	public static final String CONITNUE_AS = "Continue_As";
	
	public static final String REDEEM = "Redeem";
	
	public static final String CHECKOUT_SCREEN = "CheckOutScreen";
	
	public static final String SHIPPING_ADDRESS = "ShippingAddress";
	
	public static final String BILLING_ADDRESS = "BillingAddress";
	
	public static final String STORE_LOCATOR = "StoreLocator";
	
	public static final String CALL_STORE = "CallStore";
	
	public static final String PARTNER_WITH_US = "PartrnerWithUs";
	
	public static final String FORM_SUBMISSION = "FromSubmission";
	
	public static final String SOICIAL_CONNECT = "SocialConnect";
	public static final String CONTACT_US_CALL = "Contact_us_call";
	
	public static final String TRY_ADDONS = "TryAddOns";
	
	public static final String MENU_OPTIONS = "MenuOptions";
	
	public static final String CUSTOMER_PROFILE_UPDATED = "Customer_profile_updated";
	
	public static String category = "";
	public static String sub_category = "";
	public static String product = "";
	
}
