package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.AddressAdapter;
import com.phonethics.adapters.CartListAdapter;
import com.phonethics.adapters.CategoriesAdapter;
import com.phonethics.model.CityModel;
import com.phonethics.model.CountriesModel;
import com.phonethics.model.OrderModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.model.StateModel;
import com.phonethics.model.UserAddress;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class BillingAddressFragment extends Fragment{


	private View mView;
	private SessionManager mSessionManager;
	private ScrollView mScroll;
	private int miScrollPos = 65;
	private EditText metFirstName;
	private EditText metLastName;
	private EditText metEmail;
	private EditText metPincode;
	private EditText metMobileNumber;
	private TextView mtvCountry;
	private TextView mtvState;
	private TextView mtvCity;
	private TextView mtvContinue; 
	private ProgressDialog mProgressDialog;
	private ArrayList<CountriesModel> marrCountires;
	private ArrayList<StateModel> marrStates;
	private ArrayList<CityModel> marrCities;
	private Dialog mListDialog;
	private ListView mDialogList;
	private RelativeLayout dividerView;
	private String msSelectedCountryId = "";
	private String msSelectedStateId = "";
	private String msSelectedCityId = "";
	private String COUNTRY_TITLE = "Select Country";
	private String STATE_TITLE = "Select State";
	private String CITY_TITLE = "Select City";
	private String msUserId;
	private String msOrderId="-1";

	private String DIALOG_TITLE = COUNTRY_TITLE;
	private EditText metState;
	private EditText metCity;
	private HashMap<String, String> userDetails;

	private ArrayList<UserAddress> marrUAdds;
	private String msAddId = "0";
	private String mAddressLabel = "";
	//private String msSelectedCountryId = "";
	private EditText metAddress;
	private JSONObject userObj;
	private TextView mtvSelectPrevious;
	private EditText metAddressTitle;



	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		
		
		/**
		 * Initialize the class variables
		 * 
		 */
		mSessionManager = new SessionManager(getActivity());

		LogFile.LogData("Shipping onCreate");



		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching data please wait.");
		mListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		mSessionManager = new SessionManager(getActivity());
		
		if(mSessionManager.isLoggedInCustomer()){
			
			/**
			 * Get the previous saved address of the logged in user from server
			 */
			userDetails = mSessionManager.getUserDetails();
			msUserId = userDetails.get(SessionManager.USER_ID);
			
			
				
			getUserPreviousAddress();
			
		}else{
			msUserId = "0";
		}
		
		/**
		 * Get
		 */
		getCountryList();



	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (View)inflater.inflate(R.layout.fragment_billing_address   , null);
		mScroll = (ScrollView) mView.findViewById(R.id.mScroll);

		metFirstName = (EditText) mView.findViewById(R.id.editFirstName);
		metLastName = (EditText) mView.findViewById(R.id.editLastName);
		metEmail = (EditText) mView.findViewById(R.id.editEmail);
		metPincode = (EditText) mView.findViewById(R.id.editPinCode);
		metMobileNumber = (EditText) mView.findViewById(R.id.editMobile);
		metState = (EditText) mView.findViewById(R.id.editState);
		metCity = (EditText) mView.findViewById(R.id.editCity);
		metAddress = (EditText) mView.findViewById(R.id.editAddress);
		metAddressTitle = (EditText) mView.findViewById(R.id.editAddressTitle);
		mtvCountry = (TextView) mView.findViewById(R.id.textCountry);
		mtvState = (TextView) mView.findViewById(R.id.textState);
		mtvCity = (TextView) mView.findViewById(R.id.textCity);
		mtvContinue = (TextView) mView.findViewById(R.id.textContinue);
		mtvSelectPrevious = (TextView) mView.findViewById(R.id.textSelectPrevious);
		mtvSelectPrevious.setVisibility(View.GONE);

		mtvCountry.setText(COUNTRY_TITLE);
		mtvState.setText(STATE_TITLE);
		mtvCity.setText(CITY_TITLE);


		mtvState.setVisibility(View.GONE);
		mtvCity.setVisibility(View.GONE);
		metCity.setVisibility(View.VISIBLE);
		metState.setVisibility(View.VISIBLE);

		metFirstName.setTypeface(MonginisApplicationClass.getTypeFace());
		metLastName.setTypeface(MonginisApplicationClass.getTypeFace());
		metEmail.setTypeface(MonginisApplicationClass.getTypeFace());
		metPincode.setTypeface(MonginisApplicationClass.getTypeFace());
		metMobileNumber.setTypeface(MonginisApplicationClass.getTypeFace());
		metState.setTypeface(MonginisApplicationClass.getTypeFace(),Typeface.BOLD);
		metCity.setTypeface(MonginisApplicationClass.getTypeFace(),Typeface.BOLD);
		metAddress.setTypeface(MonginisApplicationClass.getTypeFace());
		metAddressTitle.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvCountry.setTypeface(MonginisApplicationClass.getTypeFace(),Typeface.BOLD);
		mtvState.setTypeface(MonginisApplicationClass.getTypeFace(),Typeface.BOLD);
		mtvCity.setTypeface(MonginisApplicationClass.getTypeFace(),Typeface.BOLD);
		mtvContinue.setTypeface(MonginisApplicationClass.getTypeFace());
		mtvSelectPrevious.setTypeface(MonginisApplicationClass.getTypeFace());


		/*metFirstName.setText("Nitin");
		metLastName.setText("Bathija");
		metEmail.setText("nitin@phonethics.in");
		metPincode.setText("400001");
		metMobileNumber.setText("1234567890");*/



		mtvSelectPrevious.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<String> msArr = new ArrayList<String>();
				for(int i=0;i<marrUAdds.size();i++){
					msArr.add(marrUAdds.get(i).getLabel());
				}
				showAddressDialog();

			}
		});


		metFirstName.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					//Toast.makeText(getActivity(), "Next", 0).show();
					scrollTo();
					metLastName.requestFocus();
					return true;
				}
				return false;
			}
		});
		metLastName.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					//Toast.makeText(getActivity(), "Next", 0).show();
					scrollTo();
					metEmail.requestFocus();
					return true;
				}
				return false;
			}
		});

		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(validateInfo()){
					getOrderDetails();
				}

				//showAddressTitleDialog();
			}
		});

		mtvCountry.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//mListDialog.show();
				ArrayList<String> countries = new ArrayList<String>();
				for(int i=0;i<marrCountires.size();i++){
					countries.add(marrCountires.get(i).getCounty());
				}
				DIALOG_TITLE = COUNTRY_TITLE;
				showPlaceDialog(countries);
			}
		});

		mtvState.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//mListDialog.show();
				ArrayList<String> states = new ArrayList<String>();
				if(marrStates!=null){
					for(int i=0;i<marrStates.size();i++){
						states.add(marrStates.get(i).getState());
					}
					DIALOG_TITLE = STATE_TITLE;
					showPlaceDialog(states);
				} else {

				}

			}
		});

		mtvCity.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//mListDialog.show();
				ArrayList<String> cities = new ArrayList<String>();
				if(marrCities!=null){
					for(int i=0;i<marrCities.size();i++){
						cities.add(marrCities.get(i).getCity());
					}
					DIALOG_TITLE = CITY_TITLE;
					showPlaceDialog(cities);
				} else {

				}

			}
		});


		return mView;

	}


	void scrollTo(){

		mScroll.post(new Runnable() { 
			public void run() { 
				mScroll.scrollTo(0, miScrollPos); // these are your x and y coordinates
			}
		});
		int i = 20;
		miScrollPos = miScrollPos +i;
	}


	void updateUserInfo(){

		try{

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	/*private getCountryList(){

	}*/

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	/**
	 * Network request to get the countries list from server.
	 * Check if it is saved in share pref than parse it else make a network request.
	 * 
	 */
	void getCountryList(){

		try{
			String msJsonCoutires = mSessionManager.getCountiresData();
			
			/**
			 * Check if countries data is present in share pref or not
			 * 
			 */
			if(msJsonCoutires.equalsIgnoreCase("")){
				
				/**
				 * If no than make network request to country list
				 */
				LogFile.LogData("getCountryList");
				final String TAG="GET_ALL_SUB_CATEGORIES";
				String url=Config.GET_COUNTIRES;
				LogFile.LogData("URL "+url);
				showProgressDialog();
				JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {



					@Override
					public void onResponse(JSONObject response) {
						try
						{
							dismissDialog();
							Config.Log(TAG, response.toString());
							String status=response.getString("success");
							LogFile.LogData("response --"+response);
							if(response!=null){
								if(status.equalsIgnoreCase("true")){
									
									/**
									 * Save the response in share pref.
									 * 
									 */
									mSessionManager.saveCountriesData(response.toString());
									updateCountriesList(response.toString());
									/*
									JSONArray jsonArr=response.getJSONArray("data");
									for(int i=0;i<jsonArr.length();i++){

										JSONObject jsonObject=jsonArr.getJSONObject(i);
										CountriesModel countriesModel = new CountriesModel();
										countriesModel.setId(jsonObject.getString("id"));
										countriesModel.setCounty(jsonObject.getString("country"));
									}
									 */}else{

										 if(status.equalsIgnoreCase("false")){
											 showToast(response.getString("message"));
											 dismissDialog();
										 }
									 }

							}	
						}catch(Exception ex){
							ex.printStackTrace();
							dismissDialog();
						}

					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Config.Log(TAG, error.getMessage());
						dismissDialog();
						if(error instanceof NetworkError){
							showToast(getResources().getString(R.string.Network));
						}else if(error instanceof AuthFailureError){
							showToast(getResources().getString(R.string.Authentication));
						}else if(error instanceof ServerError ){
							showToast(getResources().getString(R.string.Server));
						}else if(error instanceof NoConnectionError){
							showToast(getResources().getString(R.string.Internet));
						}else if(error instanceof TimeoutError){
							showToast(getResources().getString(R.string.TimeOut));
						}else if(error instanceof ParseError){
							showToast(getResources().getString(R.string.Parse));
						}
					}
				}){

					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers=new HashMap<String, String>();
						headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
						headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
						return headers;
					}
				};
				MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			} else {
				
				/**
				 * Get countires information from share pref and update the list
				 * 
				 */
				updateCountriesList(msJsonCoutires);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}


	}

	/**
	 * Network request to get the states list from server.
	 * Check if it is saved in share pref than parse it else make a network request.
	 * 
	 */
	void getStates(){

		try{
			String msJsonCoutires = mSessionManager.getStateData();

			/**
			 * Check if states data is present in share pref or not
			 * 
			 */
			if(msJsonCoutires.equalsIgnoreCase("")){
				/**
				 * If no than make network request to country list
				 */
				LogFile.LogData("getStates");
				final String TAG="GET_STATES";
				String url=Config.GET_STATES+msSelectedCountryId;
				LogFile.LogData("URL "+url);
				showProgressDialog();
				JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {


					@Override
					public void onResponse(JSONObject response) {
						try
						{
							dismissDialog();
							Config.Log(TAG, response.toString());
							String status=response.getString("success");
							LogFile.LogData("response --"+response);
							if(response!=null){
								if(status.equalsIgnoreCase("true")){
									/**
									 * Save the response in share pref.
									 * 
									 */
									
									mSessionManager.saveStateData(response.toString());
									updateStateList(response.toString());

								}else{

									if(status.equalsIgnoreCase("false")){
										showToast(response.getString("message"));
										dismissDialog();
									}
								}

							}	
						}catch(Exception ex){
							ex.printStackTrace();
							dismissDialog();
						}

					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Config.Log(TAG, error.getMessage());
						dismissDialog();
						if(error instanceof NetworkError){
							showToast(getResources().getString(R.string.Network));
						}else if(error instanceof AuthFailureError){
							showToast(getResources().getString(R.string.Authentication));
						}else if(error instanceof ServerError ){
							showToast(getResources().getString(R.string.Server));
						}else if(error instanceof NoConnectionError){
							showToast(getResources().getString(R.string.Internet));
						}else if(error instanceof TimeoutError){
							showToast(getResources().getString(R.string.TimeOut));
						}else if(error instanceof ParseError){
							showToast(getResources().getString(R.string.Parse));
						}
					}
				}){

					@Override
					public Map<String, String> getHeaders() throws AuthFailureError {
						HashMap<String, String> headers=new HashMap<String, String>();
						headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
						headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
						return headers;
					}
				};
				MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			} else {
				/**
				 * Get states information from share pref and update the list
				 * 
				 */
				updateStateList(msJsonCoutires);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Network request to get the cities list from server.
	 * 
	 * 
	 */
	void getCities(){

		try{
			//String msJsonCoutires = mSessionManager.getStateData();
			//if(msJsonCoutires.equalsIgnoreCase("")){
			LogFile.LogData("getCities");
			final String TAG="GET_CITY";
			String url=Config.GET_CITY+msSelectedStateId;
			LogFile.LogData("URL "+url);
			showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {


				@Override
				public void onResponse(JSONObject response) {
					try
					{
						dismissDialog();
						Config.Log(TAG, response.toString());
						String status=response.getString("success");
						LogFile.LogData("response --"+response);
						if(response!=null){
							if(status.equalsIgnoreCase("true")){

								updateCityList(response.toString());

							}else{

								if(status.equalsIgnoreCase("false")){
									showToast(response.getString("message"));
									dismissDialog();
								}
							}

						}	
					}catch(Exception ex){
						ex.printStackTrace();
						dismissDialog();
					}

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
			//} else {
			//	updateStateList(msJsonCoutires);
			//}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	
	/**
	 * Parse and store in arraylist information of countries
	 * 
	 * @param msCountries
	 */
	private void updateCountriesList(String msCountries){

		LogFile.LogData("updateCountriesList");
		try{
			JSONObject response = new JSONObject(msCountries);
			JSONArray jsonArr=response.getJSONArray("data");
			marrCountires = new ArrayList<CountriesModel>();
			for(int i=0;i<jsonArr.length();i++){

				JSONObject jsonObject=jsonArr.getJSONObject(i);
				CountriesModel countriesModel = new CountriesModel();
				countriesModel.setId(jsonObject.getString("id"));
				countriesModel.setCounty(jsonObject.getString("country"));

				marrCountires.add(countriesModel);
			}
			LogFile.LogData(marrCountires.toString());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Parse and store in arraylist information of states
	 * 
	 * @param msCountries
	 */
	private void updateStateList(String msStates){
		LogFile.LogData("updateStateList");
		try{
			JSONObject response = new JSONObject(msStates);
			JSONArray jsonArr=response.getJSONArray("data");
			marrStates = new ArrayList<StateModel>();
			for(int i=0;i<jsonArr.length();i++){

				JSONObject jsonObject=jsonArr.getJSONObject(i);
				StateModel countriesModel = new StateModel();
				countriesModel.setId(jsonObject.getString("id"));
				countriesModel.setState(jsonObject.getString("state"));

				marrStates.add(countriesModel);
			}
			LogFile.LogData(marrStates.toString());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Parse and store in arraylist information of cities
	 * 
	 * @param msCountries
	 */
	private void updateCityList(String msStates){
		LogFile.LogData("updateCountriesList");
		try{
			JSONObject response = new JSONObject(msStates);
			JSONArray jsonArr=response.getJSONArray("data");
			marrCities = new ArrayList<CityModel>();
			for(int i=0;i<jsonArr.length();i++){

				JSONObject jsonObject=jsonArr.getJSONObject(i);
				CityModel cityModel = new CityModel();
				cityModel.setId(jsonObject.getString("id"));
				cityModel.setCity(jsonObject.getString("city_name"));

				marrCities.add(cityModel);
			}
			LogFile.LogData(marrCities.toString());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private boolean validateInfo(){


		if(metFirstName.getText().toString().equalsIgnoreCase("")){
			showToast("Please enter first name.");
			metFirstName.requestFocus();
			return false;
		} else if (metLastName.getText().toString().equalsIgnoreCase("")) {
			showToast("Please enter last name.");
			metLastName.requestFocus();
			return false;
		} else if (metAddress.getText().toString().equalsIgnoreCase("")) {
			showToast("Please enter your address.");
			metAddress.requestFocus();
			return false;
		} else if(metAddressTitle.getText().toString().equalsIgnoreCase("")){
			showToast("Please enter landmark.");
			metAddressTitle.requestFocus();
			return false;
		} else if (metEmail.getText().toString().equalsIgnoreCase("")) {
			showToast("Please enter email id.");
			metEmail.requestFocus();
			return false;
		} else if(!isValidEmail(metEmail.getText().toString())){
			showToast("Please enter valid email id.");
			metEmail.requestFocus();
			return false;
		} else if(mtvCountry.isShown() && mtvCountry.getText().toString().equalsIgnoreCase(COUNTRY_TITLE)){
			showToast("Please select your country.");
			//metAddress.requestFocus();
			return false;
		} else if(mtvState.isShown() && mtvState.getText().toString().equalsIgnoreCase(STATE_TITLE)){
			showToast("Please select your state.");
			//metAddress.requestFocus();
			return false;
		} else if(metState.isShown() && metState.getText().toString().equalsIgnoreCase("")){
			showToast("Please enter your state.");
			metState.requestFocus();
			return false;
		} else if (mtvCity.isShown() && mtvCity.getText().toString().equalsIgnoreCase(CITY_TITLE)){
			showToast("Please select your city.");
			//metAddress.requestFocus();
			return false;
		} else if(metCity.isShown() && metCity.getText().toString().equalsIgnoreCase("")){
			showToast("Please enter your city.");
			metCity.requestFocus();
			return false;
		} else if (metPincode.getText().toString().equalsIgnoreCase("")) {
			showToast("Please enter pin code.");
			metPincode.requestFocus();
			return false;
		} else if (metMobileNumber.getText().toString().equalsIgnoreCase("")) {
			showToast("Please enter mobile number.");
			metMobileNumber.requestFocus();
			return false;
		} else if (metMobileNumber.length()<10) {
			showToast("Please enter 10 digit mobile number");
			return false;
		}else{
			return true;
		}


	}

	private void showToast(String msMsg){
		Toast.makeText(getActivity(), msMsg, 0).show();
	}

	private void setIndiaCountryLaout(boolean mbIsIndia){

		if(mbIsIndia) {
			//mtvCountry.setVisibility(View.VISIBLE);
			mtvState.setVisibility(View.VISIBLE);
			mtvCity.setVisibility(View.VISIBLE);
			metState.setVisibility(View.GONE);
			metCity.setVisibility(View.GONE);

		} else {
			//mtvCountry.setVisibility(View.VISIBLE);
			mtvState.setVisibility(View.GONE);
			mtvCity.setVisibility(View.GONE);
			metState.setVisibility(View.VISIBLE);
			metCity.setVisibility(View.VISIBLE);
		}

	}

	private void showPlaceDialog(final ArrayList<String> placeData){
		mListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		final EditText metSearch = (EditText)mListDialog.findViewById(R.id.citySearch);
		CategoriesAdapter mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, placeData);
		mDialogList.setAdapter(mAdapter);
		mListDialog.setTitle(DIALOG_TITLE);

		mDialogList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(DIALOG_TITLE.equalsIgnoreCase(COUNTRY_TITLE)) {
					mtvCountry.setText(placeData.get(arg2));
					msSelectedCountryId  = marrCountires.get(arg2).getId();
					if(placeData.get(arg2).equalsIgnoreCase("India")){
						setIndiaCountryLaout(true);
						//getStates();
					}else{
						setIndiaCountryLaout(false);
					}

				} else if (DIALOG_TITLE.equalsIgnoreCase(STATE_TITLE)) {
					mtvState.setText(placeData.get(arg2));
					msSelectedStateId = marrStates.get(arg2).getId();
					getCities();
				} else if (DIALOG_TITLE.equalsIgnoreCase(CITY_TITLE)) {
					mtvCity.setText(placeData.get(arg2));
					msSelectedCityId = marrCities.get(arg2).getId();

				}

				mListDialog.dismiss();
			}
		});

		metSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				final ArrayList<String> temp_place = new ArrayList<String>();

				final ArrayList<CountriesModel> marrCountiresTemp = new ArrayList<CountriesModel>();
				final ArrayList<StateModel> marrStatesTemp = new ArrayList<StateModel>();
				final ArrayList<CityModel> marrCitiesTemp = new ArrayList<CityModel>();



				for(int i=0;i<placeData.size();i++){

					if(metSearch.getText().toString().length()<=placeData.get(i).toString().length()){

						if(metSearch.getText().toString().equalsIgnoreCase((String) placeData.get(i).subSequence(0, metSearch.getText().toString().length()))){

							temp_place.add(placeData.get(i));

							if(DIALOG_TITLE.equalsIgnoreCase(COUNTRY_TITLE)) {
								marrCountiresTemp.add(marrCountires.get(i));
							} else if (DIALOG_TITLE.equalsIgnoreCase(STATE_TITLE)) {
								marrStatesTemp.add(marrStates.get(i));
							} else if (DIALOG_TITLE.equalsIgnoreCase(CITY_TITLE)) {
								marrCitiesTemp.add(marrCities.get(i));
							}
						}


					}
				}

				CategoriesAdapter mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, temp_place);
				mDialogList.setAdapter(mAdapter);

				mDialogList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
							long arg3) {
						// TODO Auto-generated method stub
						if(DIALOG_TITLE.equalsIgnoreCase(COUNTRY_TITLE)) {
							mtvCountry.setText(temp_place.get(arg2));
							msSelectedCountryId  = marrCountiresTemp.get(arg2).getId();
							if(temp_place.get(arg2).equalsIgnoreCase("India")){
								setIndiaCountryLaout(true);
								getStates();
							}else{
								setIndiaCountryLaout(false);
							}
						} else if (DIALOG_TITLE.equalsIgnoreCase(STATE_TITLE)) {
							mtvState.setText(temp_place.get(arg2));
							msSelectedStateId = marrStatesTemp.get(arg2).getId();
							getCities();
						} else if (DIALOG_TITLE.equalsIgnoreCase(CITY_TITLE)) {
							mtvCity.setText(temp_place.get(arg2));
							msSelectedCityId = marrCitiesTemp.get(arg2).getId();

						}

						mListDialog.dismiss();
					}
				});

			}

		});


		mListDialog.show();
	}

	private void updateShippingDetails(){
		try{
			
			//showToast("inside update method");

			userObj = new JSONObject();
			userObj.put("id", msAddId);
			userObj.put("user_id", msUserId);
			userObj.put("label",metAddressTitle.getText().toString());

			userObj.put("first_name", metFirstName.getText().toString());
			userObj.put("last_name", metLastName.getText().toString());
			userObj.put("address1", metAddress.getText().toString());

			userObj.put("address2", "");
			userObj.put("country", mtvCountry.getText().toString());
			if(mtvCountry.getText().toString().equalsIgnoreCase("India")){
				userObj.put("state",mtvState.getText().toString());
				userObj.put("city", mtvCity.getText().toString());
			}else{
				userObj.put("state", metState.getText().toString());
				userObj.put("city", metCity.getText().toString());
			}

			userObj.put("area", "");
			userObj.put("pincode", metPincode.getText().toString());

			userObj.put("email", metEmail.getText().toString());
			userObj.put("company_name", "");
			userObj.put("fax", "");

			userObj.put("home_phone", "");
			userObj.put("cell_phone", metMobileNumber.getText().toString());
			userObj.put("order_id", msOrderId);

			LogFile.LogData("()");
			LogFile.LogData("User Add "+userObj.toString());
			final String TAG="POST_SHIPPING_DETAILS";
			String url=Config.BILLING;
			LogFile.LogData("URL "+url);
			showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, userObj, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						dismissDialog();
						Config.Log(TAG, response.toString());
						String status=response.getString("success");
						LogFile.LogData("response --"+response);
						if(response!=null){
							if(status.equalsIgnoreCase("true")){
								
								
								mSessionManager.saveBillingAddress(userObj.toString());
								FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
								transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
								//To remove all Stack behind
								//getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
								//pDialog.dismissDialog();
								

								ShippingAddressFragment shippingAdd =new ShippingAddressFragment();
								transaction.add(R.id.content_frame, shippingAdd,"Billing");
								transaction.addToBackStack("Billing");
								transaction.commit();
								showToast("Address Updated Succefully!");
								
								
								
								/*Intent intent = new Intent(getActivity(), PaymentGateway.class);
								intent.putExtra("checksum", CalculateAdler32ForByteArray()+"");
								intent.putExtra("orderId", msOrderId);
								getActivity().startActivity(intent);
								getActivity().finish();*/
								
								
								
								//showToast("Done");
							}else{

								if(status.equalsIgnoreCase("false")){
									showToast(response.getString("message"));
									dismissDialog();
								}
							}

						}	
					}catch(Exception ex){
						ex.printStackTrace();
						dismissDialog();
					}

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);


			Map<String, String> params = new HashMap<String, String>();
			if(msAddId.equalsIgnoreCase("0")){
				params.put("Address_Selected","New Address");
			}else{
				params.put("Address_Selected","Stored Address");
			}
			EventTracker.logEvent(EventsName.BILLING_ADDRESS, params);



		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void getOrderDetails(){
		//ProgDialog.showProgressDialog(getActivity());

		showProgressDialog();
		LogFile.LogData("getOrderDetails");
		new AsyncDatabaseObjectQuery<OrderModel>(getActivity(),new OnCompleteListner<OrderModel>() {



			@Override
			public void onComplete(ArrayList<OrderModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				dismissDialog();
				for(int i =0;i<response.size();i++){
					LogFile.LogData("=======================================================");
					LogFile.LogData("Coupon id "+response.get(i).getCoupon_id());
					LogFile.LogData("Delivery Date "+response.get(i).getDelivery_date());
					LogFile.LogData("Delievry Mode "+response.get(i).getDelivery_mode());
					LogFile.LogData("Message Billing "+response.get(i).getMessage_billing());
					LogFile.LogData("Order Note "+response.get(i).getOrder_note());
					LogFile.LogData("Order id "+response.get(i).getOrder_id());
					LogFile.LogData("Points Redeem "+response.get(i).getPoints_redeem());
					LogFile.LogData("Product Amount "+response.get(i).getProduct_amount());
					LogFile.LogData("Shipping charge "+response.get(i).getShipping_charge());
					LogFile.LogData("Total Amount "+response.get(i).getTotalAmount());
					LogFile.LogData("User id "+response.get(i).getUser_id());

					msOrderId = response.get(i).getOrder_id();


				}
				updateShippingDetails();


			}
		},DatabaseHelper.TABLE_ORDERS,Config.RETRIVE_ORDER_INFO,"",Config.RETRIVE_ORDER_INFO).execute();

	}

	
	/**
	 * 
	 * Network request to get the user last saved address of the logged in user
	 */
	private void getUserPreviousAddress(){

		try{

			LogFile.LogData("getUserPreviousAddress");

			final String TAG="POST_SHIPPING_DETAILS";
			String url=Config.BILLING+"/user_id/"+msUserId;
			LogFile.LogData("URL "+url);
			showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Response.Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					try
					{
						dismissDialog();
						//Config.Log(TAG, response.toString());
						String status=response.getString("success");
						LogFile.LogData("response --"+response);
						if(response!=null){
							if(status.equalsIgnoreCase("true")){
								//showToast("Done");
								JSONArray jArr = response.getJSONArray("data");
								marrUAdds = new ArrayList<UserAddress>();
								for(int i=0;i<jArr.length();i++){
									mtvSelectPrevious.setVisibility(View.VISIBLE);
									UserAddress uAdd = new UserAddress();

									JSONObject tempObj  = jArr.getJSONObject(i);
									uAdd.setCity(tempObj.getString("city"));
									uAdd.setCountry(tempObj.getString("country"));
									uAdd.setEmail_id(tempObj.getString("email"));
									uAdd.setFirstName(tempObj.getString("first_name"));
									uAdd.setId(tempObj.getString("id"));
									uAdd.setLabel(tempObj.getString("label"));
									uAdd.setLastName(tempObj.getString("last_name"));
									uAdd.setHome_number(tempObj.getString("home_phone"));
									uAdd.setMobile_number(tempObj.getString("cell_phone"));
									uAdd.setPincone(tempObj.getString("pincode"));
									uAdd.setState(tempObj.getString("state"));
									uAdd.setUser_id(tempObj.getString("user_id"));
									uAdd.setAddress1(tempObj.getString("address1"));

									marrUAdds.add(uAdd);
								}

								/*	ArrayList<String> msArr = new ArrayList<String>();

								for(int i=0;i<marrUAdds.size();i++){
									msArr.add(marrUAdds.get(i).getLabel());
								}
								showAddressDialog(msArr);*/

							}else{
								if(status.equalsIgnoreCase("false")){
									mtvSelectPrevious.setVisibility(View.GONE);
									//showToast(response.getString("message"));
									dismissDialog();
								}
							}

						}	
					}catch(Exception ex){
						ex.printStackTrace();
						dismissDialog();
					}

				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};

			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void showAddressDialog(){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.address_dialog);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_new_adress);
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		ListView list = (ListView) dateWarning.findViewById(R.id.list_address);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				msAddId = marrUAdds.get(arg2).getId();
				//showToast(msAddId);
				setData(marrUAdds.get(arg2));
				dateWarning.dismiss();
			}
		});
		//list.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, msArr));
		list.setAdapter(new AddressAdapter(getActivity(), 0, marrUAdds));
		mtvMessage.setText("Select any of your below address");
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msAddId = "0";
				resetData();
				dateWarning.dismiss();

			}
		});


		dateWarning.show();	

	}

	private void showAddressTitleDialog(){
		final Dialog dateWarning = new Dialog(getActivity());
		dateWarning.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dateWarning.setContentView(R.layout.address_label);
		TextView mtvContinue = (TextView) dateWarning.findViewById(R.id.text_done);
		TextView mtvMessage = (TextView) dateWarning.findViewById(R.id.text_message);
		final EditText metLabel = (EditText) dateWarning.findViewById(R.id.edit_adressLabel);
		mtvContinue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//msAddId = "0";
				mAddressLabel = metLabel.getText().toString();
				if(mAddressLabel.equalsIgnoreCase("") || mAddressLabel.contains("abcde") ){
					Toast.makeText(getActivity(), "Please enter address title", Toast.LENGTH_SHORT).show();
				}else{
					dateWarning.dismiss();
					if(validateInfo()){
						getOrderDetails();
					}
				}




			}
		});

		dateWarning.show();

	}

	public long CalculateAdler32ForByteArray(){

		String str = Config.MERCHANT_ID+"|"+msOrderId+"|"+Config.TOTAL_AMOUNT+"|"+Config.RESPONSE_URL+"|"+Config.WORKING_KEY;

		//Convert string to bytes
		byte bytes[] = str.getBytes();

		Checksum checksum = new Adler32();
		/*
		 * To compute the Adler32 checksum for byte array, use
		 *
		 * void update(bytes[] b, int start, int length)
		 * method of Adler32 class.
		 */
		checksum.update(bytes,0,bytes.length);
		/*
		 * Get the generated checksum using
		 * getValue method of Adler32 class.
		 */
		long lngChecksum = checksum.getValue();
		Log.d("Checksume","Adler32 checksum for byte array is :" + lngChecksum);
		return lngChecksum;
	}

	private void getOrderId(){


		LogFile.LogData("getOrderId");
		String msWhere = " WHERE "+DatabaseHelper.USER_ID+"='"+msUserId+"'" ;
		try{
			showProgressDialog();
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {
				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete" + " " + response);
					//addToCartServer();
					msOrderId = response;
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_ORDERS, Config.GET_ORDER_DATE, msWhere, "GetOrderDate",DatabaseHelper.ORDER_ID,null).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}

	}

	private void setData(UserAddress uAdd){

		metAddressTitle.setText(uAdd.getLabel());
		metEmail.setText(uAdd.getEmail_id());
		metFirstName.setText(uAdd.getFirstName());
		metLastName.setText(uAdd.getLastName());
		metMobileNumber.setText(uAdd.getMobile_number());
		metPincode.setText(uAdd.getPincone());
		metAddress.setText(uAdd.getAddress1());

	}
	
	private void resetData(){

		metAddressTitle.setText("");
		metEmail.setText("");
		metFirstName.setText("");
		metLastName.setText("");
		metMobileNumber.setText("");
		metPincode.setText("");
		metAddress.setText("");
		metCity.setVisibility(View.GONE);
		metState.setVisibility(View.GONE);
		mtvCountry.setText(COUNTRY_TITLE);
		mtvState.setText(STATE_TITLE);
		mtvCity.setText(CITY_TITLE);
		mtvCity.setVisibility(View.VISIBLE);
		mtvState.setVisibility(View.VISIBLE);

	}
	


	public boolean isValidEmail(String target) {
		if (TextUtils.isEmpty(target)) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}

}
