package com.phonethics.monginis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.model.CartProductInfo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;


public class PaymentGateway extends Activity implements DBListener {

	Context context;
	private WebView linkwebview;
	private ImageView bckBtn;
	private ImageView forthBtn;
	private ImageView refreshBtn;
	ProgressBar prog;
	int screen;
	String msAmount;
	String msChecksum;
	private String msOrderId="";
	private RelativeLayout layoutFooter;

	private String msUserFirstName_shipping;
	private String msUserLastName_shipping;
	private String msUserAddress_shipping;
	private String msUserCity_shipping;
	private String msUSerZip_shipping;
	private JSONObject jObjShipping;
	private JSONObject jObjBilling;

	boolean isSuccess = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_link_view);

		context = this;

		initViews();
		getData();
		makeUrl();
		setwebView();
	}


	private void initViews(){
		linkwebview = (WebView) findViewById(R.id.linkwebview);
		bckBtn = (ImageView) findViewById(R.id.webback);
		forthBtn = (ImageView) findViewById(R.id.forth);
		refreshBtn = (ImageView) findViewById(R.id.refresh);
		layoutFooter = (RelativeLayout) findViewById(R.id.bck);
		layoutFooter.setVisibility(View.GONE);
		prog=(ProgressBar)findViewById(R.id.showProgress);


		Bundle b = getIntent().getExtras();

		if(b!=null){
			screen = b.getInt("position");
			msAmount = b.getString("amount");
			msChecksum = b.getString("checksum");
			msOrderId = b.getString("orderId");
		}

	}


	private void getData(){
		SessionManager mSession = new SessionManager(context);
		String shippingInfo = mSession.getShippingDetails();
		String billingInfo = mSession.getBillingAddress();

		try{
			jObjShipping =  new JSONObject(shippingInfo);
			jObjBilling =  new JSONObject(billingInfo);
		}catch(Exception ex){
			ex.printStackTrace();
		}


	}

	private void setwebView(){

		linkwebview.getSettings().setJavaScriptEnabled(true);
		linkwebview.getSettings().setPluginState(PluginState.ON);
		//linkwebview.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		linkwebview.getSettings().setBuiltInZoomControls(false);
		linkwebview.clearCache(true);


		CookieManager cookieManager = CookieManager.getInstance();        
		cookieManager.removeAllCookie();
		linkwebview.setWebViewClient(new MyWebViewClient());

		//linkwebview.setWebChromeClient(new WebChromeClient());
		//linkwebview.loadUrl("file:///android_asset/payment.html");

		//String htmlDocument = "<html><body><h1>Android Print Test</h1><p>"  + "'This is some sample content'.</p></body></html>";
		//linkwebview.loadDataWithBaseURL(null, htmlDocument,"text/HTML", "UTF-8", null);

		//linkwebview.loadUrl(getResources().getAssets()+"payment.html");
		//linkwebview.loadUrl("https://www.facebook.com/monginis");
		//linkwebview.loadUrl("javascript:callFromActivity(\""+msAmount+"\",\""+msChecksum+"\")");
	}

	private void makeUrl(){
		ArrayList<String> tags = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();	


		try{
			tags.add("'Merchant_Id'");values.add("\""+Config.MERCHANT_ID+"\"");

			tags.add("'Amount'");values.add("\""+Config.TOTAL_AMOUNT+"\"");
			//tags.add("'Amount'");values.add("1");

			tags.add("'Currency'");values.add("\"USD\"");

			//tags.add("'Order_Id'");values.add("\""+msOrderId+"\"");
			
			tags.add("'Order_Id'");values.add("\""+Config.ORDER_NUMBER+"\"");

			tags.add("'TxnType'");values.add("\"A\"");

			tags.add("'actionID'");values.add("\"TXN\"");

			tags.add("'Checksum'");values.add("\""+msChecksum+"\"");

			tags.add("'Redirect_Url'");values.add("\""+Config.RESPONSE_URL+"\"");

			tags.add("'billing_cust_name'");values.add("\""+jObjBilling.getString("first_name")+"\"");

			tags.add("'billing_middle_name'");values.add("\"\"");

			tags.add("'billing_last_name'");values.add("\""+jObjBilling.getString("last_name")+"\"");

			tags.add("'billing_cust_address'");values.add("\""+jObjBilling.getString("address1")+"\"");

			tags.add("'billing_cust_city'");values.add("\""+jObjBilling.getString("city")+"\"");

			tags.add("'billing_cust_state'");values.add("\""+jObjBilling.getString("state")+"\"");

			tags.add("'billing_zip_code'");values.add("\""+jObjBilling.getString("pincode")+"\"");

			//tags.add("'billing_cust_country'");values.add("\"IN\"");

			//tags.add("'billing_cust_tel_ctry'");values.add("\"91\"");

			//tags.add("'billing_cust_tel_Area'");values.add("\"022\"");

			tags.add("'billing_cust_tel'");values.add("\""+jObjBilling.getString("cell_phone")+"\"");

			tags.add("'billing_cust_email'");values.add("\""+jObjBilling.getString("email")+"\"");

			tags.add("'billing_cust_notes'");values.add("\"\"");

			tags.add("'delivery_cust_name'");values.add("\""+jObjShipping.getString("first_name")+"\"");

			tags.add("'delivery_middle_name'");values.add("\"\"");

			tags.add("'delivery_last_name'");values.add("\""+jObjShipping.getString("last_name")+"\"");

			tags.add("'delivery_cust_address'");values.add("\""+jObjShipping.getString("address1")+"\"");

			tags.add("'delivery_cust_city'");values.add("\""+jObjShipping.getString("city")+"\"");

			tags.add("'delivery_cust_state'");values.add("\""+jObjShipping.getString("state")+"\"");

			tags.add("'delivery_zip_code'");values.add("\""+jObjShipping.getString("pincode")+"\"");

			//tags.add("'delivery_cust_country'");values.add("\"IN\"");

			//tags.add("'delivery_cust_tel_ctry'");values.add("\"91\"");

			//tags.add("'delivery_cust_tel_Area'");values.add("\"022\"");

			tags.add("'delivery_cust_tel'");values.add("\""+jObjShipping.getString("cell_phone")+"\"");

		}catch(Exception ex){
			ex.printStackTrace();
		}



		String nameValue = "";
		String script = "<script>function load(){document.form2.submit()}</script>";
		String msPleaseWait = "<h2 style=\"text-align:center\">Please wait...</h2>"+"\n";

		String formStartTag = "<form name='form2' method='post' action='https://www.ccavenue.com/shopzone/cc_details.jsp'>"+"\n";
		for(int i=0;i<tags.size();i++){

			nameValue = nameValue +
					"<"+ getResources().getString(R.string.input_tag)+" "+
					getResources().getString(R.string.type_hidden_tag)+" "+
					getResources().getString(R.string.name_tag)+tags.get(i)+" "+
					getResources().getString(R.string.value_tag)+values.get(i)+
					">"+"\n";
		}
		String inputType = "<input type=\"submit\" value=\"checkout\">";
		String formEndTag = "</form>";

		String htmlUrl1 = "<html><head>"+script+"</head><body onload=\"load()\">"+msPleaseWait+formStartTag+inputType+nameValue+formEndTag+"</body></html>";
		String htmlUrl = "<html><body>"+msPleaseWait+formStartTag+inputType+nameValue+formEndTag+"</body></html>";
		//Log.d("Html Data", "HtmlData ========================");
		//Log.d("Html Data", htmlUrl1.toString());
		updateOrderStatusOnServer();
		deleteOrderInfoRecords();
		linkwebview.loadData(htmlUrl1,"text/HTML", null);

		//linkwebview.loadDataWithBaseURL(null, htmlUrl1,"text/HTML", "UTF-8", null);



	}

	private void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(context, ""+msg, Toast.LENGTH_SHORT).show();
	}



	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			//try {
				Log.d("My Webview ", "================================ ");
				Log.d("My Webview ", "WebViewClient ");
				Log.d("My Webview ", "Url "+url);

				view.loadUrl(url);

				//stage.phonethics.in/monginis/confirmresponse
				//monginis.net/api-new/confirmresponse
				//if(url.contains(Config.CONFIRMRESPONSE_URL)){
				if(url.contains("monginis.net/api-new/confirmresponse")){
					

					String confirmUrl = url;
					String responseCodeUrl = "";
					String orderIdUrl = "";

					responseCodeUrl = confirmUrl.substring(confirmUrl.indexOf("?")+1, confirmUrl.indexOf("&"));
					orderIdUrl = confirmUrl.substring(confirmUrl.indexOf("&")+1, confirmUrl.length());
					Log.d("My Webview ", "responseCodeUrl --"+responseCodeUrl);
					Log.d("My Webview ", "orderIdUrl --"+orderIdUrl);

					if(responseCodeUrl.contains("responseCode")){
						String temp = responseCodeUrl.substring(responseCodeUrl.indexOf("=")+1, responseCodeUrl.length());
						if(temp.equalsIgnoreCase("null")){
							//deleteOrderInfoRecords();
							showResponseDialog("Sorry!","Sorry! Payment can not be processed.");
						} else if (temp.equalsIgnoreCase("0")){
							
							deleteShoppingCartRecords();
							//deleteOrderInfoRecords();
							showResponseDialog("Thank You!"," Payment successful. We are processing your order.");
						}
					}	

				}/*else if(url.contains("ccavenue.com/shopzone/cc_details")){
					deleteOrderInfoRecords();
				}*/


				bckBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(linkwebview.canGoBack()) {
							linkwebview.goBack();
						}
					}
				});

				forthBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(linkwebview.canGoForward())
						{
							linkwebview.goForward();
						}


					}
				});


				refreshBtn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						linkwebview.reload();

					}
				});

//			} catch (Exception e) {
//				// TODO: handle exception
//				e.printStackTrace();
//			}
			
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			//linkwebview.loadUrl("javascript:callFromActivity(\""+msAmount+"\",\""+msChecksum+"\")");
			//Log.d("My Webview ", "================================ ");
			//linkwebview.loadUrl("javascript:openAndroidDialog()");
			view.clearCache(true);
			prog.setVisibility(View.GONE);

		}


		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			prog.setVisibility(View.VISIBLE);


		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);
			//Log.d("My Webview ", "================================ ");
			//Log.d("My Webview ", "onLoadResource ");
			//Log.d("My Webview ", "Url "+url);

		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
			//Log.d("My Webview ", "================================ ");
			//Log.d("My Webview ", "onReceivedError ");
			//Log.d("My Webview ", "errorCode "+errorCode);
			//Log.d("My Webview ", "description "+description);
			//Log.d("My Webview ", "failingUrl "+failingUrl);

		}

		@Override
		public void onReceivedHttpAuthRequest(WebView view,
				HttpAuthHandler handler, String host, String realm) {
			// TODO Auto-generated method stub
			super.onReceivedHttpAuthRequest(view, handler, host, realm);
			//Log.d("My Webview ", "================================ ");
			//Log.d("My Webview ", "onReceivedHttpAuthRequest ");
		}



		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			// TODO Auto-generated method stub
			super.onReceivedSslError(view, handler, error);
			//Log.d("My Webview ", "================================ ");
			//Log.d("My Webview ", "SslError ");
		}




	}

	
	
	void updateOrderStatusOnServer(){

		try{

			JSONObject json = new JSONObject();
			json.put("id", msOrderId);
			LogFile.LogData("UpdateStatus "+json.toString());
			final String TAG="POST_UPDATE_STATUS";
			String url=Config.UPDATE_ORDER_STATUS;
			LogFile.LogData("URL "+url);
			//showProgressDialog();
			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, json, new Response.Listener<JSONObject>() {


				@Override
				public void onResponse(JSONObject response) {
					try
					{
						//dismissDialog();
						Config.Log(TAG, response.toString());
						String status=response.getString("success");
						LogFile.LogData("response --"+response);
						if(response!=null){
							/*if(status.equalsIgnoreCase("true")){
								
							
							
							}else{

								if(status.equalsIgnoreCase("false")){
									showToast(response.getString("message"));
									dismissDialog();
								}
							}*/

						}	
					}catch(Exception ex){
						ex.printStackTrace();
						//dismissDialog();
					}

				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					Config.Log(TAG, error.getMessage());
					//dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){

				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		/*	Map<String, String> params = new HashMap<String, String>();
			if(msAddId.equalsIgnoreCase("0")){
				params.put("Address_Selected","New Address");
			}else{
				params.put("Address_Selected","Stored Address");
			}*/
			//EventTracker.logEvent(EventsName.SHIPPING_ADDRESS, params);

		}catch(Exception ex){
			ex.printStackTrace();
		}
	
	}

	class ChromeClient extends WebChromeClient{

		@Override
		public boolean onJsAlert(WebView view, String url, String message,
				JsResult result) {
			// TODO Auto-generated method stub
			return super.onJsAlert(view, url, message, result);
		}

		@Override
		public boolean onJsConfirm(WebView view, String url, String message,
				JsResult result) {
			// TODO Auto-generated method stub
			return super.onJsConfirm(view, url, message, result);
		}

		@Override
		public boolean onJsPrompt(WebView view, String url, String message,
				String defaultValue, JsPromptResult result) {
			// TODO Auto-generated method stub
			return super.onJsPrompt(view, url, message, defaultValue, result);
		}

	}

	void showResponseDialog(String title, String msText){
		final Dialog redeemDialog = new Dialog(context);
		redeemDialog.setTitle(title);
		redeemDialog.setContentView(R.layout.redeem_dialog);
		redeemDialog.setCancelable(false);

		TextView txtPoint = (TextView) redeemDialog.findViewById(R.id.text_points);
		TextView txtCanSaveAmnt = (TextView) redeemDialog.findViewById(R.id.text_canSaveAmount);
		final EditText mEtPoint = (EditText) redeemDialog.findViewById(R.id.edit_pts);
		TextView mtvRedeem = (TextView) redeemDialog.findViewById(R.id.text_Redeem);
		TextView mtvNoPoints = (TextView) redeemDialog.findViewById(R.id.text_no_points);
		final TextView mtvSavingAmount = (TextView) redeemDialog.findViewById(R.id.text_savingAmount);


		LinearLayout totalPointsLayout = (LinearLayout) redeemDialog.findViewById(R.id.redeemTotalPointsLayout);
		LinearLayout redeemPointsLayout = (LinearLayout) redeemDialog.findViewById(R.id.redeem_points);
		LinearLayout savingPoints = (LinearLayout) redeemDialog.findViewById(R.id.saving_points);



		totalPointsLayout.setVisibility(View.GONE);
		redeemPointsLayout.setVisibility(View.GONE);
		savingPoints.setVisibility(View.GONE);

		mtvNoPoints.setVisibility(View.VISIBLE);	


		mtvNoPoints.setText("Order No " + Config.ORDER_NUMBER + "\n" + msText);
		mtvRedeem.setText("Ok");

		mtvRedeem.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				redeemDialog.dismiss();
				Intent intent = new Intent(context, LandingScreen.class);
				//intent.putExtra("PaymentDone", true);
				startActivity(intent);
				finish();

			}
		});
		redeemDialog.show();

	}

	private void parseUserShippingInfo(){

	}

	private void parseUserBillingInfo(){

	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, LandingScreen.class);
		startActivity(intent);
		finish();
	}

	void deleteShoppingCartRecords(){
		LogFile.LogData("deleteShoppingCartRecords");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"++"'";
		try{
			new AsyncDatabaseQuery(context,this, DatabaseHelper.TABLE_SHOPPING_CART,
					"", Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void deleteOrderInfoRecords(){
		LogFile.LogData("deleteOrderInfoRecords");
		//String msWhere = "WHERE "+DatabaseHelper.USER_ID+" ='"++"'";
		try{
			new AsyncDatabaseQuery(context,this, DatabaseHelper.TABLE_ORDERS,
					"", Config.DELETE_DATA,  Config.DELETE_DATA).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	@Override
	public void onCompleteOperation(String tag) {
		// TODO Auto-generated method stub
		if(tag.equalsIgnoreCase(Config.DELETE_DATA)){
			//checkCachedData();
		}
	}


	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		// TODO Auto-generated method stub

	}


	@Override
	public void rowCount(long rowCount, String tag) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}


}
