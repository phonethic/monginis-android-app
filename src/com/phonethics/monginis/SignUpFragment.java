package com.phonethics.monginis;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

public class SignUpFragment extends Fragment {

	private View mView;
	private EditText mEmail;
	private EditText mPassword;
	private EditText mConfirmPassword;
	private TextView mSignUpBtn;
	private ProgressDialog mProgressDialog;
	String success;
	String message;
	String TAG="";
	private static boolean  mbFromContinue = false;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");
		
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle mBundle = getArguments();
		if(mBundle!=null){
			TAG = mBundle.getString("tag");
			mbFromContinue  = mBundle.getBoolean("fromContinue",false);
			LogFile.LogData("FromContinue "+mbFromContinue);
			LogFile.LogData("TAG "+TAG);
			//showToast(TAG + mbFromContinue);
		}
		
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_signup, null);
		mEmail=(EditText)mView.findViewById(R.id.mEmail);
		mPassword=(EditText)mView.findViewById(R.id.mPassword);
		mConfirmPassword=(EditText)mView.findViewById(R.id.mConfirmPassword);
		mSignUpBtn=(TextView)mView.findViewById(R.id.mSignUpBtn);
		
		
		mEmail.setTypeface(MonginisApplicationClass.getTypeFace());
		mPassword.setTypeface(MonginisApplicationClass.getTypeFace());
		mSignUpBtn.setTypeface(MonginisApplicationClass.getTypeFace());
		mConfirmPassword.setTypeface(MonginisApplicationClass.getTypeFace());
		

		mSignUpBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

				String emailId = mEmail.getText().toString();
				String password = mPassword.getText().toString();
				String confirmPassword = mConfirmPassword.getText().toString();
				if(emailId.length()==0){
					showToast("Please enter Email Id");
				}
				else if(!isEmailValid(emailId)){
					showToast("Please enter a valid Email Id");
				}
				else if(password.length()==0){
					showToast("Please enter password");
				}
				else if(confirmPassword.length()==0){
					showToast("Please enter confirm password");
				}
				else if(!(confirmPassword.equalsIgnoreCase(password))){
					showToast("Password does not match the confirm password");
				}
				else{
					//showToast("Call Registration Api");
					callRegistartionApi();
				}

			}
		});
		return mView;
	}

	void callRegistartionApi() {
		// TODO Auto-generated method stub

		try {
			showProgressDialog();
			String TAG="USER REGISTRATION";
			String url=Config.USER_REGISTRATION;
			Config.Log("URL","URL "+url);

			//adding body to registration
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("email",mEmail.getText().toString());
			jsonObj.put("password",mPassword.getText().toString());

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							success = json.getString("success");
							if(success.equalsIgnoreCase("true")){

								JSONObject data = json.getJSONObject("data");
								String userId = data.getString("user_id");
								//Config.USER_ID = userId;
								message = getResources().getString(R.string.successfully_registered);
								showAlertDialog(message);
							}
							else{
								message = json.getString("message");
								//showToast(message);
								showAlertDialog(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	private void showAlertDialog(String message) {
		// TODO Auto-generated method stub


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				LandingScreen.showNavigation();
				if(TAG.length()!=0 && !mbFromContinue){
					FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//To remove all Stack behind
					getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

					MainCategoriesFragment chkFrag=new MainCategoriesFragment();
					transaction.add(R.id.content_frame, chkFrag,"MAIN");
					//transaction.addToBackStack("MENU OPTION");
					// Commit the transaction
					transaction.commit();

				}else if(mbFromContinue){
					 getActivity().getSupportFragmentManager().popBackStack();
				}else{

					FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
					transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					//To remove all Stack behind
					getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

					MenuOptionFragment chkFrag=new MenuOptionFragment();
					transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
					//transaction.addToBackStack("MENU OPTION");
					// Commit the transaction
					transaction.commit();

				}
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
