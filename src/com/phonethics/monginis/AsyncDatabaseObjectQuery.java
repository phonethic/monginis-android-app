package com.phonethics.monginis;

import java.util.ArrayList;

import com.phonethics.model.CartProductInfo;
import com.phonethics.model.OrderModel;
import com.phonethics.model.ProductModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.model.SubCategorModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;

public class AsyncDatabaseObjectQuery<T> extends AsyncTask<Void, Void, Integer> {

	private String msTag="";
	private String msQuery="";
	private String msColumn="";
	private String msQueryType="";
	private Context mContext;
	private String msTable="";
	private String value="";
	private String msWhere = "";

	//private ContentValues [] contentValues;
	private ContentValues contentValues;
	private OnCompleteListner<T> mOnCompleteListner;
	private OnComplete<T> mOnComplete;
	
	//private ArrayList<String>mArrData=new ArrayList<String>();
	private ArrayList<T> mArrData = new ArrayList<T>();
	//private ArrayList<CartProductInfo> mProductList = new ArrayList<CartProductInfo>();
	
	private long mlRowCount;
	private ContentValues contentValue;
	private String msData;
	/**
	 * 
	 * @param context : Context of Activity
	 * @param mDbListener : Database Listener
	 * @param contentValues : Values to be inserted in Database
	 * @param msTable : Table name to insert in Database.
	 * @param msQueryType : Type of Query Eg. INSERT,UPDATE,DELTE
	 * @param msTag : Tag of calling Api
	 * 
	 */
	
	public AsyncDatabaseObjectQuery(Context context,OnCompleteListner<T> onCompleteListner,
			String msTable,String msQueryType,String msWhereCluase, String msTag){
		mContext=context;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.mOnCompleteListner = onCompleteListner;
		this.msWhere = msWhereCluase;
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}
	
	public AsyncDatabaseObjectQuery(Context context,OnComplete<T> onCompleteListner,
			String msTable,String msQueryType,String msWhereCluase, String msTag,ContentValues contentValues){
		mContext=context;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.mOnComplete = onCompleteListner;
		this.msWhere = msWhereCluase;
		this.contentValues = contentValues; 
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}
	
	public AsyncDatabaseObjectQuery(Context context,OnComplete<T> onCompleteListner,
			String msTable,String msQueryType,String msWhereCluase, String msTag,String columnName,ContentValues contentValues){
		mContext=context;
		this.msQueryType=msQueryType;
		this.msTag=msTag;
		this.msTable=msTable;
		this.mOnComplete = onCompleteListner;
		this.msWhere = msWhereCluase;
		this.value = columnName;
		this.contentValues = contentValues; 
		DatabaseManager.initInstance(new DatabaseHelper(mContext));
	}
	
	

	@Override
	protected Integer doInBackground(Void... params) {
		// TODO Auto-generated method stub
		LogFile.LogData("doInBackground");
		
		
		if(msQueryType==Config.RETRIVE_SUB_CAT){
			
			LogFile.LogData(Config.RETRIVE_SUB_CAT);
			DatabaseManager.getInstance().openDatabase();
			ArrayList<SubCategorModel> mArrData = DatabaseManager.getInstance().getSubCatData(msTable, msWhere);
			this.mArrData = (ArrayList<T>) mArrData;
			DatabaseManager.getInstance().closeDataBase();
		}
		
		if(msQueryType==Config.RETRIVE_DATA_WHERE){
			LogFile.LogData(Config.RETRIVE_DATA_WHERE);
			DatabaseManager.getInstance().openDatabase();
			ArrayList<ProductModel> mArrData = DatabaseManager.getInstance().getProducts(msWhere);
			this.mArrData = (ArrayList<T>) mArrData;
			DatabaseManager.getInstance().closeDataBase();
		}
		
		if(msQueryType==Config.RETRIVE_CART_DATA){
			LogFile.LogData(Config.RETRIVE_CART_DATA);
			DatabaseManager.getInstance().openDatabase();
			ArrayList<ShoppingCartModel> mArrData = DatabaseManager.getInstance().getCartProducts(msWhere);
			this.mArrData = (ArrayList<T>) mArrData;
			DatabaseManager.getInstance().closeDataBase();
		}
		
		if(msQueryType==Config.UPDATE_ORDER_TABLE){
			LogFile.LogData(Config.UPDATE_ORDER_TABLE);
			DatabaseManager.getInstance().openDatabase();
			long msStr = DatabaseManager.getInstance().updateOrderTable(contentValues,msWhere);
			this.msData = msStr+"";
		}
		
		if(msQueryType==Config.UPDATE_SHOPPING_TABLE){
			LogFile.LogData(Config.UPDATE_SHOPPING_TABLE);
			DatabaseManager.getInstance().openDatabase();
			long msStr = DatabaseManager.getInstance().updateTable(contentValues,msTable,msWhere);
			this.msData = msStr+"";
		}
		
		
		if(msQueryType==Config.RETRIVE_ORDER_INFO){
			LogFile.LogData(Config.RETRIVE_ORDER_INFO);
			DatabaseManager.getInstance().openDatabase();
			//ArrayList<OrderModel> mArrData = DatabaseManager.getInstance().getOrderInfo("");
			ArrayList<OrderModel> mArrData = DatabaseManager.getInstance().getOrderInfo(msWhere);
			this.mArrData = (ArrayList<T>) mArrData;
			DatabaseManager.getInstance().closeDataBase();
		}
			
		if(msQueryType==Config.RETRIVE_SEARCHED_PRODUCT_DATA){
			LogFile.LogData(Config.RETRIVE_SEARCHED_PRODUCT_DATA);
			DatabaseManager.getInstance().openDatabase();
			ArrayList<ProductModel> mArrData = DatabaseManager.getInstance().getSearchedProducts();
			this.mArrData = (ArrayList<T>) mArrData;
			DatabaseManager.getInstance().closeDataBase();
		}
		
		if(msQueryType==Config.GET_ORDER_DATE){
			LogFile.LogData(Config.GET_ORDER_DATE);
			DatabaseManager.getInstance().openDatabase();
			String msStr = DatabaseManager.getInstance().getOrderDate(msWhere);
			this.msData = msStr+"";
		}
		
		if(msQueryType==Config.GET_VALUE){
			LogFile.LogData(Config.GET_ORDER_DATE);
			DatabaseManager.getInstance().openDatabase();
			String msStr = DatabaseManager.getInstance().getValueFromColumn(msWhere, value);
			this.msData = msStr+"";
		}
		
		return null;
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(msQueryType==Config.INSERT_DATA){
			//mDbListener.onCompleteOperation(msTag);
			//mOnCompleteListner.onComplete(response, msTag);
		}
		if(msQueryType==Config.UPDATE_DATA){
			//mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.DELETE_DATA){
			//mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.RETRIVE_DATA){
			//mDbListener.onCompleteRetrival(mArrData,msTag);
		}
		if(msQueryType==Config.ROW_COUNT){
			//mDbListener.rowCount(mlRowCount,msTag);
		}
		if(msQueryType==Config.INSERT_SINGLE_DATA){
			//mDbListener.onCompleteOperation(msTag);
		}
		if(msQueryType==Config.RETRIVE_PRODUCT){
			//mDbListener.onCompleteRetrivalObject(mProductList, msTag);
		}
		if(msQueryType==Config.RETRIVE_SUB_CAT){
			mOnCompleteListner.onComplete(mArrData, msTag);
		}
		if(msQueryType==Config.RETRIVE_DATA_WHERE){
			mOnCompleteListner.onComplete(mArrData, msTag);
		}
		if(msQueryType==Config.RETRIVE_CART_DATA){
			mOnCompleteListner.onComplete(mArrData, msTag);
		}
		if(msQueryType==Config.RETRIVE_SEARCHED_PRODUCT_DATA){
			mOnCompleteListner.onComplete(mArrData, msTag);
		}
		if(msQueryType==Config.UPDATE_ORDER_TABLE){
			mOnComplete.onComplete(msData, msTag);
		}
		if(msQueryType==Config.UPDATE_SHOPPING_TABLE){
			mOnComplete.onComplete(msData, msTag);
		}
		if(msQueryType==Config.RETRIVE_ORDER_INFO){
			mOnCompleteListner.onComplete(mArrData, msTag);
		}
		if(msQueryType==Config.GET_ORDER_DATE){
			mOnComplete.onComplete(msData, msTag);
		}
	}
}
