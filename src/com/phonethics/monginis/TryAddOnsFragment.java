package com.phonethics.monginis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.phonethics.adapters.ProductListAdapter;
import com.phonethics.model.CartProductInfo;
import com.phonethics.model.ProductModel;
import com.phonethics.model.ShoppingCartModel;
import com.phonethics.monginis.DbObjectListener.OnComplete;
import com.phonethics.monginis.DbObjectListener.OnCompleteListner;
import com.squareup.picasso.Picasso;

public class TryAddOnsFragment extends Fragment implements OnItemClickListener,DBListener{

	private View mView;
	private ListView addOnList;
	private ProgressDialog mProgressDialog;
	NetworkCheck netCheck;
	ArrayList<ProductModel> mAddOnList = new ArrayList<ProductModel>();
	private ProductListAdapter mAdapter;
	private ArrayList<String> mArrAddOnName=new ArrayList<String>();
	private ArrayList<String> mArrAddOnImage=new ArrayList<String>();
	private Dialog mListDialog;

	ImageView addOnItemImg;
	TextView addOnTitle;
	TextView addOnDesc;
	TextView addOnPrice;
	TextView addOnAddToCart;

	HashMap<String,String> userDetails = new HashMap<String, String>();
	SessionManager mSessionManager;
	String mUserId="";
	int position;
	String mBasePrice="";
	private String msCartDate;
	private int miCartItems;
	protected String msCartDeliveryMode;

	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		mProgressDialog=new ProgressDialog(getActivity());
		mSessionManager = new SessionManager(getActivity());
		netCheck = new NetworkCheck(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Fetching Add On list.");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(netCheck.isNetworkAvailable())
			getAddOnList();
		else
			showToast("No internet connection.");

		userDetails = mSessionManager.getUserDetails();
		if(mSessionManager.isLoggedInCustomer()){
			mUserId = userDetails.get(SessionManager.USER_ID);
		} else {
			mUserId = "0";
		}


		getProductsFromCart();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_try_addons, null);
		addOnList=(ListView)mView.findViewById(R.id.addOnList);
		mListDialog = new Dialog(getActivity());
		mListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mListDialog.setContentView(R.layout.add_to_cart_details);
		addOnAddToCart=(TextView)mListDialog.findViewById(R.id.addOnAddToCart);
		addOnPrice=(TextView)mListDialog.findViewById(R.id.addOnPrice);
		addOnDesc=(TextView)mListDialog.findViewById(R.id.addOnDesc);
		addOnTitle=(TextView)mListDialog.findViewById(R.id.addOnTitle);
		addOnItemImg=(ImageView)mListDialog.findViewById(R.id.addOnItemImg);

		addOnAddToCart.setTypeface(MonginisApplicationClass.getTypeFace());
		addOnPrice.setTypeface(MonginisApplicationClass.getTypeFace());
		addOnDesc.setTypeface(MonginisApplicationClass.getTypeFace());
		addOnTitle.setTypeface(MonginisApplicationClass.getTypeFace());




		addOnAddToCart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mListDialog.dismiss();
				Map<String, String> param = new HashMap<String, String>();

				if(miCartItems!=0){
					if(msCartDate==null||msCartDate.equalsIgnoreCase("")){
						msCartDate = getDateAfter(2);
					}
				}
				
				if(mSessionManager.isLoggedInCustomer()){
					//showToast("Adding on server");
					//getDelivery();
					param.put("AddOn_Name", addOnTitle.getText().toString());
					param.put("StoredOn",  "Server");
					EventTracker.logEvent(EventsName.TRY_ADDONS, param);
					addToCartServer();
					
				}else{
					//showToast("Adding on local database");
					param.put("AddOn_Name", addOnTitle.getText().toString());
					param.put("StoredOn",  "ClientDatabase");
					EventTracker.logEvent(EventsName.TRY_ADDONS, param);
					callCartFragment();
				}
				
			}
		});
		return mView;
	}

	void getAddOnList(){

		try {
			showProgressDialog();
			String TAG="ADD ONS";
			String url=Config.TRY_ADD_ONS;
			Config.Log("URL","URL "+url);

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");

							if(success.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									ProductModel obj = new ProductModel();
									JSONObject values = data.getJSONObject(i);
									obj.setId(values.getString("id"));
									obj.setSubcat_id(values.getString("cat_id"));
									obj.setTitle(values.getString("title"));
									obj.setDescription(values.getString("description"));
									obj.setCode(values.getString("code"));
									obj.setWeight_grams(values.getString("weight_grams"));
									obj.setImage(values.getString("image"));
									obj.setList_price(values.getString("list_price"));
									obj.setOur_price(values.getString("our_price"));
									obj.setSpecial_price(values.getString("special_price"));
									obj.setNextday_delivery(values.getString("nextday_delivery"));
									obj.setEggless_type(values.getString("eggless_type"));
									obj.setFree_delivery(values.getString("free_delivery"));
									obj.setNormal_delivery(values.getString("normal_delivery"));
									obj.setSameday_delivery(values.getString("sameday_delivery"));
									obj.setWithegg_type(values.getString("withegg_type"));
									mAddOnList.add(obj);

									mArrAddOnName.add(values.getString("title"));
									mArrAddOnImage.add(values.getString("image"));
								}

								setData();
							}
							else{
								//message = json.getString("message");
								String message = json.getString("message");
								showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	void setData(){


		mAdapter=new ProductListAdapter(getActivity(), 0, mAddOnList,0);
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.list_layout_controller);
		addOnList.setAdapter(mAdapter);
		addOnList.setLayoutAnimation(controller);
		addOnList.setOnItemClickListener(this);
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	void showToast(String msg) {
		// TODO Auto-generated method stub

		Toast.makeText(getActivity(), ""+msg,Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		// TODO Auto-generated method stub

		this.position = position;
		addOnTitle.setText(mArrAddOnName.get(position));
		addOnDesc.setText(mAddOnList.get(position).getDescription());
		if(mAddOnList.get(position).getSpecial_price().length()==0){
			addOnPrice.setText(" "+getResources().getString(R.string.Rs)+mAddOnList.get(position).getOur_price());
			mBasePrice = mAddOnList.get(position).getOur_price();
		}
		else{
			addOnPrice.setText(" "+getResources().getString(R.string.Rs)+mAddOnList.get(position).getSpecial_price());
			mBasePrice = mAddOnList.get(position).getSpecial_price();
		}
		Picasso.with(getActivity())
		.load(Config.IMAGE_BASE_URL+ mAddOnList.get(position).getImage())
		.placeholder(R.drawable.ic_launcher)
		.error(R.drawable.ic_launcher)
		// .resize(150, 150)
		.into(addOnItemImg);
		mListDialog.show();
		
		Map<String, String> param = new HashMap<String, String>();
		param.put("AddOn_Viewed", addOnTitle.getText().toString());
		EventTracker.logEvent(EventsName.TRY_ADDONS, param);
	}

	void addToCartServer(){

		final String TAG="ADD_TO_CART";
		String url=Config.ADD_TO_CART;
		JSONObject mainObj = new JSONObject();
		JSONObject json = new JSONObject();
		JSONArray jArrProducts = new JSONArray(); 

		try{	
			mainObj.put("user_id", mUserId);
			
			json.put("product_id", mAddOnList.get(position).getId());
			json.put("quantity", "1");
			json.put("delivery_date", msCartDate);
			json.put("delivery_mode", msCartDeliveryMode);
			json.put("eggless_type", "N");
			json.put("withegg_type","N");
			json.put("sameday_delivery","N");
			json.put("nextday_delivery", "N");
			json.put("normal_delivery", "Y");
			json.put("free_delivery", "N");
			json.put("message", "");
			jArrProducts.put(json);
			mainObj.put("products", jArrProducts);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		showProgressDialog();
		LogFile.LogData("Add to cart Url - "+url);
		LogFile.LogData("Add json - "+json.toString());
		JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, mainObj, new Response.Listener<JSONObject>() {


			@Override
			public void onResponse(JSONObject response) {
				try
				{
					Config.Log(TAG, response.toString());
					String status=response.getString("success");

					if(response!=null){
						dismissDialog();
						LogFile.LogData("Add to cart response - "+response.toString());
						if(status.equalsIgnoreCase("true")){
							mSessionManager.setRefreshNeeded(true);

							String msMessage = response.getString("message");
							showToast(msMessage);

							FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
							transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
							//To remove all Stack behind
							getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

							ShoppingCartFragment chkFrag=new ShoppingCartFragment();
							transaction.add(R.id.content_frame, chkFrag,"SHOPPING CART");
							transaction.commit();

						}else{
							if(status.equalsIgnoreCase("false")){
								showToast(response.getString("message"));
								dismissDialog();
							}
						}

					}	
				}catch(Exception ex){
					ex.printStackTrace();
					dismissDialog();
				}

			}

		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Config.Log(TAG, error.getMessage());
				dismissDialog();
				if(error instanceof NetworkError){
					showToast(getResources().getString(R.string.Network));
				}else if(error instanceof AuthFailureError){
					showToast(getResources().getString(R.string.Authentication));
				}else if(error instanceof ServerError ){
					showToast(getResources().getString(R.string.Server));
				}else if(error instanceof NoConnectionError){
					showToast(getResources().getString(R.string.Internet));
				}else if(error instanceof TimeoutError){
					showToast(getResources().getString(R.string.TimeOut));
				}else if(error instanceof ParseError){
					showToast(getResources().getString(R.string.Parse));
				}
			}
		}){

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers=new HashMap<String, String>();
				headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
				headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
				return headers;
			}

		};

		MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);


	}

	protected void callCartFragment() {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();
		value.put(DatabaseHelper.PRODUCT_ID, mAddOnList.get(position).getId());
		value.put(DatabaseHelper.PRODUCT_NAME, mArrAddOnName.get(position));
		value.put(DatabaseHelper.PRODUCT_IMG_URL, mArrAddOnImage.get(position));
		value.put(DatabaseHelper.MESSAGE, "");
		value.put(DatabaseHelper.QUANTITY, "1");
		value.put(DatabaseHelper.PRODUCT_DELIVERY_DATE,msCartDate);
		value.put(DatabaseHelper.BASE_AMOUNT, mBasePrice);	
		value.put(DatabaseHelper.PRODUCT_DELIVERY_MODE,msCartDeliveryMode);
		value.put(DatabaseHelper.MESSAGE,"");
		

		if(mSessionManager.isLoggedInCustomer())
			value.put(DatabaseHelper.USER_ID, mUserId);
		else
			value.put(DatabaseHelper.USER_ID, "0");


		insertIntoDbSingle(value);


		FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		//To remove all Stack behind
		getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

		ShoppingCartFragment chkFrag=new ShoppingCartFragment();
		transaction.add(R.id.content_frame, chkFrag,"SHOPPING CART");
		//transaction.addToBackStack("SHOPPING CART");
		// Commit the transaction
		transaction.commit();
	}

	void insertIntoDbSingle(ContentValues contentValues ){
		try{
			//showProgressDialog();
			new AsyncDatabaseQuery(getActivity(), this, contentValues, DatabaseHelper.TABLE_SHOPPING_CART,
					Config.INSERT_SINGLE_DATA, Config.INSERT_SINGLE_DATA).execute();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onCompleteOperation(String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleteRetrival(ArrayList<String> mArr, String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rowCount(long rowCount, String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCompleteRetrivalObject(ArrayList<CartProductInfo> mArr,
			String tag) {
		// TODO Auto-generated method stub

	}

	void getDelivery(){

		LogFile.LogData("updateOrdeInfo");
		String msWhere = " WHERE "+DatabaseHelper.USER_ID+"='"+mUserId+"'" ;
		try{
			showProgressDialog();
			new AsyncDatabaseObjectQuery<String>(getActivity(), new OnComplete<String>() {
				@Override
				public void onComplete(String response, String msTag) {
					// TODO Auto-generated method stub
					LogFile.LogData("onComplete" + " " + response);
					addToCartServer();
					dismissDialog();
				}
			}, DatabaseHelper.TABLE_ORDERS, Config.GET_ORDER_DATE, msWhere, "GetOrderDate",null).execute();

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	void getProductsFromCart(){
		showProgressDialog();
		String UserId = "";
		if(mSessionManager.isLoggedInCustomer()){
			UserId = userDetails.get(SessionManager.USER_ID);
		}else{
			UserId = "0";
		}
		LogFile.LogData("fetchData");	
		String msWhere = "WHERE "+DatabaseHelper.USER_ID +" ='"+UserId+"' ";
		new AsyncDatabaseObjectQuery<ShoppingCartModel>(getActivity(),new OnCompleteListner<ShoppingCartModel>() {



			@Override
			public void onComplete(ArrayList<ShoppingCartModel> response,
					String msTag) {
				// TODO Auto-generated method stub
				try{
					dismissDialog();
					//marrCartData = response;
					miCartItems = response.size();
					if(response.size()!=0){
						for(int i=0;i<response.size();i++){
							msCartDate = response.get(i).getDelivery_date();
							//ring msDeliveryMode = response.get(i).getDelivery_mode();
							msCartDeliveryMode = response.get(i).getDelivery_mode();
							LogFile.LogData("CartDate "+msCartDate);

						}
					}else{
						miCartItems = 0;
					}

				}catch(Exception ex){
					ex.printStackTrace();
				}

			}
		},DatabaseHelper.TABLE_SHOPPING_CART,Config.RETRIVE_CART_DATA,msWhere,Config.RETRIVE_CART_DATA).execute();
	}

	private String getDateAfter(int days){

		String msTommDate = "";

		try{


			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR,cal.get(Calendar.YEAR));
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+days);

			Date d2= cal.getTime();

			DateFormat  dt=new SimpleDateFormat("yyyy-MM-dd");
			//DateFormat  dtReview=new SimpleDateFormat("dd-MMM-yyyy");
			//deliveryDat = dt.format(cal.getTime());
			msTommDate = dt.format(cal.getTime());



		}catch(Exception ex){
			ex.printStackTrace();
		}

		return msTommDate;
	}

}
