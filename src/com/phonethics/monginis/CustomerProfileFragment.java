package com.phonethics.monginis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;

import com.phonethics.adapters.CategoriesAdapter;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomerProfileFragment extends Fragment {

	private View mView;
	SessionManager mSessionManager;
	private EditText mFirstName;
	private EditText mLastName;
	private EditText mEmail;
	private EditText addressBox;
	private TextView dob;
	private TextView anniversaryDate;
	private TextView gender;
	private TextView mUpdateProfile;
	Typeface tf;
	Calendar mCalDate = Calendar.getInstance();
	String mCurrentDate;
	boolean dobClicked = false;
	boolean annivClicked = false;
	private Dialog mListDialog;
	ListView mDialogList;
	ArrayList<String> mGender = new ArrayList<String>();
	CategoriesAdapter mAdapter;
	String postGender;
	private ProgressDialog mProgressDialog;
	HashMap<String,String> userDetails = new HashMap<String, String>();
	String mUserId;
	String message;
	NetworkCheck isAvailable;

	//to get profile
	String fName;
	String lName;
	String eMail;
	String dobDate;
	String annivDate;
	String male;
	String add;
	RelativeLayout dividerView;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		isAvailable = new NetworkCheck(getActivity());
		mProgressDialog=new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Please wait.");

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mSessionManager = new SessionManager(getActivity());
		userDetails = mSessionManager.getUserDetails();
		mUserId = userDetails.get(SessionManager.USER_ID);
		if(isAvailable.isNetworkAvailable()){
			getUserProfile();
		}
		Date currentDate = Calendar.getInstance().getTime();
		mCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);
	}

	public void showProgressDialog()
	{
		if(mProgressDialog!=null){
			if(!mProgressDialog.isShowing()){
				mProgressDialog.show();		
			}
		}

	}

	public void dismissDialog(){
		if(mProgressDialog!=null){
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();		
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView=(View)inflater.inflate(R.layout.fragment_customer_profile, null);
		mFirstName=(EditText)mView.findViewById(R.id.mFirstName);
		mLastName=(EditText)mView.findViewById(R.id.mLastName);
		mEmail=(EditText)mView.findViewById(R.id.mEmail);
		addressBox=(EditText)mView.findViewById(R.id.addressBox);
		dob=(TextView)mView.findViewById(R.id.dob);
		anniversaryDate=(TextView)mView.findViewById(R.id.anniversaryDate);
		gender=(TextView)mView.findViewById(R.id.gender);
		mUpdateProfile=(TextView)mView.findViewById(R.id.mUpdateProfile);

		tf=Typeface.createFromAsset(getActivity().getAssets(),Config.OMENS_LIGHT);
		mFirstName.setTypeface(tf);
		mLastName.setTypeface(tf);
		mEmail.setTypeface(tf);
		addressBox.setTypeface(tf);
		dob.setTypeface(tf);
		anniversaryDate.setTypeface(tf);
		gender.setTypeface(tf);
		mUpdateProfile.setTypeface(tf);

		mListDialog = new Dialog(getActivity());
		mListDialog.setContentView(R.layout.attribute_type_selection);
		mListDialog.setTitle("Select Gender");
		mDialogList = (ListView)mListDialog.findViewById(R.id.mAttributeType);
		dividerView = (RelativeLayout)mListDialog.findViewById(R.id.dividerView);
		dividerView.setVisibility(View.GONE);
		mGender.add("Male");
		mGender.add("Female");


		dob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dobClicked = true;
				annivClicked = false;
				chooseDate();
			}
		});

		anniversaryDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				annivClicked = true;
				dobClicked = false;
				chooseDate();
			}
		});

		gender.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mAdapter = new CategoriesAdapter(getActivity(), R.drawable.ic_launcher, R.layout.attribute_type_selection, mGender);
				mDialogList.setAdapter(mAdapter);
				mListDialog.show();

			}
		});

		mDialogList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				mListDialog.dismiss();
				gender.setText(mGender.get(position));
				if(mGender.get(position).equalsIgnoreCase("Male"))
					postGender = "M";
				else
					postGender = "F";
			}
		});

		mUpdateProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mEmail.getText().toString().length()==0){
					showToast("Please enter Email Id");
				}
				else if(!SignUpFragment.isEmailValid(mEmail.getText().toString())){
					showToast("Please enter a valid Email Id");
				}
				else{
					callUpdateProfile();
				}

			}
		});
		return mView;
	}

	void callUpdateProfile() {
		// TODO Auto-generated method stub

		try {

			showProgressDialog();
			String TAG="UPDATE PROFILE";
			String url=Config.USER_PROFILE_UPDATE;
			Config.Log("URL","URL "+url);

			//adding body to udate profile
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("user_id", mUserId);
			jsonObj.put("email",mEmail.getText().toString());

			//if(mFirstName.getText().toString().length()!=0)
			jsonObj.put("first_name",mFirstName.getText().toString());
			//if(mLastName.getText().toString().length()!=0)
			jsonObj.put("last_name",mLastName.getText().toString());
			if(dob.getText().toString().length()!=0)
				jsonObj.put("dob",dob.getText().toString());
			if(anniversaryDate.getText().toString().length()!=0)
				jsonObj.put("anniversary_date",anniversaryDate.getText().toString());
			if(gender.getText().toString().length()!=0)
				jsonObj.put("gender",postGender);
			//if(addressBox.getText().toString().length()!=0)
			jsonObj.put("address1",addressBox.getText().toString());

			Config.Log("JSON ","JSON " + jsonObj.toString());

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.POST, url, jsonObj, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							EventTracker.logEvent(EventsName.CUSTOMER_PROFILE_UPDATED);
							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");
							message = json.getString("message");
							if(success.equalsIgnoreCase("true")){

								//								JSONObject data = json.getJSONObject("data");
								//								String userId = data.getString("user_id");
								//Config.USER_ID = userId;
								showAlertDialog();
								//showToast(message);
							}
							else{
								//message = json.getString("message");
								showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void showAlertDialog() {
		// TODO Auto-generated method stub

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));

		// set dialog message
		alertDialogBuilder
		.setMessage(message)
		.setCancelable(true)
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity

				FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				//To remove all Stack behind
				getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

				MenuOptionFragment chkFrag=new MenuOptionFragment();
				transaction.add(R.id.content_frame, chkFrag,"MENU OPTION");
				//transaction.addToBackStack("MENU OPTION");
				// Commit the transaction
				transaction.commit();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	void chooseDate() {
		// TODO Auto-generated method stub
		new DatePickerDialog(getActivity(),date , mCalDate.get(Calendar.YEAR), mCalDate.get(Calendar.MONTH), mCalDate.get(Calendar.DATE)).show();
	}

	
	
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
			// TODO Auto-generated method stub
			mCalDate.set(Calendar.YEAR,year);
			mCalDate.set(Calendar.MONTH,monthOfYear);
			mCalDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
			DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			String setDate = dt.format(mCalDate.getTime());
			
			if(setDate.compareTo(mCurrentDate)>0){
				showToast("Unable to set future date");

			}else{
				if(dobClicked)
					dob.setText(setDate);
				else if(annivClicked)
					anniversaryDate.setText(setDate);
			}
		}
		
		
	};

	void getUserProfile(){
		
		try {
			showProgressDialog();
			String TAG="GET USER PROFILE";
			String url=Config.GET_USER_PROFILE+mUserId;
			Config.Log("URL","URL "+url);

			JsonObjectRequest jsonObject=new JsonObjectRequest(Method.GET, url, null, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject response) {
					dismissDialog();
					try
					{
						if(response!=null){

							//JSONObject json=response.getJSONObject("data");
							//dismissDialog();
							Config.Log("Response", "Response "+response.toString());
							JSONObject json = new JSONObject(response.toString());
							String success = json.getString("success");

							if(success.equalsIgnoreCase("true")){

								JSONArray data = json.getJSONArray("data");
								for(int i=0;i<data.length();i++){

									JSONObject obj = data.getJSONObject(i);
									fName = obj.getString("first_name");
									lName = obj.getString("last_name");
									eMail = obj.getString("email");
									dobDate = obj.getString("dob");
									annivDate = obj.getString("anniversary_date");
									male = obj.getString("gender");
									add = obj.getString("address1");
								}

								if(male.equalsIgnoreCase("M"))
									male = "Male";
								else if(male.equalsIgnoreCase("F"))
									male = "Female";

								setData(fName,lName,eMail,dobDate,annivDate,male,add);
							}
							else{
								//message = json.getString("message");
								message = json.getString("message");
								//showToast(message);
							}
						}

					}catch(Exception ex){
						ex.printStackTrace();
					}

				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {

					dismissDialog();
					if(error instanceof NetworkError){
						showToast(getResources().getString(R.string.Network));
					}else if(error instanceof AuthFailureError){
						showToast(getResources().getString(R.string.Authentication));
					}else if(error instanceof ServerError ){
						showToast(getResources().getString(R.string.Server));
					}else if(error instanceof NoConnectionError){
						showToast(getResources().getString(R.string.Internet));
					}else if(error instanceof TimeoutError){
						showToast(getResources().getString(R.string.TimeOut));
					}else if(error instanceof ParseError){
						showToast(getResources().getString(R.string.Parse));
					}
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					HashMap<String, String> headers=new HashMap<String, String>();
					headers.put(Config.HEADER_KEY, Config.HEADER_VALUE);
					headers.put(Config.DEVICE_ID_KEY, Config.DEVICE_ID);
					return headers;
				}
			};
			MonginisApplicationClass.getInstance().addToRequestQueue(jsonObject, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void setData(String fName, String lName, String eMail,
			String dobDate, String annivDate, String male, String add) {
		// TODO Auto-generated method stub

		if(fName.length()!=0)
			mFirstName.setText(fName);
		if(lName.length()!=0)
			mLastName.setText(lName);
		if(eMail.length()!=0)
			mEmail.setText(eMail);
		if(!dobDate.equalsIgnoreCase("0000-00-00"))
			dob.setText(dobDate);
		if(!annivDate.equalsIgnoreCase("0000-00-00"))
			anniversaryDate.setText(annivDate);
		if(male.length()!=0)
			gender.setText(male);
		if(add.length()!=0)
			addressBox.setText(add);
	}

	void showToast(String msg) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), ""+msg, Toast.LENGTH_SHORT).show();
	}

}
